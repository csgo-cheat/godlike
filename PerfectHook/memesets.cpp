#include "memesets.h"
#include "SDK.h"


void Offsetz::GetNetvars()
{
	NetVarManager->Initialize();
	offsetz.DT_BasePlayer.m_Local = NetVarManager->GetOffset("DT_BasePlayer", "m_Local");
	offsetz.DT_BasePlayer.m_aimPunchAngle = NetVarManager->GetOffset("DT_BasePlayer", "m_aimPunchAngle");
	offsetz.DT_BasePlayer.m_aimPunchAngleVel = NetVarManager->GetOffset("DT_BasePlayer", "m_aimPunchAngleVel");
	offsetz.DT_BasePlayer.m_viewPunchAngle = NetVarManager->GetOffset("DT_BasePlayer", "m_viewPunchAngle");
	offsetz.DT_BasePlayer.m_vecViewOffset = NetVarManager->GetOffset("DT_BasePlayer", "m_vecViewOffset[0]");
	offsetz.DT_BasePlayer.m_nTickBase = NetVarManager->GetOffset("DT_BasePlayer", "m_nTickBase");
	offsetz.DT_BasePlayer.m_vecVelocity = NetVarManager->GetOffset("DT_BasePlayer", "m_vecVelocity[0]");
	offsetz.DT_BasePlayer.m_iHealth = NetVarManager->GetOffset("DT_BasePlayer", "m_iHealth");
	offsetz.DT_BasePlayer.m_iMaxHealth = NetVarManager->GetOffset("DT_BasePlayer", "m_iMaxHealth");
	offsetz.DT_BasePlayer.m_lifeState = NetVarManager->GetOffset("DT_BasePlayer", "m_lifeState");
	offsetz.DT_BasePlayer.m_fFlags = NetVarManager->GetOffset("DT_BasePlayer", "m_fFlags");
	offsetz.DT_BasePlayer.m_iObserverMode = NetVarManager->GetOffset("DT_BasePlayer", "m_iObserverMode");
	offsetz.DT_BasePlayer.m_hObserverTarget = NetVarManager->GetOffset("DT_BasePlayer", "m_hObserverTarget");
	offsetz.DT_BasePlayer.m_hViewModel = NetVarManager->GetOffset("DT_BasePlayer", "m_hViewModel[0]");
	offsetz.DT_BasePlayer.m_szLastPlaceName = NetVarManager->GetOffset("DT_BasePlayer", "m_szLastPlaceName");
	offsetz.DT_BasePlayer.deadflag = NetVarManager->GetOffset("DT_BasePlayer", "deadflag");

	offsetz.DT_BaseEntity.m_flAnimTime = NetVarManager->GetOffset("DT_BaseEntity", "m_flAnimTime");
	offsetz.DT_BaseEntity.m_flSimulationTime = NetVarManager->GetOffset("DT_BaseEntity", "m_flSimulationTime");
	offsetz.DT_BaseEntity.m_vecOrigin = NetVarManager->GetOffset("DT_BaseEntity", "m_vecOrigin");
	offsetz.DT_BaseEntity.m_angRotation = NetVarManager->GetOffset("DT_BaseEntity", "m_angRotation");
	offsetz.DT_BaseEntity.m_nRenderMode = NetVarManager->GetOffset("DT_BaseEntity", "m_nRenderMode");
	offsetz.DT_BaseEntity.m_iTeamNum = NetVarManager->GetOffset("DT_BaseEntity", "m_iTeamNum");
	offsetz.DT_BaseEntity.m_MoveType = offsetz.DT_BaseEntity.m_nRenderMode + 1;
	offsetz.DT_BaseEntity.m_Collision = NetVarManager->GetOffset("DT_BaseEntity", "m_Collision");
	offsetz.DT_BaseEntity.m_bSpotted = NetVarManager->GetOffset("DT_BaseEntity", "m_bSpotted");
	offsetz.DT_BaseEntity.m_vecMins = NetVarManager->GetOffset("DT_BaseEntity", "m_vecMins");
	offsetz.DT_BaseEntity.m_vecMaxs = NetVarManager->GetOffset("DT_BaseEntity", "m_vecMaxs");
	offsetz.DT_BaseEntity.m_nSolidType = NetVarManager->GetOffset("DT_BaseEntity", "m_nSolidType");
	offsetz.DT_BaseEntity.m_usSolidFlags = NetVarManager->GetOffset("DT_BaseEntity", "m_usSolidFlags");
	offsetz.DT_BaseEntity.m_nSurroundType = NetVarManager->GetOffset("DT_BaseEntity", "m_nSurroundType");
	offsetz.DT_BaseEntity.m_hOwner = NetVarManager->GetOffset("DT_BaseEntity", "m_hOwner");
	offsetz.DT_BaseViewModel.m_hOwner = NetVarManager->GetOffset("DT_BaseViewModel", "m_hOwner");

	offsetz.DT_BaseCombatCharacter.m_hActiveWeapon = NetVarManager->GetOffset("DT_BaseCombatCharacter", "m_hActiveWeapon");
	offsetz.DT_BaseCombatCharacter.m_hMyWeapons = NetVarManager->GetOffset("DT_BaseCombatCharacter", "m_hMyWeapons") / 2;
	offsetz.DT_BaseCombatCharacter.m_hMyWearables = NetVarManager->GetOffset("DT_BaseCombatCharacter", "m_hMyWearables");

	offsetz.DT_PlayerResource.m_iPing = NetVarManager->GetOffset("DT_PlayerResource", "m_iPing");
	offsetz.DT_PlayerResource.m_iKills = NetVarManager->GetOffset("DT_PlayerResource", "m_iKills");
	offsetz.DT_PlayerResource.m_iAssists = NetVarManager->GetOffset("DT_PlayerResource", "m_iAssists");
	offsetz.DT_PlayerResource.m_iDeaths = NetVarManager->GetOffset("DT_PlayerResource", "m_iDeaths");
	offsetz.DT_PlayerResource.m_bConnected = NetVarManager->GetOffset("DT_PlayerResource", "m_bConnected");
	offsetz.DT_PlayerResource.m_iTeam = NetVarManager->GetOffset("DT_PlayerResource", "m_iTeam");
	offsetz.DT_PlayerResource.m_iPendingTeam = NetVarManager->GetOffset("DT_PlayerResource", "m_iPendingTeam");
	offsetz.DT_PlayerResource.m_bAlive = NetVarManager->GetOffset("DT_PlayerResource", "m_bAlive");
	offsetz.DT_PlayerResource.m_iHealth = NetVarManager->GetOffset("DT_PlayerResource", "m_iHealth");

	offsetz.DT_CSPlayerResource.m_iPlayerC4 = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iPlayerC4");
	offsetz.DT_CSPlayerResource.m_iPlayerVIP = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iPlayerVIP");
	offsetz.DT_CSPlayerResource.m_bHostageAlive = NetVarManager->GetOffset("DT_CSPlayerResource", "m_bHostageAlive");
	offsetz.DT_CSPlayerResource.m_isHostageFollowingSomeone = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_isHostageFollowingSomeone");
	offsetz.DT_CSPlayerResource.m_iHostageEntityIDs = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_iHostageEntityIDs");
	offsetz.DT_CSPlayerResource.m_bombsiteCenterB = NetVarManager->GetOffset("DT_CSPlayerResource", "m_bombsiteCenterB");
	offsetz.DT_CSPlayerResource.m_hostageRescueX = NetVarManager->GetOffset("DT_CSPlayerResource", "m_hostageRescueX");
	offsetz.DT_CSPlayerResource.m_hostageRescueY = NetVarManager->GetOffset("DT_CSPlayerResource", "m_hostageRescueY");
	offsetz.DT_CSPlayerResource.m_hostageRescueZ = NetVarManager->GetOffset("DT_CSPlayerResource", "m_hostageRescueZ");
	offsetz.DT_CSPlayerResource.m_iMVPs = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iMVPs");
	offsetz.DT_CSPlayerResource.m_iArmor = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iArmor");
	offsetz.DT_CSPlayerResource.m_bHasHelmet = NetVarManager->GetOffset("DT_CSPlayerResource", "m_bHasHelmet");
	offsetz.DT_CSPlayerResource.m_bHasDefuser = NetVarManager->GetOffset("DT_CSPlayerResource", "m_bHasDefuser");
	offsetz.DT_CSPlayerResource.m_iScore = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iScore");
	offsetz.DT_CSPlayerResource.m_iCompetitiveRanking = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_iCompetitiveRanking");
	offsetz.DT_CSPlayerResource.m_iCompetitiveWins = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iCompetitiveWins");
	offsetz.DT_CSPlayerResource.m_iCompTeammateColor = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_iCompTeammateColor");
	offsetz.DT_CSPlayerResource.m_bControllingBot = NetVarManager->GetOffset("DT_CSPlayerResource", "m_bControllingBot");
	offsetz.DT_CSPlayerResource.m_iControlledPlayer = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_iControlledPlayer");
	offsetz.DT_CSPlayerResource.m_iControlledByPlayer = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_iControlledByPlayer");
	offsetz.DT_CSPlayerResource.m_iBotDifficulty = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iBotDifficulty");
	offsetz.DT_CSPlayerResource.m_szClan = NetVarManager->GetOffset("DT_CSPlayerResource", "m_szClan");
	offsetz.DT_CSPlayerResource.m_iTotalCashSpent = NetVarManager->GetOffset("DT_CSPlayerResource", "m_iTotalCashSpent");
	offsetz.DT_CSPlayerResource.m_iCashSpentThisRound = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_iCashSpentThisRound");
	offsetz.DT_CSPlayerResource.m_nEndMatchNextMapVotes = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_nEndMatchNextMapVotes");
	offsetz.DT_CSPlayerResource.m_bEndMatchNextMapAllVoted = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_bEndMatchNextMapAllVoted");
	offsetz.DT_CSPlayerResource.m_nActiveCoinRank = NetVarManager->GetOffset("DT_CSPlayerResource", "m_nActiveCoinRank");
	offsetz.DT_CSPlayerResource.m_nMusicID = NetVarManager->GetOffset("DT_CSPlayerResource", "m_nMusicID");
	offsetz.DT_CSPlayerResource.m_nPersonaDataPublicLevel = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_nPersonaDataPublicLevel");
	offsetz.DT_CSPlayerResource.m_nPersonaDataPublicCommendsLeader = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_nPersonaDataPublicCommendsLeader");
	offsetz.DT_CSPlayerResource.m_nPersonaDataPublicCommendsTeacher = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_nPersonaDataPublicCommendsTeacher");
	offsetz.DT_CSPlayerResource.m_nPersonaDataPublicCommendsFriendly = NetVarManager->GetOffset(
		"DT_CSPlayerResource", "m_nPersonaDataPublicCommendsFriendly");

	offsetz.DT_PlantedC4.m_bBombTicking = NetVarManager->GetOffset("DT_PlantedC4", "m_bBombTicking");
	offsetz.DT_PlantedC4.m_flC4Blow = NetVarManager->GetOffset("DT_PlantedC4", "m_flC4Blow");
	offsetz.DT_PlantedC4.m_bBombDefused = NetVarManager->GetOffset("DT_PlantedC4", "m_bBombDefused");
	offsetz.DT_PlantedC4.m_hBombDefuser = NetVarManager->GetOffset("DT_PlantedC4", "m_hBombDefuser");
	offsetz.DT_PlantedC4.m_flDefuseCountDown = NetVarManager->GetOffset("DT_PlantedC4", "m_flDefuseCountDown");

	offsetz.DT_CSPlayer.m_iShotsFired = NetVarManager->GetOffset("DT_CSPlayer", "m_iShotsFired");
	offsetz.DT_CSPlayer.m_angEyeAngles[0] = NetVarManager->GetOffset("DT_CSPlayer", "m_angEyeAngles[0]");
	offsetz.DT_CSPlayer.m_angEyeAngles[1] = NetVarManager->GetOffset("DT_CSPlayer", "m_angEyeAngles[1]");
	offsetz.DT_CSPlayer.m_iAccount = NetVarManager->GetOffset("DT_CSPlayer", "m_iAccount");
	offsetz.DT_CSPlayer.m_totalHitsOnServer = NetVarManager->GetOffset("DT_CSPlayer", "m_totalHitsOnServer");
	offsetz.DT_CSPlayer.m_ArmorValue = NetVarManager->GetOffset("DT_CSPlayer", "m_ArmorValue");
	offsetz.DT_CSPlayer.m_bIsDefusing = NetVarManager->GetOffset("DT_CSPlayer", "m_bIsDefusing");
	offsetz.DT_CSPlayer.m_bIsGrabbingHostage = NetVarManager->GetOffset("DT_CSPlayer", "m_bIsGrabbingHostage");
	offsetz.DT_CSPlayer.m_bIsScoped = NetVarManager->GetOffset("DT_CSPlayer", "m_bIsScoped");
	offsetz.DT_CSPlayer.m_bGunGameImmunity = NetVarManager->GetOffset("DT_CSPlayer", "m_bGunGameImmunity");
	offsetz.DT_CSPlayer.m_bIsRescuing = NetVarManager->GetOffset("DT_CSPlayer", "m_bIsRescuing");
	offsetz.DT_CSPlayer.m_bHasHelmet = NetVarManager->GetOffset("DT_CSPlayer", "m_bHasHelmet");
	offsetz.DT_CSPlayer.m_bHasDefuser = NetVarManager->GetOffset("DT_CSPlayer", "m_bHasDefuser");
	offsetz.DT_CSPlayer.m_flFlashDuration = NetVarManager->GetOffset("DT_CSPlayer", "m_flFlashDuration");
	offsetz.DT_CSPlayer.m_flFlashMaxAlpha = NetVarManager->GetOffset("DT_CSPlayer", "m_flFlashMaxAlpha");
	offsetz.DT_CSPlayer.m_flLowerBodyYawTarget = NetVarManager->GetOffset("DT_CSPlayer", "m_flLowerBodyYawTarget");
	offsetz.DT_CSPlayer.m_bHasHeavyArmor = NetVarManager->GetOffset("DT_CSPlayer", "m_bHasHeavyArmor");
	offsetz.DT_CSPlayer.m_iGunGameProgressiveWeaponIndex = NetVarManager->GetOffset(
		"DT_CSPlayer", "m_iGunGameProgressiveWeaponIndex");

	offsetz.DT_BaseAttributableItem.m_iItemDefinitionIndex = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_iItemDefinitionIndex");
	offsetz.DT_BaseAttributableItem.m_iAccountID = NetVarManager->GetOffset("DT_BaseAttributableItem", "m_iAccountID");
	offsetz.DT_BaseAttributableItem.m_iEntityQuality = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_iEntityQuality");
	offsetz.DT_BaseAttributableItem.m_szCustomName = NetVarManager->GetOffset("DT_BaseAttributableItem", "m_szCustomName");
	offsetz.DT_BaseAttributableItem.m_nFallbackPaintKit = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_nFallbackPaintKit");
	offsetz.DT_BaseAttributableItem.m_nFallbackSeed = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_nFallbackSeed");
	offsetz.DT_BaseAttributableItem.m_flFallbackWear = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_flFallbackWear");
	offsetz.DT_BaseAttributableItem.m_nFallbackStatTrak = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_nFallbackStatTrak");
	offsetz.DT_BaseAttributableItem.m_OriginalOwnerXuidLow = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_OriginalOwnerXuidLow");
	offsetz.DT_BaseAttributableItem.m_OriginalOwnerXuidHigh = NetVarManager->GetOffset(
		"DT_BaseAttributableItem", "m_OriginalOwnerXuidHigh");

	offsetz.DT_BaseViewModel.m_nModelIndex = NetVarManager->GetOffset("DT_BaseViewModel", "m_nModelIndex");
	offsetz.DT_BaseViewModel.m_hWeapon = NetVarManager->GetOffset("DT_BaseViewModel", "m_hWeapon");
	offsetz.DT_BaseViewModel.m_hOwner = NetVarManager->GetOffset("DT_BaseViewModel", "m_hOwner");

	offsetz.DT_WeaponCSBase.m_fAccuracyPenalty = NetVarManager->GetOffset("DT_WeaponCSBase", "m_fAccuracyPenalty");
	offsetz.DT_WeaponCSBase.m_flPostponeFireReadyTime = NetVarManager->GetOffset(
		"DT_WeaponCSBase", "m_flPostponeFireReadyTime");

	offsetz.DT_WeaponC4.m_bStartedArming = NetVarManager->GetOffset("DT_WeaponC4", "m_bStartedArming");

	offsetz.DT_BaseCombatWeapon.m_flNextPrimaryAttack = NetVarManager->GetOffset(
		"DT_BaseCombatWeapon", "m_flNextPrimaryAttack");
	offsetz.DT_BaseCombatWeapon.m_iViewModelIndex = NetVarManager->GetOffset("DT_BaseCombatWeapon", "m_iViewModelIndex");
	offsetz.DT_BaseCombatWeapon.m_iWorldModelIndex = NetVarManager->GetOffset("DT_BaseCombatWeapon", "m_iWorldModelIndex");
	offsetz.DT_BaseCombatWeapon.m_hOwner = NetVarManager->GetOffset("DT_BaseCombatWeapon", "m_hOwner");
	offsetz.DT_BaseCombatWeapon.m_iClip1 = NetVarManager->GetOffset("DT_BaseCombatWeapon", "m_iClip1");
	offsetz.DT_BaseCombatWeapon.m_bInReload = offsetz.DT_BaseCombatWeapon.m_flNextPrimaryAttack + 113;
	offsetz.DT_BaseCombatWeapon.m_hWeaponWorldModel = NetVarManager->GetOffset(
		"DT_BaseCombatWeapon", "m_hWeaponWorldModel");
	offsetz.DT_BaseCombatWeapon.m_iReserve = NetVarManager->GetOffset("DT_BaseCombatWeapon", "m_iPrimaryReserveAmmoCount");
	offsetz.DT_BaseCombatWeapon.m_hOwner = NetVarManager->GetOffset("DT_BaseCombatWeapon", "m_hOwner");


	offsetz.DT_BaseCSGrenade.m_bRedraw = NetVarManager->GetOffset("DT_BaseCSGrenade", "m_bRedraw");
	offsetz.DT_BaseCSGrenade.m_bIsHeldByPlayer = NetVarManager->GetOffset("DT_BaseCSGrenade", "m_bIsHeldByPlayer");
	offsetz.DT_BaseCSGrenade.m_bPinPulled = NetVarManager->GetOffset("DT_BaseCSGrenade", "m_bPinPulled");
	offsetz.DT_BaseCSGrenade.m_fThrowTime = NetVarManager->GetOffset("DT_BaseCSGrenade", "m_fThrowTime");
	offsetz.DT_BaseCSGrenade.m_bLoopingSoundPlaying = NetVarManager->GetOffset(
		"DT_BaseCSGrenade", "m_bLoopingSoundPlaying");
	offsetz.DT_BaseCSGrenade.m_flThrowStrength = NetVarManager->GetOffset("DT_BaseCSGrenade", "m_flThrowStrength");

	offsetz.DT_DynamicProp.m_bShouldGlow = NetVarManager->GetOffset("DT_DynamicProp", "m_bShouldGlow");

	offsetz.DT_CSGameRulesProxy.m_bBombPlanted = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bBombPlanted");
	offsetz.DT_CSGameRulesProxy.m_bIsValveDS = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bIsValveDS");

	offsetz.DT_CSGameRulesProxy.m_bFreezePeriod = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bFreezePeriod");
	offsetz.DT_CSGameRulesProxy.m_bMatchWaitingForResume = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_bMatchWaitingForResume");
	offsetz.DT_CSGameRulesProxy.m_bWarmupPeriod = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bWarmupPeriod");
	offsetz.DT_CSGameRulesProxy.m_fWarmupPeriodEnd = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_fWarmupPeriodEnd");
	offsetz.DT_CSGameRulesProxy.m_fWarmupPeriodStart = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_fWarmupPeriodStart");
	offsetz.DT_CSGameRulesProxy.m_bTerroristTimeOutActive = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_bTerroristTimeOutActive");
	offsetz.DT_CSGameRulesProxy.m_bCTTimeOutActive = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bCTTimeOutActive");
	offsetz.DT_CSGameRulesProxy.m_flTerroristTimeOutRemaining = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flTerroristTimeOutRemaining");
	offsetz.DT_CSGameRulesProxy.m_flCTTimeOutRemaining = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flCTTimeOutRemaining");
	offsetz.DT_CSGameRulesProxy.m_nTerroristTimeOuts = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nTerroristTimeOuts");
	offsetz.DT_CSGameRulesProxy.m_nCTTimeOuts = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_nCTTimeOuts");
	offsetz.DT_CSGameRulesProxy.m_iRoundTime = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_iRoundTime");
	offsetz.DT_CSGameRulesProxy.m_gamePhase = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_gamePhase");
	offsetz.DT_CSGameRulesProxy.m_totalRoundsPlayed = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_totalRoundsPlayed");
	offsetz.DT_CSGameRulesProxy.m_nOvertimePlaying = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_nOvertimePlaying");
	offsetz.DT_CSGameRulesProxy.m_timeUntilNextPhaseStarts = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_timeUntilNextPhaseStarts");
	offsetz.DT_CSGameRulesProxy.m_flCMMItemDropRevealStartTime = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flCMMItemDropRevealStartTime");
	offsetz.DT_CSGameRulesProxy.m_flCMMItemDropRevealEndTime = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flCMMItemDropRevealEndTime");
	offsetz.DT_CSGameRulesProxy.m_fRoundStartTime = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_fRoundStartTime");
	offsetz.DT_CSGameRulesProxy.m_bGameRestart = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bGameRestart");
	offsetz.DT_CSGameRulesProxy.m_flRestartRoundTime = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flRestartRoundTime");
	offsetz.DT_CSGameRulesProxy.m_flGameStartTime = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_flGameStartTime");
	offsetz.DT_CSGameRulesProxy.m_iHostagesRemaining = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iHostagesRemaining");
	offsetz.DT_CSGameRulesProxy.m_bAnyHostageReached = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_bAnyHostageReached");
	offsetz.DT_CSGameRulesProxy.m_bMapHasBombTarget = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_bMapHasBombTarget");
	offsetz.DT_CSGameRulesProxy.m_bMapHasRescueZone = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_bMapHasRescueZone");
	offsetz.DT_CSGameRulesProxy.m_bMapHasBuyZone = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bMapHasBuyZone");
	offsetz.DT_CSGameRulesProxy.m_bIsQueuedMatchmaking = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_bIsQueuedMatchmaking");
	offsetz.DT_CSGameRulesProxy.m_bIsValveDS = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bIsValveDS");
	offsetz.DT_CSGameRulesProxy.m_bIsQuestEligible = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bIsQuestEligible");
	offsetz.DT_CSGameRulesProxy.m_bLogoMap = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bLogoMap");
	offsetz.DT_CSGameRulesProxy.m_iNumGunGameProgressiveWeaponsCT = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iNumGunGameProgressiveWeaponsCT");
	offsetz.DT_CSGameRulesProxy.m_iNumGunGameProgressiveWeaponsT = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iNumGunGameProgressiveWeaponsT");
	offsetz.DT_CSGameRulesProxy.m_iSpectatorSlotCount = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iSpectatorSlotCount");
	offsetz.DT_CSGameRulesProxy.m_bBombDropped = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bBombDropped");
	offsetz.DT_CSGameRulesProxy.m_bBombPlanted = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bBombPlanted");
	offsetz.DT_CSGameRulesProxy.m_iRoundWinStatus = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_iRoundWinStatus");
	offsetz.DT_CSGameRulesProxy.m_eRoundWinReason = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_eRoundWinReason");
	offsetz.DT_CSGameRulesProxy.m_flDMBonusStartTime = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flDMBonusStartTime");
	offsetz.DT_CSGameRulesProxy.m_flDMBonusTimeLength = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flDMBonusTimeLength");
	offsetz.DT_CSGameRulesProxy.m_unDMBonusWeaponLoadoutSlot = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_unDMBonusWeaponLoadoutSlot");
	offsetz.DT_CSGameRulesProxy.m_bDMBonusActive = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bDMBonusActive");
	offsetz.DT_CSGameRulesProxy.m_bTCantBuy = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bTCantBuy");
	offsetz.DT_CSGameRulesProxy.m_bCTCantBuy = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bCTCantBuy");
	offsetz.DT_CSGameRulesProxy.m_flGuardianBuyUntilTime = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flGuardianBuyUntilTime");
	offsetz.DT_CSGameRulesProxy.m_iMatchStats_RoundResults = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iMatchStats_RoundResults");
	offsetz.DT_CSGameRulesProxy.m_iMatchStats_PlayersAlive_T = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iMatchStats_PlayersAlive_T");
	offsetz.DT_CSGameRulesProxy.m_iMatchStats_PlayersAlive_CT = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iMatchStats_PlayersAlive_CT");
	offsetz.DT_CSGameRulesProxy.m_GGProgressiveWeaponOrderC = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_GGProgressiveWeaponOrderC");
	offsetz.DT_CSGameRulesProxy.m_GGProgressiveWeaponOrderT = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_GGProgressiveWeaponOrderT");
	offsetz.DT_CSGameRulesProxy.m_GGProgressiveWeaponKillUpgradeOrderCT = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_GGProgressiveWeaponKillUpgradeOrderCT");
	offsetz.DT_CSGameRulesProxy.m_GGProgressiveWeaponKillUpgradeOrderT = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_GGProgressiveWeaponKillUpgradeOrderT");
	offsetz.DT_CSGameRulesProxy.m_MatchDevice = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_MatchDevice");
	offsetz.DT_CSGameRulesProxy.m_bHasMatchStarted = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bHasMatchStarted");
	offsetz.DT_CSGameRulesProxy.m_TeamRespawnWaveTimes = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_TeamRespawnWaveTimes");
	offsetz.DT_CSGameRulesProxy.m_flNextRespawnWave = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_flNextRespawnWave");
	offsetz.DT_CSGameRulesProxy.m_nNextMapInMapgroup = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nNextMapInMapgroup");
	offsetz.DT_CSGameRulesProxy.m_nEndMatchMapGroupVoteOptions = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nEndMatchMapGroupVoteOptions");
	offsetz.DT_CSGameRulesProxy.m_bIsDroppingItems = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_bIsDroppingItems");
	offsetz.DT_CSGameRulesProxy.m_iActiveAssassinationTargetMissionID = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_iActiveAssassinationTargetMissionID");
	offsetz.DT_CSGameRulesProxy.m_fMatchStartTime = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_fMatchStartTime");
	offsetz.DT_CSGameRulesProxy.m_szTournamentEventName = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_szTournamentEventName");
	offsetz.DT_CSGameRulesProxy.m_szTournamentEventStage = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_szTournamentEventStage");
	offsetz.DT_CSGameRulesProxy.m_szTournamentPredictionsTxt = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_szTournamentPredictionsTxt");
	offsetz.DT_CSGameRulesProxy.m_nTournamentPredictionsPct = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nTournamentPredictionsPct");
	offsetz.DT_CSGameRulesProxy.m_szMatchStatTxt = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_szMatchStatTxt");
	offsetz.DT_CSGameRulesProxy.m_nGuardianModeWaveNumber = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nGuardianModeWaveNumber");
	offsetz.DT_CSGameRulesProxy.m_nGuardianModeSpecialKillsRemaining = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nGuardianModeSpecialKillsRemaining");
	offsetz.DT_CSGameRulesProxy.m_nGuardianModeSpecialWeaponNeeded = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nGuardianModeSpecialWeaponNeeded");
	offsetz.DT_CSGameRulesProxy.m_nHalloweenMaskListSeed = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_nHalloweenMaskListSeed");
	offsetz.DT_CSGameRulesProxy.m_numGlobalGiftsGiven = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_numGlobalGiftsGiven");
	offsetz.DT_CSGameRulesProxy.m_numGlobalGifters = NetVarManager->GetOffset("DT_CSGameRulesProxy", "m_numGlobalGifters");
	offsetz.DT_CSGameRulesProxy.m_numGlobalGiftsPeriodSeconds = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_numGlobalGiftsPeriodSeconds");
	offsetz.DT_CSGameRulesProxy.m_arrFeaturedGiftersAccounts = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_arrFeaturedGiftersAccounts");
	offsetz.DT_CSGameRulesProxy.m_arrFeaturedGiftersGifts = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_arrFeaturedGiftersGifts");
	offsetz.DT_CSGameRulesProxy.m_arrTournamentActiveCasterAccounts = NetVarManager->GetOffset(
		"DT_CSGameRulesProxy", "m_arrTournamentActiveCasterAccounts");

	offsetz.DT_BaseAnimating.m_nHitboxSet = NetVarManager->GetOffset("DT_BaseAnimating", "m_nHitboxSet");
}

Offsetz* g_Netvars = new(Offsetz);
COffsetz offsetz;

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class ixyzxmg
{
public:
	bool cjqqyuwgjkosa;
	bool snsrwlltqay;
	ixyzxmg();
	int btckjksykgortnoxrltd(double bkzvkkhimwg, int piyifv, bool skptoypi, string qqaayadsxdpvx, double vwfhjmvgt);
	int iqyozmrzhocpnmpuelibuayji(double cjhomr, string mpbkzducbslvvdn, int ifilor, string ljjxfvg, string lmtck,
	                              int mosuhro, int gktvdma);
	bool prfznskajedmlvqf(int xytvizevb, string zuqkjwcnaaugoz, double qsvixqhhyz, string wgvcargennqfi, double hmnvlmjff);
	bool mrxkknjujoxnfaqse(string dohyyyl, int hulnsyrdmis, int eqwrcphwous, bool weqncyl, bool lcmilrlhcge,
	                       int klbngsykaoegp);
	int zcjlqdwoieuxvifuqgir(bool rzwoibnuoh, string kturz, string tgjwfwchilc, bool hllckbyrxnmzge, double uerlrbbktx,
	                         int aoxgxvgebzjkeut, int gjsidzeuhkwcokg, double clxfrvnyug, string xpzoggdeaghv);
	double losuzzrxxyxdlaxvelayhqtb(int jxmrtddtogi, string uvczhnhqvi);
	int tilibghgvhpgtsabycsweg(string eobtquypo, bool wnkpayeankr, string gokpmyyadxoj, int apjayuqybhhpwpj,
	                           bool eewjfgtsyt, double enfilaznjchvczo);
	string fijmfrtnnwajhubahdap(string ehapdwqetcwbg, int jbnverkutspinu, bool hxlwlyzrbu, string uzntoo,
	                            string yfvwwgrcklgm);

protected:
	bool adbhzizj;
	bool bxynqtnuv;
	bool wymnzflscyvrqm;
	bool hhzefzmgpxw;
	double lrjiiw;

	void jljuthxsoipy(double vvhmb, bool axkdsczlsbppojj, bool ytnmgxbb, string zbxltlgbpuqwfe, bool lqocradzplx,
	                  int feikuhhvynysms);
	bool dalinsasaoqmzztkgwpxjsxgd();
	string sqzmfsfnhtrzqpp(double vaqnpphxbvodxyf, string fcucttczwmzdaxd, string hxewazwokrh, int vkqvtkeirhxvwzc,
	                       int nzeuw, int wqmbjayinjzcjl, string ircwcgm, int kqfndgqdgl, string nxgwaolrouqad,
	                       int wlovoyfhaaaigz);
	double ylwpbkgwsctsh(double vsrqaipnhddphr, double abzezetamvl);
	int wrwhhqixucrssvgzwjml(double wwlkbwxyet, bool oilzcsi, int yduytibr);
	string dywfaxrerywgn(string bqzqsjzm, bool wvsxtydvhfzvgjr, string evpcpu, bool hrkdbzjqris, double quxurj,
	                     int mjjqst);
	int xrxfgczrsasdshhxmrc(int ahmoyo, int uoqnkwocd, string fhgztwhcdpjhxjb, double jcudmjmklb, int pydsk, int ebwqozjo,
	                        int ooylnntp, string hsujshzk);
	bool hvwvxdxjkw(bool eajlrvzuayhm, double yrhvnead);
	string gyyzlvfljvezfrz(int oyywbtmbkspuoua, int jdbigw, bool mwrbttafzi, int mitdbqezuv, string febucndzenkcsp,
	                       double lznybsbtsivzlk, string obrqfzbkfctj, int omvvbn, double yaoqjhhwtxnfxa);

private:
	double pmabcu;

	void aajklykzdzcyywo(string quxehv);
	string qecbpmaxrypznek(int nrvhgtfomskh, string qocqpfwxs, bool gdubrsc);
	int ynagohqyhpispnmkhodtjxm(int odzxtacykmcvlv, string qokhhovklmhi, bool sgnkxcc);
	int bgfsutubqvrictkdc();
	bool onuxwzgoxuqtdquaor(double rzqzz);
	string tmjmfmjswttyl(int zsetoc, int qbrcnfwmxmkzsga, string wjpzuhltplh, bool amvwlznhklrw, int hvcbvkqrgwcukxw,
	                     string zxdhqghuzbp);
	double gjcaodndqfw();
};


void ixyzxmg::aajklykzdzcyywo(string quxehv)
{
	bool ijifalwosey = false;
	string pksnbcpajvu = "tojdrwje";
	int qzaggmrmaqzxpr = 688;
	string exjskduvjgndog = "fys";
	string mcgsckjcmcni = "qdcpmhipcayguxdlpanjhylpznppshthpmfygyxbo";
	if (string("fys") != string("fys"))
	{
		int xchacsvyhj;
		for (xchacsvyhj = 83; xchacsvyhj > 0; xchacsvyhj--)
		{
		}
	}
	if (false == false)
	{
		int bgqfo;
		for (bgqfo = 79; bgqfo > 0; bgqfo--)
		{
		}
	}
}

string ixyzxmg::qecbpmaxrypznek(int nrvhgtfomskh, string qocqpfwxs, bool gdubrsc)
{
	double rvpaipqfilxru = 21906;
	double qgxoeqxqd = 9177;
	int tjznsdbkeuft = 842;
	int dnvcujxvuojm = 2473;
	int uartevkbndbnqm = 5904;
	if (2473 != 2473)
	{
		int zfqmvxacbf;
		for (zfqmvxacbf = 99; zfqmvxacbf > 0; zfqmvxacbf--)
		{
			continue;
		}
	}
	return string("w");
}

int ixyzxmg::ynagohqyhpispnmkhodtjxm(int odzxtacykmcvlv, string qokhhovklmhi, bool sgnkxcc)
{
	double xgldtpdhhuvmftr = 15811;
	bool laakzgdteimb = true;
	string oynnigodaaajbrr = "bntcjdwcxmyefiahimgbtbaznztyw";
	int gzyhzfjupydht = 3698;
	int tppdirv = 485;
	string hrfmdww = "epbsvhcbnfgrfytuxiwatszoxwswwmdgmbwtcyxo";
	double iawpvkeawlcb = 59129;
	if (string("bntcjdwcxmyefiahimgbtbaznztyw") != string("bntcjdwcxmyefiahimgbtbaznztyw"))
	{
		int rsqvjhy;
		for (rsqvjhy = 6; rsqvjhy > 0; rsqvjhy--)
		{
		}
	}
	if (485 == 485)
	{
		int twkht;
		for (twkht = 37; twkht > 0; twkht--)
		{
		}
	}
	if (59129 == 59129)
	{
		int jdey;
		for (jdey = 6; jdey > 0; jdey--)
		{
		}
	}
	if (string("bntcjdwcxmyefiahimgbtbaznztyw") == string("bntcjdwcxmyefiahimgbtbaznztyw"))
	{
		int lmsjlt;
		for (lmsjlt = 93; lmsjlt > 0; lmsjlt--)
		{
		}
	}
	if (string("epbsvhcbnfgrfytuxiwatszoxwswwmdgmbwtcyxo") == string("epbsvhcbnfgrfytuxiwatszoxwswwmdgmbwtcyxo"))
	{
		int ckrvdozwk;
		for (ckrvdozwk = 51; ckrvdozwk > 0; ckrvdozwk--)
		{
		}
	}
	return 11722;
}

int ixyzxmg::bgfsutubqvrictkdc()
{
	bool jzvfw = true;
	string zhfapixwffiwi = "jksbnpmbzmgpoxwueepvyktfkftrfuayrlyidoov";
	int ofbeezskmcqdsjh = 2748;
	int ljfhezptfgzwnc = 884;
	bool xmrahlzpkcd = false;
	int ttsdxlwnrffmft = 2179;
	string kyjtzmysejy = "bcptasxliddwgugacsjldyzoady";
	bool cyctsnkscixqvf = false;
	string zdrjqcecvxvwpz = "lejsmvtxg";
	int enxvdmlncdkkxo = 3537;
	if (2748 != 2748)
	{
		int nsujmzhs;
		for (nsujmzhs = 85; nsujmzhs > 0; nsujmzhs--)
		{
			continue;
		}
	}
	if (884 != 884)
	{
		int mm;
		for (mm = 76; mm > 0; mm--)
		{
			continue;
		}
	}
	if (3537 != 3537)
	{
		int ibajizak;
		for (ibajizak = 87; ibajizak > 0; ibajizak--)
		{
			continue;
		}
	}
	if (string("jksbnpmbzmgpoxwueepvyktfkftrfuayrlyidoov") != string("jksbnpmbzmgpoxwueepvyktfkftrfuayrlyidoov"))
	{
		int iafkvbvl;
		for (iafkvbvl = 54; iafkvbvl > 0; iafkvbvl--)
		{
		}
	}
	if (false == false)
	{
		int ddcelzf;
		for (ddcelzf = 54; ddcelzf > 0; ddcelzf--)
		{
		}
	}
	return 24313;
}

bool ixyzxmg::onuxwzgoxuqtdquaor(double rzqzz)
{
	double twzazda = 59467;
	string dydhmug = "mbbwowfculglekiudwxjtqhdinppoeesopjxdqsoobjakvsametxhotgqxecctgl";
	double minbps = 16078;
	int hohqnofbgejxw = 2168;
	string jvjwkcfkutewurj = "pxhkmtsrafndvutncogzarihsuxqimm";
	double qfrugoobogp = 2677;
	int eiuxdgjoxnmqz = 471;
	int fpqbqsbcukqli = 2062;
	bool snvsxewibpil = true;
	if (string("pxhkmtsrafndvutncogzarihsuxqimm") == string("pxhkmtsrafndvutncogzarihsuxqimm"))
	{
		int logi;
		for (logi = 98; logi > 0; logi--)
		{
		}
	}
	if (2677 == 2677)
	{
		int iwsr;
		for (iwsr = 83; iwsr > 0; iwsr--)
		{
		}
	}
	return false;
}

string ixyzxmg::tmjmfmjswttyl(int zsetoc, int qbrcnfwmxmkzsga, string wjpzuhltplh, bool amvwlznhklrw,
                              int hvcbvkqrgwcukxw, string zxdhqghuzbp)
{
	double nidagwrmqyjkyu = 27932;
	string bkfmkjgaxy = "gthpvxoaogmbztyunmfyyrrbkgpczspcsjdnkprewfqgkmkfjahzwnakwvphqhgcrcj";
	int bjcrgalra = 2405;
	bool ziaeqchfg = true;
	bool lfrxkafg = true;
	string ywbjgquwrlgxgf = "imbuuxxaulonpxhagmhpphppoadiieyshjpdrjuvcdicugmsfujfbukjzbzregoqwuojjmyoyfqrwquczx";
	double gunmhbrcv = 8293;
	int qujmyhyid = 3872;
	double zwsnug = 40051;
	if (3872 == 3872)
	{
		int ypoglj;
		for (ypoglj = 78; ypoglj > 0; ypoglj--)
		{
		}
	}
	if (true != true)
	{
		int sg;
		for (sg = 96; sg > 0; sg--)
		{
			continue;
		}
	}
	if (3872 != 3872)
	{
		int qyjyjno;
		for (qyjyjno = 90; qyjyjno > 0; qyjyjno--)
		{
			continue;
		}
	}
	if (8293 != 8293)
	{
		int bilgsdtr;
		for (bilgsdtr = 70; bilgsdtr > 0; bilgsdtr--)
		{
			continue;
		}
	}
	if (8293 != 8293)
	{
		int nn;
		for (nn = 61; nn > 0; nn--)
		{
			continue;
		}
	}
	return string("rxzufabmfinkfupojtbr");
}

double ixyzxmg::gjcaodndqfw()
{
	int xontmfl = 290;
	int vnmhufrpeunazz = 210;
	int yweuhjxjlx = 205;
	bool bczbfg = false;
	bool izhdrv = false;
	bool fvmmkoyknygzbrx = true;
	bool yvxhonzfyrzs = false;
	bool vuoisfwow = false;
	double oahbobsmjjem = 19483;
	return 14004;
}

void ixyzxmg::jljuthxsoipy(double vvhmb, bool axkdsczlsbppojj, bool ytnmgxbb, string zbxltlgbpuqwfe, bool lqocradzplx,
                           int feikuhhvynysms)
{
	double ytayttbfvqzuv = 8803;
	bool dvtsfhaplm = false;
	string dwbfqgird = "orbxefktarhmmyodgoxghdskdysemsnkbokadlqgpbqphewrncvhrsxf";
	string shzrttm = "wengnhrlwddzvjyxeslewdaazsaxffjtaczkiqzmka";
	if (8803 == 8803)
	{
		int dvvgd;
		for (dvvgd = 6; dvvgd > 0; dvvgd--)
		{
		}
	}
}

bool ixyzxmg::dalinsasaoqmzztkgwpxjsxgd()
{
	int sskdmfecg = 1362;
	bool cyuxesmjgrm = false;
	int rwnxusvx = 819;
	double dglqeqf = 10114;
	double prpqydungv = 26557;
	int npcqkvzvthhv = 2766;
	double eqbwmssxzi = 16715;
	return false;
}

string ixyzxmg::sqzmfsfnhtrzqpp(double vaqnpphxbvodxyf, string fcucttczwmzdaxd, string hxewazwokrh, int vkqvtkeirhxvwzc,
                                int nzeuw, int wqmbjayinjzcjl, string ircwcgm, int kqfndgqdgl, string nxgwaolrouqad,
                                int wlovoyfhaaaigz)
{
	string bawnnelfhbe = "fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz";
	if (string("fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz") != string(
		"fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz"))
	{
		int aaman;
		for (aaman = 46; aaman > 0; aaman--)
		{
		}
	}
	if (string("fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz") == string(
		"fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz"))
	{
		int ov;
		for (ov = 88; ov > 0; ov--)
		{
		}
	}
	if (string("fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz") == string(
		"fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz"))
	{
		int ptyfsog;
		for (ptyfsog = 86; ptyfsog > 0; ptyfsog--)
		{
		}
	}
	if (string("fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz") == string(
		"fzpdzpsmjpkufxfgvzvczyvryquzjlgwjbwzgoycmtecvzsdbdjpzmsmahetwsvumcaetjz"))
	{
		int ciwhwfgaii;
		for (ciwhwfgaii = 54; ciwhwfgaii > 0; ciwhwfgaii--)
		{
		}
	}
	return string("toaxuqcxbtz");
}

double ixyzxmg::ylwpbkgwsctsh(double vsrqaipnhddphr, double abzezetamvl)
{
	double layix = 40987;
	bool wbeks = true;
	double tjscwtujry = 27438;
	int jupbjshezfr = 1943;
	string sryjghffet = "nwivvcapscpysogxmdspunqcylncjnjqqskhjwzezagqmbwsnkjbaijerqctfoncjobkcmzbvcjxxruxr";
	bool uojtimwaoapqk = false;
	bool bgtgtcdtgrss = false;
	string ordkemibx = "dmnygcawvvhtpuiftukijnglrtzxbomjbwwzrjgufrrfziecmpvohed";
	if (27438 == 27438)
	{
		int haai;
		for (haai = 13; haai > 0; haai--)
		{
		}
	}
	if (1943 != 1943)
	{
		int tessmqf;
		for (tessmqf = 60; tessmqf > 0; tessmqf--)
		{
			continue;
		}
	}
	return 75492;
}

int ixyzxmg::wrwhhqixucrssvgzwjml(double wwlkbwxyet, bool oilzcsi, int yduytibr)
{
	bool bnkztj = false;
	string vuclquiqu = "ojjjhzpubsdbeyjwkbhxnaysxqasvkqacwdpcuccziqbgpvlxzavdztthfzsvxpaxqaaovoylypxjkehby";
	bool fppzk = false;
	bool zdnrzhtmfmg = true;
	bool pfqqhail = true;
	double vnohj = 416;
	int dxqhejzhpqhbz = 5451;
	if (false != false)
	{
		int jy;
		for (jy = 62; jy > 0; jy--)
		{
			continue;
		}
	}
	if (5451 != 5451)
	{
		int xt;
		for (xt = 29; xt > 0; xt--)
		{
			continue;
		}
	}
	if (5451 != 5451)
	{
		int yjcg;
		for (yjcg = 47; yjcg > 0; yjcg--)
		{
			continue;
		}
	}
	return 39438;
}

string ixyzxmg::dywfaxrerywgn(string bqzqsjzm, bool wvsxtydvhfzvgjr, string evpcpu, bool hrkdbzjqris, double quxurj,
                              int mjjqst)
{
	double ojihtvi = 47506;
	bool ugqriq = false;
	if (false == false)
	{
		int edzep;
		for (edzep = 13; edzep > 0; edzep--)
		{
		}
	}
	if (false == false)
	{
		int derbgfqvlx;
		for (derbgfqvlx = 6; derbgfqvlx > 0; derbgfqvlx--)
		{
		}
	}
	return string("qytnp");
}

int ixyzxmg::xrxfgczrsasdshhxmrc(int ahmoyo, int uoqnkwocd, string fhgztwhcdpjhxjb, double jcudmjmklb, int pydsk,
                                 int ebwqozjo, int ooylnntp, string hsujshzk)
{
	int hptty = 5949;
	string rqodlahndawjrq = "qutejdvlwhtqpbcfwhasbxssuvwszmqgkayexxuzqafdxqxpb";
	if (string("qutejdvlwhtqpbcfwhasbxssuvwszmqgkayexxuzqafdxqxpb") == string(
		"qutejdvlwhtqpbcfwhasbxssuvwszmqgkayexxuzqafdxqxpb"))
	{
		int ykxhiosxvi;
		for (ykxhiosxvi = 25; ykxhiosxvi > 0; ykxhiosxvi--)
		{
		}
	}
	if (5949 != 5949)
	{
		int wycztexj;
		for (wycztexj = 83; wycztexj > 0; wycztexj--)
		{
			continue;
		}
	}
	return 76775;
}

bool ixyzxmg::hvwvxdxjkw(bool eajlrvzuayhm, double yrhvnead)
{
	double neaxqmj = 88632;
	double krporkobzt = 58069;
	bool swjumx = true;
	string nwlhcjhr = "ouqgqofysmlsiejhvijysrwapcfvgdqsrbjcdduidmhkxqcantrcsoeudjapbuohbfduaoydxot";
	double lemrtn = 19421;
	bool ctmqrjnch = true;
	bool vilfiud = true;
	if (19421 == 19421)
	{
		int eur;
		for (eur = 7; eur > 0; eur--)
		{
		}
	}
	if (string("ouqgqofysmlsiejhvijysrwapcfvgdqsrbjcdduidmhkxqcantrcsoeudjapbuohbfduaoydxot") == string(
		"ouqgqofysmlsiejhvijysrwapcfvgdqsrbjcdduidmhkxqcantrcsoeudjapbuohbfduaoydxot"))
	{
		int ipgdd;
		for (ipgdd = 51; ipgdd > 0; ipgdd--)
		{
		}
	}
	if (58069 != 58069)
	{
		int ygksf;
		for (ygksf = 80; ygksf > 0; ygksf--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int qnefksqv;
		for (qnefksqv = 30; qnefksqv > 0; qnefksqv--)
		{
		}
	}
	if (true != true)
	{
		int em;
		for (em = 14; em > 0; em--)
		{
			continue;
		}
	}
	return false;
}

string ixyzxmg::gyyzlvfljvezfrz(int oyywbtmbkspuoua, int jdbigw, bool mwrbttafzi, int mitdbqezuv, string febucndzenkcsp,
                                double lznybsbtsivzlk, string obrqfzbkfctj, int omvvbn, double yaoqjhhwtxnfxa)
{
	string pasbuk = "cdxleermvidtqyvuwoideposqjzunyomsuvfpnsnlprkmkqlsugfnarmjhchunlbbqgagpmbtrzuqenwwkmflpousyzheyrce";
	double jpyntpq = 23919;
	int ehrfeuhifw = 4236;
	string pgmentiwjv = "tzrclndebvwmctnusapicyhjryvsgeulzjumyvbudysgizomkeixhxojypdndbzdbonzmvscaulepimaaeselkbdhuwgbzuo";
	double pgejcsno = 117;
	double oircqxzan = 3702;
	int pmlzcpvjlvjpwaw = 280;
	return string("");
}

int ixyzxmg::btckjksykgortnoxrltd(double bkzvkkhimwg, int piyifv, bool skptoypi, string qqaayadsxdpvx, double vwfhjmvgt)
{
	string vzuqbmkgylr = "fbhswfidoumifppzqyydwrwklwbhuwblygkgmdgvtfjbibdhndbivfuylluyewo";
	int hjeucnp = 7121;
	int mxomgifevqmpq = 682;
	double ftoyhizwre = 24556;
	bool ecemkvelycak = true;
	int wqgpjfelbbanmtb = 426;
	string ibsti = "loxlhbsuruaqjzuprbeblohwfpapogbpbtfwjuffeo";
	int pjmxswtpzzqxpyr = 1128;
	double oydjboyixruyqy = 89655;
	if (string("fbhswfidoumifppzqyydwrwklwbhuwblygkgmdgvtfjbibdhndbivfuylluyewo") == string(
		"fbhswfidoumifppzqyydwrwklwbhuwblygkgmdgvtfjbibdhndbivfuylluyewo"))
	{
		int rlwzhnmt;
		for (rlwzhnmt = 71; rlwzhnmt > 0; rlwzhnmt--)
		{
		}
	}
	return 67368;
}

int ixyzxmg::iqyozmrzhocpnmpuelibuayji(double cjhomr, string mpbkzducbslvvdn, int ifilor, string ljjxfvg, string lmtck,
                                       int mosuhro, int gktvdma)
{
	int jxvfxcix = 5851;
	bool pbvwaunnxcaq = false;
	string khykpywrkobgx = "ryuwzljqcvjlpjmfkwkrt";
	double adlppccncqlvwo = 7154;
	if (string("ryuwzljqcvjlpjmfkwkrt") == string("ryuwzljqcvjlpjmfkwkrt"))
	{
		int sgu;
		for (sgu = 44; sgu > 0; sgu--)
		{
		}
	}
	if (5851 == 5851)
	{
		int tf;
		for (tf = 89; tf > 0; tf--)
		{
		}
	}
	return 47337;
}

bool ixyzxmg::prfznskajedmlvqf(int xytvizevb, string zuqkjwcnaaugoz, double qsvixqhhyz, string wgvcargennqfi,
                               double hmnvlmjff)
{
	int ofkltakprzmqhvx = 4009;
	string hmqsxt = "qnwkyldiqhbmfsaelkjrhnrphxxx";
	string hpxje = "fvojhsdyohjh";
	int ulmyqncwc = 1449;
	int fuisfqp = 5041;
	bool nxnqfbddvq = true;
	bool jtwegrue = true;
	int pinen = 3775;
	double fktcgvdg = 16827;
	bool flvxgnwnhntuloh = true;
	if (16827 == 16827)
	{
		int xvynagyn;
		for (xvynagyn = 2; xvynagyn > 0; xvynagyn--)
		{
		}
	}
	if (4009 == 4009)
	{
		int mjokbz;
		for (mjokbz = 36; mjokbz > 0; mjokbz--)
		{
		}
	}
	if (string("qnwkyldiqhbmfsaelkjrhnrphxxx") == string("qnwkyldiqhbmfsaelkjrhnrphxxx"))
	{
		int ny;
		for (ny = 38; ny > 0; ny--)
		{
		}
	}
	return true;
}

bool ixyzxmg::mrxkknjujoxnfaqse(string dohyyyl, int hulnsyrdmis, int eqwrcphwous, bool weqncyl, bool lcmilrlhcge,
                                int klbngsykaoegp)
{
	int mxzzknzhxnh = 5929;
	bool xyqclzemxutbj = true;
	bool gsdopsaryzynh = false;
	if (false == false)
	{
		int vq;
		for (vq = 3; vq > 0; vq--)
		{
		}
	}
	if (true != true)
	{
		int fwqwvugcdx;
		for (fwqwvugcdx = 83; fwqwvugcdx > 0; fwqwvugcdx--)
		{
			continue;
		}
	}
	return false;
}

int ixyzxmg::zcjlqdwoieuxvifuqgir(bool rzwoibnuoh, string kturz, string tgjwfwchilc, bool hllckbyrxnmzge,
                                  double uerlrbbktx, int aoxgxvgebzjkeut, int gjsidzeuhkwcokg, double clxfrvnyug,
                                  string xpzoggdeaghv)
{
	double yeejcluthfg = 55990;
	bool qapjzuwl = false;
	bool ltqqucvnfzkzy = false;
	string vcelrreerzqp = "ijuqykudmkrsxzgyb";
	int twopwktpx = 1904;
	int oxskh = 2899;
	string adsorjtdqltbk = "kthdbjettpfbzocbkicxfvmhwudixbndjhxcwarrmbspsejumeicoebymuggtteiveluwdvkuijoyltrbogeagd";
	bool fyzhzhwi = true;
	if (2899 != 2899)
	{
		int tpg;
		for (tpg = 28; tpg > 0; tpg--)
		{
			continue;
		}
	}
	if (2899 == 2899)
	{
		int dfzek;
		for (dfzek = 51; dfzek > 0; dfzek--)
		{
		}
	}
	if (string("ijuqykudmkrsxzgyb") == string("ijuqykudmkrsxzgyb"))
	{
		int yyqegrvra;
		for (yyqegrvra = 73; yyqegrvra > 0; yyqegrvra--)
		{
		}
	}
	if (string("ijuqykudmkrsxzgyb") == string("ijuqykudmkrsxzgyb"))
	{
		int zvoi;
		for (zvoi = 58; zvoi > 0; zvoi--)
		{
		}
	}
	if (1904 != 1904)
	{
		int zs;
		for (zs = 37; zs > 0; zs--)
		{
			continue;
		}
	}
	return 97094;
}

double ixyzxmg::losuzzrxxyxdlaxvelayhqtb(int jxmrtddtogi, string uvczhnhqvi)
{
	return 97198;
}

int ixyzxmg::tilibghgvhpgtsabycsweg(string eobtquypo, bool wnkpayeankr, string gokpmyyadxoj, int apjayuqybhhpwpj,
                                    bool eewjfgtsyt, double enfilaznjchvczo)
{
	string tizjligmq = "dwcwvuyyrepvetipreyenuohschywoqymrkefrpypvsejfcvomsdkatfpirxhkbkztlbmnmbvlzud";
	bool pgtwr = false;
	bool dvufxmoatfzsu = false;
	double cfeidtp = 63901;
	string sojttkl = "fnzfjqpnuj";
	int sdbaimiawjs = 2757;
	if (63901 != 63901)
	{
		int wqti;
		for (wqti = 45; wqti > 0; wqti--)
		{
			continue;
		}
	}
	if (63901 == 63901)
	{
		int aobjz;
		for (aobjz = 90; aobjz > 0; aobjz--)
		{
		}
	}
	if (string("fnzfjqpnuj") != string("fnzfjqpnuj"))
	{
		int ia;
		for (ia = 12; ia > 0; ia--)
		{
		}
	}
	if (2757 == 2757)
	{
		int gswnq;
		for (gswnq = 33; gswnq > 0; gswnq--)
		{
		}
	}
	return 55854;
}

string ixyzxmg::fijmfrtnnwajhubahdap(string ehapdwqetcwbg, int jbnverkutspinu, bool hxlwlyzrbu, string uzntoo,
                                     string yfvwwgrcklgm)
{
	string omtkqnjcghjjyyu = "xigvbtlbnmzgzuoyibcovnapphcaefxcbulusoinkitpawleqrtpghjvdhargqtvprlxmzfevwuhdouw";
	bool bmksvz = false;
	bool oheniepyowtlhu = false;
	double qlwwdmioeb = 22531;
	double vnyjevbfvuwr = 14283;
	int fhjsuezeyqnyda = 4238;
	double pfclvbg = 2914;
	bool hixcsabvcrgemsz = false;
	return string("wqmdctew");
}

ixyzxmg::ixyzxmg()
{
	this->btckjksykgortnoxrltd(19286, 2362, false, string("oqenxbnaqqf"), 8108);
	this->iqyozmrzhocpnmpuelibuayji(7568, string("lwczcoxraqstrvayoazcdqmuliruebzoivyhxjwvghvo"), 1599, string("sdn"),
	                                string("pfohmjrufajlwpqyvtfnwvvlilwwzrax"), 3340, 3215);
	this->prfznskajedmlvqf(1332, string("qtdofmjiexzxoooqomohrupcsbtzbgfbbwolyxsqaelwsuiq"), 47758,
	                       string("jfhvpnvcoweymeogfjjjohiogsjxrnqqgreuywwlehwiowfabyvxent"), 36839);
	this->mrxkknjujoxnfaqse(
		string("jhenndcqzyskddqypiynorvhrailvpwcedpmjeifdvsmlcipshrqqsgumfgppgqmkufvraznmixuduyykgmgaaoborxze"), 3325, 1212,
		true, false, 6390);
	this->zcjlqdwoieuxvifuqgir(false, string("tdvehhacxxsnhgitnvambfooleejybxtnd"), string("qhmnwaoiaq"), true, 8746, 631,
	                           1642, 1821, string("utlqlyphsxfdhfcacfxtkrsttv"));
	this->losuzzrxxyxdlaxvelayhqtb(5553, string("sarszztbs"));
	this->tilibghgvhpgtsabycsweg(string("cljceybfbnpagypiqnchwplhskfqwlmywhdlm"), false,
	                             string("sslgxscptmbdgkjzrfxtwvybcznxmbtoqlxakjarnsygpvca"), 2879, true, 23361);
	this->fijmfrtnnwajhubahdap(string("gcfikopnlhilsmoycxtphyqnmlxmqdrjgdxzuejzljgpkntkrxwgeibmrnojkksiwcfwsdyz"), 327,
	                           true, string("hxfaovlmdvkavrntxuvzufloetdqhentompdyladsjbvxogowvoismhmiilebi"),
	                           string("vknmreaigwgsoadkhmknpyxfaquewyodxisqylqvrxvdac"));
	this->jljuthxsoipy(2253, true, true,
	                   string(
		                   "azsstnedqwmjkzyyaqqsdchfmhegkvlhkhbslsnmdiolsqodynzujqutjvydwybfvaggmanmzituobfznffwoipdymjflbmgo"),
	                   true, 3298);
	this->dalinsasaoqmzztkgwpxjsxgd();
	this->sqzmfsfnhtrzqpp(7432, string("xyokssfot"), string("roebnzttnyrqjtaecpvoaktrgatvxtclppoodtpzijf"), 1714, 1310,
	                      2391, string(
		                      "atrcmtcxaoklmtxcfdaxsmljfrgvrbdqoukpckjvetjbrdvnwbjbtyxejmvnrbrvkopyhzwzsggjftuhroryxlabof"),
	                      1454, string("r"), 9294);
	this->ylwpbkgwsctsh(18333, 81345);
	this->wrwhhqixucrssvgzwjml(1612, false, 593);
	this->dywfaxrerywgn(string(""), true, string("blyutmxbnlcywwktwrmabwacgchxybzoomlmidjjilleymnzvhsjyprdyosb"), true,
	                    1642, 3356);
	this->xrxfgczrsasdshhxmrc(1165, 2717, string("gurrnfvrbghgfpbqbcwcekgefiiqw"), 46127, 4245, 5403, 273,
	                          string("nuvkohfmzez"));
	this->hvwvxdxjkw(false, 15488);
	this->gyyzlvfljvezfrz(2467, 2264, true, 1579, string("wzkgzeo"), 12481, string("eqqinnuxwllanceglabjruhv"), 4246,
	                      7193);
	this->aajklykzdzcyywo(string("hjxdlkaelqyympudyqbgiiihud"));
	this->qecbpmaxrypznek(1158, string("hsjvwzydplfk"), false);
	this->ynagohqyhpispnmkhodtjxm(376, string("ogerlbvnoholqpqlbccrottkdgvzgpkjuifmtmatxgqtnibgqugsly"), true);
	this->bgfsutubqvrictkdc();
	this->onuxwzgoxuqtdquaor(5702);
	this->tmjmfmjswttyl(5671, 5519, string("jkprergpuwdkcimsnlqquezvnedo"), false, 3741,
	                    string("gnspeojvoeugdyahkjlbanotfcsc"));
	this->gjcaodndqfw();
}
