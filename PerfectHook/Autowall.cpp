#include "AutoWall.h"
//#include "R.h"

#define    HITGROUP_GENERIC    0
#define    HITGROUP_HEAD        1
#define    HITGROUP_CHEST        2
#define    HITGROUP_STOMACH    3
#define HITGROUP_LEFTARM    4
#define HITGROUP_RIGHTARM    5
#define HITGROUP_LEFTLEG    6
#define HITGROUP_RIGHTLEG    7
#define HITGROUP_GEAR        10

inline bool CGameTrace::DidHitWorld() const
{
	return m_pEnt->GetIndex() == 0;
}

inline bool CGameTrace::DidHitNonWorldEntity() const
{
	return m_pEnt != nullptr && !DidHitWorld();
}

bool HandleBulletPenetration(CSWeaponInfo* wpn_data, FireBulletData& data);

float GetHitgroupDamageMult(int iHitGroup)
{
	switch (iHitGroup)
	{
	case HITGROUP_GENERIC:
		return 1.f;
	case HITGROUP_HEAD:
		return 4.f;
	case HITGROUP_CHEST:
		return 1.f;
	case HITGROUP_STOMACH:
		return 1.25f;
	case HITGROUP_LEFTARM:
		return 1.f;
	case HITGROUP_RIGHTARM:
		return 1.f;
	case HITGROUP_LEFTLEG:
		return 0.75f;
	case HITGROUP_RIGHTLEG:
		return 0.75f;
	case HITGROUP_GEAR:
		return 1.f;
	default:
		break;
	}

	return 1.f;
}

bool IsArmored(C_BaseEntity* Entity, int ArmorValue, int Hitgroup)
{
	bool result = false;

	if (ArmorValue > 0)
	{
		switch (Hitgroup)
		{
		case HITGROUP_GENERIC:
		case HITGROUP_CHEST:
		case HITGROUP_STOMACH:
		case HITGROUP_LEFTARM:
		case HITGROUP_RIGHTARM:
			result = true;
			break;
		case HITGROUP_HEAD:
			result = Entity->HasHelmet(); // DT_CSPlayer -> m_bHasHelmet
			break;
		}
	}

	return result;
}

void ScaleDamage(int Hitgroup, C_BaseEntity* Entity, float WeaponArmorRatio, float& Damage)
{
	// NOTE: the Guardian/Coop Missions/Gamemode have bots with heavy armor which has a less damage modifier
	auto HeavyArmor = Entity->m_bHasHeavyArmor(); // DT_CSPlayer -> m_bHasHeavyArmor
	auto ArmorValue = Entity->ArmorValue(); // DT_CSPlayer -> m_ArmorValue

	switch (Hitgroup)
	{
	case HITGROUP_HEAD:
		if (HeavyArmor)
			Damage = (Damage * 4.f) * 0.5f;
		else
			Damage *= 4.f;
		break;
	case HITGROUP_STOMACH:
		Damage *= 1.25f;
		break;
	case HITGROUP_LEFTLEG:
	case HITGROUP_RIGHTLEG:
		Damage *= 0.75f;
		break;
	}

	if (IsArmored(Entity, ArmorValue, Hitgroup))
	{
		float v47 = 1.f, ArmorBonusRatio = 0.5f, ArmorRatio = WeaponArmorRatio * 0.5f;

		if (HeavyArmor)
		{
			ArmorBonusRatio = 0.33f;
			ArmorRatio = (WeaponArmorRatio * 0.5f) * 0.5f;
			v47 = 0.33f;
		}

		auto NewDamage = Damage * ArmorRatio;

		if (HeavyArmor)
			NewDamage *= 0.85f;

		if (((Damage - (Damage * ArmorRatio)) * (v47 * ArmorBonusRatio)) > ArmorValue)
			NewDamage = Damage - (ArmorValue / ArmorBonusRatio);

		Damage = NewDamage;
	}
}

/*void ScaleDamage(int hitgroup, C_BaseEntity *enemy, float weapon_armor_ratio, float &current_damage)
{
	current_damage *= GetHitgroupDamageMult(hitgroup);

	if (enemy->ArmorValue() > 0)
	{
		if (hitgroup == HITGROUP_HEAD)
		{
			if (enemy->HasHelmet())
			{
				current_damage *= (weapon_armor_ratio * 0.25f);
			}
		}
		else
		{
			current_damage *= (weapon_armor_ratio * 0.5f);
		}
	}

}*/

bool SimulateFireBullet(C_BaseEntity* entity, C_BaseEntity* local, CBaseCombatWeapon* weapon, FireBulletData& data)
{
	//Utils::ToLog("SimulateFireBullet");
	data.penetrate_count = 4;
	data.trace_length = 0.0f;
	auto* wpn_data = weapon->GetCSWpnData();

	data.current_damage = static_cast<float>(wpn_data->m_iDamage);

	while ((data.penetrate_count > 0) && (data.current_damage >= 1.0f))
	{
		data.trace_length_remaining = wpn_data->m_fRange - data.trace_length;

		Vector end = data.src + data.direction * data.trace_length_remaining;

		UTIL_TraceLine(data.src, end, 0x4600400B, local, 0, &data.enter_trace);
		UTIL_ClipTraceToPlayers(entity, data.src, end + data.direction * 40.f, 0x4600400B, &data.filter, &data.enter_trace);

		if (data.enter_trace.fraction == 1.0f)
			break;

		if ((data.enter_trace.hitgroup <= 7)
			&& (data.enter_trace.hitgroup > 0)
			&& (local->GetTeamNum() != data.enter_trace.m_pEnt->GetTeamNum()
				|| g_Options.Ragebot.FriendlyFire))
		{
			data.trace_length += (float)(data.enter_trace.fraction * data.trace_length_remaining);
			data.current_damage *= (float)(pow(wpn_data->m_fRangeModifier, data.trace_length * 0.002));
			ScaleDamage(data.enter_trace.hitgroup, data.enter_trace.m_pEnt, wpn_data->m_fArmorRatio, data.current_damage);

			return true;
		}

		if (!HandleBulletPenetration(wpn_data, data))
			break;
	}

	return false;
}

bool HandleBulletPenetration(CSWeaponInfo* wpn_data, FireBulletData& data)
{
	surfacedata_t* enter_surface_data = g_PhysProps->GetSurfaceData(data.enter_trace.surface.surfaceProps);
	int enter_material = enter_surface_data->game.material;
	float enter_surf_penetration_mod = enter_surface_data->game.flPenetrationModifier;


	data.trace_length += data.enter_trace.fraction * data.trace_length_remaining;
	data.current_damage *= (float)(pow(wpn_data->m_fRangeModifier, (data.trace_length * 0.002)));

	if ((data.trace_length > 3000.f) || (enter_surf_penetration_mod < 0.1f))
		data.penetrate_count = 0;

	if (data.penetrate_count <= 0)
		return false;

	Vector dummy;
	trace_t trace_exit;
	if (!TraceToExit(dummy, data.enter_trace, data.enter_trace.endpos, data.direction, &trace_exit))
		return false;

	surfacedata_t* exit_surface_data = g_PhysProps->GetSurfaceData(trace_exit.surface.surfaceProps);
	int exit_material = exit_surface_data->game.material;

	float exit_surf_penetration_mod = exit_surface_data->game.flPenetrationModifier;
	float final_damage_modifier = 0.16f;
	float combined_penetration_modifier = 0.0f;

	if (((data.enter_trace.contents & CONTENTS_GRATE) != 0) || (enter_material == 89) || (enter_material == 71))
	{
		combined_penetration_modifier = 3.0f;
		final_damage_modifier = 0.05f;
	}
	else
	{
		combined_penetration_modifier = (enter_surf_penetration_mod + exit_surf_penetration_mod) * 0.5f;
	}

	if (enter_material == exit_material)
	{
		if (exit_material == 87 || exit_material == 85)
			combined_penetration_modifier = 3.0f;
		else if (exit_material == 76)
			combined_penetration_modifier = 2.0f;
	}

	float v34 = fmaxf(0.f, 1.0f / combined_penetration_modifier);
	float v35 = (data.current_damage * final_damage_modifier) + v34 * 3.0f * fmaxf(
		0.0f, (3.0f / wpn_data->m_fPenetration) * 1.25f);
	float thickness = VectorLength(trace_exit.endpos - data.enter_trace.endpos);

	thickness *= thickness;
	thickness *= v34;
	thickness /= 24.0f;


	float lost_damage = fmaxf(0.0f, v35 + thickness);

	if (lost_damage > data.current_damage)
		return false;

	if (lost_damage >= 0.0f)
		data.current_damage -= lost_damage;

	if (data.current_damage < 1.0f)
		return false;

	data.src = trace_exit.endpos;
	data.penetrate_count--;

	return true;
}


/*
*    CanHit() - example of how to use the code
*     @in  point: target hitbox vector
*     @out damage_given: amount of damage the shot would do
*/
bool CanHit(C_BaseEntity* entity, const Vector& point, float* damage_given)
{
	//Utils::ToLog("CANHIT");
	auto* local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	auto data = FireBulletData(local->GetOrigin() + local->GetViewOffset());
	data.filter = CTraceFilter();
	data.filter.pSkip = local;

	Vector angles;
	//	CalcAngle(data.src, point, angles);
	VectorAngles(point - data.src, angles);
	AngleVectors(angles, &data.direction);
	VectorNormalize(data.direction);

	if (SimulateFireBullet(entity, local,
	                       reinterpret_cast<CBaseCombatWeapon*>(g_EntityList->GetClientEntityFromHandle(
		                       static_cast<HANDLE>(local->GetActiveWeaponHandle()))), data))
	{
		*damage_given = data.current_damage;
		//Utils::ToLog("CANHIT END");
		return true;
	}

	return false;
}

bool CanWallbang(int& dmg)
{
	auto* local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	CBaseCombatWeapon* weapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		(HANDLE)local->GetActiveWeaponHandle());
	if (!local)
		return false;
	FireBulletData data = FireBulletData(local->GetEyePosition());
	data.filter = CTraceFilter();
	data.filter.pSkip = local;
	auto flRange = weapon->GetCSWpnData()->m_fRange;
	Vector EyeAng;
	g_Engine->GetViewAngles(EyeAng);

	Vector dst, forward;

	AngleVectors(EyeAng, &forward);
	dst = data.src + (forward * 8000.f);

	Vector angles;
	CalcAngle(data.src, dst, angles);
	AngleVectors(angles, &data.direction);
	VectorNormalize(data.direction);


	if (!weapon)
		return false;

	data.penetrate_count = 1;

	CSWeaponInfo* weaponData = weapon->GetCSWpnData();

	if (!weaponData)
		return false;

	data.current_damage = (int)weaponData->m_iDamage;

	data.trace_length_remaining = weaponData->m_fRange;

	Vector end = data.src + data.direction * data.trace_length_remaining;

	UTIL_TraceLine(data.src, end, MASK_SHOT | CONTENTS_GRATE, local, 0, &data.enter_trace);

	if (data.enter_trace.fraction == 1.f)
		return false;

	if (HandleBulletPenetration(weaponData, data))
	{
		dmg = data.current_damage;
		return true;
	}

	return false;
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class fqqcfzi
{
public:
	int lnbdkbwjtwwmqq;
	bool lpfzedyafbn;
	fqqcfzi();
	void kdgmtptwrdqblof(string mzsjjkitgtbp, double fsiien, bool dhxydrizmnakp, int pszkdgrxwjvkon, bool pfsip,
	                     bool zanahtcuctd, double ogokyxqpk, int prguki, string pubpckp);
	void cuuhetdkdsbuhjhmnwfmul(int sobefwe);
	int bzefpqpksjodrwwgwazpqia(bool ipyydkfzteeogc, double tkfqbwmwlggmg, int rygsymixltzetqf, int sixpcmaysnhcqa,
	                            string qqhnmjotwgq, bool vdwiw, int wkqkzvimtqggdw, double gpcujk, int wbpbdrk);
	string usgnhgkniavbtkrqjm();
	void fynrcoeqhqnhzdtr(double wgncvigcmmi);

protected:
	int jorzdpkwbjaxg;
	bool oncmyx;
	bool obgdjssoof;
	int pvtzamqgivoz;

	void czjrsgvrvebpkb(int aihphnkipbnkp, int cmtappu, bool noozxremtdnk, string ewzuhxzupqgijzw, bool qugjbadohi,
	                    int ndjaupnzdsythi, double mhmmrsortaoant, int fmsoaua, double fcvmzhvqnzpc);
	double qrqytyiqcvorprnimg(string rizienmfghut, bool eurzuiaytv, double wdamlzo, double jughkhblihm, int enddif,
	                          int dwzyzsbt);
	double axbcdyudsjykxkjub(int kxvisnkobmqine);
	double nabxfjffpdstxaqgs(double mppatjbeq, double jyujppkoentrl, int seqsv, string egsjpfsldvvqdlj);
	double nqbifstsucbnkyradxrxvac(bool fosnkmgnjma, string tfowpgwoteowh, int eiuvjt, double rhjmzqtqxquap, bool owapqlly,
	                               int wjnhwaaz);
	void iaqauewknhfvmyktyifqge(bool uqcoblretlmu, bool hdpnecelwwsm, string uogautug, double wnlasbxx,
	                            bool cvshxiuxlcvktex, string yxormneq, int hwsjmiyujjje, double qukipevjxs,
	                            string ewdzvabwfmgchtf);
	bool janirxcvhsyqtxk();
	int vnsywaowbj(string ordkvuwpnkmdhv, int foxqiu, bool aadsxpy, int mhxkwlwpldps, double jkuulg, int fltglyper,
	               int ynwlnvxysif, double bpshveexu);
	int rladqmuccqazxsyghynxkib(string znhiosmhhzlz, int fqylqrwedypuu, bool qwyzovfnkdvv);
	double jyhqytuxmnpywuatmmjqce(double gakcubtwpzcq, double jxyqmbaas, string bylps, string uxkygdtdtkbeloc,
	                              string iogtzvfd, double mdilxxqtri, string kvdhpxhn);

private:
	bool lepdppuegp;

	bool gbwigdkixstmvt();
	string cimydzuglrdrdtiiboocbdrg();
	bool rabzwytsrwgc(string onjklwwpawv, bool jdwvtazmjzejqh, int hojjsbbivgcs, int ngxcggteg, string cskyp);
	int agjuhifefeliwraqhhdlb(int bsnggemgnloc, int qwdxcxhvlhdnjj, bool tpknnuvlpk, bool wjldfurlkg, double lslvoek,
	                          bool csbnmw, string gzlkpx, string pjevzmgxxsmn, int snkhv, bool gmdteunc);
};


bool fqqcfzi::gbwigdkixstmvt()
{
	double xglutyuxp = 68520;
	double cvjkrokjdztpuii = 22037;
	int jdwznzkbh = 1982;
	if (1982 == 1982)
	{
		int xjoubtlfmf;
		for (xjoubtlfmf = 25; xjoubtlfmf > 0; xjoubtlfmf--)
		{
		}
	}
	if (22037 == 22037)
	{
		int una;
		for (una = 82; una > 0; una--)
		{
		}
	}
	if (22037 != 22037)
	{
		int og;
		for (og = 24; og > 0; og--)
		{
			continue;
		}
	}
	if (1982 == 1982)
	{
		int rdmtwktww;
		for (rdmtwktww = 82; rdmtwktww > 0; rdmtwktww--)
		{
		}
	}
	if (22037 == 22037)
	{
		int vpruphkyuz;
		for (vpruphkyuz = 84; vpruphkyuz > 0; vpruphkyuz--)
		{
		}
	}
	return false;
}

string fqqcfzi::cimydzuglrdrdtiiboocbdrg()
{
	int cvihakkhyurjmjg = 418;
	double txvqbmqfygpbssw = 9717;
	string wqxapqkahrnsy = "klgpkyirpfmthoznsgcsqyhoqvoanizmerclbctedqsnqwqkscipofmmcfcrfvrjwhbjnhgkxjeabsithliq";
	double snwnnxgok = 11998;
	if (9717 == 9717)
	{
		int jioqb;
		for (jioqb = 93; jioqb > 0; jioqb--)
		{
		}
	}
	if (9717 != 9717)
	{
		int rmg;
		for (rmg = 96; rmg > 0; rmg--)
		{
			continue;
		}
	}
	if (string("klgpkyirpfmthoznsgcsqyhoqvoanizmerclbctedqsnqwqkscipofmmcfcrfvrjwhbjnhgkxjeabsithliq") == string(
		"klgpkyirpfmthoznsgcsqyhoqvoanizmerclbctedqsnqwqkscipofmmcfcrfvrjwhbjnhgkxjeabsithliq"))
	{
		int gjipfdgk;
		for (gjipfdgk = 34; gjipfdgk > 0; gjipfdgk--)
		{
		}
	}
	return string("elfmwbhqzsnrsta");
}

bool fqqcfzi::rabzwytsrwgc(string onjklwwpawv, bool jdwvtazmjzejqh, int hojjsbbivgcs, int ngxcggteg, string cskyp)
{
	double pimof = 515;
	int bumon = 4530;
	bool giidb = true;
	string hnajcvx = "xgxwizbudmjziwinyncdfiggdsgdpgqcptonxdfwzwvdzkabykrj";
	double ihvlqz = 50416;
	double upmuufmbkorzr = 26841;
	int xmmehmjimx = 1643;
	if (50416 == 50416)
	{
		int fsre;
		for (fsre = 56; fsre > 0; fsre--)
		{
		}
	}
	if (true == true)
	{
		int ltntvziyq;
		for (ltntvziyq = 53; ltntvziyq > 0; ltntvziyq--)
		{
		}
	}
	if (26841 == 26841)
	{
		int bgillurqk;
		for (bgillurqk = 23; bgillurqk > 0; bgillurqk--)
		{
		}
	}
	return false;
}

int fqqcfzi::agjuhifefeliwraqhhdlb(int bsnggemgnloc, int qwdxcxhvlhdnjj, bool tpknnuvlpk, bool wjldfurlkg,
                                   double lslvoek, bool csbnmw, string gzlkpx, string pjevzmgxxsmn, int snkhv,
                                   bool gmdteunc)
{
	string okhba = "tkbkhqayrlqjxqeuamcywrsmdpbwyzrcxhqncmjeueueqwugdqtzdgluvxnqfxtwnfixaot";
	int hbdngenphunc = 7659;
	int kxxjltawl = 3950;
	int tccquibyrkuocf = 8018;
	string kbzrlvemmyzhz = "wuolarqpvmbauagaqmyjpovhxylhxvuersovxnqnupsgfgtczxrfqyoadrwohtnsuixkhhppunukkcubbshrkfdye";
	bool nfgnp = false;
	string uecllmccprovkw = "rofoccitttkuodjrofvpuvctsftxedfs";
	string krbbayawowtbc = "earulqtmbjoelchbdrxasbbolbdsoqxccipemsayipmmknbxfdimcpdctrrmdhq";
	bool cpbrxwxu = true;
	bool rlpenb = false;
	return 4077;
}

void fqqcfzi::czjrsgvrvebpkb(int aihphnkipbnkp, int cmtappu, bool noozxremtdnk, string ewzuhxzupqgijzw, bool qugjbadohi,
                             int ndjaupnzdsythi, double mhmmrsortaoant, int fmsoaua, double fcvmzhvqnzpc)
{
	string ongxinywsjuq = "yzjkheieebbtaeajpisaaiaontdnjjkmcfdolybgkeudgnsuklwbthirwikydjtialhkwoitynwgrbucjojpwlm";
	string hdmnrr = "exswqjmvhstzvsuroecgseflbjwiablybaeupouxxhybooiosyensmikedeouvitkkeshfvupvgqm";
	if (string("exswqjmvhstzvsuroecgseflbjwiablybaeupouxxhybooiosyensmikedeouvitkkeshfvupvgqm") != string(
		"exswqjmvhstzvsuroecgseflbjwiablybaeupouxxhybooiosyensmikedeouvitkkeshfvupvgqm"))
	{
		int sfszfirq;
		for (sfszfirq = 12; sfszfirq > 0; sfszfirq--)
		{
		}
	}
	if (string("exswqjmvhstzvsuroecgseflbjwiablybaeupouxxhybooiosyensmikedeouvitkkeshfvupvgqm") != string(
		"exswqjmvhstzvsuroecgseflbjwiablybaeupouxxhybooiosyensmikedeouvitkkeshfvupvgqm"))
	{
		int sglvdc;
		for (sglvdc = 6; sglvdc > 0; sglvdc--)
		{
		}
	}
	if (string("exswqjmvhstzvsuroecgseflbjwiablybaeupouxxhybooiosyensmikedeouvitkkeshfvupvgqm") != string(
		"exswqjmvhstzvsuroecgseflbjwiablybaeupouxxhybooiosyensmikedeouvitkkeshfvupvgqm"))
	{
		int jydobxix;
		for (jydobxix = 73; jydobxix > 0; jydobxix--)
		{
		}
	}
}

double fqqcfzi::qrqytyiqcvorprnimg(string rizienmfghut, bool eurzuiaytv, double wdamlzo, double jughkhblihm, int enddif,
                                   int dwzyzsbt)
{
	string fxojwgbudll = "mkdcjtppbidlquxwwugxjvhsvtakhjaheqogfuhifcfso";
	int gyzszwh = 2815;
	string ybgjwnnjs = "lypnksffyro";
	int bylxwbm = 4157;
	if (4157 != 4157)
	{
		int jdf;
		for (jdf = 54; jdf > 0; jdf--)
		{
			continue;
		}
	}
	if (string("lypnksffyro") != string("lypnksffyro"))
	{
		int yfuiqhasl;
		for (yfuiqhasl = 20; yfuiqhasl > 0; yfuiqhasl--)
		{
		}
	}
	if (4157 == 4157)
	{
		int cmamyz;
		for (cmamyz = 20; cmamyz > 0; cmamyz--)
		{
		}
	}
	return 30994;
}

double fqqcfzi::axbcdyudsjykxkjub(int kxvisnkobmqine)
{
	bool rdmicaiundi = false;
	int mjwkgudcwowpt = 86;
	if (false != false)
	{
		int sxzmhp;
		for (sxzmhp = 34; sxzmhp > 0; sxzmhp--)
		{
			continue;
		}
	}
	if (86 != 86)
	{
		int ux;
		for (ux = 38; ux > 0; ux--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int pkriisqtl;
		for (pkriisqtl = 49; pkriisqtl > 0; pkriisqtl--)
		{
			continue;
		}
	}
	return 10180;
}

double fqqcfzi::nabxfjffpdstxaqgs(double mppatjbeq, double jyujppkoentrl, int seqsv, string egsjpfsldvvqdlj)
{
	bool pqwroujjlp = true;
	int yabakpatoqpawu = 690;
	double gwolplqvy = 13946;
	bool jxzvcmhmbzhiwj = true;
	int cfsfzjfczddb = 2128;
	bool yozdipvzkhcm = false;
	int aykulofvn = 103;
	if (true == true)
	{
		int fxkwxjxc;
		for (fxkwxjxc = 10; fxkwxjxc > 0; fxkwxjxc--)
		{
		}
	}
	if (103 == 103)
	{
		int kbrp;
		for (kbrp = 3; kbrp > 0; kbrp--)
		{
		}
	}
	if (690 != 690)
	{
		int ltneyehudb;
		for (ltneyehudb = 53; ltneyehudb > 0; ltneyehudb--)
		{
			continue;
		}
	}
	return 73248;
}

double fqqcfzi::nqbifstsucbnkyradxrxvac(bool fosnkmgnjma, string tfowpgwoteowh, int eiuvjt, double rhjmzqtqxquap,
                                        bool owapqlly, int wjnhwaaz)
{
	int maaiphynh = 6200;
	string nkiusiixqdo = "zhkxppvdesnvxztthx";
	bool pxvszkqv = true;
	string hnewq = "zyihtuxlbkjvvodjwannbvwjlkdwfhgkbfwqriuhcybompfdmintkfqcpzcftavvnbemczvdvbs";
	if (true == true)
	{
		int twvwkzz;
		for (twvwkzz = 96; twvwkzz > 0; twvwkzz--)
		{
		}
	}
	if (6200 == 6200)
	{
		int dkxdjpu;
		for (dkxdjpu = 13; dkxdjpu > 0; dkxdjpu--)
		{
		}
	}
	if (6200 != 6200)
	{
		int tuuogxewmf;
		for (tuuogxewmf = 47; tuuogxewmf > 0; tuuogxewmf--)
		{
			continue;
		}
	}
	return 40457;
}

void fqqcfzi::iaqauewknhfvmyktyifqge(bool uqcoblretlmu, bool hdpnecelwwsm, string uogautug, double wnlasbxx,
                                     bool cvshxiuxlcvktex, string yxormneq, int hwsjmiyujjje, double qukipevjxs,
                                     string ewdzvabwfmgchtf)
{
	double oyuwsrxvrbeuvrc = 17097;
	int bcfbkqmf = 7954;
	bool uyivg = false;
	int uzjorpvnkpie = 7926;
	int noiweedqoe = 287;
	double qanmrmjg = 59345;
	if (17097 == 17097)
	{
		int oqlixu;
		for (oqlixu = 72; oqlixu > 0; oqlixu--)
		{
		}
	}
	if (287 != 287)
	{
		int wcnac;
		for (wcnac = 65; wcnac > 0; wcnac--)
		{
			continue;
		}
	}
	if (17097 != 17097)
	{
		int zlgaela;
		for (zlgaela = 78; zlgaela > 0; zlgaela--)
		{
			continue;
		}
	}
	if (7926 != 7926)
	{
		int tkmtcew;
		for (tkmtcew = 58; tkmtcew > 0; tkmtcew--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int frdybwxe;
		for (frdybwxe = 64; frdybwxe > 0; frdybwxe--)
		{
		}
	}
}

bool fqqcfzi::janirxcvhsyqtxk()
{
	bool iyytptgglxyih = false;
	bool xxnhl = true;
	if (true != true)
	{
		int uislvh;
		for (uislvh = 4; uislvh > 0; uislvh--)
		{
			continue;
		}
	}
	return false;
}

int fqqcfzi::vnsywaowbj(string ordkvuwpnkmdhv, int foxqiu, bool aadsxpy, int mhxkwlwpldps, double jkuulg, int fltglyper,
                        int ynwlnvxysif, double bpshveexu)
{
	bool qnfcfgi = false;
	double paweewgybijb = 9696;
	if (false == false)
	{
		int nxa;
		for (nxa = 73; nxa > 0; nxa--)
		{
		}
	}
	if (false != false)
	{
		int iagmqig;
		for (iagmqig = 99; iagmqig > 0; iagmqig--)
		{
			continue;
		}
	}
	if (9696 != 9696)
	{
		int jvzzcppofn;
		for (jvzzcppofn = 95; jvzzcppofn > 0; jvzzcppofn--)
		{
			continue;
		}
	}
	if (9696 != 9696)
	{
		int aw;
		for (aw = 67; aw > 0; aw--)
		{
			continue;
		}
	}
	return 97275;
}

int fqqcfzi::rladqmuccqazxsyghynxkib(string znhiosmhhzlz, int fqylqrwedypuu, bool qwyzovfnkdvv)
{
	double sjujcthgqnxniki = 36209;
	bool jyklewfsdnsmwji = true;
	return 11837;
}

double fqqcfzi::jyhqytuxmnpywuatmmjqce(double gakcubtwpzcq, double jxyqmbaas, string bylps, string uxkygdtdtkbeloc,
                                       string iogtzvfd, double mdilxxqtri, string kvdhpxhn)
{
	int eikumdzh = 5706;
	bool zvaobu = false;
	int vakbjtruez = 652;
	if (652 != 652)
	{
		int vkomdsyfb;
		for (vkomdsyfb = 52; vkomdsyfb > 0; vkomdsyfb--)
		{
			continue;
		}
	}
	return 79499;
}

void fqqcfzi::kdgmtptwrdqblof(string mzsjjkitgtbp, double fsiien, bool dhxydrizmnakp, int pszkdgrxwjvkon, bool pfsip,
                              bool zanahtcuctd, double ogokyxqpk, int prguki, string pubpckp)
{
	int qtdqrzlzgcjoqe = 4472;
	double ovkdmzrnabssz = 5251;
	int dfugxhgxh = 2068;
	bool gzcnyfof = true;
	string paevugfajyoa =
		"izpmibohaxeabvetcajvrtyanqhtkzeobfaykuzbffhkegjxzrgozzufycsszbogbgmeqhjdrhvxyyhmiepfvqjwpdczbuxsnws";
	if (5251 != 5251)
	{
		int qcszypcpsn;
		for (qcszypcpsn = 58; qcszypcpsn > 0; qcszypcpsn--)
		{
			continue;
		}
	}
	if (5251 == 5251)
	{
		int rrlkdfyv;
		for (rrlkdfyv = 98; rrlkdfyv > 0; rrlkdfyv--)
		{
		}
	}
	if (2068 != 2068)
	{
		int czfikcs;
		for (czfikcs = 72; czfikcs > 0; czfikcs--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int wrjlhgpnx;
		for (wrjlhgpnx = 13; wrjlhgpnx > 0; wrjlhgpnx--)
		{
			continue;
		}
	}
}

void fqqcfzi::cuuhetdkdsbuhjhmnwfmul(int sobefwe)
{
	double zybcmfhwxmzjc = 14932;
	int qbwpnvudfyxcncl = 2846;
	string siorzuaofsg = "calikrnaduflse";
	int gymhoeikojm = 2062;
	string wxrqtwv = "fwvuctphgodtvsyyowumalnxkxyuvzawslajnxoklxihgdqgrdnutnebja";
	double mrziuasv = 88063;
	bool nakhimolp = false;
	if (2846 == 2846)
	{
		int cs;
		for (cs = 48; cs > 0; cs--)
		{
		}
	}
}

int fqqcfzi::bzefpqpksjodrwwgwazpqia(bool ipyydkfzteeogc, double tkfqbwmwlggmg, int rygsymixltzetqf, int sixpcmaysnhcqa,
                                     string qqhnmjotwgq, bool vdwiw, int wkqkzvimtqggdw, double gpcujk, int wbpbdrk)
{
	int ilrtardtmj = 2983;
	double dwpbhkl = 19115;
	string agotpd = "ysenoiokcxsdgyrajnoggyzghwypteupsphfrccbykvgm";
	bool bfubjtpeyovjqzw = false;
	int aoastndieig = 2177;
	int vuyecrk = 543;
	return 58757;
}

string fqqcfzi::usgnhgkniavbtkrqjm()
{
	string ejyfudrwi = "iomlmfqnosigfgfftkpp";
	string ggqoygeyqnam = "itqqkglbywpnnjsfzfyxfeqqwenwnodykilcanw";
	double sqfslpcqpe = 77627;
	string bfzih = "hcrboqlwdjiylmqcyzixxgfewnvwdwmrnegtznrpmzflcjguockbgvkfqelbzumszzyvrqjnhkuozxemio";
	double ntjmlf = 61287;
	if (string("iomlmfqnosigfgfftkpp") != string("iomlmfqnosigfgfftkpp"))
	{
		int mkhkymfko;
		for (mkhkymfko = 0; mkhkymfko > 0; mkhkymfko--)
		{
		}
	}
	if (string("itqqkglbywpnnjsfzfyxfeqqwenwnodykilcanw") != string("itqqkglbywpnnjsfzfyxfeqqwenwnodykilcanw"))
	{
		int rdkw;
		for (rdkw = 16; rdkw > 0; rdkw--)
		{
		}
	}
	if (61287 != 61287)
	{
		int walg;
		for (walg = 22; walg > 0; walg--)
		{
			continue;
		}
	}
	return string("anqpdzwpikv");
}

void fqqcfzi::fynrcoeqhqnhzdtr(double wgncvigcmmi)
{
	double yswsvm = 56548;
	int dxkqtairp = 1824;
	double coqfgucc = 41257;
	string blmmy = "kolubqnpafkedfbuethvyxqbyrrbcaoxxizhtbltxwehmi";
	bool srmalghntlelqu = true;
	if (56548 == 56548)
	{
		int flamc;
		for (flamc = 46; flamc > 0; flamc--)
		{
		}
	}
	if (true != true)
	{
		int dzkvi;
		for (dzkvi = 34; dzkvi > 0; dzkvi--)
		{
			continue;
		}
	}
	if (string("kolubqnpafkedfbuethvyxqbyrrbcaoxxizhtbltxwehmi") == string(
		"kolubqnpafkedfbuethvyxqbyrrbcaoxxizhtbltxwehmi"))
	{
		int mhseess;
		for (mhseess = 58; mhseess > 0; mhseess--)
		{
		}
	}
}

fqqcfzi::fqqcfzi()
{
	this->kdgmtptwrdqblof(string("zgdmdvkcsrlradszspjgrpsfxkpnzgdmzu"), 4007, true, 624, true, false, 60296, 219,
	                      string("qgysdinvarfhfuktfkmcbyinoprcaecadnpirxruwdgjmcarvnfgzrpslq"));
	this->cuuhetdkdsbuhjhmnwfmul(3578);
	this->bzefpqpksjodrwwgwazpqia(true, 46433, 5637, 5321, string("aalwwjtnsgifituhqlrebskc"), true, 3828, 28801, 3337);
	this->usgnhgkniavbtkrqjm();
	this->fynrcoeqhqnhzdtr(32521);
	this->czjrsgvrvebpkb(621, 2485, false, string("yzjuiadcjety"), false, 2617, 49486, 893, 13952);
	this->qrqytyiqcvorprnimg(string("fhjtjazcbbdqsutphjbyywkjpojiuqboiflynidjgkgcmtxnmymuh"), false, 31707, 7718, 1819,
	                         1562);
	this->axbcdyudsjykxkjub(44);
	this->nabxfjffpdstxaqgs(41539, 5788, 4, string("apjjlxnlexqhoscjddfynueksoevthkmfinlmjpdr"));
	this->nqbifstsucbnkyradxrxvac(true, string("lpxvzpnlsqxnzvzbvpdybsaoxjfbcqvvxvuaaalngnelrelvt"), 2801, 3548, false,
	                              1082);
	this->iaqauewknhfvmyktyifqge(
		true, true,
		string("uuvoazhikalxcvureqgsgzricqudtmphlagimgqwletgtgbpdsygdlwylwbyzgzcoyhdtioxnxvtcaqatgikphkpvbztiabg"), 7977,
		true, string("dpoegjqubdsickxamsmikzschzfcjaqaeypujxshhrfqhpyuvlcrocnramiozidopjwzciindjpiy"), 1509, 9580,
		string("ymaugrkmquulvsydipzcfduxmriwmlonkdncxayqoon"));
	this->janirxcvhsyqtxk();
	this->vnsywaowbj(string("jxzpgstdebj"), 1866, true, 2051, 9592, 5767, 1336, 7155);
	this->rladqmuccqazxsyghynxkib(string("zobdmauqngktphdmzpmgbfrwi"), 3867, true);
	this->jyhqytuxmnpywuatmmjqce(20797, 67588,
	                             string(
		                             "fnpisriupftqmwwctpjouvvbgmapvarrfucrjljbnbmmyvaofsaacigduihbzozajcmaxzetpqdtapksfga"),
	                             string("aiheufkvtxtsxgcyeukcnpbsdaoaiprwwzjyj"), string("main"), 20729,
	                             string("harxdooxtnwfawfmawelvslvfkbfrhmghezvwtenbfkrfhjriiwklhwaujvega"));
	this->gbwigdkixstmvt();
	this->cimydzuglrdrdtiiboocbdrg();
	this->rabzwytsrwgc(string("wazdcehlaqdvwrzhlcmocsgwlrxxqsqbjsthec"), false, 2119, 1112,
	                   string(
		                   "teavcrimfvxcnidphkfzwuzyayiycffwkaxicmdsxwegcozyjihiblzippgyoifqzylnevvuwvjqzufzxmxhfzyrkwtzaghfe"));
	this->agjuhifefeliwraqhhdlb(2048, 545, false, false, 34524, false,
	                            string(
		                            "zybthffmgznqowjevralfkgaskhqojivlvwecezlpgvieuloobqkwaolvjqmeewejgfeyjbvaroxjuqzxjyh"),
	                            string(
		                            "cjcdfxtawynjcnsudadmblptxmhwhvsiajgpmlxlyvcvqobvstdtkuyylzfswnsjrcuffheylhhdqaiwsq"),
	                            5642, false);
}
