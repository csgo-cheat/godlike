#include "RageBot.h"
#include "Render.h"
#include "Autowall.h"
#include <iostream>
#include "MathFunctions.h"
#include "SDK.h"
#include "EnginePrediction.h"
#include "LagComp.h"
//#include "Resolver.h"

using namespace std;

#define TICK_INTERVAL			(g_Globals->interval_per_tick)
#define TICKS_TO_TIME( t )		( g_Globals->interval_per_tick *( t ) )
#define TIME_TO_TICKS( dt )		( (int)( 0.5f + (float)(dt) / TICK_INTERVAL ) )

ragebot::ragebot()
{
	IsAimStepping = false;
	IsLocked = false;
	TargetID = -1;
	pTarget = nullptr;
}

void ragebot::OnCreateMove(CInput::CUserCmd* pCmd, bool& bSendPacket)
{
	bool storedHitscan = g_Options.Ragebot.Hitscan;

	if (!g_Options.Ragebot.MainSwitch)
		return;

	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	if (pLocal && pLocal->IsAlive())
	{
		if (g_Options.Ragebot.BAIMkey && G::PressedKeys[g_Options.Ragebot.BAIMkey] && g_Options.Ragebot.Hitscan != 4)
		{
			g_Options.Ragebot.Hitscan = 4;
		}
		else if (g_Options.Ragebot.BAIMkey && !G::PressedKeys[g_Options.Ragebot.BAIMkey] && g_Options.Ragebot.Hitscan != 3)
		{
			g_Options.Ragebot.Hitscan = storedHitscan;
		}

		CBaseCombatWeapon* weapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
			pLocal->GetActiveWeaponHandle());
		if (weapon && weapon->m_AttributeManager()->m_Item()->GetItemDefinitionIndex() == 64)
		{
			if (!CanAttack() && weapon->ammo() > 0)
			{
				pCmd->buttons |= IN_ATTACK;
			}
		}


		if (g_Options.Ragebot.Enabled)
			DoAimbot(pCmd, bSendPacket);

		if (g_Options.Ragebot.AntiRecoil)
			DoNoRecoil(pCmd);

		if (g_Options.Ragebot.EnabledAntiAim)
			DoAntiAim(pCmd, bSendPacket);

		if (g_Options.Ragebot.FakeLag)
			FakeLag(pCmd, bSendPacket);

		if (g_Options.Ragebot.AimStep)
		{
			Vector AddAngs = pCmd->viewangles - LastAngle;
			if (AddAngs.Length2D() > 25.f)
			{
				Normalize(AddAngs, AddAngs);
				AddAngs *= 25;
				pCmd->viewangles = LastAngle + AddAngs;
				MiscFunctions::NormaliseViewAngle(pCmd->viewangles);
			}
		}
	}

	LastAngle = pCmd->viewangles;
}

bool ragebot::hit_chance(C_BaseEntity* local, CInput::CUserCmd* cmd, CBaseCombatWeapon* weapon, C_BaseEntity* target)
{
	Vector forward, right, up;

	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());

	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		pLocal->GetActiveWeaponHandle());

	constexpr auto max_traces = 256;

	AngleVectors(cmd->viewangles, &forward, &right, &up);

	int total_hits = 0;

	int needed_hits;

	if (MiscFunctions::IsSniper(pWeapon))
	{
		needed_hits = static_cast<int>(max_traces * (g_Options.Ragebot.HitchanceSniper / 100.f));
	}
	else if (MiscFunctions::IsPistol(pWeapon))
	{
		needed_hits = static_cast<int>(max_traces * (g_Options.Ragebot.HitchancePistol / 100.f));
	}
	else if (MiscFunctions::IsHeavy(pWeapon))
	{
		needed_hits = static_cast<int>(max_traces * (g_Options.Ragebot.HitchanceHeavy / 100.f));
	}
	else if (MiscFunctions::IsSmg(pWeapon))
	{
		needed_hits = static_cast<int>(max_traces * (g_Options.Ragebot.HitchanceSmgs / 100.f));
	}
	else if (MiscFunctions::IsRifle(pWeapon))
	{
		needed_hits = static_cast<int>(max_traces * (g_Options.Ragebot.HitchanceRifle / 100.f));
	}
	else if (MiscFunctions::IsRevolver(pWeapon))
	{
		needed_hits = static_cast<int>(max_traces * (g_Options.Ragebot.HitchanceRevolver / 100.f));
	}

	weapon->UpdateAccuracyPenalty(weapon);

	auto eyes = local->GetEyePosition();
	auto flRange = weapon->GetCSWpnData()->m_fRange;

	for (int i = 0; i < max_traces; i++)
	{
		RandomSeed(i + 1);

		float fRand1 = RandomFloat(0.f, 1.f);
		float fRandPi1 = RandomFloat(0.f, XM_2PI);
		float fRand2 = RandomFloat(0.f, 1.f);
		float fRandPi2 = RandomFloat(0.f, XM_2PI);

		float fRandInaccuracy = fRand1 * weapon->GetInaccuracy();
		float fRandSpread = fRand2 * weapon->GetSpread();

		float fSpreadX = cos(fRandPi1) * fRandInaccuracy + cos(fRandPi2) * fRandSpread;
		float fSpreadY = sin(fRandPi1) * fRandInaccuracy + sin(fRandPi2) * fRandSpread;

		auto viewSpreadForward = (forward + fSpreadX * right + fSpreadY * up).Normalized();

		Vector viewAnglesSpread;
		VectorAngles(viewSpreadForward, viewAnglesSpread);
		MiscFunctions::NormaliseViewAngle(viewAnglesSpread);

		Vector viewForward;
		AngleVectors(viewAnglesSpread, &viewForward);
		viewForward.NormalizeInPlace();

		viewForward = eyes + (viewForward * flRange);

		trace_t tr;
		Ray_t ray;
		ray.Init(eyes, viewForward);

		g_EngineTrace->ClipRayToEntity(ray, MASK_SHOT | CONTENTS_GRATE, target, &tr);


		if (tr.m_pEnt == target)
			total_hits++;

		if (total_hits >= needed_hits)
			return true;

		if ((max_traces - i + total_hits) < needed_hits)
			return false;
	}

	return false;
}

template <class T, class U>
T clamp(T in, U low, U high)
{
	if (in <= low)
		return low;

	if (in >= high)
		return high;

	return in;
}

void ragebot::DoAimbot(CInput::CUserCmd* pCmd, bool& bSendPacket)
{
	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	bool FindNewTarget = true;
	//IsLocked = false;

	// Don't aimbot with the knife..
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		pLocal->GetActiveWeaponHandle());

	if (pWeapon != nullptr)
	{
		if (pWeapon->ammo() == 0 || MiscFunctions::IsKnife(pWeapon) || MiscFunctions::IsGrenade(pWeapon))
		{
			//TargetID = 0;
			//pTarget = nullptr;
			//HitBox = -1;
			return;
		}
	}
	else
		return;

	// Make sure we have a good target
	if (IsLocked && TargetID >= 0 && HitBox >= 0)
	{
		pTarget = g_EntityList->GetClientEntity(TargetID);
		if (pTarget && TargetMeetsRequirements(pTarget))
		{
			HitBox = HitScan(pTarget);
			if (HitBox >= 0)
			{
				Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
				Vector View;
				g_Engine->GetViewAngles(View);
				float FoV = FovToPlayer(ViewOffset, View, pTarget, HitBox);
				if (FoV < g_Options.Ragebot.FOV)
					FindNewTarget = false;
			}
		}
	}


	// Find a new target, apparently we need to
	if (FindNewTarget)
	{
		Globals::Shots = 0;
		TargetID = 0;
		pTarget = nullptr;
		HitBox = -1;


		TargetID = GetTargetCrosshair();


		// Memesj
		if (TargetID >= 0)
		{
			pTarget = g_EntityList->GetClientEntity(TargetID);
		}
	}

	if (TargetID >= 0 && pTarget)
	{
		HitBox = HitScan(pTarget);

		// Key
		if (g_Options.Ragebot.KeyPress)
		{
			if (g_Options.Ragebot.KeyPress > 0 && !G::PressedKeys[g_Options.Ragebot.KeyPress])
			{
				TargetID = -1;
				pTarget = nullptr;
				HitBox = -1;
				return;
			}
		}


		Globals::AimPoint = GetHitboxPosition(pTarget, HitBox);

		if (AimAtPoint(pLocal, Globals::AimPoint, pCmd, bSendPacket))
		{
			if (g_Options.Ragebot.AutoFire && CanAttack() && MiscFunctions::IsSniper(pWeapon) && g_Options.Ragebot.AutoScope)
			{
				if (pLocal->IsScoped()) if (!g_Options.Ragebot.Hitchance || hit_chance(pLocal, pCmd, pWeapon, pTarget)) pCmd->
					buttons |= IN_ATTACK;
				if (!pLocal->IsScoped()) pCmd->buttons |= IN_ATTACK2;
			}
			if (g_Options.Ragebot.AutoFire && CanAttack() && !(MiscFunctions::IsSniper(pWeapon)))
			{
				if (!g_Options.Ragebot.Hitchance || hit_chance(pLocal, pCmd, pWeapon, pTarget)) pCmd->buttons |= IN_ATTACK;
			}
			if (g_Options.Ragebot.AutoFire && CanAttack() && (MiscFunctions::IsSniper(pWeapon)) && !g_Options.Ragebot.AutoScope)
			{
				if (!g_Options.Ragebot.Hitchance || hit_chance(pLocal, pCmd, pWeapon, pTarget)) if (pLocal->IsScoped()) pCmd->
					buttons |= IN_ATTACK;
			}

			//if (CanAttack() && pCmd->buttons & IN_ATTACK)
			//	Globals::Shots += 1;
		}

		//Calculate shot fired
		if (pWeapon)
		{
			if (!(pCmd->buttons & IN_ATTACK) && pWeapon->GetNextPrimaryAttack() <= (pLocal->GetTickBase() * g_Globals->
				interval_per_tick))
			{
				Globals::Shots = 0;
				Globals::missedshots = 0;
			}
			else
			{
				Globals::Shots += pLocal->m_iShotsFired();
			}
		}


		if (g_Options.Ragebot.AutoStop)
		{
			pCmd->forwardmove = 0.f;
			pCmd->sidemove = 0.f;
		}


		if (g_Options.Ragebot.AutoCrouch)
		{
			pCmd->buttons |= IN_DUCK;
		}
	}

	// Auto Pistol
	static bool WasFiring = false;
	if (pWeapon != nullptr)
	{
		if (MiscFunctions::IsPistol(pWeapon) && g_Options.Ragebot.AutoPistol && pWeapon
		                                                                        ->m_AttributeManager()->m_Item()->
		                                                                        GetItemDefinitionIndex() != 64)
		{
			if (pCmd->buttons & IN_ATTACK && !MiscFunctions::IsKnife(pWeapon) && !MiscFunctions::IsGrenade(pWeapon))
			{
				if (WasFiring)
				{
					pCmd->buttons &= ~IN_ATTACK;
				}
			}

			WasFiring = pCmd->buttons & IN_ATTACK ? true : false;
		}
	}
}


bool ragebot::TargetMeetsRequirements(C_BaseEntity* pEntity)
{
	auto local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	// Is a valid player
	if (pEntity && pEntity->IsDormant() == false && pEntity->IsAlive() && pEntity->GetIndex() != local->GetIndex())
	{
		// Entity Type checks
		ClientClass* pClientClass = pEntity->GetClientClass();
		player_info_t pinfo;
		if (pClientClass->m_ClassID == (int)ClassID::CCSPlayer && g_Engine->GetPlayerInfo(pEntity->GetIndex(), &pinfo))
		{
			// Team Check
			if (pEntity->GetTeamNum() != local->GetTeamNum() || g_Options.Ragebot.FriendlyFire)
			{
				// Spawn Check
				if (!pEntity->HasGunGameImmunity())
				{
					return true;
				}
			}
		}
	}

	// They must have failed a requirement
	return false;
}


float ragebot::FovToPlayer(Vector ViewOffSet, Vector View, C_BaseEntity* pEntity, int aHitBox)
{
	// Anything past 180 degrees is just going to wrap around
	CONST FLOAT MaxDegrees = 180.0f;

	// Get local angles
	Vector Angles = View;

	// Get local view / eye position
	Vector Origin = ViewOffSet;

	// Create and intiialize vectors for calculations below
	Vector Delta(0, 0, 0);
	//Vector Origin(0, 0, 0);
	Vector Forward(0, 0, 0);

	// Convert angles to normalized directional forward vector
	AngleVectors(Angles, &Forward);
	Vector AimPos = GetHitboxPosition(pEntity, aHitBox); //pvs fix disabled
	// Get delta vector between our local eye position and passed vector
	VectorSubtract(AimPos, Origin, Delta);
	//Delta = AimPos - Origin;

	// Normalize our delta vector
	Normalize(Delta, Delta);

	// Get dot product between delta position and directional forward vectors
	FLOAT DotProduct = Forward.Dot(Delta);

	// Time to calculate the field of view
	return (acos(DotProduct) * (MaxDegrees / PI));
}

int ragebot::GetTargetCrosshair()
{
	// Target selection
	int target = -1;
	float minFoV = g_Options.Ragebot.FOV;

	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector View;
	g_Engine->GetViewAngles(View);

	for (int i = 0; i < g_EntityList->GetHighestEntityIndex(); i++)
	{
		C_BaseEntity* pEntity = g_EntityList->GetClientEntity(i);

		if (TargetMeetsRequirements(pEntity))
		{
			int NewHitBox = HitScan(pEntity);
			if (NewHitBox >= 0)
			{
				float fov = FovToPlayer(ViewOffset, View, pEntity, 0);
				if (fov < minFoV)
				{
					minFoV = fov;
					target = i;
				}
			}
		}
	}

	return target;
}

int ragebot::HitScan(C_BaseEntity* pEntity)
{
	vector<int> HitBoxesToScan{Head, Neck, Chest, Stomach};

	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());

	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		pLocal->GetActiveWeaponHandle());

	int HitScanMode = g_Options.Ragebot.Hitscan;

	if (!g_Options.Ragebot.Hitscan)
	{
		switch (g_Options.Ragebot.Hitbox)
		{
			//Head
		case 0:
			HitBoxesToScan.push_back(Head);
			break;
			//Neck
		case 1:
			HitBoxesToScan.push_back(Neck);
			break;
			//Chest
		case 2:
			HitBoxesToScan.push_back(Chest);
			break;
			//Stomach
		case 3:
			HitBoxesToScan.push_back(Stomach);
			break;
		}
	}
	else
	{
		switch (HitScanMode)
		{
		case 1:
			// low
			HitBoxesToScan.push_back(Head);
			HitBoxesToScan.push_back(Neck);
			HitBoxesToScan.push_back(UpperChest);
			HitBoxesToScan.push_back(Chest);
			HitBoxesToScan.push_back(Stomach);
			HitBoxesToScan.push_back(Pelvis);
			break;
		case 2:
			// medium
			HitBoxesToScan.push_back(Head);
			HitBoxesToScan.push_back(Neck);
			HitBoxesToScan.push_back(UpperChest);
			HitBoxesToScan.push_back(Chest);
			HitBoxesToScan.push_back(Stomach);
			HitBoxesToScan.push_back(Pelvis);
			HitBoxesToScan.push_back(LeftUpperArm);
			HitBoxesToScan.push_back(RightUpperArm);
			HitBoxesToScan.push_back(LeftThigh);
			HitBoxesToScan.push_back(RightThigh);
			break;
		case 3:
			// high
			HitBoxesToScan.push_back(Head);
			HitBoxesToScan.push_back(Neck);
			HitBoxesToScan.push_back(UpperChest);
			HitBoxesToScan.push_back(Chest);
			HitBoxesToScan.push_back(Stomach);
			HitBoxesToScan.push_back(Pelvis);
			HitBoxesToScan.push_back(LeftThigh);
			HitBoxesToScan.push_back(RightThigh);
			HitBoxesToScan.push_back(LeftFoot);
			HitBoxesToScan.push_back(RightFoot);
			HitBoxesToScan.push_back(LeftShin);
			HitBoxesToScan.push_back(RightShin);
			HitBoxesToScan.push_back(LeftLowerArm);
			HitBoxesToScan.push_back(RightLowerArm);
			HitBoxesToScan.push_back(LeftUpperArm);
			HitBoxesToScan.push_back(RightUpperArm);
			HitBoxesToScan.push_back(LeftHand);
			HitBoxesToScan.push_back(RightHand);
			break;
		case 4:
			// baim
			HitBoxesToScan.push_back(UpperChest);
			HitBoxesToScan.push_back(Chest);
			HitBoxesToScan.push_back(Stomach);
			HitBoxesToScan.push_back(Pelvis);
			HitBoxesToScan.push_back(LeftUpperArm);
			HitBoxesToScan.push_back(RightUpperArm);
			HitBoxesToScan.push_back(LeftThigh);
			HitBoxesToScan.push_back(RightThigh);
			HitBoxesToScan.push_back(LeftHand);
			HitBoxesToScan.push_back(RightHand);
			HitBoxesToScan.push_back(LeftFoot);
			HitBoxesToScan.push_back(RightFoot);
			HitBoxesToScan.push_back(LeftShin);
			HitBoxesToScan.push_back(RightShin);
			HitBoxesToScan.push_back(LeftLowerArm);
			HitBoxesToScan.push_back(RightLowerArm);
			break;
		}
	}
	static vector<int> baim{
		UpperChest, Chest, Stomach, Pelvis, LeftUpperArm, RightUpperArm, LeftThigh, RightThigh, LeftHand, RightHand, LeftFoot,
		RightFoot, LeftShin, RightShin, LeftLowerArm, RightLowerArm
	};

	int bestHitbox = -1;
	float highestDamage;

	if (MiscFunctions::IsSniper(pWeapon))
	{
		highestDamage = g_Options.Ragebot.MinimumDamageSniper;
	}
	else if (MiscFunctions::IsPistol(pWeapon))
	{
		highestDamage = g_Options.Ragebot.MinimumDamagePistol;
	}
	else if (MiscFunctions::IsHeavy(pWeapon))
	{
		highestDamage = g_Options.Ragebot.MinimumDamageHeavy;
	}
	else if (MiscFunctions::IsSmg(pWeapon))
	{
		highestDamage = g_Options.Ragebot.MinimumDamageSmg;
	}
	else if (MiscFunctions::IsRifle(pWeapon))
	{
		highestDamage = g_Options.Ragebot.MinimumDamageRifle;
	}
	else if (MiscFunctions::IsRevolver(pWeapon))
	{
		highestDamage = g_Options.Ragebot.MinimumDamageRevolver;
	}

	for (auto HitBoxID : HitBoxesToScan)
	{
		Vector Point = GetHitboxPosition(pEntity, HitBoxID); //pvs fix disabled

		float damage = 0.0f;
		if (CanHit(pEntity, Point, &damage))
		{
			if (damage > highestDamage || damage > pEntity->GetHealth())
			{
				bestHitbox = HitBoxID;
				highestDamage = damage;
			}
		}
	}
	/*for (auto HitBoxID : baim)
	{

	Vector Point = GetHitboxPosition(pEntity, HitBoxID); //pvs fix disabled

	float damage = 0.0f;
	if (CanHit(pEntity, Point, &damage))
	{
	if (damage > highestDamage && damage > pEntity->GetHealth())
	{
	bestHitbox = HitBoxID;
	highestDamage = damage;
	}
	}
	}*/
	return bestHitbox;
}


void ragebot::DoNoRecoil(CInput::CUserCmd* pCmd)
{
	// Ghetto rcs shit, implement properly later
	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	if (pLocal != nullptr)
	{
		Vector AimPunch = pLocal->localPlayerExclusive()->GetAimPunchAngle();
		if (AimPunch.Length2D() > 0 && AimPunch.Length2D() < 150)
		{
			pCmd->viewangles -= AimPunch * 2;
			MiscFunctions::NormaliseViewAngle(pCmd->viewangles);
		}
	}
}

float FovToPoint(Vector ViewOffSet, Vector View, Vector Point)
{
	// Get local view / eye position
	Vector Origin = ViewOffSet;

	// Create and intiialize vectors for calculations below
	Vector Delta(0, 0, 0);
	Vector Forward(0, 0, 0);

	// Convert angles to normalized directional forward vector
	AngleVectors(View, &Forward);
	Vector AimPos = Point;

	// Get delta vector between our local eye position and passed vector
	Delta = AimPos - Origin;
	//Delta = AimPos - Origin;

	// Normalize our delta vector
	Normalize(Delta, Delta);

	// Get dot product between delta position and directional forward vectors
	FLOAT DotProduct = Forward.Dot(Delta);

	// Time to calculate the field of view
	return (acos(DotProduct) * (180.f / PI));
}

bool me123 = false;

bool ragebot::AimAtPoint(C_BaseEntity* pLocal, Vector point, CInput::CUserCmd* pCmd, bool& bSendPacket)
{
	bool ReturnValue = false;

	if (point.Length() == 0) return ReturnValue;

	Vector angles;
	Vector src = pLocal->GetOrigin() + pLocal->GetViewOffset();

	CalcAngle(src, point, angles);
	MiscFunctions::NormaliseViewAngle(angles);

	if (angles[0] != angles[0] || angles[1] != angles[1])
	{
		return ReturnValue;
	}

	IsLocked = true;
	//-----------------------------------------------

	// Aim Step Calcs
	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
	if (!IsAimStepping)
		LastAimstepAngle = LastAngle; // Don't just use the viewangs because you need to consider aa

	float fovLeft = FovToPlayer(ViewOffset, LastAimstepAngle, g_EntityList->GetClientEntity(TargetID), 0);

	if (fovLeft > 25.0f && g_Options.Ragebot.AimStep)
	{
		Vector AddAngs = angles - LastAimstepAngle;
		Normalize(AddAngs, AddAngs);
		AddAngs *= 25;
		LastAimstepAngle += AddAngs;
		MiscFunctions::NormaliseViewAngle(LastAimstepAngle);
		angles = LastAimstepAngle;
	}
	else
	{
		ReturnValue = true;
	}


	if (g_Options.Ragebot.Silent)
	{
		if (CanAttack())
		{
			pCmd->viewangles = angles;
		}
	}

	if (!g_Options.Ragebot.Silent)
	{
		pCmd->viewangles = angles;
		g_Engine->SetViewAngles(pCmd->viewangles);
	}


	// pSilent Aim 
	Vector Oldview = pCmd->viewangles;

	if (g_Options.Ragebot.PSilent)
	{
		static int ChokedPackets = -1;
		ChokedPackets++;

		if (ChokedPackets < 6)
		{
			bSendPacket = false;
			pCmd->viewangles = angles;
		}
		else
		{
			bSendPacket = true;
			pCmd->viewangles = Oldview;
			ChokedPackets = -1;
			ReturnValue = false;
		}

		//pCmd->viewangles.z = 0;
	}

	return ReturnValue;
}


void NormalizeVector(Vector& vec)
{
	for (int i = 0; i < 3; ++i)
	{
		while (vec[i] > 180.f)
			vec[i] -= 360.f;

		while (vec[i] < -180.f)
			vec[i] += 360.f;
	}
	vec[2] = 0.f;
}


void VectorAngles2(const Vector& vecForward, Vector& vecAngles)
{
	Vector vecView;
	if (vecForward[1] == 0.f && vecForward[0] == 0.f)
	{
		vecView[0] = 0.f;
		vecView[1] = 0.f;
	}
	else
	{
		vecView[1] = vec_t(atan2(vecForward[1], vecForward[0]) * 180.f / M_PI);

		if (vecView[1] < 0.f)
			vecView[1] += 360.f;

		vecView[2] = sqrt(vecForward[0] * vecForward[0] + vecForward[1] * vecForward[1]);

		vecView[0] = vec_t(atan2(vecForward[2], vecView[2]) * 180.f / M_PI);
	}

	vecAngles[0] = -vecView[0];
	vecAngles[1] = vecView[1];
	vecAngles[2] = 0.f;
}


bool EdgeAntiAim(C_BaseEntity* pLocalBaseEntity, CInput::CUserCmd* cmd, float flWall, float flCornor)
{
	Ray_t ray;
	trace_t tr;

	CTraceFilter traceFilter;
	traceFilter.pSkip = pLocalBaseEntity;

	auto bRetVal = false;
	auto vecCurPos = pLocalBaseEntity->GetEyePosition();

	for (float i = 0; i < 360; i++)
	{
		Vector vecDummy(10.f, cmd->viewangles.y, 0.f);
		vecDummy.y += i;

		NormalizeVector(vecDummy);

		Vector vecForward;
		AngleVectors2(vecDummy, vecForward);

		auto flLength = ((16.f + 3.f) + ((16.f + 3.f) * sin(DEG2RAD(10.f)))) + 7.f;
		vecForward *= flLength;

		ray.Init(vecCurPos, (vecCurPos + vecForward));
		g_EngineTrace->TraceRay(ray, MASK_SHOT, (CTraceFilter *)&traceFilter, &tr);

		if (tr.fraction != 1.0f)
		{
			Vector qAngles;
			auto vecNegate = tr.plane.normal;

			vecNegate *= -1.f;
			VectorAngles2(vecNegate, qAngles);

			vecDummy.y = qAngles.y;

			NormalizeVector(vecDummy);
			trace_t leftTrace, rightTrace;

			Vector vecLeft;
			AngleVectors2(vecDummy + Vector(0.f, 30.f, 0.f), vecLeft);

			Vector vecRight;
			AngleVectors2(vecDummy - Vector(0.f, 30.f, 0.f), vecRight);

			vecLeft *= (flLength + (flLength * sin(DEG2RAD(30.f))));
			vecRight *= (flLength + (flLength * sin(DEG2RAD(30.f))));

			ray.Init(vecCurPos, (vecCurPos + vecLeft));
			g_EngineTrace->TraceRay(ray, MASK_SHOT, (CTraceFilter*)&traceFilter, &leftTrace);

			ray.Init(vecCurPos, (vecCurPos + vecRight));
			g_EngineTrace->TraceRay(ray, MASK_SHOT, (CTraceFilter*)&traceFilter, &rightTrace);

			if ((leftTrace.fraction == 1.f) && (rightTrace.fraction != 1.f))
				vecDummy.y -= flCornor; // left
			else if ((leftTrace.fraction != 1.f) && (rightTrace.fraction == 1.f))
				vecDummy.y += flCornor; // right			

			cmd->viewangles.y = vecDummy.y;
			cmd->viewangles.y -= flWall;
			cmd->viewangles.x = 89.0f;
			bRetVal = true;
		}
	}
	return bRetVal;
}

bool CanOpenFire() // Creds to untrusted guy
{
	C_BaseEntity* pLocalEntity = (C_BaseEntity*)g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	if (!pLocalEntity)
		return false;

	CBaseCombatWeapon* entwep = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		pLocalEntity->GetActiveWeaponHandle());

	float flServerTime = (float)pLocalEntity->GetTickBase() * g_Globals->interval_per_tick;
	float flNextPrimaryAttack = entwep->GetNextPrimaryAttack();

	std::cout << flServerTime << " " << flNextPrimaryAttack << std::endl;

	return !(flNextPrimaryAttack > flServerTime);
}

// AntiAim
void ragebot::DoAntiAim(CInput::CUserCmd* pCmd, bool& bSendPacket)
{
	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());

	// If the aimbot is doing something don't do anything
	if ((IsAimStepping || pCmd->buttons & IN_ATTACK) && !g_Options.Ragebot.PSilent)
		return;

	// Weapon shit
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		pLocal->GetActiveWeaponHandle());

	if (pWeapon)
	{
		CSWeaponInfo* pWeaponInfo = pWeapon->GetCSWpnData();
		// Knives or grenades
		if (!MiscFunctions::IsBallisticWeapon(pWeapon))
		{
			if (g_Options.Ragebot.KnifeAA)
			{
				if (!CanOpenFire() || pCmd->buttons & IN_ATTACK2)
					return;
			}
			else
			{
				return;
			}
		}
	}

	// Don't do antiaim
	// if (DoExit) return;

	if (g_Options.Ragebot.Edge)
	{
		auto bEdge = EdgeAntiAim(pLocal, pCmd, 360.f, 89.f);
		if (bEdge)
			return;
	}

	if (g_Options.Ragebot.YawFake != 0)
		Globals::ySwitch = !Globals::ySwitch;
	else
		Globals::ySwitch = true;

	bSendPacket = Globals::ySwitch;

	Vector SpinAngles = {0, 0, 0};
	Vector FakeAngles = {0, 0, 0};
	float server_time = pLocal->GetTickBase() * g_Globals->interval_per_tick;
	static int ticks;
	static bool flip;
	if (ticks < 15 + rand() % 20)
		ticks++;
	else
	{
		flip = !flip;
		ticks = 0;
	}
	Vector StartAngles;
	double rate = 360.0 / 1.618033988749895;
	double yaw = fmod(static_cast<double>(server_time) * rate, 360.0);
	double factor = 360.0 / M_PI;
	factor *= 25;
	switch (g_Options.Ragebot.YawTrue)
	{
	case 1: //sideways
		{
			g_Engine->GetViewAngles(StartAngles);
			SpinAngles.y = flip ? StartAngles.y - 90.f : StartAngles.y + 90.f;
		}
		break;
	case 2: //slowspin
		SpinAngles.y += static_cast<float>(yaw);
		break;
	case 3: //fastspin
		{
			SpinAngles.y = (float)(fmod(server_time / 0.05f * 360.0f, 360.0f));
		}
		break;
	case 4: //backwards
		{
			g_Engine->GetViewAngles(StartAngles);
			StartAngles.y -= 180.f;
			SpinAngles = StartAngles;
		}
		break;
	case 5:
		{
			SpinAngles.y = pLocal->GetLowerBodyYaw();
		}
		break;
	case 6:
		{
			g_Engine->GetViewAngles(StartAngles);

			static bool dir = false;

			if (GetAsyncKeyState(VK_LEFT)) dir = false;
			else if (GetAsyncKeyState(VK_RIGHT)) dir = true;

			if (dir && pLocal->GetVelocity().Length2D() < 1)
			{
				if (Globals::shouldflip)
				{
					SpinAngles.y = StartAngles.y - 110;
				}
				else
				{
					SpinAngles.y = StartAngles.y + 135;
				}
			}
			else if (!dir && pLocal->GetVelocity().Length2D() < 1)
			{
				if (Globals::shouldflip)
				{
					SpinAngles.y = StartAngles.y + 110;
				}
				else
				{
					SpinAngles.y = StartAngles.y - 135;
				}
			}
			else if (pLocal->GetVelocity().Length2D() > 0)
			{
				SpinAngles.y = StartAngles.y + 180;
			}
		}
		break;
	case 7:
		{
			g_Engine->GetViewAngles(StartAngles);

			SpinAngles.y = flip ? StartAngles.y - 145.f : StartAngles.y + 145.f;
		}
		break;
	case 8:
		{
			g_Engine->GetViewAngles(StartAngles);

			static bool dir = false;
			static int jitterangle = 0;

			if (GetAsyncKeyState(VK_LEFT)) dir = false;
			else if (GetAsyncKeyState(VK_RIGHT)) dir = true;


			if (dir && pLocal->GetVelocity().Length2D() < 1)
			{
				if (Globals::shouldflip)
				{
					SpinAngles.y = StartAngles.y - 90;
				}
				else
				{
					SpinAngles.y = StartAngles.y + 125;

					if (jitterangle <= 1)
					{
						SpinAngles.y = StartAngles.y + 125;
						jitterangle += 1;
					}
					else if (jitterangle > 1 && jitterangle <= 3)
					{
						SpinAngles.y = StartAngles.y + 145;
						jitterangle += 1;
					}
					else
					{
						jitterangle = 0;
					}
				}
			}
			else if (!dir && pLocal->GetVelocity().Length2D() < 1)
			{
				if (Globals::shouldflip)
				{
					SpinAngles.y = StartAngles.y + 90;
				}
				else
				{
					SpinAngles.y = StartAngles.y - 125;

					if (jitterangle <= 1)
					{
						SpinAngles.y = StartAngles.y - 125;
						jitterangle += 1;
					}
					else if (jitterangle > 1 && jitterangle <= 3)
					{
						SpinAngles.y = StartAngles.y - 145;
						jitterangle += 1;
					}
					else
					{
						jitterangle = 0;
					}
				}
			}
			else if (pLocal->GetVelocity().Length2D() > 0)
			{
				SpinAngles.y = StartAngles.y + 155;

				if (jitterangle <= 1)
				{
					SpinAngles.y = StartAngles.y + 155;
					jitterangle += 1;
				}
				else if (jitterangle > 1 && jitterangle <= 3)
				{
					SpinAngles.y = StartAngles.y - 155;
					jitterangle += 1;
				}
				else
				{
					jitterangle = 0;
				}
			}
		}
		break;
	}


	switch (g_Options.Ragebot.YawFake)
	{
	case 1: //sideways
		{
			g_Engine->GetViewAngles(StartAngles);
			FakeAngles.y = flip ? StartAngles.y + 90.f : StartAngles.y - 90.f;
		}
		break;
	case 2: //slowspin
		FakeAngles.y += static_cast<float>(yaw);
		break;
	case 3: //fastspin
		FakeAngles.y = (float)(fmod(server_time / 0.05f * 360.0f, 360.0f));
		break;
	case 4: //backwards
		{
			g_Engine->GetViewAngles(StartAngles);

			StartAngles -= 180.f;
			FakeAngles = StartAngles;
		}
		break;
	case 5: //lby antiaim
		{
			g_Engine->GetViewAngles(StartAngles);
			static bool llamaflip;
			static float oldLBY = 0.0f;
			float LBY = pLocal->GetLowerBodyYaw();
			if (LBY != oldLBY) // did lowerbody update?
			{
				llamaflip = !llamaflip;
				oldLBY = LBY;
			}
			FakeAngles.y = llamaflip ? StartAngles.y - 90.f : StartAngles.y + 90.f;
		}
		break;
	case 6:
		{
			g_Engine->GetViewAngles(StartAngles);
			static bool dir = false;

			if (GetAsyncKeyState(VK_LEFT)) dir = false;
			else if (GetAsyncKeyState(VK_RIGHT)) dir = true;

			if (dir && pLocal->GetVelocity().Length2D() < 1)
			{
				FakeAngles.y -= StartAngles.y - 110;
			}
			else if (!dir && pLocal->GetVelocity().Length2D() < 1)
			{
				FakeAngles.y += StartAngles.y + 110;
			}
			else if (pLocal->GetVelocity().Length2D() > 0)
			{
				FakeAngles.y = (float)(fmod(server_time / 0.05f * 360.0f, 360.0f));
			}
		}
		break;
	case 7:
		{
			g_Engine->GetViewAngles(StartAngles);

			FakeAngles.y = flip ? StartAngles.y - 145.f : StartAngles.y + 145.f;
		}
		break;
	case 8:
		{
			g_Engine->GetViewAngles(StartAngles);
			static bool dir = false;
			static int jitterangle = 0;

			if (GetAsyncKeyState(VK_LEFT)) dir = false;
			else if (GetAsyncKeyState(VK_RIGHT)) dir = true;

			if (dir && pLocal->GetVelocity().Length2D() < 1)
			{
				FakeAngles.y = StartAngles.y - 75;
				if (jitterangle <= 1)
				{
					FakeAngles.y = StartAngles.y - 75;
					jitterangle += 1;
				}
				else if (jitterangle > 1 && jitterangle <= 3)
				{
					FakeAngles.y = StartAngles.y - 105;
					jitterangle += 1;
				}
				else
				{
					jitterangle = 0;
				}
			}
			else if (!dir && pLocal->GetVelocity().Length2D() < 1)
			{
				FakeAngles.y = StartAngles.y + 75;
				if (jitterangle <= 1)
				{
					FakeAngles.y = StartAngles.y + 75;
					jitterangle += 1;
				}
				else if (jitterangle > 1 && jitterangle <= 3)
				{
					FakeAngles.y = StartAngles.y + 105;
					jitterangle += 1;
				}
				else
				{
					jitterangle = 0;
				}
			}
			else if (pLocal->GetVelocity().Length2D() > 0)
			{
				FakeAngles.y = StartAngles.y + 45 + RandomFloat(90, -90);
				if (jitterangle <= 1)
				{
					FakeAngles.y += StartAngles.y + 45 + RandomFloat(90, -90);
					jitterangle += 1;
				}
				else if (jitterangle > 1 && jitterangle <= 3)
				{
					FakeAngles.y -= StartAngles.y - 45 - RandomFloat(90, -90);
					jitterangle += 1;
				}
				else
				{
					jitterangle = 0;
				}
			}
		}
		break;
	case 9:
		{
			g_Engine->GetViewAngles(StartAngles);

			static int jitterangle = 0;

			FakeAngles.y = StartAngles.y + 45 + RandomFloat(90, -90);
			if (jitterangle <= 1)
			{
				FakeAngles.y += StartAngles.y + 45 + RandomFloat(90, -90);
				jitterangle += 1;
			}
			else if (jitterangle > 1 && jitterangle <= 3)
			{
				FakeAngles.y -= StartAngles.y - 45 - RandomFloat(90, -90);
				jitterangle += 1;
			}
			else
			{
				jitterangle = 0;
			}
		}
		break;
	}

	if (g_Options.Ragebot.PreAAs && !g_Options.Ragebot.BuilderAAs)
	{
		if (Globals::ySwitch && g_Options.Ragebot.YawTrue != 0)
			pCmd->viewangles.y = FakeAngles.y + g_Options.Ragebot.YawFakeAdder;
		if (!Globals::ySwitch && g_Options.Ragebot.YawFake != 0)
			pCmd->viewangles.y = SpinAngles.y + g_Options.Ragebot.YawTrueAdder;
	}
	else if (!g_Options.Ragebot.PreAAs && g_Options.Ragebot.BuilderAAs)
	{
		g_Engine->GetViewAngles(StartAngles);
		static int jitterangle;

		pCmd->viewangles.x = g_Options.Ragebot.BuilderPitch;

		if (Globals::ySwitch) //Builder FakeAngle
		{
			if (g_Options.Ragebot.FJitter)
			{
				pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake;
				if (jitterangle <= 1)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake + g_Options.Ragebot.FJitterRange;
					jitterangle += 1;
				}
				else if (jitterangle > 1 && jitterangle <= 3)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake - g_Options.Ragebot.FJitterRange;
					jitterangle += 1;
				}
				else
				{
					jitterangle = 0;
				}
			}
			else if (g_Options.Ragebot.FJitter && g_Options.Ragebot.LBYBreaker)
			{
				if (Globals::shouldflip)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake + 118;
				}
				else
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake;
					if (jitterangle <= 1)
					{
						pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake + g_Options.Ragebot.FJitterRange;
						jitterangle += 1;
					}
					else if (jitterangle > 1 && jitterangle <= 3)
					{
						pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake - g_Options.Ragebot.FJitterRange;
						jitterangle += 1;
					}
					else
					{
						jitterangle = 0;
					}
				}
			}
			else if (!g_Options.Ragebot.FJitter && g_Options.Ragebot.LBYBreaker)
			{
				if (Globals::shouldflip)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake + 118;
				}
				else
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake;
				}
			}
			else
			{
				pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderFake;
			}
		}
		if (!Globals::ySwitch) //Builder RealAngle
		{
			if (g_Options.Ragebot.Jitter)
			{
				pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal;
				if (jitterangle <= 1)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal + g_Options.Ragebot.JitterRange;
					jitterangle += 1;
				}
				else if (jitterangle > 1 && jitterangle <= 3)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal - g_Options.Ragebot.JitterRange;
					jitterangle += 1;
				}
				else
				{
					jitterangle = 0;
				}
			}
			else if (g_Options.Ragebot.Jitter && g_Options.Ragebot.LBYBreaker)
			{
				if (Globals::shouldflip)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal + 118;
				}
				else
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal;
					if (jitterangle <= 1)
					{
						pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal + g_Options.Ragebot.JitterRange;
						jitterangle += 1;
					}
					else if (jitterangle > 1 && jitterangle <= 3)
					{
						pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal - g_Options.Ragebot.JitterRange;
						jitterangle += 1;
					}
					else
					{
						jitterangle = 0;
					}
				}
			}
			else if (!g_Options.Ragebot.Jitter && g_Options.Ragebot.LBYBreaker)
			{
				if (Globals::shouldflip)
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal + 118;
				}
				else
				{
					pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal;
				}
			}
			else
			{
				pCmd->viewangles.y = StartAngles.y + g_Options.Ragebot.BuilderReal;
			}
		}
	}

	if (g_Options.Ragebot.PreAAs && g_Options.Ragebot.BuilderAAs)
	{
		Globals::error = true;
	}

	switch (g_Options.Ragebot.Pitch && g_Options.Ragebot.PreAAs && !g_Options.Ragebot.BuilderAAs)
	{
	case 0:
		// No Pitch AA
		break;
	case 1:
		// Down
		pCmd->viewangles.x = 89 + g_Options.Ragebot.PitchAdder;
		break;
	case 2:
		pCmd->viewangles.x = -89 + g_Options.Ragebot.PitchAdder;
		break;
	case 3:
		pCmd->viewangles.x = -180 + g_Options.Ragebot.PitchAdder;
		break;
	case 4:
		pCmd->viewangles.x = 180 + g_Options.Ragebot.PitchAdder;
		break;
	case 5:
		pCmd->viewangles.x = 1080 + g_Options.Ragebot.PitchAdder;
		break;
	}
}

void ragebot::FakeLag(CInput::CUserCmd* cmd, bool& bSendPacket)
{
	int iChoke = g_Options.Ragebot.FakeLagAmt;

	static int iFakeLag = -1;
	iFakeLag++;

	if (iFakeLag <= iChoke && iFakeLag > -1)
	{
		bSendPacket = false;
		Globals::ySwitch = false;
	}
	else
	{
		bSendPacket = true;
		Globals::ySwitch = true;
		iFakeLag = -1;
	}
}

void ragebot::PositionAdjustment(CInput::CUserCmd* pCmd)
{
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class gwvrabf
{
public:
	int mrvvkgzcejvmzxa;
	string hrsidlryat;
	gwvrabf();
	bool avgnpeohaaqgdcvkxpbtcu(string kvuitsspfdme, int vokcuxithcuhfge, string kdzltlwc, double gsfpc, bool kcfiuuk);
	string xbghdgnxzhukgazlprqpwzvb();
	void owpelpmkolodreiu(bool nfazktxsdmeh, string ndmutsogf, string wnagoysmxnnbx, double loziszxywrdzamo, int qaulevi,
	                      int xfhnvlxl, double aldaxblzxgcfz, bool uuckwyia, bool mscoyyajpbrbpu, bool yqwvl);
	int juhsannvvchfxyvoaayjlpl(string favjq, string rujthuamir, bool oxmvigjjed, double rsxji, bool wsukwgcpyuu,
	                            string lngmnrwuccmp, string zmoahwpurtv, string jnbnxnwkzl);

protected:
	double bfvbhlapwfvhe;
	bool sxiyozaxevvkhm;
	string pyseermcabywg;
	string iheeqxovxqu;
	double fcbacyiqbirgr;

	bool osuoltcylqbptoyvbrto(string foizdoiimbtft, string ogrslhdqdepqbnw, bool fqvjqnapfjlnh);
	double lhbpcktptx(bool vapwxsdpkbryvsu);
	void npnexcilbmmeccvkzmb(int unttfmkwaa, int hutqmcqelinbdj, string cpsddrmeoeu, bool ymxgecijuewdor, int hwhgjqijq,
	                         double wwogghw, bool fhkjzeayns, int yyqnotvuvvadd);
	double wftceuhjxe(double mwnmnuhcndjux, bool ehqgpwbdwnwak, bool rdigttxig, string swlljfa, int raxvtpwpzj,
	                  bool anyvjyvodlzc, double hnymwcas, bool zjhkyxmhu, bool lunabpyqavyzffh, int dbsaylpm);
	void axykjdrmmtpzzzgdeddjxj(bool addeturlphmy, double pyjtfyu, string zfofmjp, double sewcekeqbtaq, bool dhsbzawfqhdnu,
	                            bool wpwyfbcdcc, bool bwbqlvppzoblw, bool ykcrjgnqcucwe, string ohqepjwdeemwp);
	bool curgkgbqhjeuyd(string lsjsgahbje, double fpfngquzdnnsu);
	bool jcrjupcbkifsu(double hcmgtye, int ajotnybs, double wneosbjvdbgn);

private:
	double qlmazlyapfunrxn;
	string tlktpbhknq;
	string rgztuhteilpu;

	double igppvyorfmqfdxitisrh(int eylmme, int ifhxqmgbjjxvpse, int dmeufc, int ekaeuzucznoywx, bool xvpkpzrwq);
	int gwcctridtziigmzygqxyotwpi(bool ozghzu, bool puessfmouqihzhh, bool dhwoy, int zlyme, int ylbmyemetgzxl,
	                              string wyicfycwwpwwtlm, string rbgevrenhmiqbxg, bool znmmrufvnce, bool xrcyse,
	                              double xlapxptvufieopq);
	void zjgizhamovpasbsijrviyyr(string xumuqfypfrycyc, bool qsgkskjbbzz, int tsqjsrfabvabotq, string bxeqepgbazhdc,
	                             double stksrsyvxvdz, string dfxnpuefdz, double bqshe, double phddnwnxpom);
	bool swaherxpyezum(bool zsqvrjpwg, int oytjddgs);
	void ljjzznyvwxlptt(bool tjsdf, string pavjut);
	string pdvhwqkioup(bool vwdsukc, string xzacbvsnldox);
	int gaugdtdlzjtfhapi(double aoaltxwrybeor, string xfplzhodfmaaim, string xopwtpfopvwqc, string wnkiplh,
	                     string aucwfvlb, double ghseujedkvv, int amxmczbmbpmw, int lshqfenfq, double ghajkmfigzroey);
	double fmvoszymlxxrfatyselgu(bool dnfpiuglnkr, double lrbkldiiolvngu, string ydnmwhnx, bool qntolslrocf,
	                             int znhiskgipefapb, int ntemweqjbgqg, string ruyti, int yysnkaakuxe, string dnavdhgd);
};


double gwvrabf::igppvyorfmqfdxitisrh(int eylmme, int ifhxqmgbjjxvpse, int dmeufc, int ekaeuzucznoywx, bool xvpkpzrwq)
{
	int seumladrqfimog = 4293;
	string djgjfhwuno = "rximmvponknyeeqsjvvpgvguymakhzmwxzpsiaexhcyyptwziyjjreprtamzhdssfwfelfwqzchexoyvww";
	int jqxpdydmbfso = 1861;
	if (1861 != 1861)
	{
		int eqchp;
		for (eqchp = 39; eqchp > 0; eqchp--)
		{
			continue;
		}
	}
	if (1861 != 1861)
	{
		int zus;
		for (zus = 19; zus > 0; zus--)
		{
			continue;
		}
	}
	if (4293 != 4293)
	{
		int ivwdqhm;
		for (ivwdqhm = 64; ivwdqhm > 0; ivwdqhm--)
		{
			continue;
		}
	}
	if (1861 != 1861)
	{
		int agpsjpyxgi;
		for (agpsjpyxgi = 90; agpsjpyxgi > 0; agpsjpyxgi--)
		{
			continue;
		}
	}
	return 43578;
}

int gwvrabf::gwcctridtziigmzygqxyotwpi(bool ozghzu, bool puessfmouqihzhh, bool dhwoy, int zlyme, int ylbmyemetgzxl,
                                       string wyicfycwwpwwtlm, string rbgevrenhmiqbxg, bool znmmrufvnce, bool xrcyse,
                                       double xlapxptvufieopq)
{
	bool yphvxteplnp = true;
	bool fmbsighfhepp = true;
	bool vkigessht = true;
	bool wnybdjxsdyeb = true;
	bool ckgawf = true;
	bool nqdwgsgbfzn = false;
	double stoadxyv = 23582;
	string lizpl = "rjowyqjeesaexq";
	double xksnsmjxym = 48480;
	string ouxpyysdtm = "qmiseltuowrlidkhsrmsyyefqwjtozibujadsalmbgadkitmimdaslaotiobsbn";
	if (true == true)
	{
		int zrbuyp;
		for (zrbuyp = 73; zrbuyp > 0; zrbuyp--)
		{
		}
	}
	return 83271;
}

void gwvrabf::zjgizhamovpasbsijrviyyr(string xumuqfypfrycyc, bool qsgkskjbbzz, int tsqjsrfabvabotq,
                                      string bxeqepgbazhdc, double stksrsyvxvdz, string dfxnpuefdz, double bqshe,
                                      double phddnwnxpom)
{
	bool bwlrjvv = false;
	double bvxewrqud = 36256;
	if (36256 != 36256)
	{
		int scjprzqh;
		for (scjprzqh = 69; scjprzqh > 0; scjprzqh--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int gzvzm;
		for (gzvzm = 34; gzvzm > 0; gzvzm--)
		{
		}
	}
	if (false == false)
	{
		int xclhe;
		for (xclhe = 7; xclhe > 0; xclhe--)
		{
		}
	}
}

bool gwvrabf::swaherxpyezum(bool zsqvrjpwg, int oytjddgs)
{
	double iwyuvzsvmlgwpq = 21782;
	if (21782 == 21782)
	{
		int igf;
		for (igf = 90; igf > 0; igf--)
		{
		}
	}
	return true;
}

void gwvrabf::ljjzznyvwxlptt(bool tjsdf, string pavjut)
{
	double mbscfsgqx = 24727;
	double rhkfohkjdl = 25501;
	bool khqzfubs = true;
	int qmvousd = 493;
	string ivavj = "cxdhsllrfqgshxzvkrhxfsjpcxjuekkmwxwjpmbymwihmimsilokrvxriovojexohodpleybjxtiwaqdfeqzh";
	string zmcweegotcfs = "titeswnebxibqfebguuhkfrpumngtawsvsffjashekibnhdrhny";
	int wzmwe = 3968;
	double ikbumfcurbg = 6674;
	int tvuvkixeftcrbp = 376;
}

string gwvrabf::pdvhwqkioup(bool vwdsukc, string xzacbvsnldox)
{
	int cmvbgojnxzzmttg = 2373;
	int taskgvsbxddrwti = 3687;
	string ntfhrpsk = "egnjkafmvocitjbldwypcvtgxhpmwkvymrsvhbwmiizyornhssaugoylxwigbfmiqrfrupnctpcgwyloyxm";
	if (3687 == 3687)
	{
		int vtgnbnfble;
		for (vtgnbnfble = 64; vtgnbnfble > 0; vtgnbnfble--)
		{
		}
	}
	if (3687 != 3687)
	{
		int vzgra;
		for (vzgra = 21; vzgra > 0; vzgra--)
		{
			continue;
		}
	}
	if (3687 == 3687)
	{
		int ewvoomyvn;
		for (ewvoomyvn = 66; ewvoomyvn > 0; ewvoomyvn--)
		{
		}
	}
	if (string("egnjkafmvocitjbldwypcvtgxhpmwkvymrsvhbwmiizyornhssaugoylxwigbfmiqrfrupnctpcgwyloyxm") == string(
		"egnjkafmvocitjbldwypcvtgxhpmwkvymrsvhbwmiizyornhssaugoylxwigbfmiqrfrupnctpcgwyloyxm"))
	{
		int jbwag;
		for (jbwag = 94; jbwag > 0; jbwag--)
		{
		}
	}
	if (2373 != 2373)
	{
		int cclwfda;
		for (cclwfda = 67; cclwfda > 0; cclwfda--)
		{
			continue;
		}
	}
	return string("ecbwfhgnljiuhtu");
}

int gwvrabf::gaugdtdlzjtfhapi(double aoaltxwrybeor, string xfplzhodfmaaim, string xopwtpfopvwqc, string wnkiplh,
                              string aucwfvlb, double ghseujedkvv, int amxmczbmbpmw, int lshqfenfq,
                              double ghajkmfigzroey)
{
	bool kmgpzutef = true;
	string auljbjimerpgub = "spcjnhbkbcmhncinvgvmwihlthovaxuqmxtsgtgywspmvnjaxseywwuzfmmwuevgycjaygvl";
	string ihszhi = "bdcvrjltnaerqqgqyrjcksxpkrayiijcpvguxwmmbzufpetorbpuvwdpxnpghmrhdpxn";
	if (true != true)
	{
		int ie;
		for (ie = 12; ie > 0; ie--)
		{
			continue;
		}
	}
	return 43930;
}

double gwvrabf::fmvoszymlxxrfatyselgu(bool dnfpiuglnkr, double lrbkldiiolvngu, string ydnmwhnx, bool qntolslrocf,
                                      int znhiskgipefapb, int ntemweqjbgqg, string ruyti, int yysnkaakuxe,
                                      string dnavdhgd)
{
	bool iaxyn = true;
	int aanddkxeoquhnt = 4505;
	string gztiluib =
		"wlmvbltuiuqnlwnwkgczoojpchcnlfgodtdcdtantsretmvqogbpxrbvylaxbuxfjfphyokabygjpkyarredtxdpwckhagenfpkh";
	double ihupfqndiym = 52550;
	string innybl = "nltlfcxdgzokxjvrwqypwwrt";
	double bbterapat = 16942;
	if (string("wlmvbltuiuqnlwnwkgczoojpchcnlfgodtdcdtantsretmvqogbpxrbvylaxbuxfjfphyokabygjpkyarredtxdpwckhagenfpkh") !=
		string("wlmvbltuiuqnlwnwkgczoojpchcnlfgodtdcdtantsretmvqogbpxrbvylaxbuxfjfphyokabygjpkyarredtxdpwckhagenfpkh"))
	{
		int gpuolout;
		for (gpuolout = 89; gpuolout > 0; gpuolout--)
		{
		}
	}
	if (true != true)
	{
		int trby;
		for (trby = 95; trby > 0; trby--)
		{
			continue;
		}
	}
	if (string("nltlfcxdgzokxjvrwqypwwrt") != string("nltlfcxdgzokxjvrwqypwwrt"))
	{
		int kqdbi;
		for (kqdbi = 8; kqdbi > 0; kqdbi--)
		{
		}
	}
	return 50007;
}

bool gwvrabf::osuoltcylqbptoyvbrto(string foizdoiimbtft, string ogrslhdqdepqbnw, bool fqvjqnapfjlnh)
{
	string rdwybyackd = "nalde";
	double gwjuxy = 20994;
	int egrodnfrtdeljo = 2596;
	int fpbelvz = 5802;
	double fubekpaepyyklim = 19349;
	double scgqbalhonmjd = 16191;
	int kfmcfxer = 599;
	double xylhmmksdomze = 14924;
	int gvmysegsnjkn = 793;
	double dnxzifzlzkcwea = 48565;
	return false;
}

double gwvrabf::lhbpcktptx(bool vapwxsdpkbryvsu)
{
	string azjhojdb = "akhezvmapghbzirbhihxcbkgxiqftrercwunzxxgmxpxoqnrupqcdxdngmrctuasiqthbouqahzmsyuhtgypyxkespwll";
	string qljopwbulv = "culpczrybparswutfzazogzxfhaem";
	bool hlpbavzq = false;
	string eivtd = "kryjfnllwunqzrvbjslgwbohupotuzjsyakxpzxpmjlcgpsckpthccwgqqlivbrqpuujugkpvnwiogbxzbkughajboxy";
	int wsavgfxurvnjhy = 2103;
	bool tzdwtdqrbgkcsc = false;
	if (false == false)
	{
		int suy;
		for (suy = 41; suy > 0; suy--)
		{
		}
	}
	if (false != false)
	{
		int hxmhzqbed;
		for (hxmhzqbed = 62; hxmhzqbed > 0; hxmhzqbed--)
		{
			continue;
		}
	}
	return 22477;
}

void gwvrabf::npnexcilbmmeccvkzmb(int unttfmkwaa, int hutqmcqelinbdj, string cpsddrmeoeu, bool ymxgecijuewdor,
                                  int hwhgjqijq, double wwogghw, bool fhkjzeayns, int yyqnotvuvvadd)
{
	bool pnalqbrqhkbi = true;
	string vurtcblvffa = "iplslbgdhgipuuwxphpfveaswwiqdypkqhmpid";
	double imfwlmmyrodh = 4918;
	bool zwnaoic = false;
	string rbewlalxdentj = "aiybqvp";
	string hqoipj = "tueooghappinexmuwqtqjxctisqwhdgwxxtelinypeigjflxebecrrmukpedkikukjpabaupi";
	int tzfbxeykrpbi = 2133;
	int mcdjbib = 3286;
	double squgd = 20200;
}

double gwvrabf::wftceuhjxe(double mwnmnuhcndjux, bool ehqgpwbdwnwak, bool rdigttxig, string swlljfa, int raxvtpwpzj,
                           bool anyvjyvodlzc, double hnymwcas, bool zjhkyxmhu, bool lunabpyqavyzffh, int dbsaylpm)
{
	string czttpcik = "c";
	double dmqhlvicse = 7735;
	if (7735 == 7735)
	{
		int bs;
		for (bs = 92; bs > 0; bs--)
		{
		}
	}
	if (string("c") == string("c"))
	{
		int vcmn;
		for (vcmn = 83; vcmn > 0; vcmn--)
		{
		}
	}
	if (7735 != 7735)
	{
		int kfovjup;
		for (kfovjup = 69; kfovjup > 0; kfovjup--)
		{
			continue;
		}
	}
	if (7735 == 7735)
	{
		int shrcpl;
		for (shrcpl = 48; shrcpl > 0; shrcpl--)
		{
		}
	}
	if (string("c") == string("c"))
	{
		int ook;
		for (ook = 78; ook > 0; ook--)
		{
		}
	}
	return 82027;
}

void gwvrabf::axykjdrmmtpzzzgdeddjxj(bool addeturlphmy, double pyjtfyu, string zfofmjp, double sewcekeqbtaq,
                                     bool dhsbzawfqhdnu, bool wpwyfbcdcc, bool bwbqlvppzoblw, bool ykcrjgnqcucwe,
                                     string ohqepjwdeemwp)
{
	int omgyz = 5748;
	string vnkbfgrsqtvino = "kfpbtxpvpildbmgm";
	double uesrtlykzlsb = 6391;
	string zenscmgawx = "kqjcqvltekwjxxmqtvjoilmgdglmxrrdyrdsajsvggrmcalqttzxgilaeyagjfnjlmalojqqcbgebypcpjoyhotcaeids";
	double zzpdg = 58847;
	if (string("kfpbtxpvpildbmgm") != string("kfpbtxpvpildbmgm"))
	{
		int ptjh;
		for (ptjh = 20; ptjh > 0; ptjh--)
		{
		}
	}
}

bool gwvrabf::curgkgbqhjeuyd(string lsjsgahbje, double fpfngquzdnnsu)
{
	bool btzqbwfihszorph = true;
	string ttbpxnrh = "crosggskdfhvglhgykrzpqbjitlsmivhjswsntscgkgrpgyxsodrzqvsnvvvkhblnbanvnpidofcizdgmerecr";
	string cyrlbxzqm = "mzwjctmjjhyovvfndovfuuepgzumvxcdzexkhgifnjduxleualqnhtippsioqwum";
	string sinpyaptvuop = "jndtxxxvkeaxspqkibnuwoujpxsbyhlj";
	string fhytthfergxh = "j";
	double jqlfmzh = 31066;
	if (string("crosggskdfhvglhgykrzpqbjitlsmivhjswsntscgkgrpgyxsodrzqvsnvvvkhblnbanvnpidofcizdgmerecr") == string(
		"crosggskdfhvglhgykrzpqbjitlsmivhjswsntscgkgrpgyxsodrzqvsnvvvkhblnbanvnpidofcizdgmerecr"))
	{
		int lq;
		for (lq = 79; lq > 0; lq--)
		{
		}
	}
	if (string("mzwjctmjjhyovvfndovfuuepgzumvxcdzexkhgifnjduxleualqnhtippsioqwum") != string(
		"mzwjctmjjhyovvfndovfuuepgzumvxcdzexkhgifnjduxleualqnhtippsioqwum"))
	{
		int mqxome;
		for (mqxome = 24; mqxome > 0; mqxome--)
		{
		}
	}
	if (string("mzwjctmjjhyovvfndovfuuepgzumvxcdzexkhgifnjduxleualqnhtippsioqwum") != string(
		"mzwjctmjjhyovvfndovfuuepgzumvxcdzexkhgifnjduxleualqnhtippsioqwum"))
	{
		int zxvxhap;
		for (zxvxhap = 41; zxvxhap > 0; zxvxhap--)
		{
		}
	}
	return true;
}

bool gwvrabf::jcrjupcbkifsu(double hcmgtye, int ajotnybs, double wneosbjvdbgn)
{
	string tprbevuutcu = "ujnhyfemzixkhjmbqgwkbpjgasssdznocdjbessyjhteyshdefvk";
	string ijhepu = "uyxwdfawcckzzubayeee";
	bool nnjbxkixwekb = false;
	return false;
}

bool gwvrabf::avgnpeohaaqgdcvkxpbtcu(string kvuitsspfdme, int vokcuxithcuhfge, string kdzltlwc, double gsfpc,
                                     bool kcfiuuk)
{
	int qdywjcb = 1097;
	bool khsofzynhmpvyc = false;
	bool gxbaglpnw = true;
	if (1097 != 1097)
	{
		int mk;
		for (mk = 46; mk > 0; mk--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int khbyxc;
		for (khbyxc = 19; khbyxc > 0; khbyxc--)
		{
		}
	}
	if (true != true)
	{
		int oknwqazplz;
		for (oknwqazplz = 94; oknwqazplz > 0; oknwqazplz--)
		{
			continue;
		}
	}
	if (1097 != 1097)
	{
		int gdrf;
		for (gdrf = 51; gdrf > 0; gdrf--)
		{
			continue;
		}
	}
	return false;
}

string gwvrabf::xbghdgnxzhukgazlprqpwzvb()
{
	return string("ncsosjxwxcwnmmcy");
}

void gwvrabf::owpelpmkolodreiu(bool nfazktxsdmeh, string ndmutsogf, string wnagoysmxnnbx, double loziszxywrdzamo,
                               int qaulevi, int xfhnvlxl, double aldaxblzxgcfz, bool uuckwyia, bool mscoyyajpbrbpu,
                               bool yqwvl)
{
	string vgmrstuiudebef = "uqqxdexdqvjkgbowdakhppwiswzrqejtgcctgolerljnygjemxzpidghvbkrkcwxkysjjdu";
	bool djytmedd = false;
	string tmjshnksfpjv = "rngaervzvjmukmqfhpbqlpzjgfucfgzqyrreqtdyvlrlexvcqlptyvnoqawywltluppsqefsfycysirzomyewuq";
	double vfnjpild = 34641;
	int ejfjcs = 3179;
	bool dnckmvrt = true;
	string mvcsjnjgkbsvi = "ucxlviwcigwqnxeoywgfrfnygemzbqoixzhhpzmunpxxbhvdcminjokvbbjni";
	string jmyqloobgel = "mvtygydnlonxqjhqjnrirtp";
	int ktwaubhpi = 4064;
	if (string("rngaervzvjmukmqfhpbqlpzjgfucfgzqyrreqtdyvlrlexvcqlptyvnoqawywltluppsqefsfycysirzomyewuq") != string(
		"rngaervzvjmukmqfhpbqlpzjgfucfgzqyrreqtdyvlrlexvcqlptyvnoqawywltluppsqefsfycysirzomyewuq"))
	{
		int nejrbx;
		for (nejrbx = 59; nejrbx > 0; nejrbx--)
		{
		}
	}
	if (string("ucxlviwcigwqnxeoywgfrfnygemzbqoixzhhpzmunpxxbhvdcminjokvbbjni") != string(
		"ucxlviwcigwqnxeoywgfrfnygemzbqoixzhhpzmunpxxbhvdcminjokvbbjni"))
	{
		int qynz;
		for (qynz = 84; qynz > 0; qynz--)
		{
		}
	}
	if (true == true)
	{
		int ttmyilp;
		for (ttmyilp = 66; ttmyilp > 0; ttmyilp--)
		{
		}
	}
	if (string("rngaervzvjmukmqfhpbqlpzjgfucfgzqyrreqtdyvlrlexvcqlptyvnoqawywltluppsqefsfycysirzomyewuq") != string(
		"rngaervzvjmukmqfhpbqlpzjgfucfgzqyrreqtdyvlrlexvcqlptyvnoqawywltluppsqefsfycysirzomyewuq"))
	{
		int ulk;
		for (ulk = 15; ulk > 0; ulk--)
		{
		}
	}
}

int gwvrabf::juhsannvvchfxyvoaayjlpl(string favjq, string rujthuamir, bool oxmvigjjed, double rsxji, bool wsukwgcpyuu,
                                     string lngmnrwuccmp, string zmoahwpurtv, string jnbnxnwkzl)
{
	int hkaiqzplmw = 7061;
	bool xeigtuidtydwff = false;
	double zmctjepw = 20980;
	int tbathvezomva = 1958;
	double oghzbkew = 40939;
	int auauaho = 1026;
	if (40939 == 40939)
	{
		int dnfcdxfc;
		for (dnfcdxfc = 40; dnfcdxfc > 0; dnfcdxfc--)
		{
		}
	}
	if (1958 != 1958)
	{
		int hhwlhg;
		for (hhwlhg = 42; hhwlhg > 0; hhwlhg--)
		{
			continue;
		}
	}
	if (20980 == 20980)
	{
		int kulgik;
		for (kulgik = 99; kulgik > 0; kulgik--)
		{
		}
	}
	return 96924;
}

gwvrabf::gwvrabf()
{
	this->avgnpeohaaqgdcvkxpbtcu(string("grlyrjfxuimnqivdnzstmyfnethmti"), 3962,
	                             string(
		                             "ygzzzskgxsysppkzppfkgyxyhayhhjwsgyavjroygbnrklxmmunbxgeuxknyzrlldqoofevjeikpbidabumotfr"),
	                             67858, false);
	this->xbghdgnxzhukgazlprqpwzvb();
	this->owpelpmkolodreiu(false, string("bkpbanufnmujdlhjliymozsvdszzufiqejpzmnczmavmdqsfeuwuigepdwzwigjnqohfmtiaxsun"),
	                       string("rrpxqgffqelusrelyjcsnntdfmvlnde"), 5546, 4087, 1311, 10605, true, false, true);
	this->juhsannvvchfxyvoaayjlpl(string("ylcegqzlqxgnqzsrrofdpweoigpzvsddfpgizdjbgpkwhdfwuxdly"),
	                              string("curubmbtraskwoyipplucqcpprdmotfjtyvsgqymeljutbpyrln"), false, 42629, true,
	                              string("bdwfqmymcmefuqpxzqevaarv"), string("rtecyfufejdkzbmiuajytdpcctfuzebfta"),
	                              string(
		                              "hkpvzyhmjrgjyiajqphyqrrecempeoruiwufmtsztsutktwobvhpxfszovffzkkqfiikeyktrjyzbtoakfjtfopzdsoa"));
	this->osuoltcylqbptoyvbrto(string("udjisafwwvywuobdpcxdftxoiofskyduswpze"),
	                           string(
		                           "qlruufpqodpvsqhefwfgmgompeddrjlysibtxzetgvtimrnaggbzrvqibckzjnfwzmrhdamhpwgxmjwhfyxkyytrthe"),
	                           false);
	this->lhbpcktptx(false);
	this->npnexcilbmmeccvkzmb(
		3922, 1175, string("kwxryfvtjbcbtcqubkcngyufmtpjaccuqeprnbivxoumfuwyqrhawvwwnlmszvfrcwmqkckhaxcm"), true, 4026, 3333,
		true, 2825);
	this->wftceuhjxe(11800, true, false, string("srstaqgqefhbzgstiv"), 251, false, 50639, false, true, 3265);
	this->axykjdrmmtpzzzgdeddjxj(false, 17001,
	                             string("evbpvqenxeiemufrzgpovnsqfpiqkqrshkptkehtuqaxgksqkrygchlxwbieoosxnglzi"), 14304,
	                             false, false, false, false,
	                             string("qwrhdomrpntaglwxmwygawezuguwwrhjxwwxmiiqotkhklxulkfsu"));
	this->curgkgbqhjeuyd(string("pweqryjrejxcxvehvedjpksqkpagzqqnjgldkbsgxteqxbfwtkeypr"), 44486);
	this->jcrjupcbkifsu(21070, 706, 49289);
	this->igppvyorfmqfdxitisrh(268, 754, 2549, 7390, false);
	this->gwcctridtziigmzygqxyotwpi(true, true, true, 5109, 6146,
	                                string(
		                                "skwlvqzefduekvioiuyyuveqqhmeqluwafghawzhotycfkvzlhduhvrsceiopsqzwkiwoncmwaeccxbqhznzlrllkuvlcrpszrfx"),
	                                string(
		                                "xqyrsqjleziylpihtomeqzqpsnhxyxqinbjopnobyqvejcofyqwultnpvuudvlonzlbdrjlbrikuwqwvsdw"),
	                                false, false, 84273);
	this->zjgizhamovpasbsijrviyyr(string("behoyqkkpbedtkzomfoxznqnzyscqbtflnikslhhrhcdfmbttli"), false, 5694,
	                              string("ucdbwwljux"), 16421, string("mviuqmuasciduyyxejsnlhavhhjawnrlbatfijxzmd"), 33123,
	                              15757);
	this->swaherxpyezum(false, 705);
	this->ljjzznyvwxlptt(
		false, string("jojxvrxrswzwbscnvkikauyecwslteqxowflqtvptplseskohrzldfrndlohgkokpsuowjmmyufkfcjvqymmpmnwbzybhl"));
	this->pdvhwqkioup(true, string("hxvhboqkrqhnwcdmvhshyrzzsbrmkgoceweejjneynesjyolldgcpqxsnocpyohqraeorimvvbmzjzhtul"));
	this->gaugdtdlzjtfhapi(
		39904, string("xppwfaitkgdlwzmatfubuspuezdzozzxxfglunhibcvxiewkqaghqexnvtameduekszikmoaoxrzblsctikecu"),
		string("oniisnvalnvzrcgztmfwjgxgpqdkxdfbpjulqcqbsezixqkecdxpqpexqxbrngdnjz"),
		string("smcttqnrfduwcbtihtbglvajuikmcvbnzdjiypmysbryzjdkaicyyrblieolzzdfwfuuylwyahf"),
		string("dtcojqomokobdtjepveagztisrswfdjigbgtpldqzsbdikgfazljablcopkzrtezhqnuqwajxnlzlqfctwxbyzdxwklnbz"), 33484, 7610,
		2554, 11635);
	this->fmvoszymlxxrfatyselgu(true, 8859, string("todoghpheyarhqvkh"), true, 1461, 7254,
	                            string("tdkitstovkprgapckzorjlxmnjkzwcvewe"), 4950,
	                            string(
		                            "cnerlymhxxujqwqbggbyoslyeoyrwpnvjuygkzndearslhhjpopmxdveimppwivhieddqzlwefrcbuvspaelyopxdjzvpk"));
}
