#include "SDK.h"


#define SEQUENCE_DEFAULT_DRAW 0
#define SEQUENCE_DEFAULT_IDLE1 1
#define SEQUENCE_DEFAULT_IDLE2 2
#define SEQUENCE_DEFAULT_LIGHT_MISS1 3
#define SEQUENCE_DEFAULT_LIGHT_MISS2 4
#define SEQUENCE_DEFAULT_HEAVY_MISS1 9
#define SEQUENCE_DEFAULT_HEAVY_HIT1 10
#define SEQUENCE_DEFAULT_HEAVY_BACKSTAB 11
#define SEQUENCE_DEFAULT_LOOKAT01 12

#define SEQUENCE_BUTTERFLY_DRAW 0
#define SEQUENCE_BUTTERFLY_DRAW2 1
#define SEQUENCE_BUTTERFLY_LOOKAT01 13
#define SEQUENCE_BUTTERFLY_LOOKAT03 15

#define SEQUENCE_FALCHION_IDLE1 1
#define SEQUENCE_FALCHION_HEAVY_MISS1 8
#define SEQUENCE_FALCHION_HEAVY_MISS1_NOFLIP 9
#define SEQUENCE_FALCHION_LOOKAT01 12
#define SEQUENCE_FALCHION_LOOKAT02 13

#define SEQUENCE_DAGGERS_IDLE1 1
#define SEQUENCE_DAGGERS_LIGHT_MISS1 2
#define SEQUENCE_DAGGERS_LIGHT_MISS5 6
#define SEQUENCE_DAGGERS_HEAVY_MISS2 11
#define SEQUENCE_DAGGERS_HEAVY_MISS1 12

#define SEQUENCE_BOWIE_IDLE1 1
using namespace std;
// Helper function to generate a random sequence number.
inline int RandomSequence(int low, int high)
{
	return (rand() % (high - low + 1) + low);
}

#define	LIFE_ALIVE 0

#define RandomInt(nMin, nMax) (rand() % (nMax - nMin + 1) + nMin);


RecvVarProxyFn fnSequenceProxyFn = nullptr;

RecvVarProxyFn oRecvnModelIndex;

void Hooked_RecvProxy_Viewmodel(CRecvProxyData* pData, void* pStruct, void* pOut)
{
	// Get the knife view model id's
	int default_t = g_ModelInfo->GetModelIndex("models/weapons/v_knife_default_t.mdl");
	int default_ct = g_ModelInfo->GetModelIndex("models/weapons/v_knife_default_ct.mdl");
	int iBayonet = g_ModelInfo->GetModelIndex("models/weapons/v_knife_bayonet.mdl");
	int iButterfly = g_ModelInfo->GetModelIndex("models/weapons/v_knife_butterfly.mdl");
	int iFlip = g_ModelInfo->GetModelIndex("models/weapons/v_knife_flip.mdl");
	int iGut = g_ModelInfo->GetModelIndex("models/weapons/v_knife_gut.mdl");
	int iKarambit = g_ModelInfo->GetModelIndex("models/weapons/v_knife_karam.mdl");
	int iM9Bayonet = g_ModelInfo->GetModelIndex("models/weapons/v_knife_m9_bay.mdl");
	int iHuntsman = g_ModelInfo->GetModelIndex("models/weapons/v_knife_tactical.mdl");
	int iFalchion = g_ModelInfo->GetModelIndex("models/weapons/v_knife_falchion_advanced.mdl");
	int iDagger = g_ModelInfo->GetModelIndex("models/weapons/v_knife_push.mdl");
	int iBowie = g_ModelInfo->GetModelIndex("models/weapons/v_knife_survival_bowie.mdl");

	int iGunGame = g_ModelInfo->GetModelIndex("models/weapons/v_knife_gg.mdl");

	// Get local player (just to stop replacing spectators knifes)
	C_BaseEntity* pLocal = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
	if (g_Options.Skinchanger.Enabled && pLocal)
	{
		// If we are alive and holding a default knife(if we already have a knife don't worry about changing)
		if (pLocal->IsAlive() && (
			pData->m_Value.m_Int == default_t ||
			pData->m_Value.m_Int == default_ct ||
			pData->m_Value.m_Int == iBayonet ||
			pData->m_Value.m_Int == iFlip ||
			pData->m_Value.m_Int == iGunGame ||
			pData->m_Value.m_Int == iGut ||
			pData->m_Value.m_Int == iKarambit ||
			pData->m_Value.m_Int == iM9Bayonet ||
			pData->m_Value.m_Int == iHuntsman ||
			pData->m_Value.m_Int == iBowie ||
			pData->m_Value.m_Int == iButterfly ||
			pData->m_Value.m_Int == iFalchion ||
			pData->m_Value.m_Int == iDagger))
		{
			// Set whatever knife we want
			if (g_Options.Skinchanger.Knife == 0)
				pData->m_Value.m_Int = iBayonet;
			else if (g_Options.Skinchanger.Knife == 1)
				pData->m_Value.m_Int = iBowie;
			else if (g_Options.Skinchanger.Knife == 2)
				pData->m_Value.m_Int = iButterfly;
			else if (g_Options.Skinchanger.Knife == 3)
				pData->m_Value.m_Int = iFalchion;
			else if (g_Options.Skinchanger.Knife == 4)
				pData->m_Value.m_Int = iFlip;
			else if (g_Options.Skinchanger.Knife == 5)
				pData->m_Value.m_Int = iGut;
			else if (g_Options.Skinchanger.Knife == 6)
				pData->m_Value.m_Int = iHuntsman;
			else if (g_Options.Skinchanger.Knife == 7)
				pData->m_Value.m_Int = iKarambit;
			else if (g_Options.Skinchanger.Knife == 8)
				pData->m_Value.m_Int = iM9Bayonet;
			else if (g_Options.Skinchanger.Knife == 9)
				pData->m_Value.m_Int = iDagger;
		}
	}
	// Carry on the to original proxy
	oRecvnModelIndex(pData, pStruct, pOut);
}


void SetViewModelSequence2(const CRecvProxyData* pDataConst, void* pStruct, void* pOut)
{
	// Make the incoming data editable.
	CRecvProxyData* pData = const_cast<CRecvProxyData*>(pDataConst);

	// Confirm that we are replacing our view model and not someone elses.
	CBaseViewModel* pViewModel = static_cast<CBaseViewModel*>(pStruct);

	if (pViewModel)
	{
		C_BaseEntity* pOwner = static_cast<C_BaseEntity*>(g_EntityList->GetClientEntity(pViewModel->GetOwner() & 0xFFF));

		// Compare the owner entity of this view model to the local player entity.
		if (pOwner && pOwner->GetIndex() == g_Engine->GetLocalPlayer())
		{
			// Get the filename of the current view model.
			void* pModel = g_ModelInfo->GetModel(pViewModel->GetModelIndex());
			std::string szModel = g_ModelInfo->GetModelName(pModel);

			// Store the current sequence.
			int m_nSequence = pData->m_Value.m_Int;
			if (szModel == "models/weapons/v_knife_butterfly.mdl")
			{
				// Fix animations for the Butterfly Knife.
				switch (m_nSequence)
				{
				case SEQUENCE_DEFAULT_DRAW:
					m_nSequence = RandomInt(SEQUENCE_BUTTERFLY_DRAW, SEQUENCE_BUTTERFLY_DRAW2);
					break;
				case SEQUENCE_DEFAULT_LOOKAT01:
					m_nSequence = RandomInt(SEQUENCE_BUTTERFLY_LOOKAT01, SEQUENCE_BUTTERFLY_LOOKAT03);
					break;
				default:
					m_nSequence++;
				}
			}
			else if (szModel == "models/weapons/v_knife_falchion_advanced.mdl")
			{
				// Fix animations for the Falchion Knife.
				switch (m_nSequence)
				{
				case SEQUENCE_DEFAULT_IDLE2:
					m_nSequence = SEQUENCE_FALCHION_IDLE1;
					break;
				case SEQUENCE_DEFAULT_HEAVY_MISS1:
					m_nSequence = RandomInt(SEQUENCE_FALCHION_HEAVY_MISS1, SEQUENCE_FALCHION_HEAVY_MISS1_NOFLIP);
					break;
				case SEQUENCE_DEFAULT_LOOKAT01:
					m_nSequence = RandomInt(SEQUENCE_FALCHION_LOOKAT01, SEQUENCE_FALCHION_LOOKAT02);
					break;
				case SEQUENCE_DEFAULT_DRAW:
				case SEQUENCE_DEFAULT_IDLE1:
					break;
				default:
					m_nSequence--;
				}
			}
			else if (szModel == "models/weapons/v_knife_push.mdl")
			{
				// Fix animations for the Shadow Daggers.
				switch (m_nSequence)
				{
				case SEQUENCE_DEFAULT_IDLE2:
					m_nSequence = SEQUENCE_DAGGERS_IDLE1;
					break;
				case SEQUENCE_DEFAULT_LIGHT_MISS1:
				case SEQUENCE_DEFAULT_LIGHT_MISS2:
					m_nSequence = RandomInt(SEQUENCE_DAGGERS_LIGHT_MISS1, SEQUENCE_DAGGERS_LIGHT_MISS5);
					break;
				case SEQUENCE_DEFAULT_HEAVY_MISS1:
					m_nSequence = RandomInt(SEQUENCE_DAGGERS_HEAVY_MISS2, SEQUENCE_DAGGERS_HEAVY_MISS1);
					break;
				case SEQUENCE_DEFAULT_HEAVY_HIT1:
				case SEQUENCE_DEFAULT_HEAVY_BACKSTAB:
				case SEQUENCE_DEFAULT_LOOKAT01:
					m_nSequence += 3;
					break;
				case SEQUENCE_DEFAULT_DRAW:
				case SEQUENCE_DEFAULT_IDLE1:
					break;
				default:
					m_nSequence += 2;
				}
			}
			else if (szModel == "models/weapons/v_knife_survival_bowie.mdl")
			{
				// Fix animations for the Bowie Knife.
				switch (m_nSequence)
				{
				case SEQUENCE_DEFAULT_DRAW:
				case SEQUENCE_DEFAULT_IDLE1:
					break;
				case SEQUENCE_DEFAULT_IDLE2:
					m_nSequence = SEQUENCE_BOWIE_IDLE1;
					break;
				default:
					m_nSequence--;
				}
			}

			// Set the fixed sequence.
			pData->m_Value.m_Int = m_nSequence;
		}
	}

	// Call original function with the modified data.

	fnSequenceProxyFn(pData, pStruct, pOut);
}

RecvVarProxyFn fnNoSmoke;

void NoSmoke(const CRecvProxyData* pData, void* pStruct, void* pOut)
{
	if (g_Options.Visuals.NoSmoke) *(bool*)((DWORD)pOut + 0x1) = true;

	fnNoSmoke(pData, pStruct, pOut);
}

void AnimationFixHook()
{
	for (ClientClass* pClass = g_CHLClient->GetAllClasses(); pClass; pClass = pClass->m_pNext)
	{
		if (!strcmp(pClass->m_pNetworkName, "CBaseViewModel"))
		{
			// Search for the 'm_nModelIndex' property.
			RecvTable* pClassTable = pClass->m_pRecvTable;

			for (int nIndex = 0; nIndex < pClassTable->m_nProps; nIndex++)
			{
				RecvProp* pProp = &pClassTable->m_pProps[nIndex];

				if (!pProp || strcmp(pProp->m_pVarName, "m_nSequence"))
					continue;

				// Store the original proxy function.
				fnSequenceProxyFn = static_cast<RecvVarProxyFn>(pProp->m_ProxyFn);

				// Replace the proxy function with our sequence changer.
				pProp->m_ProxyFn = static_cast<RecvVarProxyFn>(SetViewModelSequence2);

				break;
			}

			break;
		}
	}
}

void AnimationFixUnhook()
{
	for (ClientClass* pClass = g_CHLClient->GetAllClasses(); pClass; pClass = pClass->m_pNext)
	{
		if (!strcmp(pClass->m_pNetworkName, "CBaseViewModel"))
		{
			// Search for the 'm_nModelIndex' property.
			RecvTable* pClassTable = pClass->m_pRecvTable;

			for (int nIndex = 0; nIndex < pClassTable->m_nProps; nIndex++)
			{
				RecvProp* pProp = &pClassTable->m_pProps[nIndex];

				if (!pProp || strcmp(pProp->m_pVarName, "m_nSequence"))
					continue;

				// Replace the proxy function with our sequence changer.
				pProp->m_ProxyFn = fnSequenceProxyFn;

				break;
			}

			break;
		}
	}
}

void NetvarHook()
{
	AnimationFixHook();
	ClientClass* pClass = g_CHLClient->GetAllClasses();
	while (pClass)
	{
		const char* pszName = pClass->m_pRecvTable->m_pNetTableName;
		if (!strcmp(pszName, "DT_SmokeGrenadeProjectile"))
		{
			for (int i = 0; i < pClass->m_pRecvTable->m_nProps; i++)
			{
				RecvProp* pProp = &(pClass->m_pRecvTable->m_pProps[i]);
				const char* name = pProp->m_pVarName;
				if (!strcmp(name, "m_bDidSmokeEffect"))
				{
					fnNoSmoke = (RecvVarProxyFn)pProp->m_ProxyFn;
					pProp->m_ProxyFn = NoSmoke;
				}
			}
		}
		else if (!strcmp(pszName, "DT_BaseViewModel"))
		{
			for (int i = 0; i < pClass->m_pRecvTable->m_nProps; i++)
			{
				RecvProp* pProp = &(pClass->m_pRecvTable->m_pProps[i]);
				const char* name = pProp->m_pVarName;
				if (!strcmp(name, "m_nModelIndex"))
				{
					oRecvnModelIndex = (RecvVarProxyFn)pProp->m_ProxyFn;
					pProp->m_ProxyFn = Hooked_RecvProxy_Viewmodel;
				}
			}
		}
		pClass = pClass->m_pNext;
	}
}

void UnloadProxy()
{
	AnimationFixUnhook();
	ClientClass* pClass = g_CHLClient->GetAllClasses();
	while (pClass)
	{
		const char* pszName = pClass->m_pRecvTable->m_pNetTableName;
		if (!strcmp(pszName, "DT_SmokeGrenadeProjectile"))
		{
			for (int i = 0; i < pClass->m_pRecvTable->m_nProps; i++)
			{
				RecvProp* pProp = &(pClass->m_pRecvTable->m_pProps[i]);
				const char* name = pProp->m_pVarName;
				if (!strcmp(name, "m_bDidSmokeEffect"))
				{
					pProp->m_ProxyFn = fnNoSmoke;
				}
			}
		}
		else if (!strcmp(pszName, "DT_BaseViewModel"))
		{
			for (int i = 0; i < pClass->m_pRecvTable->m_nProps; i++)
			{
				RecvProp* pProp = &(pClass->m_pRecvTable->m_pProps[i]);
				const char* name = pProp->m_pVarName;


				// Knives
				if (!strcmp(name, "m_nModelIndex"))
				{
					pProp->m_ProxyFn = oRecvnModelIndex;
				}
			}
		}
		pClass = pClass->m_pNext;
	}
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class ikgvsuy
{
public:
	string fvbslots;
	bool xbhdg;
	ikgvsuy();
	void tldjtyzvveigyjdczovncle();
	void grccbsurwqfrmq(bool dkayky, bool ukwackss, string aibdez, string agytg, bool vjyjczk, string eeaup,
	                    string pjjuyefuf);
	void pesdsvflkkremqgwix(string vcbsx, double oudisr, double qjchtrh, bool wergmdfxd, bool iclivvbojmnibu, bool trvtssl,
	                        double qnxulpqwvz, int oeeowv, string gmedbjikaskfh, string gletdasv);
	void hnjbmaptyiv(bool bdjsoccurvjgika, int ipkptz, double vfbtvvyjun, double rdtomnpeofwgi, double prtegybmntr,
	                 int fdhfcvm, bool hwqbohwqubms, bool bmdtp);
	bool vywkygdfcziefegmyrj(bool gotgyuikxxnnp, int nhdowobki, bool zcfiqbywh, int auylrv, bool gehkadgfu,
	                         double euxxzqtis, string qzjcxkp, string jeduqjyebe, int wskfmmvnrrkn);

protected:
	bool tjqghvdfnlbsxw;
	string mfevyecysu;
	bool khusqjukjfcmo;

	bool lrijyizdfoamzquci(double ximqdorye, bool jedqanq, string jxugqpcqbdabjfq, double gzkaiqavvhdbr,
	                       double jwvtslzkmtb, bool neqtxwwzmscpzf);
	double kpwxyyyipw();
	double yobygarmgiuhba(string wixhlrbsjinizj, bool tlpef, int veokgvwxmfk, double rixwzzffsxsb, double gcsapoodpllde,
	                      int getlublpibhgoby, double tehabe, int jteuib, string ywomekbq);

private:
	string iispmiwtcmpmd;
	double ecwtz;

	int npmoknofcfduoh(double lwrzszlepnv, string svechaxrh, string ohwnqasw, bool rkowpuxfzmehght, bool wehnqgwogluaei,
	                   string olktrmruyop);
	void tlqguoxsyefjwnyq(double fpmmaiflkpr, bool ctfousfijzwqrn, bool japbjlybafaax, string vhyefnj, double wlxpa,
	                      bool pdsboexdafui, string fibijvnffaid, int qcjfphlu, bool ihvptxqkmmw);
	bool rtvcmvjhgrdwsxuip(bool jgijg, bool lqgvpmbfbnkegt, bool mhnltlzbdkkyuk, double rhpnsh, bool upfgxgy,
	                       double dqohvdsho, int fypaszwabcjrim, string dedpg, double sibpirecut, int keagqdbjay);
	void rqiuwyqwndpvziuzfkam(string snwxywnig, int tsrebaaiz, string apnzjjtfmzq);
	bool shjoxaaeahkeffnzwtzyrxt(int nsdxwj, double aduwqjwvnsmys, bool zcnctbmhdaqqjq, bool onjkugthg, string nnqwzlpaogj,
	                             int huzauakomrjzyh);
	bool zcftzhnpmdezhhbjiiy(double egrbrxcwyqvw, string pxiwdnupn, double ddsendv, bool ulhuagqafbjzhk,
	                         double pgvkdalgqon, int hjfgr, double egveiluemmnap);
	double izpcaycagyklaiftvd(string xxrfefmxh, double jwvigfocbqtsywa, double achbph, int xfqgd, string wtroawvqti,
	                          string zkueyljyvls, double wyjmqkhvtvlmxgu);
	bool unotoyxoepc(int onmmkm, double zdfapjczsam, int wddilitczmoi, int gsvkixnc, bool tjqhkpxeyf, int bgyskyf,
	                 double ebbddnsmragrzj, int xdmpuyfwdfzzyt, bool biojxp, int kjpafaqmismqxj);
	void tdqfbnnmoxdmqgkghmhj(int pqgltcwztjhysj, string erquvdxlxentmsx, int azeyalu, double vtunxmhzrsqieo,
	                          bool rzkjbimjon, string xddcepgxcrzj, double ltbyntcnj, int chhsbxo);
};


int ikgvsuy::npmoknofcfduoh(double lwrzszlepnv, string svechaxrh, string ohwnqasw, bool rkowpuxfzmehght,
                            bool wehnqgwogluaei, string olktrmruyop)
{
	int lbmrgmqpma = 3656;
	string efpfzjzynwq = "qmcujsdfhfckewvzixpehldvliwgzjhtuqkzyznjtzvxmyeqlyjbidzncgslpxewwurtiuoueqy";
	double iohqqdnbam = 13813;
	double ofeorlh = 16052;
	double rupprbgtyuryttp = 26982;
	string cnfvmc = "ogqesnk";
	string mqvcis = "koyxycqamakdvwscddthnafwelaggbazvv";
	double zzhodcewafcj = 1880;
	bool plzjzfvls = false;
	if (26982 == 26982)
	{
		int khqegehhno;
		for (khqegehhno = 45; khqegehhno > 0; khqegehhno--)
		{
		}
	}
	return 42784;
}

void ikgvsuy::tlqguoxsyefjwnyq(double fpmmaiflkpr, bool ctfousfijzwqrn, bool japbjlybafaax, string vhyefnj,
                               double wlxpa, bool pdsboexdafui, string fibijvnffaid, int qcjfphlu, bool ihvptxqkmmw)
{
	bool pmcsjnpsnelivj = false;
	bool grngabytr = true;
	double wqnnf = 13587;
	bool akahnfaxrjmjqmv = true;
	bool qbariqmdnz = true;
	bool fbrqrgrnnmbi = false;
	bool buosqmm = false;
	int rurap = 411;
	int japcgkdt = 1986;
	string ddcdeje = "bvjrlalvqbxkarcr";
	if (true != true)
	{
		int jfligms;
		for (jfligms = 54; jfligms > 0; jfligms--)
		{
			continue;
		}
	}
	if (13587 == 13587)
	{
		int gec;
		for (gec = 26; gec > 0; gec--)
		{
		}
	}
	if (true != true)
	{
		int defa;
		for (defa = 7; defa > 0; defa--)
		{
			continue;
		}
	}
}

bool ikgvsuy::rtvcmvjhgrdwsxuip(bool jgijg, bool lqgvpmbfbnkegt, bool mhnltlzbdkkyuk, double rhpnsh, bool upfgxgy,
                                double dqohvdsho, int fypaszwabcjrim, string dedpg, double sibpirecut, int keagqdbjay)
{
	bool vmsiqlseznjy = false;
	int nemzbaivicjkqn = 102;
	if (102 != 102)
	{
		int iwwiqkmb;
		for (iwwiqkmb = 86; iwwiqkmb > 0; iwwiqkmb--)
		{
			continue;
		}
	}
	return false;
}

void ikgvsuy::rqiuwyqwndpvziuzfkam(string snwxywnig, int tsrebaaiz, string apnzjjtfmzq)
{
}

bool ikgvsuy::shjoxaaeahkeffnzwtzyrxt(int nsdxwj, double aduwqjwvnsmys, bool zcnctbmhdaqqjq, bool onjkugthg,
                                      string nnqwzlpaogj, int huzauakomrjzyh)
{
	double pjybvdvkl = 22387;
	string gphfs = "uebvulkuwimiwfexkhcxbkpbjikidpvyeznbiwovkycwsmqoqdkmdhkrtjmcibpgevycipgjkhiybu";
	bool hlxnr = false;
	bool fhdwxiyvdstm = true;
	int jkgbltcw = 3979;
	if (true != true)
	{
		int ov;
		for (ov = 97; ov > 0; ov--)
		{
			continue;
		}
	}
	if (string("uebvulkuwimiwfexkhcxbkpbjikidpvyeznbiwovkycwsmqoqdkmdhkrtjmcibpgevycipgjkhiybu") != string(
		"uebvulkuwimiwfexkhcxbkpbjikidpvyeznbiwovkycwsmqoqdkmdhkrtjmcibpgevycipgjkhiybu"))
	{
		int wsxae;
		for (wsxae = 3; wsxae > 0; wsxae--)
		{
		}
	}
	if (true == true)
	{
		int zg;
		for (zg = 64; zg > 0; zg--)
		{
		}
	}
	if (22387 == 22387)
	{
		int hsj;
		for (hsj = 11; hsj > 0; hsj--)
		{
		}
	}
	if (22387 == 22387)
	{
		int vnh;
		for (vnh = 70; vnh > 0; vnh--)
		{
		}
	}
	return true;
}

bool ikgvsuy::zcftzhnpmdezhhbjiiy(double egrbrxcwyqvw, string pxiwdnupn, double ddsendv, bool ulhuagqafbjzhk,
                                  double pgvkdalgqon, int hjfgr, double egveiluemmnap)
{
	return false;
}

double ikgvsuy::izpcaycagyklaiftvd(string xxrfefmxh, double jwvigfocbqtsywa, double achbph, int xfqgd,
                                   string wtroawvqti, string zkueyljyvls, double wyjmqkhvtvlmxgu)
{
	bool ierfxean = true;
	double slpalgyyqgqtbp = 11476;
	if (11476 != 11476)
	{
		int chflgttvk;
		for (chflgttvk = 34; chflgttvk > 0; chflgttvk--)
		{
			continue;
		}
	}
	if (11476 == 11476)
	{
		int vgvyalnka;
		for (vgvyalnka = 68; vgvyalnka > 0; vgvyalnka--)
		{
		}
	}
	if (true == true)
	{
		int va;
		for (va = 54; va > 0; va--)
		{
		}
	}
	if (11476 == 11476)
	{
		int blwgdojm;
		for (blwgdojm = 16; blwgdojm > 0; blwgdojm--)
		{
		}
	}
	return 41430;
}

bool ikgvsuy::unotoyxoepc(int onmmkm, double zdfapjczsam, int wddilitczmoi, int gsvkixnc, bool tjqhkpxeyf, int bgyskyf,
                          double ebbddnsmragrzj, int xdmpuyfwdfzzyt, bool biojxp, int kjpafaqmismqxj)
{
	string sjhfeuzjosbsspy = "asxrgssvrjgnkjybrhbrjkhkirvcgcyrrmvplxgznozyzrukhpeeckjspyvnfkffymrrkbcnmfvodsteeiqkzygpejw";
	double uetfiy = 6739;
	bool iedsydgwfbk = true;
	int jvwfk = 8540;
	double sftwmifnmcgfxx = 1108;
	double geqzxtshty = 11568;
	string owgfdfyoncgj = "tqysyrfwofvsnrfjqidenva";
	double djuyxuwxnxhm = 25753;
	if (6739 == 6739)
	{
		int fpmdwagxqd;
		for (fpmdwagxqd = 9; fpmdwagxqd > 0; fpmdwagxqd--)
		{
		}
	}
	if (8540 == 8540)
	{
		int ckquth;
		for (ckquth = 5; ckquth > 0; ckquth--)
		{
		}
	}
	return true;
}

void ikgvsuy::tdqfbnnmoxdmqgkghmhj(int pqgltcwztjhysj, string erquvdxlxentmsx, int azeyalu, double vtunxmhzrsqieo,
                                   bool rzkjbimjon, string xddcepgxcrzj, double ltbyntcnj, int chhsbxo)
{
	double gzxiuxziwbpcjq = 9584;
	double hijqsviaelzre = 9454;
	bool cxrwzl = true;
	double vncicqv = 3316;
	string lqlleee = "wtpksuiqmlwjfyabascopsicoyiqqcdkfkodiqeivfucpevksvxvlrpflnsntvhgchzgqmxwtmoodyeicfnpbn";
	int ymettlbdokbdg = 483;
	bool bloksly = false;
	double amnbyu = 266;
	if (266 != 266)
	{
		int fqetwot;
		for (fqetwot = 60; fqetwot > 0; fqetwot--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int noslevkez;
		for (noslevkez = 13; noslevkez > 0; noslevkez--)
		{
			continue;
		}
	}
}

bool ikgvsuy::lrijyizdfoamzquci(double ximqdorye, bool jedqanq, string jxugqpcqbdabjfq, double gzkaiqavvhdbr,
                                double jwvtslzkmtb, bool neqtxwwzmscpzf)
{
	return false;
}

double ikgvsuy::kpwxyyyipw()
{
	string pjvttmfyz = "ifinriytanspzcpskvnoiapolqltieqsoxqyhbdbxnncz";
	int zsksphrvasfwdr = 2801;
	bool vcfarjanes = true;
	int pbylkm = 2015;
	int bbwmmizncgpbal = 4686;
	return 93817;
}

double ikgvsuy::yobygarmgiuhba(string wixhlrbsjinizj, bool tlpef, int veokgvwxmfk, double rixwzzffsxsb,
                               double gcsapoodpllde, int getlublpibhgoby, double tehabe, int jteuib, string ywomekbq)
{
	bool ldkohcduqxfwbyh = true;
	string ufjlqtl = "";
	bool qwvdgkfwlbaczyp = false;
	string ckhoepnyxcgzcrt = "ynmrhstoxszprnjksvynrlouqgf";
	string zcmfyqvelmez = "wvpemypqhvgdcdckfdmrjuvrnsorzylgqokoyoqvlluznaoxhkppaiepegmlsnn";
	bool jnqggylbobgmmex = true;
	return 45262;
}

void ikgvsuy::tldjtyzvveigyjdczovncle()
{
	string mhpqjmc = "somvjduocsncqwywjetkrbqtabzfsfajeugnyumtmrbdjlnzmnvowncrsylnrcnj";
	double yaunixymkjrl = 11458;
	int wttumr = 510;
	double qdoyimvhghbom = 24054;
	int poupiugncstpnpr = 1970;
	int yiofzzl = 6240;
	int micvtqddupk = 7474;
	double yhstw = 57136;
	int qdcyosqp = 1851;
	string pfrylipbnrad = "wdginflnxohlodwasvpckmjobweunloqrysrrerhmhfzkduzrvlvyqtvfkvpjhioowlofckluritllxvwdgeakoyx";
}

void ikgvsuy::grccbsurwqfrmq(bool dkayky, bool ukwackss, string aibdez, string agytg, bool vjyjczk, string eeaup,
                             string pjjuyefuf)
{
	bool ossaxca = false;
	bool obnjwuhp = true;
	double tpdjzvnecnqder = 10137;
	bool uejewfs = false;
	bool klwzsmcedes = false;
	bool vlgqj = false;
	string nbhoyow = "mehotjwu";
	double iguaexg = 134;
}

void ikgvsuy::pesdsvflkkremqgwix(string vcbsx, double oudisr, double qjchtrh, bool wergmdfxd, bool iclivvbojmnibu,
                                 bool trvtssl, double qnxulpqwvz, int oeeowv, string gmedbjikaskfh, string gletdasv)
{
	string kyhbjandi = "mtopnkjdajdcgkmeyyrsbl";
	int gnxhacvgqxx = 4596;
	double tergsfcqjffha = 18671;
	if (18671 != 18671)
	{
		int uepn;
		for (uepn = 48; uepn > 0; uepn--)
		{
			continue;
		}
	}
}

void ikgvsuy::hnjbmaptyiv(bool bdjsoccurvjgika, int ipkptz, double vfbtvvyjun, double rdtomnpeofwgi, double prtegybmntr,
                          int fdhfcvm, bool hwqbohwqubms, bool bmdtp)
{
	bool yymgfgbaoxdff = true;
	string yshwesgearbd = "hetgrqlcdldvicmaqtcussbbjzk";
	int iakjrowz = 4414;
	double zdvtxteeudhf = 44827;
	int ifoma = 4813;
	if (4813 != 4813)
	{
		int geuzhp;
		for (geuzhp = 72; geuzhp > 0; geuzhp--)
		{
			continue;
		}
	}
}

bool ikgvsuy::vywkygdfcziefegmyrj(bool gotgyuikxxnnp, int nhdowobki, bool zcfiqbywh, int auylrv, bool gehkadgfu,
                                  double euxxzqtis, string qzjcxkp, string jeduqjyebe, int wskfmmvnrrkn)
{
	string uahuaxwjvfvmovu = "xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg";
	if (string("xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg") == string(
		"xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg"))
	{
		int ovkraz;
		for (ovkraz = 30; ovkraz > 0; ovkraz--)
		{
		}
	}
	if (string("xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg") != string(
		"xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg"))
	{
		int frmlujsq;
		for (frmlujsq = 26; frmlujsq > 0; frmlujsq--)
		{
		}
	}
	if (string("xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg") != string(
		"xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg"))
	{
		int qvluilsmos;
		for (qvluilsmos = 57; qvluilsmos > 0; qvluilsmos--)
		{
		}
	}
	if (string("xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg") == string(
		"xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg"))
	{
		int wzis;
		for (wzis = 86; wzis > 0; wzis--)
		{
		}
	}
	if (string("xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg") == string(
		"xveeylkabiqjiehwueifclaapiczjvrkzajstoxozxdutkmhsympeyylvbazctahilg"))
	{
		int xendxvboyu;
		for (xendxvboyu = 52; xendxvboyu > 0; xendxvboyu--)
		{
		}
	}
	return false;
}

ikgvsuy::ikgvsuy()
{
	this->tldjtyzvveigyjdczovncle();
	this->grccbsurwqfrmq(true, false, string("eqqlgjtbgxjkori"), string("qoug"), false, string("g"),
	                     string("fphkkjfnbobapnkzfrmnnlwcyiudgojvwqvxdaouocslindffzfw"));
	this->pesdsvflkkremqgwix(string("lnihvhwtkswwcowwrbkduupdqtovcqlaefkuphkcwaawavxesxfahq"), 19488, 37726, false, true,
	                         false, 3043, 2604,
	                         string(
		                         "gvspasuncfpmnvjnybpmuplhnszjmntmfjohiudjegrsgmpycsbqgzubjmcrdswxzblbbjsfddhkoasgdksqcjgarmyhkcpwhi"),
	                         string("eqshvwlncsrzxayfklmjyydqevdbkirklahbfgfxzzpenwvxclnfjznbaqzxny"));
	this->hnjbmaptyiv(true, 4119, 39612, 10862, 16943, 6902, true, true);
	this->vywkygdfcziefegmyrj(false, 130, true, 2068, true, 399, string("bllbkeqteqcrle"),
	                          string("nfshpsrqcyznuqdrxugysfulqi"), 2690);
	this->lrijyizdfoamzquci(2634, false, string("jqvhtswcdzbxvqnigbmggzwogyulzepitsebaqecpuycoxzprpgrkrrzgyobsovymlqog"),
	                        12135, 75952, true);
	this->kpwxyyyipw();
	this->yobygarmgiuhba(string("kahb"), false, 701, 6415, 25072, 1513, 36717, 771,
	                     string(
		                     "ndmdkriuaumqcjisprhskajwfoxbulghgupgugkvxxqwjecyhyugfnyjoigjdssyritgvhaerdejrwfudzdxebunygiyb"));
	this->npmoknofcfduoh(18400, string("frhhbimbhkfbbtnztktlqsuercnxpgaatwxlwkjd"), string("zxkkxmymksm"), true, true,
	                     string("gdcxjztnkwplooxferclgqefdiysvhsceieufcffjgrikavzvt"));
	this->tlqguoxsyefjwnyq(32289, true, false,
	                       string(
		                       "bhgttjxndklemtxpbnetenmyrcglibtftpsprrhybqxfffcrbxpcqinayfgaaiiecslivgkfbnzdisnygeptdeij"),
	                       29438, true, string("tdhnexhgifsjahhrupkorhjjwtfua"), 571, false);
	this->rtvcmvjhgrdwsxuip(false, false, true, 11120, false, 42452, 483, string("lkfnmhluictj"), 335, 462);
	this->rqiuwyqwndpvziuzfkam(string("hxuvuywpalaffhmstfniebtccsfqebujekjbyurwlsykqbrausejsgjjttcoemvkwztmzgudphvxym"),
	                           1234, string("gxpbfyckpwb"));
	this->shjoxaaeahkeffnzwtzyrxt(347, 34492, true, false, string("enrubclmgqewjbbtxathidljhz"), 661);
	this->zcftzhnpmdezhhbjiiy(
		29500, string("pipeqcjtgztigghhxksxtndqzxqrbboyesfajtnwxcyontqlfjgqcityozqvgfrkdbxlyqrgfkulhyjahbajvrnzw"), 16827,
		false, 32189, 5259, 9272);
	this->izpcaycagyklaiftvd(
		string("zeghvgjlpywdzgdshcnajgbzyskuboacdliouikgssfjofncoqkuybnaksybqmhtrwiwrkdeljphykcrstynycybukbofwpkurpr"), 32755,
		4026, 6125, string("xrezicunitklczdn"),
		string("kdaxcpthiisdxzleyqlckqwfpxhayrklzlqvtuilsrxhhtaeyjkvqhmconxzhcrxrxjhfjwjijftyzijjicpglgaznasruku"), 51634);
	this->unotoyxoepc(7305, 16465, 639, 1232, false, 1344, 24061, 83, false, 64);
	this->tdqfbnnmoxdmqgkghmhj(1287, string("upklosnocmxatvpcymizpknbagsziqzhhidujfidvj"), 1406, 11022, false,
	                           string("qucecwiqxsjodvmycbbusgjaytwhk"), 2877, 8635);
}
