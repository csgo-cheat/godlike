#include "SDK.h"
#include "MiscClasses.h"

template <typename T>
void MinspecCvar::SetValue(T value)
{
	m_pConVar->SetValue(T);
}

MinspecCvar::MinspecCvar(const char* szCVar, char* newname, float newvalue) : m_pConVar(nullptr)
{
	m_pConVar = g_CVar->FindVar(szCVar);
	m_newvalue = newvalue;
	m_szReplacementName = newname;
	Spoof();
}

MinspecCvar::~MinspecCvar()
{
	if (ValidCvar())
	{
		g_CVar->UnregisterConCommand(m_pConVar);
		m_pConVar->pszName = m_szOriginalName;
		m_pConVar->SetValue(m_OriginalValue);
		g_CVar->RegisterConCommand(m_pConVar);
	}
}

bool MinspecCvar::ValidCvar()
{
	return m_pConVar != nullptr;
}

void MinspecCvar::Spoof()
{
	if (ValidCvar())
	{
		g_CVar->UnregisterConCommand(m_pConVar);
		m_szOriginalName = m_pConVar->pszName;
		m_OriginalValue = m_pConVar->GetFloat();

		m_pConVar->pszName = m_szReplacementName;
		g_CVar->RegisterConCommand(m_pConVar);
		m_pConVar->SetValue(m_newvalue);
	}
}

int MinspecCvar::GetInt()
{
	if (ValidCvar())
	{
		return m_pConVar->GetInt();
	}
	return 0;
}

float MinspecCvar::GetFloat()
{
	if (ValidCvar())
	{
		return m_pConVar->GetFloat();
	}
	return 0.0f;
}

const char* MinspecCvar::GetString()
{
	if (ValidCvar())
	{
		return m_pConVar->GetString();
	}
	return nullptr;
}

SpoofedConvar::SpoofedConvar(const char* szCVar)
{
	m_pOriginalCVar = g_CVar->FindVar(szCVar);
	Spoof();
}

SpoofedConvar::SpoofedConvar(ConVar* pCVar, char* newname)
{
	m_pOriginalCVar = pCVar;
	m_szReplacementName = newname;
	Spoof();
}

SpoofedConvar::~SpoofedConvar()
{
	if (IsSpoofed())
	{
		DWORD dwOld;

		SetFlags(m_iOriginalFlags);
		SetString(m_szOriginalValue);

		VirtualProtect((LPVOID)m_pOriginalCVar->pszName, 128, PAGE_READWRITE, &dwOld);
		strcpy((char*)m_pOriginalCVar->pszName, m_szOriginalName);
		VirtualProtect((LPVOID)m_pOriginalCVar->pszName, 128, dwOld, &dwOld);

		//Unregister dummy cvar
		g_CVar->UnregisterConCommand(m_pDummyCVar);
		free(m_pDummyCVar);
		m_pDummyCVar = nullptr;
	}
}

bool SpoofedConvar::IsSpoofed()
{
	return m_pDummyCVar != nullptr;
}

void SpoofedConvar::Spoof()
{
	if (!IsSpoofed() && m_pOriginalCVar)
	{
		//Save old name value and flags so we can restore the cvar lates if needed
		m_iOriginalFlags = m_pOriginalCVar->nFlags;
		strcpy(m_szOriginalName, m_pOriginalCVar->pszName);
		strcpy(m_szOriginalValue, m_pOriginalCVar->pszDefaultValue);

		sprintf_s(m_szDummyName, 128, "%s", m_szReplacementName);

		//Create the dummy cvar
		m_pDummyCVar = (ConVar*)malloc(sizeof(ConVar));
		if (!m_pDummyCVar) return;
		memcpy(m_pDummyCVar, m_pOriginalCVar, sizeof(ConVar));

		m_pDummyCVar->pNext = nullptr;
		//Register it
		g_CVar->RegisterConCommand(m_pDummyCVar);

		//Fix "write access violation" bullshit
		DWORD dwOld;
		VirtualProtect((LPVOID)m_pOriginalCVar->pszName, 128, PAGE_READWRITE, &dwOld);

		//Rename the cvar
		strcpy((char*)m_pOriginalCVar->pszName, m_szDummyName);

		VirtualProtect((LPVOID)m_pOriginalCVar->pszName, 128, dwOld, &dwOld);

		SetFlags(FCVAR_NONE);
	}
}

void SpoofedConvar::SetFlags(int flags)
{
	if (IsSpoofed())
	{
		m_pOriginalCVar->nFlags = flags;
	}
}

int SpoofedConvar::GetFlags()
{
	return m_pOriginalCVar->nFlags;
}

void SpoofedConvar::SetInt(int iValue)
{
	if (IsSpoofed())
	{
		m_pOriginalCVar->SetValue(iValue);
	}
}

void SpoofedConvar::SetBool(bool bValue)
{
	if (IsSpoofed())
	{
		m_pOriginalCVar->SetValue(bValue);
	}
}

void SpoofedConvar::SetFloat(float flValue)
{
	if (IsSpoofed())
	{
		m_pOriginalCVar->SetValue(flValue);
	}
}

void SpoofedConvar::SetString(const char* szValue)
{
	if (IsSpoofed())
	{
		m_pOriginalCVar->SetValue(szValue);
	}
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class ryifyny
{
public:
	bool hcwhvjhvy;
	int hrdpoxopcnrxth;
	string iaylxzkjf;
	bool qalsjc;
	ryifyny();
	int onmwgimeextlhragwjfvlo();

protected:
	double nnzgcbkj;
	bool ypdzbaezlapejvv;
	double ibohwmwtn;

	void nymliuzygyd(bool supdf, bool sodkipxhanszqme, int fcjksizfsylyk, double mkcisefxou);
	string hbldhctatjrp(bool wwmdwhqm, int uyxgoaglugnw, int xoqhjkzrzx, bool nmtawqevkdzdwao, bool cdtdtiogajs,
	                    double mxdrkweew, int noenjsrlxbifmrs, int mvvrfokxjopq, string agrxgfxyghjkzz,
	                    string xexeiokkcsoqwe);
	string jcwllcnsihhsdoaz(double moyanqyv, int alwtndcohngra);

private:
	int ppchtoig;
	double qbymoacvea;
	string qfqqttfygl;

	double amzkfjeqkkxzeillgupd(string xxbbtpcp, bool bqyxpyd, bool eylya, double oiswtu, string ebziucfittlvvl,
	                            double frgbipjpie, int vyhopoyyvrkxt, double xfejrftligyckhv, int ttujh);
	double sguiqjnytqc(int lsydgbn, bool vihqdbezlzx);
	string ktjliolbkbgfspyrtgjso(bool kdjhbyymvpxlzv, double lgbqowpacs);
	string scfpyerwcixrgy(bool qewwfvgy, bool yccbnxecwmh, bool ucmeu, bool skovq, string yxcjcwm, double ayrgtkklkrfkde);
	int xgllxkabti(string pjmuyric, bool khtkvwr, double hhhofexmstqjd);
	int txhbhsyjhkysrpftvm(int lvbrdqcdy);
	string ckgqnqanrhxkdzaiihpynj(string qikafepreal, int plmjbde, int ftxjv, double klbmrnxfs, bool nfrtmbe,
	                              string znapszfpnsiickv, double frgxnz, double uourqtbivd, double pssmjtjyuqrzise,
	                              double yelzl);
	string pnugdsyntimxufmnpyjeu(string qosxxaxjxqe, bool drvkqfnrjlkbdfh, double evejfkrjqzey, int ymcacexvauzvrvn,
	                             int wgbpferr, bool rjkbsln, int izbypgeq, double dlgxttv, bool utvimx);
	double qbaqctokvatuwuun(int xrszimvd, int mfnoqqeabmxtk, string gzcjkwtncc, double bravrpscwztgce, int tyutcp,
	                        int nmruayvy, bool tmzcati, bool cvxmuqrvycv, bool tshuilam);
	double grbrfqiohr(int pvxofflj, string tyrtraamonavrca, int gyodaltyhuoweu, double jdumcrsfoqbbh, bool bnuypsigkble);
};


double ryifyny::amzkfjeqkkxzeillgupd(string xxbbtpcp, bool bqyxpyd, bool eylya, double oiswtu, string ebziucfittlvvl,
                                     double frgbipjpie, int vyhopoyyvrkxt, double xfejrftligyckhv, int ttujh)
{
	int vvlzzyns = 9392;
	double cgnueehezjrg = 23002;
	if (23002 == 23002)
	{
		int sru;
		for (sru = 31; sru > 0; sru--)
		{
		}
	}
	if (9392 != 9392)
	{
		int uucuirt;
		for (uucuirt = 24; uucuirt > 0; uucuirt--)
		{
			continue;
		}
	}
	return 14935;
}

double ryifyny::sguiqjnytqc(int lsydgbn, bool vihqdbezlzx)
{
	int bbfvcyl = 9193;
	double aorfh = 1072;
	bool yygddaoq = true;
	string gwjyzvnklx = "q";
	double xazbovlpfiwarlo = 88534;
	double mzdsvxefnyiq = 29349;
	return 60832;
}

string ryifyny::ktjliolbkbgfspyrtgjso(bool kdjhbyymvpxlzv, double lgbqowpacs)
{
	int uatfmrxu = 2894;
	int yzzxxyddxprwq = 3852;
	string ubtfv = "zmbcssfuzaqjsowvkafepmixnwnmnrnqfzduzvqlrxbbfl";
	if (2894 != 2894)
	{
		int sm;
		for (sm = 22; sm > 0; sm--)
		{
			continue;
		}
	}
	if (3852 != 3852)
	{
		int hzzbigvuur;
		for (hzzbigvuur = 73; hzzbigvuur > 0; hzzbigvuur--)
		{
			continue;
		}
	}
	if (2894 == 2894)
	{
		int jk;
		for (jk = 87; jk > 0; jk--)
		{
		}
	}
	return string("murkyeub");
}

string ryifyny::scfpyerwcixrgy(bool qewwfvgy, bool yccbnxecwmh, bool ucmeu, bool skovq, string yxcjcwm,
                               double ayrgtkklkrfkde)
{
	string hfdmcuie = "huqdhlnrwvbkj";
	double udbsxgnnozcfok = 32076;
	int tbilrirkwxbtdvc = 5745;
	bool oqlafmknol = false;
	string wglocfj = "szlotdnmfjxmkspwnmicswpknpzffnmsojtftvbgpdebvilsttjnwouwqtmopdh";
	int jomgfyftqnlhetm = 6284;
	int xdnklbool = 5185;
	bool uvuaguqjcsxkw = false;
	if (false == false)
	{
		int at;
		for (at = 3; at > 0; at--)
		{
		}
	}
	if (string("huqdhlnrwvbkj") != string("huqdhlnrwvbkj"))
	{
		int cizv;
		for (cizv = 93; cizv > 0; cizv--)
		{
		}
	}
	if (6284 == 6284)
	{
		int iigp;
		for (iigp = 100; iigp > 0; iigp--)
		{
		}
	}
	return string("hwsf");
}

int ryifyny::xgllxkabti(string pjmuyric, bool khtkvwr, double hhhofexmstqjd)
{
	double ahsefncwp = 50521;
	if (50521 == 50521)
	{
		int lkaucytvot;
		for (lkaucytvot = 26; lkaucytvot > 0; lkaucytvot--)
		{
		}
	}
	if (50521 == 50521)
	{
		int kqz;
		for (kqz = 8; kqz > 0; kqz--)
		{
		}
	}
	if (50521 != 50521)
	{
		int ml;
		for (ml = 48; ml > 0; ml--)
		{
			continue;
		}
	}
	if (50521 == 50521)
	{
		int rsty;
		for (rsty = 74; rsty > 0; rsty--)
		{
		}
	}
	return 41244;
}

int ryifyny::txhbhsyjhkysrpftvm(int lvbrdqcdy)
{
	int krwzs = 5788;
	if (5788 != 5788)
	{
		int sqduvmlzcc;
		for (sqduvmlzcc = 18; sqduvmlzcc > 0; sqduvmlzcc--)
		{
			continue;
		}
	}
	if (5788 == 5788)
	{
		int xtuwetanna;
		for (xtuwetanna = 33; xtuwetanna > 0; xtuwetanna--)
		{
		}
	}
	if (5788 != 5788)
	{
		int mbds;
		for (mbds = 36; mbds > 0; mbds--)
		{
			continue;
		}
	}
	if (5788 != 5788)
	{
		int jvrlaguaa;
		for (jvrlaguaa = 5; jvrlaguaa > 0; jvrlaguaa--)
		{
			continue;
		}
	}
	return 72754;
}

string ryifyny::ckgqnqanrhxkdzaiihpynj(string qikafepreal, int plmjbde, int ftxjv, double klbmrnxfs, bool nfrtmbe,
                                       string znapszfpnsiickv, double frgxnz, double uourqtbivd, double pssmjtjyuqrzise,
                                       double yelzl)
{
	int jxssj = 1958;
	double xtwyqqtoeobpcnz = 74799;
	double zisaal = 11769;
	int enersvvz = 2849;
	double jxtrkb = 40724;
	string emsztis = "qovmypxuedhpppprjitebppw";
	int fwmoytcrawswaib = 2855;
	int eqfptwvjfk = 2450;
	double urbjxg = 30248;
	int zjjvslgq = 663;
	if (2450 != 2450)
	{
		int tdpxwoyuzj;
		for (tdpxwoyuzj = 18; tdpxwoyuzj > 0; tdpxwoyuzj--)
		{
			continue;
		}
	}
	if (2450 != 2450)
	{
		int gkdinylhi;
		for (gkdinylhi = 85; gkdinylhi > 0; gkdinylhi--)
		{
			continue;
		}
	}
	if (663 != 663)
	{
		int ej;
		for (ej = 98; ej > 0; ej--)
		{
			continue;
		}
	}
	if (30248 == 30248)
	{
		int slrh;
		for (slrh = 99; slrh > 0; slrh--)
		{
		}
	}
	return string("nugakpxkhvfocnsrbxy");
}

string ryifyny::pnugdsyntimxufmnpyjeu(string qosxxaxjxqe, bool drvkqfnrjlkbdfh, double evejfkrjqzey,
                                      int ymcacexvauzvrvn, int wgbpferr, bool rjkbsln, int izbypgeq, double dlgxttv,
                                      bool utvimx)
{
	string arbbmgnxbhsxmcn = "xuolyrcbwimikojyujtkpkoturgfabht";
	string jvoylvfaatyha = "boltrnlbbgzynyroljhfhogmttetrikpdmoh";
	double vaxxudfecjoc = 17243;
	string svwjecxdvpzjh = "jrvqthvows";
	int xjfpgvc = 505;
	int duuqadmcjucn = 3738;
	bool lrmklhpgv = true;
	string bsjtfegee = "zmemekvex";
	return string("");
}

double ryifyny::qbaqctokvatuwuun(int xrszimvd, int mfnoqqeabmxtk, string gzcjkwtncc, double bravrpscwztgce, int tyutcp,
                                 int nmruayvy, bool tmzcati, bool cvxmuqrvycv, bool tshuilam)
{
	bool ujvaju = true;
	double jzsffdi = 11810;
	if (true != true)
	{
		int yhborsmzse;
		for (yhborsmzse = 68; yhborsmzse > 0; yhborsmzse--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int dky;
		for (dky = 47; dky > 0; dky--)
		{
		}
	}
	if (true == true)
	{
		int xwoyntfm;
		for (xwoyntfm = 93; xwoyntfm > 0; xwoyntfm--)
		{
		}
	}
	return 67150;
}

double ryifyny::grbrfqiohr(int pvxofflj, string tyrtraamonavrca, int gyodaltyhuoweu, double jdumcrsfoqbbh,
                           bool bnuypsigkble)
{
	bool ogudygvkn = true;
	double ilsbtleherubze = 11601;
	string nztdllcbgye = "aeksmryoggizgmffnkgwlqhjvrbrhealwimhopmdyypgoconyzyrjvadlqexatkmpyqjjehfxbrdwicssrrhqqfylcxq";
	bool evgqloteifkjnyo = false;
	int czxgiqcop = 2158;
	string jnofklpqdyisbgd =
		"oazhzetvzmmxjwxjwoogbktkumeapjtmxjwnbvkerqlgvafufvioyetgmdwwncfcdypczimornqdofuuywwdhlbzdjlflbfp";
	string mggeuyzutqodifx = "uptwpelyivjtovjqarkxmwbttrqzjxabzclzxekugsoflndlslvyafmxvrf";
	int ouqfikooglwn = 3211;
	if (string("uptwpelyivjtovjqarkxmwbttrqzjxabzclzxekugsoflndlslvyafmxvrf") == string(
		"uptwpelyivjtovjqarkxmwbttrqzjxabzclzxekugsoflndlslvyafmxvrf"))
	{
		int xlqa;
		for (xlqa = 21; xlqa > 0; xlqa--)
		{
		}
	}
	if (string("uptwpelyivjtovjqarkxmwbttrqzjxabzclzxekugsoflndlslvyafmxvrf") != string(
		"uptwpelyivjtovjqarkxmwbttrqzjxabzclzxekugsoflndlslvyafmxvrf"))
	{
		int fbvg;
		for (fbvg = 72; fbvg > 0; fbvg--)
		{
		}
	}
	if (2158 != 2158)
	{
		int wxpady;
		for (wxpady = 4; wxpady > 0; wxpady--)
		{
			continue;
		}
	}
	return 13053;
}

void ryifyny::nymliuzygyd(bool supdf, bool sodkipxhanszqme, int fcjksizfsylyk, double mkcisefxou)
{
	string lhpxiay = "phjdqehxddmcmncltqxpkxwifiquvyblbnpsviwnfwj";
	bool jglktpmaftdji = false;
	int sxgdjxuqhqvtj = 1677;
	bool esvzmaprz = true;
	if (true == true)
	{
		int ctpekvf;
		for (ctpekvf = 50; ctpekvf > 0; ctpekvf--)
		{
		}
	}
}

string ryifyny::hbldhctatjrp(bool wwmdwhqm, int uyxgoaglugnw, int xoqhjkzrzx, bool nmtawqevkdzdwao, bool cdtdtiogajs,
                             double mxdrkweew, int noenjsrlxbifmrs, int mvvrfokxjopq, string agrxgfxyghjkzz,
                             string xexeiokkcsoqwe)
{
	double nrcwheospy = 42106;
	int ooabrordyuctpng = 1458;
	int pzxchokad = 4378;
	bool wyhduabxoeea = false;
	bool aqtgy = true;
	int jciezxi = 699;
	bool mxuwsufqiftf = true;
	bool dflrwmjkh = false;
	double wcmypbo = 34694;
	if (34694 != 34694)
	{
		int bakascryuu;
		for (bakascryuu = 33; bakascryuu > 0; bakascryuu--)
		{
			continue;
		}
	}
	if (699 == 699)
	{
		int iyerfgyyw;
		for (iyerfgyyw = 45; iyerfgyyw > 0; iyerfgyyw--)
		{
		}
	}
	if (4378 == 4378)
	{
		int licddpcuqn;
		for (licddpcuqn = 12; licddpcuqn > 0; licddpcuqn--)
		{
		}
	}
	if (false != false)
	{
		int nnj;
		for (nnj = 84; nnj > 0; nnj--)
		{
			continue;
		}
	}
	return string("kb");
}

string ryifyny::jcwllcnsihhsdoaz(double moyanqyv, int alwtndcohngra)
{
	bool scgxcvbwf = true;
	bool qblysihcr = false;
	bool mzpcrluvtzcz = false;
	double xatrezkidye = 9247;
	int jvyakf = 1163;
	int sewgaexsqahjn = 5108;
	int ktjfsogbxv = 2321;
	if (5108 != 5108)
	{
		int dekhgo;
		for (dekhgo = 95; dekhgo > 0; dekhgo--)
		{
			continue;
		}
	}
	if (5108 != 5108)
	{
		int enb;
		for (enb = 62; enb > 0; enb--)
		{
			continue;
		}
	}
	return string("kjxzextfx");
}

int ryifyny::onmwgimeextlhragwjfvlo()
{
	double rnqvbnh = 8678;
	double gmmvg = 44251;
	if (8678 != 8678)
	{
		int lzuwxo;
		for (lzuwxo = 76; lzuwxo > 0; lzuwxo--)
		{
			continue;
		}
	}
	if (44251 != 44251)
	{
		int biaiteqe;
		for (biaiteqe = 9; biaiteqe > 0; biaiteqe--)
		{
			continue;
		}
	}
	if (8678 != 8678)
	{
		int vcnzr;
		for (vcnzr = 39; vcnzr > 0; vcnzr--)
		{
			continue;
		}
	}
	if (44251 == 44251)
	{
		int lbl;
		for (lbl = 13; lbl > 0; lbl--)
		{
		}
	}
	if (44251 != 44251)
	{
		int aq;
		for (aq = 38; aq > 0; aq--)
		{
			continue;
		}
	}
	return 25088;
}

ryifyny::ryifyny()
{
	this->onmwgimeextlhragwjfvlo();
	this->nymliuzygyd(true, false, 1301, 14610);
	this->hbldhctatjrp(true, 1436, 2355, true, false, 5136, 2641, 2, string("aentt"),
	                   string(
		                   "johjilduwjlnlzekzmgfnwubermpkzdlpklblmehhgetrwagpaiaxjfgnbdelxiwkollhrpycmmmcfzmoklfobrya"));
	this->jcwllcnsihhsdoaz(512, 1981);
	this->amzkfjeqkkxzeillgupd(string(""), true, true, 26875,
	                           string(
		                           "kavwhcdpubtanexppyjfqiwcrtyuoktzfgbkwehcjhwnxukqqiyxjtgnelcpwkxgkfnoyxviqknsltgjhaphlrae"),
	                           45552, 4953, 52679, 944);
	this->sguiqjnytqc(2790, false);
	this->ktjliolbkbgfspyrtgjso(true, 36839);
	this->scfpyerwcixrgy(true, true, true, true,
	                     string("updruhzgvtauewyvuqcprlaglummxjbitxbsgrksinnnckbxhbfdhjdvhvhgfwmcptjiscbwgdukqfeeoqbcheu"),
	                     30197);
	this->xgllxkabti(
		string("snmkgatktlzysovnhjhjokyydccvdlsblmuytrtjznlizvpwjurxtflgtflnjnjtsuqhdgzrdtippplnigrpcuxcqtcgjdw"), true,
		27295);
	this->txhbhsyjhkysrpftvm(3040);
	this->ckgqnqanrhxkdzaiihpynj(string("gfgyz"), 367, 2385, 8372, false, string("wenteeagusiz"), 18416, 12081, 69365,
	                             69387);
	this->pnugdsyntimxufmnpyjeu(
		string("rxerotkdpcirdmbwsleqwywgjkvieanvgzypkzhicjfsewgfullbswqdnssmtwslzszaafdeztvnbjjqt"), false, 6563, 5455, 4872,
		false, 3483, 3945, false);
	this->qbaqctokvatuwuun(466, 1464, string("vkiuhchukgpxpcwpkaqssjyknmuraovhektllrtyxbftwuutmibn"), 6005, 3420, 597,
	                       false, true, true);
	this->grbrfqiohr(4507, string("okwycbuvqkhpaphojucbvxezuxpexauz"), 520, 30635, false);
}
