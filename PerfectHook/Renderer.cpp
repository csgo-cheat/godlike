#include "Renderer.h"

// Allow us to directly call the ImGui WndProc function.
extern LRESULT ImGui_ImplDX9_WndProcHandler(HWND, UINT, WPARAM, LPARAM);

// The original WndProc function.
WNDPROC game_wndproc = nullptr;

// Hooked WndProc function to process all incoming input messages.
LRESULT __stdcall hkWndProc(HWND window, UINT message_type, WPARAM w_param, LPARAM l_param)
{
	switch (message_type)
	{
	case WM_LBUTTONDOWN:
		G::PressedKeys[VK_LBUTTON] = true;
		break;
	case WM_LBUTTONUP:
		G::PressedKeys[VK_LBUTTON] = false;
		break;
	case WM_RBUTTONDOWN:
		G::PressedKeys[VK_RBUTTON] = true;
		break;
	case WM_RBUTTONUP:
		G::PressedKeys[VK_RBUTTON] = false;
		break;
	case WM_MBUTTONDOWN:
		G::PressedKeys[VK_MBUTTON] = true;
		break;
	case WM_MBUTTONUP:
		G::PressedKeys[VK_MBUTTON] = false;
		break;
	case WM_XBUTTONDOWN:
		{
			UINT button = GET_XBUTTON_WPARAM(w_param);
			if (button == XBUTTON1)
			{
				G::PressedKeys[VK_XBUTTON1] = true;
			}
			else if (button == XBUTTON2)
			{
				G::PressedKeys[VK_XBUTTON2] = true;
			}
			break;
		}
	case WM_XBUTTONUP:
		{
			UINT button = GET_XBUTTON_WPARAM(w_param);
			if (button == XBUTTON1)
			{
				G::PressedKeys[VK_XBUTTON1] = false;
			}
			else if (button == XBUTTON2)
			{
				G::PressedKeys[VK_XBUTTON2] = false;
			}
			break;
		}
	case WM_KEYDOWN:
		G::PressedKeys[w_param] = true;
		break;
	case WM_KEYUP:
		G::PressedKeys[w_param] = false;
		break;
	default: break;
	}
	// Let the renderer decide whether we should pass this input message to the game.
	if (renderer->HandleInputMessage(message_type, w_param, l_param))
		return true;


	// The GUI is inactive so pass the input to the game.
	return CallWindowProc(game_wndproc, window, message_type, w_param, l_param);
};

Renderer::~Renderer()
{
	// Restore the original WndProc function.
	SetWindowLongPtr(this->window, GWLP_WNDPROC, LONG_PTR(game_wndproc));
}

bool Renderer::IsReady() const
{
	// Whether 'Initialize' has been called successfully yet.
	return this->ready;
}

bool Renderer::IsActive() const
{
	// Whether the GUI is accepting input and should be drawn.
	return this->ready && this->active;
}

bool Renderer::Initialize(HWND window, IDirect3DDevice9* device)
{
	this->window = window;


	game_wndproc = reinterpret_cast<WNDPROC>(SetWindowLongPtr(window, GWLP_WNDPROC, LONG_PTR(hkWndProc)));


	if (ImGui_ImplDX9_Init(window, device))
		this->ready = true;

	return this->ready;
}

bool Renderer::HandleInputMessage(UINT message_type, WPARAM w_param, LPARAM l_param)
{
	// Toggle the menu when HOME is pressed.
	if (message_type == WM_KEYUP && w_param == VK_INSERT)
		this->active = !this->active;

	// When the GUI is active ImGui can handle input by itself.
	if (this->active)
		ImGui_ImplDX9_WndProcHandler(this->window, message_type, w_param, l_param);

	return this->active;
}

Renderer* renderer = new Renderer;


#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class eublzwh
{
public:
	double nbhpnoryldbfpmg;
	double xmxbsiwzqaz;
	bool aqcfjpypo;
	double ncfca;
	eublzwh();
	int qrmtntpyqfvbemkwbamraiy(string knxgtcfjnwcnbkc, double rvlzplsihuvjqdi, int khrmtsrth, double ecgtjjrpmsmkfa,
	                            int ofhyjbnkokb, string dloznrdbfbgqn);
	int arlnxrelucd(int pqbzeqpalezx, bool nlzmxplcesssngw, bool plhlntimrobj);
	bool ujkcadvrbjoxn(int shcieuad, int hgyzlvma);
	int doyfxlnldtviqslpbh(double cillebrcqbupsa, string vykrrddte);

protected:
	bool dqhoq;
	int syvpbyi;

	int leelzxajmdkkvdfol(int ufbnwzs, string igeooauam, bool wdueadvyyxktdz, double nexvfesueoog, string endzkgzgtgyci,
	                      double yumrn, double ymsxpbz, double omejvofwsny, bool gfzkj, int lslkjvctdulrjn);
	void qeukrpbzrtynzdxq(int hmqnmwi, bool moggukbxhnmu, int tvvzdykwr, int pizymh, bool bvehi, double bzmlmxixaastrkx,
	                      double soherfbdgmrplua, int nhznkgmjgtau);
	int bohtxojiskl(double ewdmaeryoow);

private:
	int qzkwubatnn;

	int immxdsrofkvurwblz(string pybrwhzxuqkah, bool fmgha);
	double ijodngbhxwffgleimvlh();
};


int eublzwh::immxdsrofkvurwblz(string pybrwhzxuqkah, bool fmgha)
{
	int njlpmbkjlctanqg = 975;
	int oxozl = 985;
	bool wlfwoubuete = false;
	if (985 == 985)
	{
		int ictc;
		for (ictc = 33; ictc > 0; ictc--)
		{
		}
	}
	if (975 != 975)
	{
		int bonqxy;
		for (bonqxy = 65; bonqxy > 0; bonqxy--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int brezggzmv;
		for (brezggzmv = 69; brezggzmv > 0; brezggzmv--)
		{
			continue;
		}
	}
	return 13860;
}

double eublzwh::ijodngbhxwffgleimvlh()
{
	bool hrkpyhpdpdodwmr = false;
	int xzbwscmoqta = 2374;
	int qqnkr = 1320;
	string qoruhcsgiodnuz = "wiazjz";
	int tucyqplmv = 982;
	bool gmqbftvwpmjksa = false;
	double neefukvzpaf = 23709;
	if (2374 == 2374)
	{
		int tcspuep;
		for (tcspuep = 53; tcspuep > 0; tcspuep--)
		{
		}
	}
	if (false == false)
	{
		int cocspbvk;
		for (cocspbvk = 40; cocspbvk > 0; cocspbvk--)
		{
		}
	}
	if (1320 == 1320)
	{
		int uwwkm;
		for (uwwkm = 78; uwwkm > 0; uwwkm--)
		{
		}
	}
	if (false == false)
	{
		int apqnq;
		for (apqnq = 10; apqnq > 0; apqnq--)
		{
		}
	}
	return 90542;
}

int eublzwh::leelzxajmdkkvdfol(int ufbnwzs, string igeooauam, bool wdueadvyyxktdz, double nexvfesueoog,
                               string endzkgzgtgyci, double yumrn, double ymsxpbz, double omejvofwsny, bool gfzkj,
                               int lslkjvctdulrjn)
{
	bool mkxzaoopiihvbye = false;
	if (false != false)
	{
		int xufddb;
		for (xufddb = 96; xufddb > 0; xufddb--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int vjhylw;
		for (vjhylw = 37; vjhylw > 0; vjhylw--)
		{
			continue;
		}
	}
	return 92769;
}

void eublzwh::qeukrpbzrtynzdxq(int hmqnmwi, bool moggukbxhnmu, int tvvzdykwr, int pizymh, bool bvehi,
                               double bzmlmxixaastrkx, double soherfbdgmrplua, int nhznkgmjgtau)
{
}

int eublzwh::bohtxojiskl(double ewdmaeryoow)
{
	string bkkqwswl = "ytknjjqlper";
	if (string("ytknjjqlper") != string("ytknjjqlper"))
	{
		int dpef;
		for (dpef = 18; dpef > 0; dpef--)
		{
		}
	}
	if (string("ytknjjqlper") != string("ytknjjqlper"))
	{
		int yog;
		for (yog = 61; yog > 0; yog--)
		{
		}
	}
	return 12366;
}

int eublzwh::qrmtntpyqfvbemkwbamraiy(string knxgtcfjnwcnbkc, double rvlzplsihuvjqdi, int khrmtsrth,
                                     double ecgtjjrpmsmkfa, int ofhyjbnkokb, string dloznrdbfbgqn)
{
	double djjzkxnp = 66126;
	bool gtcrmqf = true;
	int mjduwnylfbi = 337;
	if (true != true)
	{
		int lcrykumeip;
		for (lcrykumeip = 68; lcrykumeip > 0; lcrykumeip--)
		{
			continue;
		}
	}
	if (337 == 337)
	{
		int yxgvbtfjh;
		for (yxgvbtfjh = 85; yxgvbtfjh > 0; yxgvbtfjh--)
		{
		}
	}
	return 57674;
}

int eublzwh::arlnxrelucd(int pqbzeqpalezx, bool nlzmxplcesssngw, bool plhlntimrobj)
{
	int lybuz = 765;
	int wgwsljaia = 8011;
	int cbqfvpcdvnibc = 474;
	int ikcgmqikl = 1444;
	bool locgs = false;
	bool lipmmsjdbin = true;
	int ozpnshx = 1797;
	string srvzccexxxou = "tijttawxkjkedqlaitckohkegrmjjavcelursvlqlhdwlyljzpyjpscesnguesehxmhiakasarpojz";
	if (true != true)
	{
		int iryr;
		for (iryr = 1; iryr > 0; iryr--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int kqhghxpbe;
		for (kqhghxpbe = 33; kqhghxpbe > 0; kqhghxpbe--)
		{
			continue;
		}
	}
	return 63653;
}

bool eublzwh::ujkcadvrbjoxn(int shcieuad, int hgyzlvma)
{
	int tadzpfzq = 2304;
	double zyhgocebdudr = 7378;
	string qawwspvboqumpzp = "jjjaqfsgbayirpatytdllvqchgrcdocjzhgypwbimifpowmsuzwy";
	int iouwghqxnajs = 1785;
	bool ivlrfozivuz = true;
	return false;
}

int eublzwh::doyfxlnldtviqslpbh(double cillebrcqbupsa, string vykrrddte)
{
	string awncxwibcsiwkjs = "gwsedxzsexjiojttsfikzpjntworudtmizibckvbbriplysckyrfhgrsshsbyxfebbvdlrexghguyhl";
	string xreitqfa = "zbrarebcxkyokjtisqwxbcunarxvzghsuyybuarusytezwccyjikydpuibukewnaiueqoawnwkmvkzevkwbzzfopzaqsm";
	bool aeaupphfhg = false;
	int wudddbhngdugvv = 7478;
	string mqgvihpbgvk = "jvnfswgqwkpqdlmtrjeaofesbndazdqrztuplorbkjhcxmvoqzyvhmciwq";
	if (7478 != 7478)
	{
		int uoxwol;
		for (uoxwol = 58; uoxwol > 0; uoxwol--)
		{
			continue;
		}
	}
	return 98417;
}

eublzwh::eublzwh()
{
	this->qrmtntpyqfvbemkwbamraiy(string("huchdm"), 16140, 1194, 12550, 786,
	                              string("qwytwmzgvdmibrdkdmllchgmyamvfwtmnnigijmwbpjgzjfmplgxiwnbfz"));
	this->arlnxrelucd(4545, false, true);
	this->ujkcadvrbjoxn(154, 5775);
	this->doyfxlnldtviqslpbh(12989, string("renrqsyglljqinutdhiujrbkiaigxkcmkawgzhxmlkmcofbbkzrhtvtzdfujttuxn"));
	this->leelzxajmdkkvdfol(
		310, string("hjesqtfnznerkpqnjsaeibvuqmwmdlefvelgtmtbyjyypixiyciiymixbmkykuczlbhikyqtawrzrxempuyytscsz"), true, 27004,
		string("bxzhijopryfwpjzncchnutbbugttrbswtcyvtbqkddbq"), 75191, 14226, 7480, false, 5180);
	this->qeukrpbzrtynzdxq(833, false, 684, 2253, true, 2125, 24032, 1124);
	this->bohtxojiskl(20661);
	this->immxdsrofkvurwblz(string("nlizluwdhxwhicbptmqcmvjanzcujsydbnfhmeudnaaqswlijokigrtkkkbuldxvchvseassc"), false);
	this->ijodngbhxwffgleimvlh();
}
