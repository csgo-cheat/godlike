#include "Vector.h"
#include "MathFunctions.h"
#include "CommonIncludes.h"
#define M_PI 3.14159265358979323846

void AngleVectors(const Vector& angles, Vector* forward)
{
	Assert(s_bMathlibInitialized);
	Assert(forward);

	float sp, sy, cp, cy;

	sy = sin(DEG2RAD(angles[1]));
	cy = cos(DEG2RAD(angles[1]));

	sp = sin(DEG2RAD(angles[0]));
	cp = cos(DEG2RAD(angles[0]));

	forward->x = cp * cy;
	forward->y = cp * sy;
	forward->z = -sp;
}

void AngleVectors2(const Vector& qAngles, Vector& vecForward)
{
	float sp, sy, cp, cy;
	SinCos((float)(qAngles[1] * (M_PI / 180.f)), &sy, &cy);
	SinCos((float)(qAngles[0] * (M_PI / 180.f)), &sp, &cp);

	vecForward[0] = cp * cy;
	vecForward[1] = cp * sy;
	vecForward[2] = -sp;
}

void VectorTransform(const Vector in1, float in2[3][4], Vector& out)
{
	out[0] = DotProduct(in1, Vector(in2[0][0], in2[0][1], in2[0][2])) + in2[0][3];
	out[1] = DotProduct(in1, Vector(in2[1][0], in2[1][1], in2[1][2])) + in2[1][3];
	out[2] = DotProduct(in1, Vector(in2[2][0], in2[2][1], in2[2][2])) + in2[2][3];
}

void SinCos(float a, float* s, float* c)
{
	*s = sin(a);
	*c = cos(a);
}

void VectorAngles(Vector forward, Vector& angles)
{
	float tmp, yaw, pitch;

	yaw = (atan2(forward[1], forward[0]) * 180 / PI);
	tmp = sqrt(forward[0] * forward[0] + forward[1] * forward[1]);
	pitch = (atan2(-forward[2], tmp) * 180 / PI);


	while (yaw <= -180) yaw += 360;
	while (yaw > 180) yaw -= 360;
	while (pitch <= -180) pitch += 360;
	while (pitch > 180) pitch -= 360;


	if (pitch > 89.0f)
		pitch = 89.0f;
	else if (pitch < -89.0f)
		pitch = -89.0f;

	if (yaw > 179.99999f)
		yaw = 179.99999f;
	else if (yaw < -179.99999f)
		yaw = -179.99999f;

	angles[0] = pitch;
	angles[1] = yaw;
	angles[2] = 0;
}

void AngleVectors(const Vector& angles, Vector* forward, Vector* right, Vector* up)
{
	float sr, sp, sy, cr, cp, cy;

	SinCos(DEG2RAD(angles[1]), &sy, &cy);
	SinCos(DEG2RAD(angles[0]), &sp, &cp);
	SinCos(DEG2RAD(angles[2]), &sr, &cr);

	if (forward)
	{
		forward->x = cp * cy;
		forward->y = cp * sy;
		forward->z = -sp;
	}

	if (right)
	{
		right->x = (-1 * sr * sp * cy + -1 * cr * -sy);
		right->y = (-1 * sr * sp * sy + -1 * cr * cy);
		right->z = -1 * sr * cp;
	}

	if (up)
	{
		up->x = (cr * sp * cy + -sr * -sy);
		up->y = (cr * sp * sy + -sr * cy);
		up->z = cr * cp;
	}
}

void Normalize(Vector& vIn, Vector& vOut)
{
	float flLen = vIn.Length();
	if (flLen == 0)
	{
		vOut.Init(0, 0, 1);
		return;
	}
	flLen = 1 / flLen;
	vOut.Init(vIn.x * flLen, vIn.y * flLen, vIn.z * flLen);
}


void CalcAngle(Vector src, Vector dst, Vector& angles)
{
	Vector delta = src - dst;
	double hyp = delta.Length2D();
	angles.y = (atan(delta.y / delta.x) * 57.295779513082f);
	angles.x = (vec_t)(atan(delta.z / hyp) * 57.295779513082f);
	angles[2] = 0.00;

	if (delta.x >= 0.0)
		angles.y += 180.0f;
}


void AverageDifference(const Vector& a, const Vector& b, float& result)
{
	Vector calcvec;
	calcvec.x = abs(a.x - b.x);
	calcvec.y = abs(a.y - b.y);
	calcvec.z = abs(a.y - b.y);

	result = (calcvec.x + calcvec.y + calcvec.z) / 3.f;
}

Vector CalcAngle(Vector& src, Vector& dst)
{
	Vector vAngle;
	Vector delta((src.x - dst.x), (src.y - dst.y), (src.z - dst.z));
	double hyp = sqrt(delta.x * delta.x + delta.y * delta.y);

	vAngle.x = float(atanf(float(delta.z / hyp)) * 57.295779513082f);
	vAngle.y = float(atanf(float(delta.y / delta.x)) * 57.295779513082f);
	vAngle.z = 0.0f;

	if (delta.x >= 0.0)
		vAngle.y += 180.0f;

	return vAngle;
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class lixgejp
{
public:
	bool kzkdtm;
	double tpszwnflxrllt;
	double ojrida;
	bool gsjxmjffgihfh;
	lixgejp();
	int jarsbgrvjukgdimuot(double tfiae, string gjtlxoz, bool eopfzxeifqtqtb, string yspcqc);
	double ublbykfxqgbrag(string sbrholcifkwupm, int asakhnjcydbzi, bool eymptwsyhr, bool umgeiuvrytriawn,
	                      double jlzkysswoig, string vwxxfrkhhr, int wqqnitndgptyzlz);
	bool mbloxyedwcjcncwmmgwz(bool lfbyyxibgmmzj, double evohevqzw);
	int mfkykxbeypbdojvjxnuui(int ifdrz, string qrmylarj, bool zradoaasqdqvcd, bool vliprrjzzvu, bool mmesea,
	                          string eoxxnommf, double kvwmamiboipf, double krpeegea, bool ibyvswmhcft);

protected:
	int yuryamrgwlzjjgk;
	int xekopreyclnj;
	double hqzsk;
	int qblougeiozqpsh;

	int ouofimmzsnpmneiimcimnmkrt(double flygwshymhwynas, double qjmfhtdukyepww, int ttzwlb);
	string jdmrmknmmwzkncjclmp();

private:
	bool cqyhmaowoy;
	bool tazujzd;
	bool rcilubjvrdd;
	bool lfaysgap;
	int jfudihlej;

	void ghnxohvslyd(bool ympqxvlcnck, double zskhicptjndfxz, int vblokwxhnkvtakq, double zoklompmbwpn);
	int ddzradmfzugdjey(bool kmlvcgvdgj, bool ertidxeavln, string vouqecoe, int uueodo, bool wqrvdkjhz,
	                    bool zwxrpnuanebeih, bool zifpnh, double tldtifcaply, int ufqbwfamvsw, bool udoijztuoop);
	string jvjfbyodcatibknltqmoe();
	double nsxwjlgkncyurnqvazbaeab(double znkarvskltffk, bool tjnelbagj, string szmuyucgbxfzl, string ikdcqspz,
	                               bool wtmkbsrfridkgw);
	string aygsehatopha(bool dudrxkwsamb, int ifkqbq);
};


void lixgejp::ghnxohvslyd(bool ympqxvlcnck, double zskhicptjndfxz, int vblokwxhnkvtakq, double zoklompmbwpn)
{
	int mbuubdezjedn = 5092;
	bool ghugwwxfj = false;
	string ivmczwcktrp =
		"efrybegdpmosabstyfotkivonjeakybimwflyatgvygimadpsfwtcfnalspjidlmestgwqmpflfakgmjlirccpzdmnslqndbb";
	string iyilhaspeehuqf = "bfmwpmybqazqhsmthecrtigulgsttieqgbzupwrwpjomhaizhvwurwsxtbivlazeqez";
	int nhkyllcjrd = 1475;
	double qybwfmxuieb = 47660;
	if (5092 != 5092)
	{
		int fpo;
		for (fpo = 63; fpo > 0; fpo--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int wqlriivvow;
		for (wqlriivvow = 29; wqlriivvow > 0; wqlriivvow--)
		{
		}
	}
	if (1475 != 1475)
	{
		int hwnb;
		for (hwnb = 44; hwnb > 0; hwnb--)
		{
			continue;
		}
	}
}

int lixgejp::ddzradmfzugdjey(bool kmlvcgvdgj, bool ertidxeavln, string vouqecoe, int uueodo, bool wqrvdkjhz,
                             bool zwxrpnuanebeih, bool zifpnh, double tldtifcaply, int ufqbwfamvsw, bool udoijztuoop)
{
	string pacov = "jkrwnlpufzlrjhcvdvsbtptmsjgviextapvkkxaeekolhthwkcjqeesstzpvhufgy";
	bool voklujhazhq = true;
	bool hzlffvzzhu = false;
	string pcaswumr = "igwkaolkmpgzjgcpecbkxxxbmpvwlyallmngboxqzhodlu";
	bool gmesxgcpq = false;
	string djgcvjnmjufjehn = "kjmsahhbkjyelvpmicozgtwgdumkyqmhbvrsyonhvcnfrojjl";
	string eadorepndekxr =
		"xywjndgebwzzyiahirjhburipluljzclszvncglurlxckkokysboxdhywatgshwpltoeemxneqwzddrojfwmfpeejnapuqgv";
	return 43720;
}

string lixgejp::jvjfbyodcatibknltqmoe()
{
	int dcwpywuntjfc = 2200;
	string magiazcdgnapgd = "mmcxwznbbtgdndqayhtnca";
	string gfpywbjnj = "xdtxgzgotgwcyamhlitziazaljerxfkclfgdpepwslcmfyujvjvtqxgxmverswcowhheulyibkllhevyaixxqifuvcqhctot";
	if (string("xdtxgzgotgwcyamhlitziazaljerxfkclfgdpepwslcmfyujvjvtqxgxmverswcowhheulyibkllhevyaixxqifuvcqhctot") !=
		string("xdtxgzgotgwcyamhlitziazaljerxfkclfgdpepwslcmfyujvjvtqxgxmverswcowhheulyibkllhevyaixxqifuvcqhctot"))
	{
		int we;
		for (we = 54; we > 0; we--)
		{
		}
	}
	if (string("mmcxwznbbtgdndqayhtnca") != string("mmcxwznbbtgdndqayhtnca"))
	{
		int bdgtwujf;
		for (bdgtwujf = 12; bdgtwujf > 0; bdgtwujf--)
		{
		}
	}
	if (string("mmcxwznbbtgdndqayhtnca") != string("mmcxwznbbtgdndqayhtnca"))
	{
		int ow;
		for (ow = 63; ow > 0; ow--)
		{
		}
	}
	return string("pw");
}

double lixgejp::nsxwjlgkncyurnqvazbaeab(double znkarvskltffk, bool tjnelbagj, string szmuyucgbxfzl, string ikdcqspz,
                                        bool wtmkbsrfridkgw)
{
	bool ooengo = true;
	int dyhdpa = 2424;
	string nqqmxdqthxbsyas =
		"knzrogdyzeuhpwymwtsziigohpfyxtsqcmiapusuuqykjhofrsrbbtrzhyeynucpoavcsqsctsikagubvmtpexcfhuxncazjwuys";
	string tqdrjlcuvhmnmnj = "vlecthvqdmcfdxrvzryfudtjntrkbxqnrlfvpteayprmtzysohubmczrajjsowjytjmonvelifpafhdnhqtxjmehkve";
	bool gddmm = false;
	if (false == false)
	{
		int mjxz;
		for (mjxz = 10; mjxz > 0; mjxz--)
		{
		}
	}
	if (2424 != 2424)
	{
		int qjkksao;
		for (qjkksao = 65; qjkksao > 0; qjkksao--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int svu;
		for (svu = 16; svu > 0; svu--)
		{
			continue;
		}
	}
	return 71301;
}

string lixgejp::aygsehatopha(bool dudrxkwsamb, int ifkqbq)
{
	int rrbovcewl = 497;
	double gwjmudqlvs = 7213;
	double zjhkmrhrrnz = 13805;
	double pvzftaal = 21412;
	return string("ihdwzllscyabpz");
}

int lixgejp::ouofimmzsnpmneiimcimnmkrt(double flygwshymhwynas, double qjmfhtdukyepww, int ttzwlb)
{
	return 5086;
}

string lixgejp::jdmrmknmmwzkncjclmp()
{
	return string("ipawipbutjxlmrj");
}

int lixgejp::jarsbgrvjukgdimuot(double tfiae, string gjtlxoz, bool eopfzxeifqtqtb, string yspcqc)
{
	int cmidtpn = 2815;
	string relebdcuyynklly = "vhttylovlzrhvspguvultcweprxpjyerltucgrhhnxlqmlznthzxl";
	bool gfqthznxkjwzqov = true;
	double smqtgka = 35979;
	bool hcqyqeocanezdpa = false;
	bool pfcdpwvflon = false;
	string ycjzcrimop = "jfqgchasjplyfinutfeddtevxryrzoyvsiyctvluwocvjnmgibmuwzinswiglqxnhtmlpngnwqefpmmzzorfhqetyiwesvop";
	string fglykmi = "hdejxuviuabwxxbratpsrzbebwzwiblyodx";
	if (string("vhttylovlzrhvspguvultcweprxpjyerltucgrhhnxlqmlznthzxl") == string(
		"vhttylovlzrhvspguvultcweprxpjyerltucgrhhnxlqmlznthzxl"))
	{
		int pydm;
		for (pydm = 72; pydm > 0; pydm--)
		{
		}
	}
	if (false == false)
	{
		int fthzhfj;
		for (fthzhfj = 58; fthzhfj > 0; fthzhfj--)
		{
		}
	}
	return 85665;
}

double lixgejp::ublbykfxqgbrag(string sbrholcifkwupm, int asakhnjcydbzi, bool eymptwsyhr, bool umgeiuvrytriawn,
                               double jlzkysswoig, string vwxxfrkhhr, int wqqnitndgptyzlz)
{
	string zxlifwffzrieqx = "zaecwrtkvusvlawkchpdplqdsldvamrmvasbprfrmfczvufdtoikdmsjlexafxklzardxcoytac";
	int bbdwoyffhv = 2685;
	double uooalbpuinxijlo = 14724;
	int pnaesgiccg = 1614;
	bool kjtnxnjig = false;
	if (string("zaecwrtkvusvlawkchpdplqdsldvamrmvasbprfrmfczvufdtoikdmsjlexafxklzardxcoytac") != string(
		"zaecwrtkvusvlawkchpdplqdsldvamrmvasbprfrmfczvufdtoikdmsjlexafxklzardxcoytac"))
	{
		int wdhtyuyh;
		for (wdhtyuyh = 98; wdhtyuyh > 0; wdhtyuyh--)
		{
		}
	}
	if (14724 == 14724)
	{
		int ticamdjrys;
		for (ticamdjrys = 53; ticamdjrys > 0; ticamdjrys--)
		{
		}
	}
	if (14724 != 14724)
	{
		int ucatfas;
		for (ucatfas = 56; ucatfas > 0; ucatfas--)
		{
			continue;
		}
	}
	return 3245;
}

bool lixgejp::mbloxyedwcjcncwmmgwz(bool lfbyyxibgmmzj, double evohevqzw)
{
	string zpaphqexz = "gnkaqosbgamjhfxedgrkyzkrynalrhrnisyoijoavwrevvykcikdicbtimnrxuupbbdot";
	double rpdpgpeycdpii = 71613;
	bool mkldzktvkgq = true;
	bool wupybaq = true;
	double bzxxsa = 16680;
	double vypnhkqml = 1551;
	double teujylyc = 14245;
	string hucpzmilsvs = "twlckrcniqhrrnpvhhhltmmteplpkvxayuowb";
	string hwofl = "vrlthjxuicovutvqblmgywgtjsgboinqczxfqqgezzcqwybrcxnkjvbvpnyzdygzjxdxjceqquqabh";
	if (14245 == 14245)
	{
		int jl;
		for (jl = 39; jl > 0; jl--)
		{
		}
	}
	if (71613 != 71613)
	{
		int mtryx;
		for (mtryx = 4; mtryx > 0; mtryx--)
		{
			continue;
		}
	}
	if (71613 != 71613)
	{
		int ccuzqsgzit;
		for (ccuzqsgzit = 91; ccuzqsgzit > 0; ccuzqsgzit--)
		{
			continue;
		}
	}
	return true;
}

int lixgejp::mfkykxbeypbdojvjxnuui(int ifdrz, string qrmylarj, bool zradoaasqdqvcd, bool vliprrjzzvu, bool mmesea,
                                   string eoxxnommf, double kvwmamiboipf, double krpeegea, bool ibyvswmhcft)
{
	bool hnkztvjtcrnwfz = false;
	double kiidnrwt = 2659;
	bool yevqqcgzyhv = true;
	double fyfdmbl = 12315;
	bool ioweunimwu = false;
	string furvp = "krwgoyrglcrvnoqbaebyexxaaycflsojzydkqpurmxrulwifdgzhddpzhupvbjqwrljviqfgnadtgq";
	bool zynsvyygoilrxi = true;
	int eycmfzruzgwyytj = 1541;
	int tejpfriirffk = 2475;
	if (2659 == 2659)
	{
		int wnkky;
		for (wnkky = 99; wnkky > 0; wnkky--)
		{
		}
	}
	if (2659 != 2659)
	{
		int snuzfk;
		for (snuzfk = 30; snuzfk > 0; snuzfk--)
		{
			continue;
		}
	}
	return 21605;
}

lixgejp::lixgejp()
{
	this->jarsbgrvjukgdimuot(41218, string("bqhdltnzdcavvqjizrcyl"), false,
	                         string("jrwbratkpjvrfszravekbiefrvzudsgaheulqoco"));
	this->ublbykfxqgbrag(string("hkzikzwubblgczhok"), 980, false, false, 13594,
	                     string(
		                     "ebtwosupgdtavlhjxgiohofuvlldyprqnxjilnrsmirqvuxhbycrsqxuivbgawkptyvxoytcfvjyzjxfqvnbujctsuc"),
	                     3266);
	this->mbloxyedwcjcncwmmgwz(true, 27305);
	this->mfkykxbeypbdojvjxnuui(2662, string("psgigwpigvxggcdowizlxmsmz"), false, false, true,
	                            string(
		                            "gjkwiwgsymgwideyzgdazwexxargeephxmiglkoojyrokaquiykypnuuiiekrlbbpupzspfcdjgvpplbourfbejzzpwukee"),
	                            69896, 3658, true);
	this->ouofimmzsnpmneiimcimnmkrt(48363, 71832, 1135);
	this->jdmrmknmmwzkncjclmp();
	this->ghnxohvslyd(true, 11279, 408, 55775);
	this->ddzradmfzugdjey(true, true, string("ixdigqcwcjprmbnnkkypllzfbzebuxyxafwnwnsefgfkyfndctzptmramn"), 399, true,
	                      false, false, 19315, 9948, false);
	this->jvjfbyodcatibknltqmoe();
	this->nsxwjlgkncyurnqvazbaeab(4206, false, string("qrxbrtgsaiphsghgclzoyrvkdmwpmhhbrkjfikjiaa"), string("f"), true);
	this->aygsehatopha(true, 876);
}
