#pragma once
#include "singleton.hpp"
#include "MiscClasses.h"
#include "Interfaces.h"
#include "Interface.h"
#include "Sounds.h"

#pragma comment(lib, "winmm.lib")

class item_purchase
	: public singleton<item_purchase>
{
	class item_purchase_listener
		: public IGameEventListener2
	{
	public:
		void start()
		{
			g_EventManager->AddListener(this, "item_purchase", false);
			g_EventManager->AddListener(this, "player_hurt", false);
		}

		void stop()
		{
			g_EventManager->RemoveListener(this);
		}

		void FireGameEvent(IGameEvent* event) override
		{
			singleton()->on_fire_event(event);
		}

		int GetEventDebugID(void) override
		{
			return 42 /*0x2A*/;
		}
	};

public:
	static item_purchase* singleton()
	{
		static item_purchase* instance = new item_purchase;
		return instance;
	}

	void initialize()
	{
		listener.start();
	}

	void remove()
	{
		listener.stop();
	}

	void on_fire_event(IGameEvent* event)
	{
		C_BaseEntity* local = g_EntityList->GetClientEntity(g_Engine->GetLocalPlayer());
		if (!strcmp(event->GetName(), "item_purchase"))
		{
			auto buyer = event->GetInt("userid");
			std::string gun = event->GetString("weapon");


			if (strstr(gun.c_str(), "molotov")
				|| strstr(gun.c_str(), "nade")
				|| strstr(gun.c_str(), "kevlar")
				|| strstr(gun.c_str(), "decoy")
				|| strstr(gun.c_str(), "suit")
				|| strstr(gun.c_str(), "flash")
				|| strstr(gun.c_str(), "vest")
				|| strstr(gun.c_str(), "cutter")
				|| strstr(gun.c_str(), "defuse")
			)
				return;

			auto player_index = g_Engine->GetPlayerForUserID(buyer);
			C_BaseEntity* player = (C_BaseEntity*)g_EntityList->GetClientEntity(player_index);
			player_info_t pinfo;

			if (player && local && g_Engine->GetPlayerInfo(player_index, &pinfo))
			{
				if (player->GetTeamNum() == local->GetTeamNum() && g_Options.Misc.boughtFilterEnemyOnly)
					return;

				if (g_ChatElement && g_Options.Misc.bought)
				{
					gun.erase(gun.find("weapon_"), 7);
					g_ChatElement->ChatPrintf(0, 0, " ""\x04""%s bought %s\n", pinfo.name, gun.c_str());
				}
			}
		}

		if (!strcmp(event->GetName(), "player_hurt"))
		{
			auto bitch = event->GetInt("userid");
			auto coolguy49 = event->GetInt("attacker");
			int dmg = event->GetInt("dmg_health");


			auto bitch_index = g_Engine->GetPlayerForUserID(bitch);
			auto coolguy49_index = g_Engine->GetPlayerForUserID(coolguy49);
			C_BaseEntity* bitch_ = (C_BaseEntity*)g_EntityList->GetClientEntity(bitch_index);
			C_BaseEntity* coolguy49_ = (C_BaseEntity*)g_EntityList->GetClientEntity(coolguy49_index);
			player_info_t bitch_info;
			player_info_t coolguy49_info;

			if (coolguy49_ == local)
			{
				G::hitmarkeralpha = 1.f;
				switch (g_Options.Misc.hitsound)
				{
				case 0: break;
				case 1: PlaySoundA(rawData, nullptr, SND_ASYNC | SND_MEMORY);
					break; // Default
				case 2: PlaySoundA(china, nullptr, SND_ASYNC | SND_MEMORY);
					break; // CHINA
				case 3: PlaySoundA(skeethitmarker_wav, nullptr, SND_ASYNC | SND_MEMORY);
					break; // Skeet
				}
			}

			if (bitch && local && g_Engine->GetPlayerInfo(bitch_index, &bitch_info), g_Engine->GetPlayerInfo(
				coolguy49_index, &coolguy49_info) && bitch_ && coolguy49_)
			{
				int health = bitch_->GetHealth() - dmg;

				if (health <= 0)
					health = 0;

				if (!g_ChatElement)
				{
					return;
				}

				if (g_Options.Misc.Damage && g_Options.Misc.Health)
				{
					if (g_Options.Misc.DamageFilterEnemyOnly && bitch_->GetTeamNum() == local->GetTeamNum())
						return;
					if (health == 0)
						g_ChatElement->ChatPrintf(0, 0, " ""\x07""""%s did %i Damage to %s (DEAD)\n", coolguy49_info.name, dmg,
						                          bitch_info.name);
					else
						g_ChatElement->ChatPrintf(0, 0, " ""\x07""""%s did %i Damage to %s (%i HP Left)\n", coolguy49_info.name, dmg,
						                          bitch_info.name, health);
					return;
				}

				if (g_Options.Misc.Damage)
				{
					if (g_Options.Misc.DamageFilterEnemyOnly && bitch_->GetTeamNum() == local->GetTeamNum())
						return;

					g_ChatElement->ChatPrintf(0, 0, " ""\x07""Damage ""%s did %i to %s\n", coolguy49_info.name, dmg, bitch_info.name);
					return;
				}

				if (g_Options.Misc.Health)
				{
					if (g_Options.Misc.HealthFilterEnemyOnly && bitch_->GetTeamNum() == local->GetTeamNum())
						return;

					g_ChatElement->ChatPrintf(0, 0, " ""\x09""Health of ""%s is %i \n", bitch_info.name, health);
				}
			}
		}
	}

private:
	item_purchase_listener listener;
};
