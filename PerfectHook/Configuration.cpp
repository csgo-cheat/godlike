#include "Configuration.hpp"
#include "Variables.h"
#include <winerror.h>
#pragma warning( disable : 4091)
#include <ShlObj.h>
#include <string>
#include <sstream>


void CConfig::Setup()
{
	SetupValue(g_Options.Ragebot.MainSwitch, false, ("Ragebot"), ("RageToggle"));
	SetupValue(g_Options.Ragebot.Enabled, false, ("Ragebot"), ("Enabled"));
	SetupValue(g_Options.Ragebot.AutoFire, false, ("Ragebot"), ("Auto Fire"));
	SetupValue(g_Options.Ragebot.FOV, 39.f, ("Ragebot"), ("FOV"));
	SetupValue(g_Options.Ragebot.Silent, false, ("Ragebot"), ("Silent"));
	SetupValue(g_Options.Ragebot.PSilent, false, ("Ragebot"), ("PSilent"));
	SetupValue(g_Options.Ragebot.AimStep, false, ("Ragebot"), ("AimStep"));
	SetupValue(g_Options.Ragebot.AutoPistol, false, ("Ragebot"), ("AutoPistol"));
	SetupValue(g_Options.Ragebot.KeyPress, 0, ("Ragebot"), ("Key"));

	SetupValue(g_Options.Ragebot.EnabledAntiAim, false, ("Ragebot"), ("AntiaimEnabled"));
	SetupValue(g_Options.Ragebot.PreAAs, false, ("Ragebot"), ("Pre-set-AAs"));
	SetupValue(g_Options.Ragebot.Pitch, 0, ("Ragebot"), ("AntiaimPitch"));
	SetupValue(g_Options.Ragebot.YawTrue, 0, ("Ragebot"), ("AntiaimYaw-true"));
	SetupValue(g_Options.Ragebot.YawFake, 0, ("Ragebot"), ("AntiaimYaw-fake"));
	SetupValue(g_Options.Ragebot.AtTarget, false, ("Ragebot"), ("attargets"));
	SetupValue(g_Options.Ragebot.Edge, false, ("Ragebot"), ("edge"));
	SetupValue(g_Options.Ragebot.KnifeAA, false, ("Ragebot"), ("KnifeAA"));
	SetupValue(g_Options.Ragebot.BuilderAAs, false, ("Ragebot"), ("Builder-AAs"));
	SetupValue(g_Options.Ragebot.BuilderPitch, 0, ("Ragebot"), ("Builder-Pitch"));
	SetupValue(g_Options.Ragebot.BuilderReal, 0, ("Ragebot"), ("Builder-Yaw"));
	SetupValue(g_Options.Ragebot.BuilderFake, 0, ("Ragebot"), ("Builder-Fake"));
	SetupValue(g_Options.Ragebot.Jitter, false, ("Ragebot"), ("Builder-Jitter"));
	SetupValue(g_Options.Ragebot.JitterRange, 0, ("Ragebot"), ("Builder-Jitter-Range"));
	SetupValue(g_Options.Ragebot.FJitter, false, ("Ragebot"), ("Builder-fJitter"));
	SetupValue(g_Options.Ragebot.FJitterRange, 0, ("Ragebot"), ("Builder-fJitter-Range"));
	SetupValue(g_Options.Ragebot.LBYBreaker, false, ("Ragebot"), ("Builder-LBY-Breaker"));
	SetupValue(g_Options.Ragebot.FakeLag, false, ("Ragebot"), ("Fakelag"));
	SetupValue(g_Options.Ragebot.FakeLagAmt, 9.f, ("Ragebot"), ("Fakelag Amount"));

	SetupValue(g_Options.Ragebot.FriendlyFire, false, ("Ragebot"), ("FriendlyFire"));
	SetupValue(g_Options.Ragebot.Hitbox, 0, ("Ragebot"), ("Hitbox"));
	SetupValue(g_Options.Ragebot.Hitscan, 0, ("Ragebot"), ("Hitscan"));

	SetupValue(g_Options.Ragebot.AntiRecoil, false, ("Ragebot"), ("AntiRecoil"));
	SetupValue(g_Options.Ragebot.AutoStop, false, ("Ragebot"), ("AutoStop"));
	SetupValue(g_Options.Ragebot.AutoCrouch, false, ("Ragebot"), ("AutoCrouch"));
	SetupValue(g_Options.Ragebot.AutoScope, false, ("Ragebot"), ("AutoScope"));
	SetupValue(g_Options.Ragebot.MinimumDamageSniper, 0.f, ("Ragebot"), ("AutoWallDamageSniper"));
	SetupValue(g_Options.Ragebot.MinimumDamagePistol, 0.f, ("Ragebot"), ("AutoWallDamagePistol"));
	SetupValue(g_Options.Ragebot.MinimumDamageRifle, 0.f, ("Ragebot"), ("AutoWallDamageRifle"));
	SetupValue(g_Options.Ragebot.MinimumDamageHeavy, 0.f, ("Ragebot"), ("AutoWallDamageHeavy"));
	SetupValue(g_Options.Ragebot.MinimumDamageSmg, 0.f, ("Ragebot"), ("AutoWallDamageSmgs"));
	SetupValue(g_Options.Ragebot.MinimumDamageRevolver, 0.f, ("Ragebot"), ("AutoWallDamageRevolver"));
	SetupValue(g_Options.Ragebot.Hitchance, false, ("Ragebot"), ("HitChance"));
	SetupValue(g_Options.Ragebot.HitchanceSniper, 0.f, ("Ragebot"), ("HitChanceSniper"));
	SetupValue(g_Options.Ragebot.HitchancePistol, 0.f, ("Ragebot"), ("HitChancePistol"));
	SetupValue(g_Options.Ragebot.HitchanceHeavy, 0.f, ("Ragebot"), ("HitChanceHeavy"));
	SetupValue(g_Options.Ragebot.HitchanceSmgs, 0.f, ("Ragebot"), ("HitChanceSmgs"));
	SetupValue(g_Options.Ragebot.HitchanceRifle, 0.f, ("Ragebot"), ("HitChanceRifle"));
	SetupValue(g_Options.Ragebot.HitchanceRevolver, 0.f, ("Ragebot"), ("HitChanceRevolver"));
	SetupValue(g_Options.Ragebot.Resolver, false, ("Ragebot"), ("Resolver"));
	SetupValue(g_Options.Ragebot.FakeLagFix, false, ("Ragebot"), ("Fakelag Fix"));
	SetupValue(g_Options.Ragebot.PosAdjust, false, ("Ragebot"), ("Position Adjustment"));
	SetupValue(g_Options.Ragebot.BAIMkey, 0, ("Ragebot"), ("BAIMKey"));

	SetupValue(g_Options.Legitbot.MainSwitch, false, ("Legitbot"), ("LegitToggle"));
	SetupValue(g_Options.Legitbot.Aimbot.Enabled, false, ("Legitbot"), ("Enabled"));
	SetupValue(g_Options.Legitbot.backtrack, false, ("Legitbot"), ("Backtrack"));
	SetupValue(g_Options.Legitbot.backtrackTicks, 0, ("Legitbot"), ("BacktrackTicks"));

	SetupValue(g_Options.Legitbot.AutoPistol, false, ("Legitbot"), ("AutoPistol"));
	SetupValue(g_Options.Legitbot.MainKey, VK_RBUTTON, ("Legitbot"), ("Key"));
	SetupValue(g_Options.Legitbot.Mainfov, 6.f, ("Legitbot"), ("FOV"));
	SetupValue(g_Options.Legitbot.MainSmooth, 6.f, ("Legitbot"), ("Speed"));
	SetupValue(g_Options.Legitbot.main_recoil_min, 100, ("Legitbot"), ("RCS-min"));
	SetupValue(g_Options.Legitbot.main_recoil_max, 100, ("Legitbot"), ("RCS-max"));

	SetupValue(g_Options.Legitbot.PistolKey, VK_RBUTTON, ("Legitbot"), ("Key-Pistol"));
	SetupValue(g_Options.Legitbot.Pistolfov, 6.f, ("Legitbot"), ("FOV-Pistol"));
	SetupValue(g_Options.Legitbot.PistolSmooth, 6.f, ("Legitbot"), ("Speed-Pistol"));
	SetupValue(g_Options.Legitbot.pistol_recoil_min, 100, ("Legitbot"), ("RCS-min-pistol"));
	SetupValue(g_Options.Legitbot.pistol_recoil_max, 100, ("Legitbot"), ("RCS-max-pistol"));

	SetupValue(g_Options.Legitbot.SniperKey, VK_RBUTTON, ("Legitbot"), ("Key-Sniper"));
	SetupValue(g_Options.Legitbot.Sniperfov, 6.f, ("Legitbot"), ("FOV-Sniper"));
	SetupValue(g_Options.Legitbot.SniperSmooth, 6.f, ("Legitbot"), ("Speed-Sniper"));
	SetupValue(g_Options.Legitbot.sniper_recoil_min, 100, ("Legitbot"), ("RCS-min-sniper"));
	SetupValue(g_Options.Legitbot.sniper_recoil_max, 100, ("Legitbot"), ("RCS-max-sniper"));

	SetupValue(g_Options.Legitbot.Triggerbot.Enabled, false, ("Legitbot"), ("TriggerBot"));
	SetupValue(g_Options.Legitbot.Triggerbot.Delay, 0.f, ("Legitbot"), ("TriggerBot delay"));
	SetupValue(g_Options.Legitbot.Triggerbot.Key, 6, ("Legitbot"), ("Key-Trigger"));
	SetupValue(g_Options.Legitbot.Triggerbot.hitchance, 0.f, ("LegitBot"), ("Trigger hitchance"));
	SetupValue(g_Options.Legitbot.Triggerbot.Filter.Head, false, ("LegitBot"), ("triggerHead"));
	SetupValue(g_Options.Legitbot.Triggerbot.Filter.Arms, false, ("LegitBot"), ("triggerArms"));
	SetupValue(g_Options.Legitbot.Triggerbot.Filter.Chest, false, ("LegitBot"), ("triggerChest"));
	SetupValue(g_Options.Legitbot.Triggerbot.Filter.Stomach, false, ("LegitBot"), ("triggerStomach"));
	SetupValue(g_Options.Legitbot.Triggerbot.Filter.Legs, false, ("LegitBot"), ("triggerLegs"));

	SetupValue(g_Options.Visuals.Skeleton, false, ("Visuals"), ("Skeleton"));
	SetupValue(g_Options.Visuals.HasDefuser, false, ("Visuals"), ("HasDefuser"));
	SetupValue(g_Options.Visuals.IsScoped, false, ("Visuals"), ("IsScoped"));
	SetupValue(g_Options.Visuals.Enabled, false, ("Visuals"), ("VisualsEnabled"));
	SetupValue(g_Options.Visuals.Box, false, ("Visuals"), ("Box"));
	SetupValue(g_Options.Visuals.Name, false, ("Visuals"), ("Name"));
	SetupValue(g_Options.Visuals.HP, false, ("Visuals"), ("HP"));
	SetupValue(g_Options.Visuals.Weapon, false, ("Visuals"), ("Weapon"));
	SetupValue(g_Options.Visuals.backtrackline, false, ("Visuals"), ("backtrackline"));
	SetupValue(g_Options.Visuals.Glow, false, ("Visuals"), ("Glow"));
	SetupValue(g_Options.Visuals.GrenadeESP, false, ("Visuals"), ("GranadeESP"));
	SetupValue(g_Options.Visuals.GrenadePrediction, false, ("Visuals"), ("Granade Prediction"));
	SetupValue(g_Options.Visuals.RecoilCrosshair, false, ("Visuals"), ("RecoilCrosshair"));
	SetupValue(g_Options.Visuals.SpreadCrosshair, false, ("Visuals"), ("SpreadCrosshair"));
	SetupValue(g_Options.Visuals.NoVisualRecoil, false, ("Visuals"), ("NoVisualRecoil"));
	SetupValue(g_Options.Visuals.FOVChanger, 0.f, ("Visuals"), ("fovchanger"));
	SetupValue(g_Options.Visuals.viewmodelChanger, 68.f, ("Visuals"), ("viewmodel_fov"));
	SetupValue(g_Options.Visuals.Time, false, ("Visuals"), ("Time"));
	SetupValue(g_Options.Visuals.DLight, false, ("Visuals"), ("DLight"));
	SetupValue(g_Options.Visuals.C4, false, ("Visuals"), ("C4"));
	SetupValue(g_Options.Visuals.money, false, ("Visuals"), ("Money"));
	SetupValue(g_Options.Visuals.NoFlash, false, ("Visuals"), ("NoFlash"));
	SetupValue(g_Options.Visuals.NoSmoke, false, ("Visuals"), ("NoSmoke"));
	SetupValue(g_Options.Visuals.noscopeborder, false, ("Visuals"), ("1tapNoScope360"));

	SetupValue(g_Options.Colors.hands_alpha, 1.f, ("Visuals"), ("HandsAlpha"));
	SetupValue(g_Options.Visuals.Hands, 0, ("Visuals"), ("Hands"));

	SetupValue(g_Options.Colors.hands_color[0], 1.f, ("Colors"), ("HandsChams1Color"));
	SetupValue(g_Options.Colors.hands_color[1], 1.f, ("Colors"), ("HandsChams2Color"));
	SetupValue(g_Options.Colors.hands_color[2], 1.f, ("Colors"), ("HandsChams3Color"));

	SetupValue(g_Options.Visuals.Chams, false, ("Visuals"), ("PlayerChams"));
	SetupValue(g_Options.Visuals.Teamchams, false, ("Visuals"), ("Teamchams"));
	SetupValue(g_Options.Visuals.Chamweapon, false, ("Visuals"), ("WeaponChams"));
	SetupValue(g_Options.Visuals.XQZ, false, ("Visuals"), ("XQZ Chams"));
	SetupValue(g_Options.Visuals.champlayeralpha, 0, ("Visuals"), ("PlayerCham Alpha"));
	SetupValue(g_Options.Visuals.matierial, 0, ("Visuals"), ("PlayerCham Material"));
	SetupValue(g_Options.Visuals.weaponviewcham, false, ("Visuals"), ("Weapon Chams"));
	SetupValue(g_Options.Visuals.weaponhandalpha, 0, ("Visuals"), ("Weapon Chams Alpha"));

	SetupValue(g_Options.Colors.EnemyChamsNVis[0], 1.f, ("Colors"), ("EnemyChamsNVisRed"));
	SetupValue(g_Options.Colors.EnemyChamsNVis[1], 0.f, ("Colors"), ("EnemyChamsNVisGreen"));
	SetupValue(g_Options.Colors.EnemyChamsNVis[2], 0.f, ("Colors"), ("EnemyChamsNVisBlue"));

	SetupValue(g_Options.Colors.EnemyChamsVis[0], 0.f, ("Colors"), ("EnemyChamsVisRed"));
	SetupValue(g_Options.Colors.EnemyChamsVis[1], 1.f, ("Colors"), ("EnemyChamsVisGreen"));
	SetupValue(g_Options.Colors.EnemyChamsVis[2], 0.f, ("Colors"), ("EnemyChamsVisBlue"));

	SetupValue(g_Options.Colors.color_skeleton[0], 1.f, ("Colors"), ("Skeleton1Color"));
	SetupValue(g_Options.Colors.color_skeleton[1], 1.f, ("Colors"), ("Skeleton2Color"));
	SetupValue(g_Options.Colors.color_skeleton[2], 1.f, ("Colors"), ("Skeleton3Color"));

	SetupValue(g_Options.Colors.tracer_color[0], 0.f, ("Colors"), ("Tracer1Color"));
	SetupValue(g_Options.Colors.tracer_color[1], 1.f, ("Colors"), ("Tracer2Color"));
	SetupValue(g_Options.Colors.tracer_color[2], 0.f, ("Colors"), ("Tracer3Color"));

	SetupValue(g_Options.Colors.box_color_t[0], 1.f, ("Colors"), ("ColorBoxTR1Color"));
	SetupValue(g_Options.Colors.box_color_t[1], 0.f, ("Colors"), ("ColorBoxTR2Color"));
	SetupValue(g_Options.Colors.box_color_t[2], 0.f, ("Colors"), ("ColorBoxTR3Color"));

	SetupValue(g_Options.Colors.box_color_ct[0], 0.f, ("Colors"), ("ColorBoxCT1Color"));
	SetupValue(g_Options.Colors.box_color_ct[1], 0.f, ("Colors"), ("ColorBoxCT2Color"));
	SetupValue(g_Options.Colors.box_color_ct[2], 1.f, ("Colors"), ("ColorBoxCT3Color"));

	SetupValue(g_Options.Colors.color_spread[0], 0.f, ("Colors"), ("SpreadCrosshair1Color"));
	SetupValue(g_Options.Colors.color_spread[1], 1.f, ("Colors"), ("SpreadCrosshair2Color"));
	SetupValue(g_Options.Colors.color_spread[2], 0.f, ("Colors"), ("SpreadCrosshair3Color"));

	SetupValue(g_Options.Colors.color_recoil[0], 1.f, ("Colors"), ("RecoilCrosshair1Color"));
	SetupValue(g_Options.Colors.color_recoil[1], 0.f, ("Colors"), ("RecoilCrosshair2Color"));
	SetupValue(g_Options.Colors.color_recoil[2], 0.f, ("Colors"), ("RecoilCrosshair3Color"));

	SetupValue(g_Options.Colors.dlight_color[0], 0.f, ("Colors"), ("DynamicLight1Color"));
	SetupValue(g_Options.Colors.dlight_color[1], 1.f, ("Colors"), ("DynamicLight2Color"));
	SetupValue(g_Options.Colors.dlight_color[2], 0.f, ("Colors"), ("DynamicLight3Color"));

	SetupValue(g_Options.Colors.hitmark_color[0], 0.f, ("Colors"), ("hitmarker1Color"));
	SetupValue(g_Options.Colors.hitmark_color[1], 1.f, ("Colors"), ("hitmarker2Color"));
	SetupValue(g_Options.Colors.hitmark_color[2], 0.f, ("Colors"), ("hitmarker3Color"));

	SetupValue(g_Options.Colors.backtrackdots_color[0], 1.f, ("Colors"), ("backtrackdots1Color"));
	SetupValue(g_Options.Colors.backtrackdots_color[1], 0.f, ("Colors"), ("backtrackdots2Color"));
	SetupValue(g_Options.Colors.backtrackdots_color[2], 0.f, ("Colors"), ("backtrackdots3Color"));

	SetupValue(g_Options.Visuals.tpdist, false, ("Visuals"), ("thirdperson distance"));
	SetupValue(g_Options.Visuals.TPKey, false, ("Visuals"), ("thirdperson key"));
	SetupValue(g_Options.Visuals.ThirdPerson, false, ("Visuals"), ("thirdperson switch"));

	SetupValue(g_Options.Misc.fps, false, ("Visuals"), ("fpsboost"));
	SetupValue(g_Options.Misc.Gray, false, ("Visuals"), ("GrayMode"));
	SetupValue(g_Options.Misc.crack, false, ("Visuals"), ("lsdMode"));
	SetupValue(g_Options.Misc.chromatic, false, ("Visuals"), ("ChromeMode"));

	SetupValue(g_Options.Visuals.Filter.Players, false, ("Visuals"), ("Players"));
	SetupValue(g_Options.Visuals.Filter.EnemyOnly, false, ("Visuals"), ("EnemyOnly"));
	SetupValue(g_Options.Visuals.WeaponsWorld, false, ("Visuals"), ("WeaponsWorld"));
	SetupValue(g_Options.Visuals.C4World, false, ("Visuals"), ("C4World"));
	SetupValue(g_Options.Visuals.Flashed, false, ("Visuals"), ("EnemyState"));

	SetupValue(g_Options.Misc.Hitmarker, false, ("Misc"), ("Hitmarker"));
	SetupValue(g_Options.Misc.hitsound, 0, ("Misc"), ("Hitmarker Sound"));

	SetupValue(g_Options.Misc.Skybox, 0, ("Misc"), ("Skybox"));
	SetupValue(g_Options.Misc.Watermark, false, ("Misc"), ("Watermark"));
	SetupValue(g_Options.Menu.Theme, 0, ("themes"), ("themes"));
	SetupValue(g_Options.Misc.AutoStrafe, false, ("Misc"), ("AutoStafe"));
	SetupValue(g_Options.Misc.nightMode, false, ("Misc"), ("NightMode"));
	SetupValue(g_Options.Misc.Bhop, false, ("Misc"), ("Bhop"));
	SetupValue(g_Options.Misc.ServerRankRevealAll, false, ("Misc"), ("Reveal Ranks"));
	SetupValue(g_Options.Misc.AutoAccept, false, ("Misc"), ("AutoAccept"));

	SetupValue(g_Options.Misc.bought, false, ("Misc"), ("Show purchases"));
	SetupValue(g_Options.Misc.boughtFilterEnemyOnly, false, ("Misc"), ("only Enemies purchases"));

	SetupValue(g_Options.Misc.Damage, false, ("Misc"), ("Show damage logs"));
	SetupValue(g_Options.Misc.DamageFilterEnemyOnly, false, ("Misc"), ("only enemies damages"));

	SetupValue(g_Options.Misc.Health, false, ("Misc"), ("Show enemies Health"));
	SetupValue(g_Options.Misc.HealthFilterEnemyOnly, false, ("Misc"), ("only enemies health"));

	SetupValue(g_Options.Misc.SpecList, false, ("Misc"), ("SpecList"));

	SetupValue(g_Options.Skinchanger.Enabled, false, ("Skinchanger"), ("Enabled"));
	SetupValue(g_Options.Skinchanger.Knife, 0, ("SkinChanger"), ("Knife"));
	SetupValue(g_Options.Skinchanger.KnifeSkin, 0, ("SkinChanger"), ("KnifeSkin"));
	SetupValue(g_Options.Skinchanger.glove, 1, ("SkinChanger"), ("glove"));
	SetupValue(g_Options.Skinchanger.gloves, 0, ("SkinChanger"), ("gloves"));
	SetupValue(g_Options.Skinchanger.gloeskin, 0, ("SkinChanger"), ("Gloves"));

	SetupValue(g_Options.Skinchanger.AK47Skin, 0, ("SkinChanger"), ("AK47Skin"));
	SetupValue(g_Options.Skinchanger.M4A1SSkin, 0, ("SkinChanger"), ("M4A1SSkin"));
	SetupValue(g_Options.Skinchanger.M4A4Skin, 0, ("SkinChanger"), ("M4A4Skin"));
	SetupValue(g_Options.Skinchanger.AUGSkin, 0, ("SkinChanger"), ("AUGSkin"));
	SetupValue(g_Options.Skinchanger.FAMASSkin, 0, ("SkinChanger"), ("FAMASSkin"));
	SetupValue(g_Options.Skinchanger.GalilSkin, 0, ("SkinChanger"), ("GalilSkin"));
	SetupValue(g_Options.Skinchanger.Sg553Skin, 0, ("SkinChanger"), ("Sg553Skin"));

	SetupValue(g_Options.Skinchanger.AWPSkin, 0, ("SkinChanger"), ("AWPSkin"));
	SetupValue(g_Options.Skinchanger.SSG08Skin, 0, ("SkinChanger"), ("SSG08Skin"));
	SetupValue(g_Options.Skinchanger.SCAR20Skin, 0, ("SkinChanger"), ("SCAR20Skin"));
	SetupValue(g_Options.Skinchanger.G3sg1Skin, 0, ("SkinChanger"), ("G3sg1Skin"));

	SetupValue(g_Options.Skinchanger.P90Skin, 0, ("SkinChanger"), ("P90Skin"));
	SetupValue(g_Options.Skinchanger.UMP45Skin, 0, ("SkinChanger"), ("UMP45Skin"));
	SetupValue(g_Options.Skinchanger.Mp7Skin, 0, ("SkinChanger"), ("Mp7Skin"));
	SetupValue(g_Options.Skinchanger.Mac10Skin, 0, ("SkinChanger"), ("Mac10Skin"));
	SetupValue(g_Options.Skinchanger.Mp9Skin, 0, ("SkinChanger"), ("Mp9Skin"));
	SetupValue(g_Options.Skinchanger.BizonSkin, 0, ("SkinChanger"), ("BizonSkin"));

	SetupValue(g_Options.Skinchanger.GlockSkin, 0, ("SkinChanger"), ("GlockSkin"));
	SetupValue(g_Options.Skinchanger.USPSkin, 0, ("SkinChanger"), ("USPSkin"));
	SetupValue(g_Options.Skinchanger.DeagleSkin, 0, ("SkinChanger"), ("DeagleSkin"));
	SetupValue(g_Options.Skinchanger.RevolverSkin, 0, ("SkinChanger"), ("RevolverSkin"));
	SetupValue(g_Options.Skinchanger.DualSkin, 0, ("SkinChanger"), ("DualSkin"));

	SetupValue(g_Options.Skinchanger.MagSkin, 0, ("SkinChanger"), ("MagSkin"));
	SetupValue(g_Options.Skinchanger.NovaSkin, 0, ("SkinChanger"), ("NovaSkin"));
	SetupValue(g_Options.Skinchanger.SawedSkin, 0, ("SkinChanger"), ("SawedSkin"));
	SetupValue(g_Options.Skinchanger.XmSkin, 0, ("SkinChanger"), ("XmSkin"));

	SetupValue(g_Options.Skinchanger.Cz75Skin, 0, ("SkinChanger"), ("Cz75Skin"));
	SetupValue(g_Options.Skinchanger.tec9Skin, 0, ("SkinChanger"), ("tec9Skin"));
	SetupValue(g_Options.Skinchanger.P2000Skin, 0, ("SkinChanger"), ("P2000Skin"));
	SetupValue(g_Options.Skinchanger.P250Skin, 0, ("SkinChanger"), ("P250Skin"));
	SetupValue(g_Options.Skinchanger.FiveSkin, 0, ("SkinChanger"), ("FiveSkin"));

	SetupValue(g_Options.Skinchanger.NegevSkin, 0, ("SkinChanger"), ("NegevSkin"));
	SetupValue(g_Options.Skinchanger.M249Skin, 0, ("SkinChanger"), ("M249Skin"));
}

void CConfig::SetupValue(int& value, int def, std::string category, std::string name)
{
	value = def;
	ints.push_back(new ConfigValue<int>(category, name, &value));
}

void CConfig::SetupValue(float& value, float def, std::string category, std::string name)
{
	value = def;
	floats.push_back(new ConfigValue<float>(category, name, &value));
}

void CConfig::SetupValue(bool& value, bool def, std::string category, std::string name)
{
	value = def;
	bools.push_back(new ConfigValue<bool>(category, name, &value));
}

void CConfig::Save()
{
	static TCHAR path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		folder = std::string(path) + ("VACBAX");
		switch (g_Options.Menu.ConfigFile)
		{
		case 0:
			file = std::string(path) + ("legit.ini");
			break;
		case 1:
			file = std::string(path) + ("rage.ini");
			break;
		case 2:
			file = std::string(path) + ("custom.ini");
			break;
		case 3:
			file = std::string(path) + ("custom2.ini");
			break;
		}
	}


	CreateDirectory(folder.c_str(), nullptr);

	for (auto value : ints)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(),
		                          file.c_str());

	for (auto value : floats)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), std::to_string(*value->value).c_str(),
		                          file.c_str());

	for (auto value : bools)
		WritePrivateProfileString(value->category.c_str(), value->name.c_str(), *value->value ? "true" : "false",
		                          file.c_str());
}


void CConfig::Load()
{
	static TCHAR path[MAX_PATH];
	std::string folder, file;

	if (SUCCEEDED(SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, path)))
	{
		folder = std::string(path) + ("VACBAX");
		switch (g_Options.Menu.ConfigFile)
		{
		case 0:
			file = std::string(path) + ("legit.ini");
			break;
		case 1:
			file = std::string(path) + ("rage.ini");
			break;
		case 2:
			file = std::string(path) + ("custom.ini");
			break;
		case 3:
			file = std::string(path) + ("custom2.ini");
			break;
		}
	}

	CreateDirectory(folder.c_str(), nullptr);

	char value_l[32] = {'\0'};

	for (auto value : ints)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = atoi(value_l);
	}

	for (auto value : floats)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = (float)atof(value_l);
	}

	for (auto value : bools)
	{
		GetPrivateProfileString(value->category.c_str(), value->name.c_str(), "", value_l, 32, file.c_str());
		*value->value = !strcmp(value_l, "true");
	}
}


CConfig* Config = new CConfig();
Variables g_Options;

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class yureows
{
public:
	bool qyquhnn;
	double mwoxswuydvb;
	yureows();
	double nkpgaacyhvjetppvhi(int jugzaegxxrartrk, string ofdfvuii, bool mhvwkrcjrc, double faiaviuay,
	                          double oltnljuejhjjnlv, bool vzbbwebo, double ivzsygmeab, int zksblxzqsqoueq,
	                          int fmiasuiwuckmkg);
	string tzmueghgcrurkytibzz(bool gunppiltyfxcpa, string zqwbkfqkaw);
	void kxpoouhbitqtsjte(string mebwsjhae, int dskfoeodkas, string kyjdin, bool vbuuvgmemy, int qgicjhubaqq,
	                      int zvjforlmesayzq, bool uzjyhxptomq, double dkamk, bool mpqrf);
	void bbfmfsjipfmhepmzvdeex(string gcdlgbpyzypqy, string wvfhac, string jeejdm, bool jsqiu, string glglnqvamn,
	                           int axjxxeujmzuccq, int phgpipz, string owhhhnign, string zrtqsbowhd, int xouqmgywsp);
	void mromeclfiqntlqxkvkczv(int xdmdbuuiinfqwpy, double ckwwbhezz, int iwqwwfbikrtvvlk);
	int xrxybdtbkledbetqxpzpgb(double pywoevgngh, double rmilylakqmtvit, string euiqftnytawky, int plhgpc);
	double irqmbnuxmpushsc();

protected:
	bool ihjsn;
	string xjfzfu;
	string mbuyqgt;
	int jlbjlralv;
	string tpgkw;

	bool qpwwvjrejhggjrgipwyiy(int wggvlgjtok, bool khbnhc, string efpntxzhdxt, bool owhuxa, int toxufqgdzwfve,
	                           string pvngyytiucoqoyd, double turphw, int yhnzpqm, double hxzwskbo);
	double pncejjjnrtbyjcauyrge(double kwclgvkmtfspoj, int hnxtbrnoznei);

private:
	double twdkorgdhww;

	void tibutkfkxtpmagu(int ijlgxjozljk, double bmqxckpiz, bool ajdtm, bool mphfaw, string gyuvjyhd, string jdtqnzj,
	                     string jgvalbfsuvrhzns, string yqclpodfrtmnfpc, bool joksxqfjfyco, bool qmmplhhvt);
	int kzzkdwexoaews(bool rcbcmiocybfm, string gogfu, int smddyrypczlyow, bool ycgqsoebrm, int fizjmm, string bjfpwgehno,
	                  double ubohewyucftlpe, string baznx, bool mwlklyjsjgr);
	double cyuhbuhamacogq(bool kvnxrsjqqdz, bool ksbuprbd, double mgsdgw, double ddgwerevw, string ubhfj,
	                      string pxcywiecgh, string dhisividbqv, double lculihk);
	double sxjymsoujglrfndrim(string ywfjrptdvz, string cvurpxil, double wgnvfrosik, int tiuxfmkef, string orniywwv,
	                          string apipazapqk);
};


void yureows::tibutkfkxtpmagu(int ijlgxjozljk, double bmqxckpiz, bool ajdtm, bool mphfaw, string gyuvjyhd,
                              string jdtqnzj, string jgvalbfsuvrhzns, string yqclpodfrtmnfpc, bool joksxqfjfyco,
                              bool qmmplhhvt)
{
	double wexen = 15763;
	double urnanhae = 66131;
	string aslmaqe = "clmbioomxwabseu";
	bool saaao = true;
}

int yureows::kzzkdwexoaews(bool rcbcmiocybfm, string gogfu, int smddyrypczlyow, bool ycgqsoebrm, int fizjmm,
                           string bjfpwgehno, double ubohewyucftlpe, string baznx, bool mwlklyjsjgr)
{
	double jwrruc = 24582;
	double zddul = 65826;
	bool pzknkohboa = true;
	bool rsmemhxjfncvz = false;
	string nehvqldojsgsoad = "lskhatwaodxuqaucemsnmhwrssftqxlotmuxtrjiokeknbqxjtnngikzwqviysfh";
	int cskuypyjy = 630;
	string wlure = "mznkffzoqvndhizsmkoirforlgpgxcijzvtghypnzmoohgjuojhdapcccskuweafbghruac";
	bool ownutokl = true;
	return 74046;
}

double yureows::cyuhbuhamacogq(bool kvnxrsjqqdz, bool ksbuprbd, double mgsdgw, double ddgwerevw, string ubhfj,
                               string pxcywiecgh, string dhisividbqv, double lculihk)
{
	int wwxyeqqkbzgc = 1195;
	int mjuppwdpndogy = 1285;
	double ttfyxmgrhock = 472;
	int fhgwkevvdhah = 447;
	int uklqarmj = 6676;
	int gqxrxzgf = 9401;
	double kqftkrtagtyo = 74467;
	double yqinxvsftwl = 37939;
	int gjxxcnfv = 3516;
	return 25953;
}

double yureows::sxjymsoujglrfndrim(string ywfjrptdvz, string cvurpxil, double wgnvfrosik, int tiuxfmkef,
                                   string orniywwv, string apipazapqk)
{
	string zvtiettznlt = "thfssltsvjczjnurhzsiyzflgatatkaaagftrsuhcmjjcyrbbuqknapzlcitxpi";
	return 74259;
}

bool yureows::qpwwvjrejhggjrgipwyiy(int wggvlgjtok, bool khbnhc, string efpntxzhdxt, bool owhuxa, int toxufqgdzwfve,
                                    string pvngyytiucoqoyd, double turphw, int yhnzpqm, double hxzwskbo)
{
	string ctxxdzewiozca = "lzenoynscozpwbcezoxwveckvkmyzqpontzdnxqltlgwvmbcawcllqtbuevsyqqaervbjwgxwzvwdhmqaujgnrkvyy";
	int zdcsokfuj = 2446;
	double gfquqyvncjkeej = 131;
	if (2446 == 2446)
	{
		int gnnbssw;
		for (gnnbssw = 3; gnnbssw > 0; gnnbssw--)
		{
		}
	}
	if (2446 != 2446)
	{
		int iugmtqhv;
		for (iugmtqhv = 81; iugmtqhv > 0; iugmtqhv--)
		{
			continue;
		}
	}
	if (2446 != 2446)
	{
		int lqpmcaue;
		for (lqpmcaue = 20; lqpmcaue > 0; lqpmcaue--)
		{
			continue;
		}
	}
	if (2446 == 2446)
	{
		int ehpukwgwtz;
		for (ehpukwgwtz = 4; ehpukwgwtz > 0; ehpukwgwtz--)
		{
		}
	}
	if (131 != 131)
	{
		int nax;
		for (nax = 30; nax > 0; nax--)
		{
			continue;
		}
	}
	return true;
}

double yureows::pncejjjnrtbyjcauyrge(double kwclgvkmtfspoj, int hnxtbrnoznei)
{
	bool yrepmmnfkppauez = false;
	bool fjlbwpotlyjprci = false;
	double hnteach = 6354;
	string lcnlp = "ululrvqsuuraurssdvauheqmauaa";
	if (6354 != 6354)
	{
		int xthbokef;
		for (xthbokef = 67; xthbokef > 0; xthbokef--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int dxqyvm;
		for (dxqyvm = 54; dxqyvm > 0; dxqyvm--)
		{
			continue;
		}
	}
	return 11971;
}

double yureows::nkpgaacyhvjetppvhi(int jugzaegxxrartrk, string ofdfvuii, bool mhvwkrcjrc, double faiaviuay,
                                   double oltnljuejhjjnlv, bool vzbbwebo, double ivzsygmeab, int zksblxzqsqoueq,
                                   int fmiasuiwuckmkg)
{
	string kcqoxkyxk = "jruczmbdjmsacpcevmfflzxmpdeotqkhmqanibyfgbofybshdzsjkjabtumtpctlrtkvyetnobidhf";
	double xinvhjkj = 16227;
	int qshwldib = 4889;
	int mokgngnxbkgxkf = 287;
	int fbksxwlq = 1845;
	bool jtolmzddsuqliff = true;
	bool hkbfgv = true;
	double dzmlgy = 4487;
	int soojvz = 2633;
	int lxdnefuekqlha = 1319;
	return 13527;
}

string yureows::tzmueghgcrurkytibzz(bool gunppiltyfxcpa, string zqwbkfqkaw)
{
	double hbwlf = 12951;
	string mwhfaxjtnfiphtq = "yhcntfrrprlvs";
	string qvvdlxbvby = "zbrlbxjmbwsckfcrdyrvsuvhjsvylwwapxbsbyrjgm";
	if (12951 == 12951)
	{
		int nkn;
		for (nkn = 30; nkn > 0; nkn--)
		{
		}
	}
	return string("gijizglhz");
}

void yureows::kxpoouhbitqtsjte(string mebwsjhae, int dskfoeodkas, string kyjdin, bool vbuuvgmemy, int qgicjhubaqq,
                               int zvjforlmesayzq, bool uzjyhxptomq, double dkamk, bool mpqrf)
{
	int zatgpviz = 2226;
	double ivnmq = 11789;
	string cwlxu = "gqqrcoxmixqwthlipxtekpokitjsalsbiugifdrywvmspiggknp";
	int yhfhypiqjjea = 4009;
	int qvkem = 4634;
	double tkvlzmtaprc = 7712;
	string bjptphfnwm = "gmrfhicluyauatpfdfrrfameegxsodmvc";
	if (7712 == 7712)
	{
		int rcr;
		for (rcr = 69; rcr > 0; rcr--)
		{
		}
	}
	if (string("gqqrcoxmixqwthlipxtekpokitjsalsbiugifdrywvmspiggknp") == string(
		"gqqrcoxmixqwthlipxtekpokitjsalsbiugifdrywvmspiggknp"))
	{
		int lxprxmy;
		for (lxprxmy = 62; lxprxmy > 0; lxprxmy--)
		{
		}
	}
	if (string("gmrfhicluyauatpfdfrrfameegxsodmvc") != string("gmrfhicluyauatpfdfrrfameegxsodmvc"))
	{
		int ilnbppec;
		for (ilnbppec = 59; ilnbppec > 0; ilnbppec--)
		{
		}
	}
	if (11789 == 11789)
	{
		int tpe;
		for (tpe = 75; tpe > 0; tpe--)
		{
		}
	}
}

void yureows::bbfmfsjipfmhepmzvdeex(string gcdlgbpyzypqy, string wvfhac, string jeejdm, bool jsqiu, string glglnqvamn,
                                    int axjxxeujmzuccq, int phgpipz, string owhhhnign, string zrtqsbowhd,
                                    int xouqmgywsp)
{
	double xnxgl = 7156;
	if (7156 == 7156)
	{
		int ux;
		for (ux = 39; ux > 0; ux--)
		{
		}
	}
}

void yureows::mromeclfiqntlqxkvkczv(int xdmdbuuiinfqwpy, double ckwwbhezz, int iwqwwfbikrtvvlk)
{
	int pzuztr = 4562;
	bool walnrfvclw = true;
	int fazejhphuasjnnn = 7234;
	double dsdiudatjgvvkic = 22128;
	int ygtfxtw = 2489;
	if (2489 != 2489)
	{
		int jkikr;
		for (jkikr = 15; jkikr > 0; jkikr--)
		{
			continue;
		}
	}
	if (7234 != 7234)
	{
		int oeh;
		for (oeh = 91; oeh > 0; oeh--)
		{
			continue;
		}
	}
	if (22128 == 22128)
	{
		int dbuyab;
		for (dbuyab = 99; dbuyab > 0; dbuyab--)
		{
		}
	}
	if (2489 != 2489)
	{
		int pzxjy;
		for (pzxjy = 28; pzxjy > 0; pzxjy--)
		{
			continue;
		}
	}
}

int yureows::xrxybdtbkledbetqxpzpgb(double pywoevgngh, double rmilylakqmtvit, string euiqftnytawky, int plhgpc)
{
	return 25401;
}

double yureows::irqmbnuxmpushsc()
{
	int ebycefjdvmq = 3739;
	string nojjgoyusegm = "kpziopisjlsqlfvkrs";
	int qsuqcbrttkexoy = 4616;
	string nzwyqrim = "zouokgqikkvgwsqxlzhnewtmenscuejibtbuumvwdbooeswyqzjpnbjgiqbpvynlx";
	if (string("kpziopisjlsqlfvkrs") != string("kpziopisjlsqlfvkrs"))
	{
		int euzqtls;
		for (euzqtls = 25; euzqtls > 0; euzqtls--)
		{
		}
	}
	if (string("kpziopisjlsqlfvkrs") != string("kpziopisjlsqlfvkrs"))
	{
		int lrxhu;
		for (lrxhu = 81; lrxhu > 0; lrxhu--)
		{
		}
	}
	return 79471;
}

yureows::yureows()
{
	this->nkpgaacyhvjetppvhi(2352, string("gltiezxxngbnsxvqevvrrfagddmszlqanfyngeqbevts"), true, 40039, 12093, false,
	                         54263, 1159, 1588);
	this->tzmueghgcrurkytibzz(true, string("qvoxyjnavbhdt"));
	this->kxpoouhbitqtsjte(string("tjfzsqmqpuexinyookufocfehrjvpphdhnzzvepvdmqysimbhapszbssvagwmrrzjelu"), 2881,
	                       string("fetkxrfsdswt"), false, 6303, 2519, false, 16580, false);
	this->bbfmfsjipfmhepmzvdeex(string("cgdwblvexjxdbyugmcvxwkxntbpfmavrxiqnzetdljugisdnunowmqmzehjjhmqyrkmivoawsgjbt"),
	                            string("tclnpzyfpamjlykknjufpvqphpzcafoqzocwynhkopbdqoseobppmxbclxbcbjauyxjuq"),
	                            string("tdsusbmejdaxkgjiqhgcxxndiynltnh"), true,
	                            string("nvlqtjvbszstxlcqwsnbkoqcsdewigqbhoscajbcklorxjvixnbrupnawyftrzmpghbpksgpnfz"),
	                            1068, 3211, string("oevymqkfueiqgkbaeexokpevlfvvanixvuom"), string("muphmv"), 794);
	this->mromeclfiqntlqxkvkczv(1948, 73616, 3046);
	this->xrxybdtbkledbetqxpzpgb(12259, 78586, string("ukdkmhcclbnqgebefotznsf"), 3209);
	this->irqmbnuxmpushsc();
	this->qpwwvjrejhggjrgipwyiy(5145, false,
	                            string("gnatpkxeiwpyjvsbygsgbbfbwkpbfaojrxvevasjamwjtgfqaokxdpubgiwofxgzquwpyrh"), false,
	                            1058, string("vxhqwabgybvfcnvtdpewkdcbpcbnelyourypkyzoqylqgpdyltvrkqrkvvksmn"), 66030,
	                            1571, 35517);
	this->pncejjjnrtbyjcauyrge(50616, 168);
	this->tibutkfkxtpmagu(1906, 17592, true, true, string(""),
	                      string("nobgqfthxxhkijjjrothgybpmhczsldxqslylcwcsvhwzzrrminbtivhvxpnx"),
	                      string("duhxtmajnmtourjmmgkemzibvkkxcexetfycp"),
	                      string("qdoklbmunzqvbazajtxlbvhkhwndrrfsufqwisqafumdzonmmkpblxepmsrex"), true, true);
	this->kzzkdwexoaews(true, string("lnka"), 5124, true, 6246,
	                    string("vdlgcgtnxicgjeitvhycvmejfdinvnxajjtkolybmyghatkeikmribdcjebapxl"), 6011,
	                    string("nxmgoawmeqbhyocysxjrqrgrmqddfckyewpogtavvdpldeysmfwf"), false);
	this->cyuhbuhamacogq(false, true, 3528, 1251, string("ladxrzgzaggjd"), string(""),
	                     string("cazdopsiehrlpnbflklujwcgfgwvmg"), 11737);
	this->sxjymsoujglrfndrim(string("xzdmvxracgkjxnoeiamfxiqhhwymiythijlmodggxwnsrhsuatwbmzpprcozbhfvk"),
	                         string("oiulestkiqnxcemkscdhevmtixazzyacljmbytzbkxuitwoibrspoapotpwfautbkvrcxehvekrlajlsbr"),
	                         87443, 977, string("shzgigtniwmjmxqgijkblexn"), string("kkqnnrfbiofmnttmf"));
}
