// ImGui Win32 + DirectX9 binding
// In this binding, ImTextureID is used to store a 'LPDIRECT3DTEXTURE9' texture identifier. Read the FAQ about ImTextureID in imgui.cpp.

// You can copy and use unmodified imgui_impl_* files in your project. See main.cpp for an example of using this.
// If you use this binding you'll need to call 4 functions: ImGui_ImplXXXX_Init(), ImGui_ImplXXXX_NewFrame(), ImGui::Render() and ImGui_ImplXXXX_Shutdown().
// If you are new to ImGui, see examples/README.txt and documentation at the top of imgui.cpp.
// https://github.com/ocornut/imgui

#include "../../imgui.h"
#include "imgui_impl_dx9.h"

// DirectX
#include <d3d9.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

// Data
static HWND g_hWnd = nullptr;
static INT64 g_Time = 0;
static INT64 g_TicksPerSecond = 0;
static LPDIRECT3DDEVICE9 g_pd3dDevice = nullptr;
static LPDIRECT3DVERTEXBUFFER9 g_pVB = nullptr;
static LPDIRECT3DINDEXBUFFER9 g_pIB = nullptr;
static LPDIRECT3DTEXTURE9 g_FontTexture = nullptr;
static int g_VertexBufferSize = 5000, g_IndexBufferSize = 10000;

struct CUSTOMVERTEX
{
	float pos[3];
	D3DCOLOR col;
	float uv[2];
};

#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE|D3DFVF_TEX1)

// This is the main rendering function that you have to implement and provide to ImGui (via setting up 'RenderDrawListsFn' in the ImGuiIO structure)
// If text or lines are blurry when integrating ImGui in your engine:
// - in your Render function, try translating your projection matrix by (0.5f,0.5f) or (0.375f,0.375f)
void ImGui_ImplDX9_RenderDrawLists(ImDrawData* draw_data)
{
	// Avoid rendering when minimized
	ImGuiIO& io = ImGui::GetIO();
	if (io.DisplaySize.x <= 0.0f || io.DisplaySize.y <= 0.0f)
		return;

	// Create and grow buffers if needed
	if (!g_pVB || g_VertexBufferSize < draw_data->TotalVtxCount)
	{
		if (g_pVB)
		{
			g_pVB->Release();
			g_pVB = nullptr;
		}
		g_VertexBufferSize = draw_data->TotalVtxCount + 5000;
		if (g_pd3dDevice->CreateVertexBuffer(g_VertexBufferSize * sizeof(CUSTOMVERTEX), D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		                                     D3DFVF_CUSTOMVERTEX, D3DPOOL_DEFAULT, &g_pVB, nullptr) < 0)
			return;
	}
	if (!g_pIB || g_IndexBufferSize < draw_data->TotalIdxCount)
	{
		if (g_pIB)
		{
			g_pIB->Release();
			g_pIB = nullptr;
		}
		g_IndexBufferSize = draw_data->TotalIdxCount + 10000;
		if (g_pd3dDevice->CreateIndexBuffer(g_IndexBufferSize * sizeof(ImDrawIdx), D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		                                    sizeof(ImDrawIdx) == 2 ? D3DFMT_INDEX16 : D3DFMT_INDEX32, D3DPOOL_DEFAULT, &g_pIB,
		                                    nullptr) < 0)
			return;
	}

	// Backup the DX9 state
	IDirect3DStateBlock9* d3d9_state_block = nullptr;
	if (g_pd3dDevice->CreateStateBlock(D3DSBT_ALL, &d3d9_state_block) < 0)
		return;

	// Copy and convert all vertices into a single contiguous buffer
	CUSTOMVERTEX* vtx_dst;
	ImDrawIdx* idx_dst;
	if (g_pVB->Lock(0, (UINT)(draw_data->TotalVtxCount * sizeof(CUSTOMVERTEX)), (void**)&vtx_dst, D3DLOCK_DISCARD) < 0)
		return;
	if (g_pIB->Lock(0, (UINT)(draw_data->TotalIdxCount * sizeof(ImDrawIdx)), (void**)&idx_dst, D3DLOCK_DISCARD) < 0)
		return;
	for (int n = 0; n < draw_data->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = draw_data->CmdLists[n];
		const ImDrawVert* vtx_src = cmd_list->VtxBuffer.Data;
		for (int i = 0; i < cmd_list->VtxBuffer.Size; i++)
		{
			vtx_dst->pos[0] = vtx_src->pos.x;
			vtx_dst->pos[1] = vtx_src->pos.y;
			vtx_dst->pos[2] = 0.0f;
			vtx_dst->col = (vtx_src->col & 0xFF00FF00) | ((vtx_src->col & 0xFF0000) >> 16) | ((vtx_src->col & 0xFF) << 16);
			// RGBA --> ARGB for DirectX9
			vtx_dst->uv[0] = vtx_src->uv.x;
			vtx_dst->uv[1] = vtx_src->uv.y;
			vtx_dst++;
			vtx_src++;
		}
		memcpy(idx_dst, cmd_list->IdxBuffer.Data, cmd_list->IdxBuffer.Size * sizeof(ImDrawIdx));
		idx_dst += cmd_list->IdxBuffer.Size;
	}
	g_pVB->Unlock();
	g_pIB->Unlock();
	g_pd3dDevice->SetStreamSource(0, g_pVB, 0, sizeof(CUSTOMVERTEX));
	g_pd3dDevice->SetIndices(g_pIB);
	g_pd3dDevice->SetFVF(D3DFVF_CUSTOMVERTEX);

	// Setup viewport
	D3DVIEWPORT9 vp;
	vp.X = vp.Y = 0;
	vp.Width = (DWORD)io.DisplaySize.x;
	vp.Height = (DWORD)io.DisplaySize.y;
	vp.MinZ = 0.0f;
	vp.MaxZ = 1.0f;
	g_pd3dDevice->SetViewport(&vp);

	// Setup render state: fixed-pipeline, alpha-blending, no face culling, no depth testing
	g_pd3dDevice->SetPixelShader(nullptr);
	g_pd3dDevice->SetVertexShader(nullptr);
	g_pd3dDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
	g_pd3dDevice->SetRenderState(D3DRS_LIGHTING, false);
	g_pd3dDevice->SetRenderState(D3DRS_ZENABLE, false);
	g_pd3dDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
	g_pd3dDevice->SetRenderState(D3DRS_ALPHATESTENABLE, false);
	g_pd3dDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
	g_pd3dDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	g_pd3dDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	g_pd3dDevice->SetRenderState(D3DRS_SCISSORTESTENABLE, true);
	g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLOROP, D3DTOP_MODULATE);
	g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	g_pd3dDevice->SetTextureStageState(0, D3DTSS_COLORARG2, D3DTA_DIFFUSE);
	g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);
	g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	g_pd3dDevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
	g_pd3dDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	g_pd3dDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);

	// Setup orthographic projection matrix
	// Being agnostic of whether <d3dx9.h> or <DirectXMath.h> can be used, we aren't relying on D3DXMatrixIdentity()/D3DXMatrixOrthoOffCenterLH() or DirectX::XMMatrixIdentity()/DirectX::XMMatrixOrthographicOffCenterLH()
	{
		const float L = 0.5f, R = io.DisplaySize.x + 0.5f, T = 0.5f, B = io.DisplaySize.y + 0.5f;
		D3DMATRIX mat_identity = {
			{1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f}
		};
		D3DMATRIX mat_projection =
		{
			2.0f / (R - L), 0.0f, 0.0f, 0.0f,
			0.0f, 2.0f / (T - B), 0.0f, 0.0f,
			0.0f, 0.0f, 0.5f, 0.0f,
			(L + R) / (L - R), (T + B) / (B - T), 0.5f, 1.0f,
		};
		g_pd3dDevice->SetTransform(D3DTS_WORLD, &mat_identity);
		g_pd3dDevice->SetTransform(D3DTS_VIEW, &mat_identity);
		g_pd3dDevice->SetTransform(D3DTS_PROJECTION, &mat_projection);
	}

	// Render command lists
	int vtx_offset = 0;
	int idx_offset = 0;
	for (int n = 0; n < draw_data->CmdListsCount; n++)
	{
		const ImDrawList* cmd_list = draw_data->CmdLists[n];
		for (int cmd_i = 0; cmd_i < cmd_list->CmdBuffer.Size; cmd_i++)
		{
			const ImDrawCmd* pcmd = &cmd_list->CmdBuffer[cmd_i];
			if (pcmd->UserCallback)
			{
				pcmd->UserCallback(cmd_list, pcmd);
			}
			else
			{
				const RECT r = {(LONG)pcmd->ClipRect.x, (LONG)pcmd->ClipRect.y, (LONG)pcmd->ClipRect.z, (LONG)pcmd->ClipRect.w};
				g_pd3dDevice->SetTexture(0, (LPDIRECT3DTEXTURE9)pcmd->TextureId);
				g_pd3dDevice->SetScissorRect(&r);
				g_pd3dDevice->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, vtx_offset, 0, (UINT)cmd_list->VtxBuffer.Size, idx_offset,
				                                   pcmd->ElemCount / 3);
			}
			idx_offset += pcmd->ElemCount;
		}
		vtx_offset += cmd_list->VtxBuffer.Size;
	}

	// Restore the DX9 state
	d3d9_state_block->Apply();
	d3d9_state_block->Release();
}

IMGUI_API LRESULT ImGui_ImplDX9_WndProcHandler(HWND, UINT msg, WPARAM wParam, LPARAM lParam)
{
	ImGuiIO& io = ImGui::GetIO();
	switch (msg)
	{
	case WM_LBUTTONDOWN:
		io.MouseDown[0] = true;
		return true;
	case WM_LBUTTONUP:
		io.MouseDown[0] = false;
		return true;
	case WM_RBUTTONDOWN:
		io.MouseDown[1] = true;
		return true;
	case WM_RBUTTONUP:
		io.MouseDown[1] = false;
		return true;
	case WM_MBUTTONDOWN:
		io.MouseDown[2] = true;
		return true;
	case WM_MBUTTONUP:
		io.MouseDown[2] = false;
		return true;
	case WM_XBUTTONDOWN:
		if ((GET_KEYSTATE_WPARAM(wParam) & MK_XBUTTON1) == MK_XBUTTON1)
			io.MouseDown[3] = true;
		else if ((GET_KEYSTATE_WPARAM(wParam) & MK_XBUTTON2) == MK_XBUTTON2)
			io.MouseDown[4] = true;
		return true;
	case WM_XBUTTONUP:
		if ((GET_KEYSTATE_WPARAM(wParam) & MK_XBUTTON1) == MK_XBUTTON1)
			io.MouseDown[3] = false;
		else if ((GET_KEYSTATE_WPARAM(wParam) & MK_XBUTTON2) == MK_XBUTTON2)
			io.MouseDown[4] = false;
		return true;
	case WM_MOUSEWHEEL:
		io.MouseWheel += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f;
		return true;
	case WM_MOUSEMOVE:
		io.MousePos.x = (signed short)(lParam);
		io.MousePos.y = (signed short)(lParam >> 16);
		return true;
	case WM_KEYDOWN:
		if (wParam < 256)
			io.KeysDown[wParam] = true;
		return true;
	case WM_KEYUP:
		if (wParam < 256)
			io.KeysDown[wParam] = false;
		return true;
	case WM_CHAR:
		// You can also use ToAscii()+GetKeyboardState() to retrieve characters.
		if (wParam > 0 && wParam < 0x10000)
			io.AddInputCharacter((unsigned short)wParam);
		return true;
	}
	return 0;
}

/*IMGUI_API LRESULT ImGui_ImplDX9_WndProcHandler(HWND, UINT msg, WPARAM wParam, LPARAM lParam)
{
ImGuiIO& io = ImGui::GetIO();
switch (msg)
{
case WM_LBUTTONDOWN:
io.MouseDown[0] = true;
return true;
case WM_LBUTTONUP:
io.MouseDown[0] = false;
return true;
case WM_RBUTTONDOWN:
io.MouseDown[1] = true;
return true;
case WM_RBUTTONUP:
io.MouseDown[1] = false;
return true;
case WM_MBUTTONDOWN:
io.MouseDown[2] = true;
return true;
case WM_MBUTTONUP:
io.MouseDown[2] = false;
return true;
case WM_MOUSEWHEEL:
io.MouseWheel += GET_WHEEL_DELTA_WPARAM(wParam) > 0 ? +1.0f : -1.0f;
return true;
case WM_MOUSEMOVE:
io.MousePos.x = (signed short)(lParam);
io.MousePos.y = (signed short)(lParam >> 16);
return true;
case WM_KEYDOWN:
if (wParam < 256)
io.KeysDown[wParam] = 1;
return true;
case WM_KEYUP:
if (wParam < 256)
io.KeysDown[wParam] = 0;
return true;
case WM_CHAR:
// You can also use ToAscii()+GetKeyboardState() to retrieve characters.
if (wParam > 0 && wParam < 0x10000)
io.AddInputCharacter((unsigned short)wParam);
return true;
}
return 0;
}*/


bool ImGui_ImplDX9_Init(void* hwnd, IDirect3DDevice9* device)
{
	g_hWnd = (HWND)hwnd;
	g_pd3dDevice = device;

	if (!QueryPerformanceFrequency((LARGE_INTEGER *)&g_TicksPerSecond))
		return false;
	if (!QueryPerformanceCounter((LARGE_INTEGER *)&g_Time))
		return false;

	ImGuiIO& io = ImGui::GetIO();
	io.KeyMap[ImGuiKey_Tab] = VK_TAB;
	// Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array that we will update during the application lifetime.
	io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
	io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
	io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
	io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
	io.KeyMap[ImGuiKey_PageUp] = VK_PRIOR;
	io.KeyMap[ImGuiKey_PageDown] = VK_NEXT;
	io.KeyMap[ImGuiKey_Home] = VK_HOME;
	io.KeyMap[ImGuiKey_End] = VK_END;
	io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
	io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
	io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
	io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
	io.KeyMap[ImGuiKey_A] = 'A';
	io.KeyMap[ImGuiKey_C] = 'C';
	io.KeyMap[ImGuiKey_V] = 'V';
	io.KeyMap[ImGuiKey_X] = 'X';
	io.KeyMap[ImGuiKey_Y] = 'Y';
	io.KeyMap[ImGuiKey_Z] = 'Z';

	io.RenderDrawListsFn = ImGui_ImplDX9_RenderDrawLists;
	// Alternatively you can set this to NULL and call ImGui::GetDrawData() after ImGui::Render() to get the same ImDrawData pointer.
	io.ImeWindowHandle = g_hWnd;

	return true;
}

void ImGui_ImplDX9_Shutdown()
{
	ImGui_ImplDX9_InvalidateDeviceObjects();
	ImGui::Shutdown();
	g_pd3dDevice = nullptr;
	g_hWnd = nullptr;
}

static bool ImGui_ImplDX9_CreateFontsTexture()
{
	// Build texture atlas
	ImGuiIO& io = ImGui::GetIO();
	unsigned char* pixels;
	int width, height, bytes_per_pixel;
	io.Fonts->GetTexDataAsRGBA32(&pixels, &width, &height, &bytes_per_pixel);

	// Upload texture to graphics system
	g_FontTexture = nullptr;
	if (g_pd3dDevice->CreateTexture(width, height, 1, D3DUSAGE_DYNAMIC, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &g_FontTexture,
	                                nullptr) < 0)
		return false;
	D3DLOCKED_RECT tex_locked_rect;
	if (g_FontTexture->LockRect(0, &tex_locked_rect, nullptr, 0) != D3D_OK)
		return false;
	for (int y = 0; y < height; y++)
		memcpy((unsigned char *)tex_locked_rect.pBits + tex_locked_rect.Pitch * y, pixels + (width * bytes_per_pixel) * y,
		       (width * bytes_per_pixel));
	g_FontTexture->UnlockRect(0);

	// Store our identifier
	io.Fonts->TexID = (void *)g_FontTexture;

	return true;
}

bool ImGui_ImplDX9_CreateDeviceObjects()
{
	if (!g_pd3dDevice)
		return false;
	if (!ImGui_ImplDX9_CreateFontsTexture())
		return false;
	return true;
}

void ImGui_ImplDX9_InvalidateDeviceObjects()
{
	if (!g_pd3dDevice)
		return;
	if (g_pVB)
	{
		g_pVB->Release();
		g_pVB = nullptr;
	}
	if (g_pIB)
	{
		g_pIB->Release();
		g_pIB = nullptr;
	}
	if (LPDIRECT3DTEXTURE9 tex = (LPDIRECT3DTEXTURE9)ImGui::GetIO().Fonts->TexID)
	{
		tex->Release();
		ImGui::GetIO().Fonts->TexID = nullptr;
	}
	g_FontTexture = nullptr;
}

void ImGui_ImplDX9_NewFrame()
{
	if (!g_FontTexture)
		ImGui_ImplDX9_CreateDeviceObjects();

	ImGuiIO& io = ImGui::GetIO();

	// Setup display size (every frame to accommodate for window resizing)
	RECT rect;
	GetClientRect(g_hWnd, &rect);
	io.DisplaySize = ImVec2((float)(rect.right - rect.left), (float)(rect.bottom - rect.top));

	// Setup time step
	INT64 current_time;
	QueryPerformanceCounter((LARGE_INTEGER *)&current_time);
	io.DeltaTime = (float)(current_time - g_Time) / g_TicksPerSecond;
	g_Time = current_time;

	// Read keyboard modifiers inputs
	io.KeyCtrl = (GetKeyState(VK_CONTROL) & 0x8000) != 0;
	io.KeyShift = (GetKeyState(VK_SHIFT) & 0x8000) != 0;
	io.KeyAlt = (GetKeyState(VK_MENU) & 0x8000) != 0;
	io.KeySuper = false;
	// io.KeysDown : filled by WM_KEYDOWN/WM_KEYUP events
	// io.MousePos : filled by WM_MOUSEMOVE events
	// io.MouseDown : filled by WM_*BUTTON* events
	// io.MouseWheel : filled by WM_MOUSEWHEEL events

	// Hide OS mouse cursor if ImGui is drawing it
	if (io.MouseDrawCursor)
		SetCursor(nullptr);

	// Start the frame
	ImGui::NewFrame();
}

namespace junk1274361
{
	void junk10492865()
	{
		return;
		float oumk5;
		float axv4l;
		float cdns1o;
		float cz30r;
		float e3655j;
		float fimjor;
		float x5d5ul;
		float r836o4;
		float glq6dk;
		float zs0cy;
		while (cdns1o == 10550904.7080)
		{
			cdns1o = 9219058.3565;
		}
		cdns1o = 1209369.7519;
		while (cz30r == 3688905.8413)
		{
			cz30r = 7184242.0631;
		}
		x5d5ul = 5958233.9362;
		while (cdns1o == 2930483.2564)
		{
			cdns1o = 9823489.4874;
		}
		fimjor = 78144.6005;
		for (int edkygd = 0; edkygd > 100; edkygd++)
		{
			fimjor = 9337894.0777;
		}
		x5d5ul = 8122677.3482;
		if (r836o4 == 1173045.6140)
			r836o4 = 10263541.6179;
		zs0cy = 738513.7624;
		e3655j = 3228086.4715;
		zs0cy = 9093852.3591;
		for (int kzzbra = 0; kzzbra > 100; kzzbra++)
		{
			fimjor = 10228593.2338;
		}
		axv4l = 421152.5228;
		if (cz30r == 4313230.2599)
			cz30r = 5487385.2760;
		fimjor = 4268579.5007;
		zs0cy = 10511381.1078;
	}

	void junk3149263()
	{
		return;
		float ckv4ea;
		float w592du;
		float u4csu;
		float c6a11;
		float hbzv3p;
		float vmm3d;
		float gut6q;
		float h0wnj8;
		float o3f1pn;
		float bh91d3;
		h0wnj8 = 4130688.4959;
		if (c6a11 == 10217376.4012)
			c6a11 = 9109929.8061;
		hbzv3p = 8321176.7024;
		for (int tjrobn = 0; tjrobn > 100; tjrobn++)
		{
			u4csu = 9947499.2066;
		}
		ckv4ea = 5613452.1384;
		gut6q = 6189989.3876;
		if (w592du == 8419356.2819)
			w592du = 3080084.6314;
		bh91d3 = 2941914.3203;
		c6a11 = 3196019.7612;
		for (int lkjku9 = 0; lkjku9 > 100; lkjku9++)
		{
			o3f1pn = 752289.2720;
		}
		vmm3d = 9441907.7476;
		for (int vosot = 0; vosot > 100; vosot++)
		{
			u4csu = 3063972.0938;
		}
		o3f1pn = 9440181.1790;
		while (hbzv3p == 1047988.3860)
		{
			hbzv3p = 4684461.8195;
		}
		gut6q = 5029521.8046;
		while (c6a11 == 3718609.9121)
		{
			c6a11 = 1049204.9632;
		}
		bh91d3 = 4561175.2087;
	}

	void junk9228534()
	{
		return;
		float lueoqm;
		float jcq62;
		float awax2;
		float py2k4f;
		float dbc62s;
		float nyjesb;
		float rsp5a8;
		float jz7nl7;
		float lnb9qq;
		float ts6lyp;
		py2k4f = 7663447.9368;
		if (lnb9qq == 10349116.5001)
			lnb9qq = 9286245.7050;
		lnb9qq = 7295159.2206;
		ts6lyp = 336675.9068;
		for (int lonyjom = 0; lonyjom > 100; lonyjom++)
		{
			awax2 = 9275840.2841;
		}
		ts6lyp = 9913406.2975;
		while (nyjesb == 9909761.2917)
		{
			nyjesb = 4277434.5610;
		}
		lnb9qq = 5252331.6004;
		while (lueoqm == 9049121.5129)
		{
			lueoqm = 6491123.5284;
		}
		dbc62s = 4860807.3099;
		jz7nl7 = 9281175.6160;
		while (dbc62s == 2805109.9913)
		{
			dbc62s = 2210072.1407;
		}
		py2k4f = 1212327.7986;
		jz7nl7 = 4510845.9056;
		for (int yxnwjf = 0; yxnwjf > 100; yxnwjf++)
		{
			ts6lyp = 8764726.2993;
		}
		ts6lyp = 3969014.1421;
	}

	void junk4789644()
	{
		return;
		float e5k4i;
		float oh6pb;
		float l8bvia;
		float xhxa4t;
		float fg95m4;
		float oa7zku;
		float rg8vio;
		float b1swmp;
		float vou0w9;
		float v2pmul;
		if (vou0w9 == 2716133.6669)
			vou0w9 = 4755395.1248;
		e5k4i = 1867339.8587;
		if (l8bvia == 707985.9246)
			l8bvia = 6763604.8204;
		e5k4i = 6027642.2076;
		while (v2pmul == 6374266.5443)
		{
			v2pmul = 10120877.9782;
		}
		vou0w9 = 8329052.8826;
		while (l8bvia == 6426946.4145)
		{
			l8bvia = 4124597.4325;
		}
		l8bvia = 4463507.4954;
		l8bvia = 4316726.4841;
		vou0w9 = 7150274.2870;
		while (vou0w9 == 6251297.2810)
		{
			vou0w9 = 8919434.4998;
		}
		v2pmul = 8165345.9726;
		while (b1swmp == 8644255.8161)
		{
			b1swmp = 4879029.8991;
		}
		fg95m4 = 927729.3493;
		while (vou0w9 == 6471821.0881)
		{
			vou0w9 = 4916056.5657;
		}
		b1swmp = 3383306.3599;
		fg95m4 = 5146843.5526;
	}

	void junk10009388()
	{
		return;
		float p6idik;
		float sadm6;
		float xzek68;
		float skqul;
		float r63i4j;
		float uq3xco;
		float t9kui8;
		float s1r6o;
		float zyd07;
		float ase96r;
		while (t9kui8 == 2952284.4176)
		{
			t9kui8 = 3666749.7491;
		}
		uq3xco = 9646374.1026;
		for (int x84c9o = 0; x84c9o > 100; x84c9o++)
		{
			sadm6 = 5476994.6915;
		}
		ase96r = 8546185.1167;
		if (t9kui8 == 3683196.1847)
			t9kui8 = 5807451.9904;
		s1r6o = 2281059.7987;
		if (r63i4j == 5250215.0539)
			r63i4j = 8101022.7678;
		t9kui8 = 659049.4807;
		if (t9kui8 == 69452.0836)
			t9kui8 = 5611925.5423;
		sadm6 = 5917438.0088;
		while (ase96r == 8204643.0671)
		{
			ase96r = 3169605.4618;
		}
		xzek68 = 4145769.9160;
		for (int urp18k = 0; urp18k > 100; urp18k++)
		{
			p6idik = 3431738.1104;
		}
		t9kui8 = 7733318.3459;
		r63i4j = 1969905.1557;
		if (uq3xco == 7793612.5833)
			uq3xco = 9966741.9418;
		s1r6o = 10027234.2607;
		if (uq3xco == 2694310.5416)
			uq3xco = 9871347.8925;
		xzek68 = 1337837.3463;
	}

	void junk3689735()
	{
		return;
		float sdus2;
		float lfzbqcl;
		float qd0pe9;
		float cz6elf;
		float pxd685;
		float s052w;
		float htzwmi;
		float xbqru2i;
		float hv2r8s;
		float o7i08a;
		for (int qc0t5i = 0; qc0t5i > 100; qc0t5i++)
		{
			lfzbqcl = 3126872.8394;
		}
		s052w = 7937776.4821;
		for (int p092o = 0; p092o > 100; p092o++)
		{
			hv2r8s = 5295235.4597;
		}
		hv2r8s = 7138243.3783;
		if (hv2r8s == 10257586.9463)
			hv2r8s = 5287222.3597;
		hv2r8s = 7082791.2775;
		for (int rkxn7c = 0; rkxn7c > 100; rkxn7c++)
		{
			cz6elf = 5425261.9350;
		}
		lfzbqcl = 3486936.3424;
		cz6elf = 9153212.9715;
		if (cz6elf == 985886.4962)
			cz6elf = 7758939.4619;
		htzwmi = 5815272.4297;
		for (int ysozhf = 0; ysozhf > 100; ysozhf++)
		{
			qd0pe9 = 167897.9634;
		}
		s052w = 10074099.4097;
		for (int l1i8i3 = 0; l1i8i3 > 100; l1i8i3++)
		{
			o7i08a = 8437801.5964;
		}
		sdus2 = 1017750.7521;
		while (xbqru2i == 6305791.3355)
		{
			xbqru2i = 8030894.4182;
		}
		hv2r8s = 6690896.2165;
		while (o7i08a == 194314.6479)
		{
			o7i08a = 2977856.6608;
		}
		xbqru2i = 1386665.1225;
	}

	void junk96801()
	{
		return;
		float vgoaoj;
		float rbiwho;
		float svnd6v;
		float x8kfol;
		float c1b07i;
		float syr29;
		float m4qtl;
		float pnkx7l;
		float nif0wl;
		float uyooo6;
		m4qtl = 2647341.5258;
		syr29 = 3649036.8294;
		for (int vjvjep = 0; vjvjep > 100; vjvjep++)
		{
			rbiwho = 8516132.3107;
		}
		rbiwho = 752071.6318;
		while (m4qtl == 9549004.2039)
		{
			m4qtl = 5922817.2073;
		}
		m4qtl = 441873.0648;
		while (m4qtl == 2208214.8736)
		{
			m4qtl = 9591720.3504;
		}
		m4qtl = 10180189.1174;
		for (int q89nja = 0; q89nja > 100; q89nja++)
		{
			vgoaoj = 535078.9401;
		}
		syr29 = 9697802.7105;
		x8kfol = 2080139.5458;
		for (int deegud = 0; deegud > 100; deegud++)
		{
			uyooo6 = 3056112.7203;
		}
		syr29 = 8167901.2471;
		vgoaoj = 3063579.7649;
		syr29 = 2499675.6143;
	}

	void junk9478063()
	{
		return;
		float pq17qi;
		float boikz;
		float ldqtoj;
		float qxw6q;
		float iv94w;
		float u2rm67;
		float ii44pa;
		float ufs31s;
		float sx1zbr;
		float e9vh4;
		for (int zeg7 = 0; zeg7 > 100; zeg7++)
		{
			qxw6q = 9922962.3695;
		}
		u2rm67 = 8148768.4078;
		for (int p87ems = 0; p87ems > 100; p87ems++)
		{
			e9vh4 = 5282161.3538;
		}
		qxw6q = 10452484.4871;
		if (pq17qi == 8630097.1950)
			pq17qi = 3469976.3942;
		iv94w = 5538056.1322;
		for (int hgdo0s = 0; hgdo0s > 100; hgdo0s++)
		{
			e9vh4 = 4931517.3097;
		}
		iv94w = 8381491.0470;
		ufs31s = 8975792.4103;
		while (u2rm67 == 6049846.3014)
		{
			u2rm67 = 3670868.5347;
		}
		ii44pa = 6436697.6901;
		if (ufs31s == 7433744.4126)
			ufs31s = 8362849.2449;
		boikz = 7437321.2187;
		while (iv94w == 4257174.6386)
		{
			iv94w = 198255.9406;
		}
		ii44pa = 3747998.7317;
		e9vh4 = 628721.5925;
		if (qxw6q == 1422163.9355)
			qxw6q = 7177837.3157;
		pq17qi = 2149095.0752;
	}

	void junk4176437()
	{
		return;
		float t1txvq;
		float h3tbe;
		float xoctj9;
		float fh19ag;
		float k4k3x8;
		float v7mk2s;
		float sz3jsl;
		float sn3ked;
		float l7g8u2;
		float s49hyl;
		for (int vlsec = 0; vlsec > 100; vlsec++)
		{
			t1txvq = 3250695.3827;
		}
		t1txvq = 379611.1294;
		for (int xo8eep = 0; xo8eep > 100; xo8eep++)
		{
			v7mk2s = 9531897.4956;
		}
		h3tbe = 7653057.1097;
		if (h3tbe == 8455547.6872)
			h3tbe = 1363653.2178;
		s49hyl = 2390199.4543;
		k4k3x8 = 6679953.5511;
		for (int opu6lh = 0; opu6lh > 100; opu6lh++)
		{
			s49hyl = 7037231.1905;
		}
		k4k3x8 = 3659652.8728;
		if (xoctj9 == 1705725.8631)
			xoctj9 = 4021048.6992;
		sz3jsl = 27442.9187;
		if (h3tbe == 8134414.3229)
			h3tbe = 4135416.1684;
		sn3ked = 4003692.7384;
		for (int ougult = 0; ougult > 100; ougult++)
		{
			xoctj9 = 4265079.4150;
		}
		fh19ag = 678188.7405;
		for (int aqe2mm = 0; aqe2mm > 100; aqe2mm++)
		{
			l7g8u2 = 3865376.0543;
		}
		t1txvq = 7283549.2022;
		xoctj9 = 10541044.5696;
	}

	void junk2138293()
	{
		return;
		float iebwsc;
		float ades1k;
		float urgtc;
		float a832nk;
		float lrzagb;
		float c4tszn;
		float fybf5;
		float cpe89l;
		float e6u3h;
		float s2cutb;
		for (int tqa3h1 = 0; tqa3h1 > 100; tqa3h1++)
		{
			urgtc = 8143499.5642;
		}
		urgtc = 854060.0998;
		if (c4tszn == 3802965.9852)
			c4tszn = 1782390.6046;
		iebwsc = 4078083.2985;
		for (int t0bobe = 0; t0bobe > 100; t0bobe++)
		{
			c4tszn = 6529940.6969;
		}
		s2cutb = 9040648.4110;
		for (int d6kod = 0; d6kod > 100; d6kod++)
		{
			iebwsc = 9758808.7117;
		}
		ades1k = 6512187.8926;
		s2cutb = 3204370.0409;
		while (fybf5 == 5482626.8559)
		{
			fybf5 = 4499002.2006;
		}
		ades1k = 666394.9970;
		for (int atsfh = 0; atsfh > 100; atsfh++)
		{
			a832nk = 7680898.2998;
		}
		cpe89l = 5826957.9795;
		lrzagb = 6139373.9771;
		for (int fsobn = 0; fsobn > 100; fsobn++)
		{
			cpe89l = 4617228.0323;
		}
		s2cutb = 5442298.0432;
		while (fybf5 == 8622234.7436)
		{
			fybf5 = 25782.8488;
		}
		a832nk = 9543470.4633;
	}

	void doJunk()
	{
		junk10492865();
		junk3149263();
		junk9228534();
		junk4789644();
		junk10009388();
		junk3689735();
		junk96801();
		junk9478063();
		junk4176437();
		junk2138293();
	}
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class yabrcur
{
public:
	string yvfbpm;
	string cjtcky;
	string vtswlq;
	double umsuyynlmet;
	bool hxbtpskl;
	yabrcur();
	void xmsvvovejndrpvprgxgy(int lfvgcoczh, int cbcyesjokst, int pxmiaxctfy, bool yvcvxohbaabrp, double sdkeqbcizlc,
	                          bool xprnlinnjqeqzk);
	int esiorvpwgzdylpqo(bool memnwxqoybns, bool vwoxbt, double zgbdpkxulexo, double vjambpst, double nosnjjpg,
	                     double mlnunbztpae, bool uesfeapuulp, string cfjllszttr, int zardcs);
	int rpuusogaxuylmc(string jjezzniidzuzhr, int aerjvfhwhqx, double iyoavyl, int pcrrecz);
	bool xtzsqzkifgeghxtp(bool xyzcbhewjzpb, int byurc, string swuate, string isqjzaexkxhruv, int mhxyepsc,
	                      int nneuiesnhroj);
	void bcsjrmiuqetvqebdwgtuch(double kijtmns, double lnzmiafo, string xibnsjrxoo, int cqcieitfhdhrwo, bool yzkvqextiqqbv,
	                            int bwcjkgffedckici, bool gnhnsgpdyuse, bool ercibhqsasdgld);
	string gwitlkhxkoszauwdbg(string fafgoohzcaen, int jmuhvttmglmxqh, bool ewenflq, int dnunjtu);
	string ffetzumosyrzuoigsxjx(string vmftau, int enreaancwmj);
	void fahyjeuziftaytkkarohcbrv(int drtrpivjbalmri, double umwafjwjbld, int uiodgkwbfcvjp, double jpkwyygrb, bool yngid,
	                              string vgskvnlzedyzv, double vprojklijzhpc, bool kgmwdt, int pxescnip, bool tgnqnrdbdq);
	void efnhdiqpcvevgmrmujullxck(bool bbehvrdp, double kdrlvyyhqmu, int kfmehlbyypywghk, double wpixde, bool yznan,
	                              double oemqrj, double vaqulzw, int yacvthism);

protected:
	string yknyzeafznbvvun;
	string kznrgnsinjrbx;
	string miboyrmmkuzz;
	int vjuvkckorabenmz;
	bool sldax;

	bool xofcvgcbodrfugxfkj(int ahztintipgpdca, string tljyb, string tirqxsdbaf, int tbpbxe, double xqvid,
	                        bool dlzewissedwz, string svciop, string hmbbxpsvv, int usntf);
	bool zvawozzcxnmhmo();
	void mbqohmcrdkpdfw(int pnmrom, double ftfekuva, int mdujjdhgun);
	void rlcoofefpr(int yrvplszhkyhwe, int pvlhekoedtqmo, string jzfdozp, bool sfnhjiiqimubty, double qfzwnostdzxu,
	                int temnxvazdcxkl, double qqfkfeuw, bool lojqwbva, bool gcwzydu);
	string pqsvkpqpwzbckubjv(int cqypkp, bool qmxeihc, bool nkecboy, bool kieddwunkq, bool dnbflshgtnmqowb);
	double itunleebfjfxh(int bsaxzsdmzncl, double wbbnok, int vawsjc, bool mumbx, double mqwkrf, int rmldtzi);
	bool snkcdnhofzkd(string chwsfrfunezte, bool rnwukc, string ytxceneezgxdp);
	string usudwcjmmywluaeafgiqxywi(double txzzyhalttc, string akqdk, double mojnknczh, string tipdwkroty, int chsqfw,
	                                string ejkucml, string imioewj, bool xjwrogzj, int datvbtmlakf);
	string fujckikcvpfjgstyxgjmyrct(string wxkmskuihpxhxcs, bool ajicnkbvbhosp, double uifgbfuboeniy);
	void yrhevuivtwqaz(double ibwukat, bool imejguvltsxkuo, int ubmmcp);

private:
	bool lduzsznxfqebsok;
	int pwlhar;

	bool ffgudwefbnilzp();
	int rjzatgkpcx(bool icparxdgzxc, string bebnlkqefd, string ldfvjp, string bovnqccdoown, bool sxqwiw, int cnsuo,
	               int evmbbhdyyhtnrqb, bool glonkns, int ondxoh);
	bool eylbeujulskbkftdjrdhqbkc(string tuwrpdffmga, double ndfzbrfyq, int tnoternjs, string mjqwtroilpf, int dtzxzmdhjs,
	                              bool gkgxlznfuhsw, int mpwkbrtityrbdxc, int agogcnsjjsplbq, bool meuxzzzbkxzga);
	string phaucckxkqadsscjk();
	void dgjzgvuefhxo(bool vmhtrr, string iczpiollhxwt, bool ysxlmmhqhzoxycq, double xyqpeabdzdl, int jzekzjudjvx,
	                  double hwoinkrthz, int lgfxwid);
	bool ejicwcemfcokao(bool ktnxil, double mnljbgohdmos, double piayztnxr, string zdsaiorui, bool vedgirnhhrmkish,
	                    double jdjhcg, int totxjml);
};


bool yabrcur::ffgudwefbnilzp()
{
	return false;
}

int yabrcur::rjzatgkpcx(bool icparxdgzxc, string bebnlkqefd, string ldfvjp, string bovnqccdoown, bool sxqwiw, int cnsuo,
                        int evmbbhdyyhtnrqb, bool glonkns, int ondxoh)
{
	bool zjlpfqzyafdekgy = false;
	return 6244;
}

bool yabrcur::eylbeujulskbkftdjrdhqbkc(string tuwrpdffmga, double ndfzbrfyq, int tnoternjs, string mjqwtroilpf,
                                       int dtzxzmdhjs, bool gkgxlznfuhsw, int mpwkbrtityrbdxc, int agogcnsjjsplbq,
                                       bool meuxzzzbkxzga)
{
	double nwxbhczpnlqi = 95439;
	string skvokqtlbj = "trepdgnjglwmbregxfdfdpcviqeaylbaapediicl";
	bool ldgfxorn = false;
	if (false == false)
	{
		int oi;
		for (oi = 15; oi > 0; oi--)
		{
		}
	}
	return true;
}

string yabrcur::phaucckxkqadsscjk()
{
	double lgrparb = 47872;
	double ecrqe = 38982;
	double aullzppttlk = 9645;
	string yxiykfypswye = "bjhlkhetytcwamffbwsr";
	double bijorrkpi = 8080;
	double fbqwvpobxwqxumn = 4490;
	double uatxszszewp = 31789;
	double whkmdwjfjm = 7383;
	int ywrapwwociybxcu = 4253;
	if (string("bjhlkhetytcwamffbwsr") == string("bjhlkhetytcwamffbwsr"))
	{
		int afyjtk;
		for (afyjtk = 39; afyjtk > 0; afyjtk--)
		{
		}
	}
	if (31789 == 31789)
	{
		int fx;
		for (fx = 12; fx > 0; fx--)
		{
		}
	}
	if (8080 != 8080)
	{
		int qkwkai;
		for (qkwkai = 72; qkwkai > 0; qkwkai--)
		{
			continue;
		}
	}
	if (47872 != 47872)
	{
		int rdvnjir;
		for (rdvnjir = 69; rdvnjir > 0; rdvnjir--)
		{
			continue;
		}
	}
	if (31789 == 31789)
	{
		int gvridldrx;
		for (gvridldrx = 30; gvridldrx > 0; gvridldrx--)
		{
		}
	}
	return string("yogprw");
}

void yabrcur::dgjzgvuefhxo(bool vmhtrr, string iczpiollhxwt, bool ysxlmmhqhzoxycq, double xyqpeabdzdl, int jzekzjudjvx,
                           double hwoinkrthz, int lgfxwid)
{
	int myjorwowasdgoxb = 1605;
	bool exgeswwwpqpzocw = false;
	bool sroczhj = true;
	int avmpnlbbwkrzcp = 4331;
	double kiyflvxg = 334;
	double qfvxju = 37195;
	bool rnzzilnn = false;
	int rfqulorf = 684;
	double ynbmme = 11289;
	bool uzpylctpoujvtpx = false;
	if (684 == 684)
	{
		int cgdng;
		for (cgdng = 65; cgdng > 0; cgdng--)
		{
		}
	}
}

bool yabrcur::ejicwcemfcokao(bool ktnxil, double mnljbgohdmos, double piayztnxr, string zdsaiorui, bool vedgirnhhrmkish,
                             double jdjhcg, int totxjml)
{
	double pgtfhzuaiparqs = 3818;
	string nqfjauc = "sbcfbexvytbnyblpzvbchgkzvuitndmgcxrbsfcehuupqqxielwnwwojftyqgnnueebhfbzzsfctfdzoyzo";
	string nlfnx = "gwmldfbowsgemukfeaaliffzxkojerosudckklrnywnrtumbcklnuvu";
	bool odipt = false;
	string hmvnhlmapig = "tsxhllykkunicrsycmuztboorbmzlkuoxijrbqcktqvljwns";
	double asyxuznfjfh = 15922;
	string gndpdjbn = "skzpfkbldrccllwtpwqgbgpaudbegkyqnzzhiydbthndezdpfshdojgcsfgrybrlvchowbcmavywslvaczqbqroxrfmlu";
	bool yaoduzqmcf = false;
	if (string("tsxhllykkunicrsycmuztboorbmzlkuoxijrbqcktqvljwns") != string(
		"tsxhllykkunicrsycmuztboorbmzlkuoxijrbqcktqvljwns"))
	{
		int lsf;
		for (lsf = 39; lsf > 0; lsf--)
		{
		}
	}
	if (string("gwmldfbowsgemukfeaaliffzxkojerosudckklrnywnrtumbcklnuvu") == string(
		"gwmldfbowsgemukfeaaliffzxkojerosudckklrnywnrtumbcklnuvu"))
	{
		int pldnnujv;
		for (pldnnujv = 9; pldnnujv > 0; pldnnujv--)
		{
		}
	}
	if (string("tsxhllykkunicrsycmuztboorbmzlkuoxijrbqcktqvljwns") != string(
		"tsxhllykkunicrsycmuztboorbmzlkuoxijrbqcktqvljwns"))
	{
		int ifctasbjrj;
		for (ifctasbjrj = 39; ifctasbjrj > 0; ifctasbjrj--)
		{
		}
	}
	if (string("gwmldfbowsgemukfeaaliffzxkojerosudckklrnywnrtumbcklnuvu") != string(
		"gwmldfbowsgemukfeaaliffzxkojerosudckklrnywnrtumbcklnuvu"))
	{
		int bdxfwukcrh;
		for (bdxfwukcrh = 90; bdxfwukcrh > 0; bdxfwukcrh--)
		{
		}
	}
	if (string("tsxhllykkunicrsycmuztboorbmzlkuoxijrbqcktqvljwns") == string(
		"tsxhllykkunicrsycmuztboorbmzlkuoxijrbqcktqvljwns"))
	{
		int ez;
		for (ez = 34; ez > 0; ez--)
		{
		}
	}
	return false;
}

bool yabrcur::xofcvgcbodrfugxfkj(int ahztintipgpdca, string tljyb, string tirqxsdbaf, int tbpbxe, double xqvid,
                                 bool dlzewissedwz, string svciop, string hmbbxpsvv, int usntf)
{
	return true;
}

bool yabrcur::zvawozzcxnmhmo()
{
	double civgsfaovgiq = 24661;
	string jjfkkakps = "fcbwa";
	bool hrdbvpc = true;
	string vlsumswjlws = "xlfwypxzazpsniysntylhylajsapmvzpkocuuyufiuqvkqjqfylvxggdspmndkoghgirrlkctjzawzgonwfxtldidng";
	double gcpum = 720;
	double yendpelbmorfxzc = 785;
	if (785 == 785)
	{
		int mgkyrzebvs;
		for (mgkyrzebvs = 12; mgkyrzebvs > 0; mgkyrzebvs--)
		{
		}
	}
	if (785 != 785)
	{
		int lgdmzsh;
		for (lgdmzsh = 74; lgdmzsh > 0; lgdmzsh--)
		{
			continue;
		}
	}
	if (string("fcbwa") == string("fcbwa"))
	{
		int widzwaqh;
		for (widzwaqh = 48; widzwaqh > 0; widzwaqh--)
		{
		}
	}
	return false;
}

void yabrcur::mbqohmcrdkpdfw(int pnmrom, double ftfekuva, int mdujjdhgun)
{
	bool jwwydfj = false;
	if (false == false)
	{
		int bq;
		for (bq = 86; bq > 0; bq--)
		{
		}
	}
	if (false != false)
	{
		int vkglvizhca;
		for (vkglvizhca = 88; vkglvizhca > 0; vkglvizhca--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int fgcpc;
		for (fgcpc = 82; fgcpc > 0; fgcpc--)
		{
			continue;
		}
	}
}

void yabrcur::rlcoofefpr(int yrvplszhkyhwe, int pvlhekoedtqmo, string jzfdozp, bool sfnhjiiqimubty, double qfzwnostdzxu,
                         int temnxvazdcxkl, double qqfkfeuw, bool lojqwbva, bool gcwzydu)
{
	int kxorqart = 6595;
	double axlhmm = 6413;
	string crbzukpsbprrbl = "xgldzbwpqkqijqzeszdjgyaxqktjzifvqswktaezusvvedulexugbngriwqmhawdtcrxczbsqgkrbelvmmoyljtqpqd";
	string ihvczxo = "dasdvvxqsxpuknwukscnpsrzfrlhzyhahawkslusxwkcyriafewowjz";
	string dzmcnrhgmf = "dxvgalhyebgobownuhnxaaoadxuommxizkdriozxzebadclreaknv";
	double jdhyvsbgfwkzq = 29627;
}

string yabrcur::pqsvkpqpwzbckubjv(int cqypkp, bool qmxeihc, bool nkecboy, bool kieddwunkq, bool dnbflshgtnmqowb)
{
	string igucr = "pfuninqblrz";
	int liwxmdn = 2015;
	double xilexxoquel = 44399;
	string bymxeknafpnn = "dtwjc";
	bool odtwrv = false;
	double bhawb = 13901;
	string lbtbpactlaxju = "rjbxcslwvvycehdlpocsbhq";
	return string("acudyrnztmxmzgsjdimq");
}

double yabrcur::itunleebfjfxh(int bsaxzsdmzncl, double wbbnok, int vawsjc, bool mumbx, double mqwkrf, int rmldtzi)
{
	bool cubmzelwu = false;
	int ytsiir = 1441;
	int zzahgda = 1955;
	bool clgoxfunb = true;
	double foajwrnff = 18189;
	int stwdomnnlbe = 8733;
	int wvhwnhidmy = 792;
	int bmqnmagclkpdtw = 5688;
	return 79919;
}

bool yabrcur::snkcdnhofzkd(string chwsfrfunezte, bool rnwukc, string ytxceneezgxdp)
{
	double ecaaxvflerndm = 53686;
	string hpjgqqmdwi = "bfagofopblsxppkahxljil";
	bool klpri = true;
	double qneoqqblmff = 76864;
	bool ojggs = true;
	string zfwijiiwtpfoywb = "vkmyhjdgvtwtkkjkijifodrrhxzzbxqpeedbntlkogyakuwfrzaustklawdrzsjqpugzfgexwkcpncfjxwau";
	bool fudkedcwok = true;
	int ewlqakmemp = 578;
	if (string("bfagofopblsxppkahxljil") == string("bfagofopblsxppkahxljil"))
	{
		int mfdak;
		for (mfdak = 49; mfdak > 0; mfdak--)
		{
		}
	}
	if (string("bfagofopblsxppkahxljil") == string("bfagofopblsxppkahxljil"))
	{
		int grqvoyrdat;
		for (grqvoyrdat = 12; grqvoyrdat > 0; grqvoyrdat--)
		{
		}
	}
	return false;
}

string yabrcur::usudwcjmmywluaeafgiqxywi(double txzzyhalttc, string akqdk, double mojnknczh, string tipdwkroty,
                                         int chsqfw, string ejkucml, string imioewj, bool xjwrogzj, int datvbtmlakf)
{
	string wfaugpwlouyot = "oaxtpirjnuijinmcwvvxipehrtqvzicmjtultkbegf";
	bool cahopl = true;
	double qdacoxloyzwum = 10462;
	string ktehfuxjqimwf = "ytdufznzfjzzxmsusljxoawvzxjcaapammfdkvtitxomcmxqhxlykejhiidebpqckpgzzl";
	int skpqq = 7588;
	if (7588 != 7588)
	{
		int xgw;
		for (xgw = 99; xgw > 0; xgw--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int eftahjwl;
		for (eftahjwl = 27; eftahjwl > 0; eftahjwl--)
		{
		}
	}
	if (7588 == 7588)
	{
		int jodkpbc;
		for (jodkpbc = 87; jodkpbc > 0; jodkpbc--)
		{
		}
	}
	if (string("oaxtpirjnuijinmcwvvxipehrtqvzicmjtultkbegf") == string("oaxtpirjnuijinmcwvvxipehrtqvzicmjtultkbegf"))
	{
		int ypi;
		for (ypi = 30; ypi > 0; ypi--)
		{
		}
	}
	if (10462 != 10462)
	{
		int esyzoidib;
		for (esyzoidib = 45; esyzoidib > 0; esyzoidib--)
		{
			continue;
		}
	}
	return string("a");
}

string yabrcur::fujckikcvpfjgstyxgjmyrct(string wxkmskuihpxhxcs, bool ajicnkbvbhosp, double uifgbfuboeniy)
{
	return string("dn");
}

void yabrcur::yrhevuivtwqaz(double ibwukat, bool imejguvltsxkuo, int ubmmcp)
{
	double hkibrm = 40319;
	double husjdjkak = 44869;
	double jweseeqqelwupgy = 34927;
	int gdfmhvdtmufqu = 889;
	string qljstcbqtp = "jzisjosydlpgdyfisvbyokrxdztgapqnchbuvgrlttoyzfitxuwalbbn";
	if (40319 != 40319)
	{
		int qgqysrrmft;
		for (qgqysrrmft = 82; qgqysrrmft > 0; qgqysrrmft--)
		{
			continue;
		}
	}
	if (34927 != 34927)
	{
		int zjrt;
		for (zjrt = 13; zjrt > 0; zjrt--)
		{
			continue;
		}
	}
	if (889 == 889)
	{
		int kwqd;
		for (kwqd = 63; kwqd > 0; kwqd--)
		{
		}
	}
}

void yabrcur::xmsvvovejndrpvprgxgy(int lfvgcoczh, int cbcyesjokst, int pxmiaxctfy, bool yvcvxohbaabrp,
                                   double sdkeqbcizlc, bool xprnlinnjqeqzk)
{
	double cbdkddqhbxdv = 29195;
	if (29195 != 29195)
	{
		int oovwpjrk;
		for (oovwpjrk = 91; oovwpjrk > 0; oovwpjrk--)
		{
			continue;
		}
	}
	if (29195 != 29195)
	{
		int dlpkrccns;
		for (dlpkrccns = 82; dlpkrccns > 0; dlpkrccns--)
		{
			continue;
		}
	}
	if (29195 == 29195)
	{
		int dypdoamn;
		for (dypdoamn = 49; dypdoamn > 0; dypdoamn--)
		{
		}
	}
	if (29195 == 29195)
	{
		int zjvydsxwmx;
		for (zjvydsxwmx = 98; zjvydsxwmx > 0; zjvydsxwmx--)
		{
		}
	}
	if (29195 == 29195)
	{
		int zjszu;
		for (zjszu = 97; zjszu > 0; zjszu--)
		{
		}
	}
}

int yabrcur::esiorvpwgzdylpqo(bool memnwxqoybns, bool vwoxbt, double zgbdpkxulexo, double vjambpst, double nosnjjpg,
                              double mlnunbztpae, bool uesfeapuulp, string cfjllszttr, int zardcs)
{
	string zsasuvmb = "nzqpobtpjsdjvjetoljaheqcchkxtbdockfezwgxlwmhtixwzepzvsztgauvegbtvuoeikfbltfpskdeqfwyppq";
	int lfzaytjhjcbsbar = 1903;
	int lpdxnizddqu = 2157;
	string mrzqfuolyfj = "fsngokteqrzfyfmfuppbwgtsdyeoa";
	int frziraddwvubj = 3236;
	bool tiudiq = false;
	int nzduoaospeezpik = 369;
	int tlojkx = 1627;
	double vettvlovy = 10131;
	int rzgwcp = 1379;
	if (string("fsngokteqrzfyfmfuppbwgtsdyeoa") == string("fsngokteqrzfyfmfuppbwgtsdyeoa"))
	{
		int bqfspqbyf;
		for (bqfspqbyf = 60; bqfspqbyf > 0; bqfspqbyf--)
		{
		}
	}
	if (369 == 369)
	{
		int cpf;
		for (cpf = 66; cpf > 0; cpf--)
		{
		}
	}
	if (369 == 369)
	{
		int ad;
		for (ad = 44; ad > 0; ad--)
		{
		}
	}
	if (369 == 369)
	{
		int xj;
		for (xj = 95; xj > 0; xj--)
		{
		}
	}
	if (1379 != 1379)
	{
		int kf;
		for (kf = 50; kf > 0; kf--)
		{
			continue;
		}
	}
	return 34456;
}

int yabrcur::rpuusogaxuylmc(string jjezzniidzuzhr, int aerjvfhwhqx, double iyoavyl, int pcrrecz)
{
	int jaxhjygwcvlncg = 3589;
	int phkzvcqskl = 637;
	bool nqdtktgvhskhml = true;
	int luorvlmzc = 549;
	return 7001;
}

bool yabrcur::xtzsqzkifgeghxtp(bool xyzcbhewjzpb, int byurc, string swuate, string isqjzaexkxhruv, int mhxyepsc,
                               int nneuiesnhroj)
{
	double pikzdwfmdxxsojf = 34833;
	bool ebhfcqubsqhe = true;
	int cjwbnz = 2237;
	double wchybnnm = 9623;
	bool murydhgesluf = false;
	int gsjuvglyic = 671;
	double hwxjreesq = 1513;
	if (2237 == 2237)
	{
		int ednamoot;
		for (ednamoot = 70; ednamoot > 0; ednamoot--)
		{
		}
	}
	if (true != true)
	{
		int nzgwd;
		for (nzgwd = 4; nzgwd > 0; nzgwd--)
		{
			continue;
		}
	}
	if (1513 != 1513)
	{
		int kzndjqgtab;
		for (kzndjqgtab = 91; kzndjqgtab > 0; kzndjqgtab--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int jei;
		for (jei = 4; jei > 0; jei--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int pk;
		for (pk = 30; pk > 0; pk--)
		{
			continue;
		}
	}
	return false;
}

void yabrcur::bcsjrmiuqetvqebdwgtuch(double kijtmns, double lnzmiafo, string xibnsjrxoo, int cqcieitfhdhrwo,
                                     bool yzkvqextiqqbv, int bwcjkgffedckici, bool gnhnsgpdyuse, bool ercibhqsasdgld)
{
	int hoivgrdiuqwf = 3575;
	bool gchgkcwuvwefhlk = false;
	bool afjtx = false;
	string wuksvegvuphqlqc = "ojpkxdzzxclghlcwpymiwtygevym";
	bool dumcxy = true;
	bool xcmeorhieg = false;
	if (string("ojpkxdzzxclghlcwpymiwtygevym") == string("ojpkxdzzxclghlcwpymiwtygevym"))
	{
		int dipodpaask;
		for (dipodpaask = 83; dipodpaask > 0; dipodpaask--)
		{
		}
	}
	if (string("ojpkxdzzxclghlcwpymiwtygevym") == string("ojpkxdzzxclghlcwpymiwtygevym"))
	{
		int lzfnt;
		for (lzfnt = 80; lzfnt > 0; lzfnt--)
		{
		}
	}
	if (false != false)
	{
		int zeenc;
		for (zeenc = 39; zeenc > 0; zeenc--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int glwlu;
		for (glwlu = 72; glwlu > 0; glwlu--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int qu;
		for (qu = 17; qu > 0; qu--)
		{
			continue;
		}
	}
}

string yabrcur::gwitlkhxkoszauwdbg(string fafgoohzcaen, int jmuhvttmglmxqh, bool ewenflq, int dnunjtu)
{
	bool kvyzgnhvk = true;
	string gfvpx = "pyjluulkdlqzmvhkhcgmtgoqbtmtwqbwgvxbh";
	int fpkhw = 3174;
	bool viywjltbxh = true;
	int itdmgrzhtsyww = 624;
	string bmktlrdxjkbfj = "";
	if (string("") == string(""))
	{
		int byhkbn;
		for (byhkbn = 51; byhkbn > 0; byhkbn--)
		{
		}
	}
	if (3174 == 3174)
	{
		int znfhwtjh;
		for (znfhwtjh = 89; znfhwtjh > 0; znfhwtjh--)
		{
		}
	}
	if (string("pyjluulkdlqzmvhkhcgmtgoqbtmtwqbwgvxbh") != string("pyjluulkdlqzmvhkhcgmtgoqbtmtwqbwgvxbh"))
	{
		int eoqapqa;
		for (eoqapqa = 10; eoqapqa > 0; eoqapqa--)
		{
		}
	}
	return string("gyycvcqc");
}

string yabrcur::ffetzumosyrzuoigsxjx(string vmftau, int enreaancwmj)
{
	bool hyuibe = true;
	double xckwtyrghpzfq = 50800;
	double cfscvzglj = 30648;
	bool csvepb = true;
	int qniktkzdfbivcyk = 935;
	bool kfstgbivh = false;
	string eoazisrvsgvdbht = "nkkqkotqqtkqptiatvouqzzllzqwpdzjogyornfltqhkwurtixjmsqyexdasefitcrz";
	double hoyoacazmqlkhy = 79389;
	double sdftsx = 16712;
	int sqtjh = 2767;
	if (string("nkkqkotqqtkqptiatvouqzzllzqwpdzjogyornfltqhkwurtixjmsqyexdasefitcrz") == string(
		"nkkqkotqqtkqptiatvouqzzllzqwpdzjogyornfltqhkwurtixjmsqyexdasefitcrz"))
	{
		int nkx;
		for (nkx = 65; nkx > 0; nkx--)
		{
		}
	}
	return string("vemdhmzytbwekuqadut");
}

void yabrcur::fahyjeuziftaytkkarohcbrv(int drtrpivjbalmri, double umwafjwjbld, int uiodgkwbfcvjp, double jpkwyygrb,
                                       bool yngid, string vgskvnlzedyzv, double vprojklijzhpc, bool kgmwdt,
                                       int pxescnip, bool tgnqnrdbdq)
{
	double aqjnqwn = 17653;
	string kzohrvfgl = "bgozhvbprzozobqohungkwyrivttksjxscadlazfueffarfaswhtnmrgsyfjeqozostjcothmzchwvsebthllkqcgkwwf";
	string zbmaoricqganmjp = "pmlgfkclhbypalpofkzvbsluzshzbqzwlozhuctagizunfqydzvblxmctemwzdf";
	if (string("bgozhvbprzozobqohungkwyrivttksjxscadlazfueffarfaswhtnmrgsyfjeqozostjcothmzchwvsebthllkqcgkwwf") != string(
		"bgozhvbprzozobqohungkwyrivttksjxscadlazfueffarfaswhtnmrgsyfjeqozostjcothmzchwvsebthllkqcgkwwf"))
	{
		int smxrscrtw;
		for (smxrscrtw = 12; smxrscrtw > 0; smxrscrtw--)
		{
		}
	}
	if (string("pmlgfkclhbypalpofkzvbsluzshzbqzwlozhuctagizunfqydzvblxmctemwzdf") != string(
		"pmlgfkclhbypalpofkzvbsluzshzbqzwlozhuctagizunfqydzvblxmctemwzdf"))
	{
		int ypkzvx;
		for (ypkzvx = 42; ypkzvx > 0; ypkzvx--)
		{
		}
	}
	if (string("pmlgfkclhbypalpofkzvbsluzshzbqzwlozhuctagizunfqydzvblxmctemwzdf") != string(
		"pmlgfkclhbypalpofkzvbsluzshzbqzwlozhuctagizunfqydzvblxmctemwzdf"))
	{
		int nopjrqtoy;
		for (nopjrqtoy = 13; nopjrqtoy > 0; nopjrqtoy--)
		{
		}
	}
	if (string("bgozhvbprzozobqohungkwyrivttksjxscadlazfueffarfaswhtnmrgsyfjeqozostjcothmzchwvsebthllkqcgkwwf") != string(
		"bgozhvbprzozobqohungkwyrivttksjxscadlazfueffarfaswhtnmrgsyfjeqozostjcothmzchwvsebthllkqcgkwwf"))
	{
		int ezpct;
		for (ezpct = 6; ezpct > 0; ezpct--)
		{
		}
	}
}

void yabrcur::efnhdiqpcvevgmrmujullxck(bool bbehvrdp, double kdrlvyyhqmu, int kfmehlbyypywghk, double wpixde,
                                       bool yznan, double oemqrj, double vaqulzw, int yacvthism)
{
	double lovrtcadhyk = 16607;
	string kccee = "ntcvhiveefxpieuypdivtcwgthzasxdzxdqhrlrjkwhobaatudqcfwolcpfpvdymcasjjuephmpchnshzhqrrtacowwukta";
	if (string("ntcvhiveefxpieuypdivtcwgthzasxdzxdqhrlrjkwhobaatudqcfwolcpfpvdymcasjjuephmpchnshzhqrrtacowwukta") !=
		string("ntcvhiveefxpieuypdivtcwgthzasxdzxdqhrlrjkwhobaatudqcfwolcpfpvdymcasjjuephmpchnshzhqrrtacowwukta"))
	{
		int bvqybysg;
		for (bvqybysg = 29; bvqybysg > 0; bvqybysg--)
		{
		}
	}
	if (16607 == 16607)
	{
		int bzjzuxajrg;
		for (bzjzuxajrg = 39; bzjzuxajrg > 0; bzjzuxajrg--)
		{
		}
	}
	if (16607 != 16607)
	{
		int kiolcvj;
		for (kiolcvj = 58; kiolcvj > 0; kiolcvj--)
		{
			continue;
		}
	}
	if (16607 == 16607)
	{
		int yndhbx;
		for (yndhbx = 78; yndhbx > 0; yndhbx--)
		{
		}
	}
	if (string("ntcvhiveefxpieuypdivtcwgthzasxdzxdqhrlrjkwhobaatudqcfwolcpfpvdymcasjjuephmpchnshzhqrrtacowwukta") !=
		string("ntcvhiveefxpieuypdivtcwgthzasxdzxdqhrlrjkwhobaatudqcfwolcpfpvdymcasjjuephmpchnshzhqrrtacowwukta"))
	{
		int ee;
		for (ee = 95; ee > 0; ee--)
		{
		}
	}
}

yabrcur::yabrcur()
{
	this->xmsvvovejndrpvprgxgy(9511, 1704, 3681, true, 18890, false);
	this->esiorvpwgzdylpqo(false, false, 59379, 62088, 38856, 11644, false,
	                       string("ugscfdgzpvkjcxojtbpdqtdadfzdjotwfidslcbdvnlaslqmvknivvhmohfdiu"), 9227);
	this->rpuusogaxuylmc(string("czrovkzdlmsfhkh"), 3254, 36104, 5679);
	this->xtzsqzkifgeghxtp(false, 5517, string("bzjwwfxpqdtvdhrjtcvwiflqnzxxjhceqlxmdzibsbnratmbnthkrexylizwuseeyu"),
	                       string(
		                       "cbbleeiaijbnlthhbijerwipmoocxbmlpvakblsdlbqtpscddamyloiajmxuatufyfbaojcenbdmrojwpvmtboqcucyrab"),
	                       2149, 108);
	this->bcsjrmiuqetvqebdwgtuch(40377, 6400, string("sedhqxorthtyutvfhkstvfadvywyahhmywoklm"), 1079, false, 688, true,
	                             false);
	this->gwitlkhxkoszauwdbg(string("fqpyhlrsshdswcpbo"), 2966, true, 1771);
	this->ffetzumosyrzuoigsxjx(string("gfvbldujhdnrfzrqickmfxumvlkzgrfvxgavxehxzrasr"), 3773);
	this->fahyjeuziftaytkkarohcbrv(1751, 9442, 1079, 1683, true,
	                               string("nbeydkcahnnlpcsaekfnlqzkqicysvjjejnxwomcynzuqhcecisxbcwtshdprmqkxca"), 55600,
	                               true, 2872, true);
	this->efnhdiqpcvevgmrmujullxck(false, 9511, 6399, 47778, true, 14841, 9993, 7883);
	this->xofcvgcbodrfugxfkj(3406, string("eihtvpoqxsezllsu"), string("nqlufgjisjnyxurqtdumt"), 473, 32647, true,
	                         string(
		                         "vpiwcvrrfzifmppneizgbzpdlkttiaottcpmzlnhnvdrqwmpkdxpkvxivtsfsrqagdjivfaephhiidcoxhfrijrgtrqlola"),
	                         string("voaepgqkwdhquvozbjeurtunfyofufirsbjtiqgdkfgafwkyblwmwenssvtvcmrrbxdcstktgedqyygwco"),
	                         1027);
	this->zvawozzcxnmhmo();
	this->mbqohmcrdkpdfw(4236, 49676, 44);
	this->rlcoofefpr(5700, 820, string("vlylfmziaimvfucmqnqmpjnqhhsltmrabigiarwd"), true, 19170, 1645, 23286, false, true);
	this->pqsvkpqpwzbckubjv(2301, true, true, false, false);
	this->itunleebfjfxh(3765, 34465, 5749, false, 55524, 87);
	this->snkcdnhofzkd(string("cakgzayqghuxmumdevlhbtaubspnboyfdtwlalnvcytuetlxhvfcowthmqurykyetqdawywvkhpifbogjgoep"),
	                   false, string(
		                   "vcjczoehaujufhydnnlcdyqynlioxmzccvnyadsiusetaywiepnfartbuntszhpzifjoywdycdgoymsorhskq"));
	this->usudwcjmmywluaeafgiqxywi(
		16032, string("fwmkutumznlwskuuxoexzstdvsopchmgunsceuthqyjookukblwmluchjaraxfboevujodsylfgofsmbfbwxucpol"), 35213,
		string("fakal"), 5847, string("dldrgmvshaexvdahinvwrrwjod"),
		string("blrdmjmrnkmltkmznkzqsyqarwtusoiixqzaopozdvkfqapljcqdynkzrmyeaqozmmnundxfkfjl"), false, 6173);
	this->fujckikcvpfjgstyxgjmyrct(
		string("ouvgfnvgbpznhgddcwjvzaitlyegqwgjrzgpkjvwumevxmfjvirbleuxdvdadahwrebrcvcxigbvfjbuyilqqhtbfxlpeadqvok"), true,
		57050);
	this->yrhevuivtwqaz(27506, false, 5012);
	this->ffgudwefbnilzp();
	this->rjzatgkpcx(true, string("fimsgufyhlnnnicpcqnfrbrfnnhrtuocibkpjdgkrewldl"),
	                 string("tpwtuejardtvkspzkdcvwbadpxquiauccccwzwvfbkvuvfblneepbqhdvpidikrxjwstlhfyedwvw"),
	                 string(
		                 "syaqxdndvcstiyfqzgoqjuijjkcpbrkduwqhbdqwlbyarorcwuhponthnvwujiwqowtzelzfjwczmsnxyvelddclrebbcxpfsyq"),
	                 true, 1301, 5866, true, 6589);
	this->eylbeujulskbkftdjrdhqbkc(
		string("lrudxfyourdttchpgkvfdkwithqdbhfmevyxlfdgivoclhodmjfhvzbqvozsdqjerxnagdsqfcpgmoomcdarfeaxhmjdhxcyowrd"), 8413,
		595, string("kgsystmrwrminuqrtvfghptdetayiwnlkqaxpxcumfeiblihrvzhvluqfgolibtnzdugrqkcropwloi"), 2021, false, 4717,
		1363, true);
	this->phaucckxkqadsscjk();
	this->dgjzgvuefhxo(true, string("qahuutokdbsbzvwnwapjiicytpslvgixkiyccevsgzliaslydwnisqeaknlctmraoylu"), false, 8846,
	                   1355, 21699, 1799);
	this->ejicwcemfcokao(true, 30932, 1692, string("wkeoedrlokpxlzetwikrozqgubueyxevjxvqifpbloesqrweriizda"), true, 27316,
	                     3740);
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class qbnvgod
{
public:
	double mxitkwrem;
	string kzkwrpvtqadjfkp;
	double uiactufnztrw;
	bool voadyfkjhxzonz;
	qbnvgod();
	bool hcgaftcnfvwwhkbdyrj(string iheiq, double dgclufpnxlyxku, bool oywmy, double gzmfmsxpj, bool yksffixodlbc,
	                         string repqg, double fsgfqfgfv, string ibzngwrzqtrgxoq, double ofxzfgjewr);
	bool ztulxrfwdxch();

protected:
	int gcjsrbbbxlevq;
	string zopdk;
	double kjxwe;
	bool hdzitwhthsngejh;

	int qllvgjmflwslnfbtlrjgnodq(int ogmhw, bool kkhmgwjc, string ukeftvqxxcyhbno, int wnkqgwewioud, string rigzt,
	                             string rsrqsycfcgt);
	double mbvllhgfvgmuyas(string vmkwugzvgktmbv, string veckov, int yaxyhmlmeu, double vrjvqkxmclz);

private:
	int khjdxggkhapqtd;
	int fdzfpmxawghm;
	string brjjblkamp;
	double znfyjlwlb;

	void hnkdiyieoghoetgqu(bool qvavzmsp, double pzndz, int arulifmfewb, int gxcaerap, int zirfncsmursr,
	                       double oqtvrbiwwpgjoi, bool kzlbr, string mofriogbay);
	bool nkkhtyaqoe(int depiciugoktiwpd, string yjbcbeb, int iggpgqpc, string yigtujuqyunmuq);
	int tvqeledulzqilzwx(string oswvnbxwmwka, int lznndapfa, bool dkoarlkesboz, double czrxslygqcu, string nwadpzjvgwevkpy,
	                     bool jpvnc, bool wycrqksxqxggkde);
	int ydpaukfvwwdborc(string pscjfphpkqibj, bool jtxfotcmnn, bool hvpfm, string rnygzpcikvh, string zwndr,
	                    int klsjbltklyb, string upfcgatndof, bool nnfktyzlldmv, string cqluf, string uedmwnniprkbuk);
	double ypumxjkhieklurlfsyf(string rfeqjeyuw, string fendlvlif, string uzgtdq, bool spair, double sutcjhz,
	                           bool qwfwtbdyotlag, bool pksrxrscvuwr, bool bvltr);
	void vljcyeefkzapliclztkygext(int dnueobysigjgt, string ytvtkqibi, string dgnasnplh, bool mdmjzr, double hjitm,
	                              double rnmdmzuabkfc, double gpzhoz, string kdkcplukfqb, int jsdsrlzaidrhktw,
	                              double hllpkxznkvwc);
};


void qbnvgod::hnkdiyieoghoetgqu(bool qvavzmsp, double pzndz, int arulifmfewb, int gxcaerap, int zirfncsmursr,
                                double oqtvrbiwwpgjoi, bool kzlbr, string mofriogbay)
{
	bool xkvbzops = false;
	int dysfylzem = 1190;
	double amwcsjgfnl = 11642;
	double pmrsxztoxycrf = 29043;
	int apkjg = 5161;
	int tycnhidfwkafhi = 4684;
	if (11642 != 11642)
	{
		int ww;
		for (ww = 0; ww > 0; ww--)
		{
			continue;
		}
	}
	if (1190 != 1190)
	{
		int jwqussde;
		for (jwqussde = 28; jwqussde > 0; jwqussde--)
		{
			continue;
		}
	}
}

bool qbnvgod::nkkhtyaqoe(int depiciugoktiwpd, string yjbcbeb, int iggpgqpc, string yigtujuqyunmuq)
{
	int ihfag = 2020;
	bool ptosdmnypm = false;
	int pucezdvfehv = 1486;
	return false;
}

int qbnvgod::tvqeledulzqilzwx(string oswvnbxwmwka, int lznndapfa, bool dkoarlkesboz, double czrxslygqcu,
                              string nwadpzjvgwevkpy, bool jpvnc, bool wycrqksxqxggkde)
{
	string npefcxazszvhg = "jcidxfbsusatcgnraephnppizwbcgaqyjtiswzhyooqe";
	bool rbcliddpuaedk = true;
	double hjnzolazcyxta = 10359;
	string kejlpmhp = "urzkiawpqypagcgvji";
	string zfckro = "mqrvaaipccezuflxhixilkjujckyouhbbxyuepgbaplzimqwdegvnpbqbmwqjokhbvxairmgtqgbe";
	int rmyjwblzywzcfl = 746;
	string ucmaypi = "zkzylxbywnexovp";
	string dukmcbygixeoti = "tebsiqdxhrsemsxvyrmvfvabodvjnfspfcb";
	double fknkxm = 57215;
	if (10359 == 10359)
	{
		int xu;
		for (xu = 44; xu > 0; xu--)
		{
		}
	}
	if (true != true)
	{
		int ey;
		for (ey = 68; ey > 0; ey--)
		{
			continue;
		}
	}
	if (string("mqrvaaipccezuflxhixilkjujckyouhbbxyuepgbaplzimqwdegvnpbqbmwqjokhbvxairmgtqgbe") != string(
		"mqrvaaipccezuflxhixilkjujckyouhbbxyuepgbaplzimqwdegvnpbqbmwqjokhbvxairmgtqgbe"))
	{
		int fvcij;
		for (fvcij = 74; fvcij > 0; fvcij--)
		{
		}
	}
	if (10359 == 10359)
	{
		int vgc;
		for (vgc = 82; vgc > 0; vgc--)
		{
		}
	}
	return 58806;
}

int qbnvgod::ydpaukfvwwdborc(string pscjfphpkqibj, bool jtxfotcmnn, bool hvpfm, string rnygzpcikvh, string zwndr,
                             int klsjbltklyb, string upfcgatndof, bool nnfktyzlldmv, string cqluf,
                             string uedmwnniprkbuk)
{
	double rgxyzkwlbwnk = 13532;
	string bshhmvom = "cyaebbjptisckairrrupzsszfuspikjulxvkuqsxqmmiolrrawgaq";
	int rotpangecoucke = 336;
	bool bzgetxdyr = false;
	if (13532 == 13532)
	{
		int cfz;
		for (cfz = 73; cfz > 0; cfz--)
		{
		}
	}
	if (false == false)
	{
		int wllbjgghh;
		for (wllbjgghh = 37; wllbjgghh > 0; wllbjgghh--)
		{
		}
	}
	if (13532 != 13532)
	{
		int bmtxckkqsv;
		for (bmtxckkqsv = 26; bmtxckkqsv > 0; bmtxckkqsv--)
		{
			continue;
		}
	}
	return 1097;
}

double qbnvgod::ypumxjkhieklurlfsyf(string rfeqjeyuw, string fendlvlif, string uzgtdq, bool spair, double sutcjhz,
                                    bool qwfwtbdyotlag, bool pksrxrscvuwr, bool bvltr)
{
	double yscph = 2226;
	double lutkycblvucmuks = 20382;
	string atlfs = "mebwzzvvfentrxmtebzqjwwlsgetwdetpapnwvl";
	string wmgxa = "xbbeyxh";
	bool mrfgufco = true;
	bool qpnkugybnzpy = true;
	int hgawmvujsjsvgf = 4534;
	bool dvktlcixoigigz = true;
	if (2226 != 2226)
	{
		int kpbvotgvpq;
		for (kpbvotgvpq = 56; kpbvotgvpq > 0; kpbvotgvpq--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int tweaw;
		for (tweaw = 75; tweaw > 0; tweaw--)
		{
			continue;
		}
	}
	if (string("xbbeyxh") != string("xbbeyxh"))
	{
		int ihad;
		for (ihad = 34; ihad > 0; ihad--)
		{
		}
	}
	if (true == true)
	{
		int zhgy;
		for (zhgy = 96; zhgy > 0; zhgy--)
		{
		}
	}
	return 60544;
}

void qbnvgod::vljcyeefkzapliclztkygext(int dnueobysigjgt, string ytvtkqibi, string dgnasnplh, bool mdmjzr, double hjitm,
                                       double rnmdmzuabkfc, double gpzhoz, string kdkcplukfqb, int jsdsrlzaidrhktw,
                                       double hllpkxznkvwc)
{
	int yymkgvowbplkazv = 1487;
	double qzhmmfgljnkr = 16999;
	bool prndcla = true;
	bool xjkqosxf = true;
	int lproqpifqc = 1432;
	string waxtejnvsdmm = "gcrzplpr";
	double wbxyowlrivib = 58914;
	if (16999 == 16999)
	{
		int qcfvetqv;
		for (qcfvetqv = 82; qcfvetqv > 0; qcfvetqv--)
		{
		}
	}
}

int qbnvgod::qllvgjmflwslnfbtlrjgnodq(int ogmhw, bool kkhmgwjc, string ukeftvqxxcyhbno, int wnkqgwewioud, string rigzt,
                                      string rsrqsycfcgt)
{
	double rbmin = 10563;
	int uoejehmbhwlfb = 2953;
	string rrkzepksljhfqjf = "fuzghhbqynfgrfgvwgqkgegzphftpqrqbdmniiqckzqphyakpgtqilhsaeulwgcsbkftrqhgsvgjd";
	string seazbl = "exbnnyywutqjrojrvrnuechojevtlpwrsriydprntuxbikjxljnrdrvlwsmrtgrcvvqfmvgsnsnigmrbkwizbgyp";
	bool wwxgmzk = true;
	string mwlccu = "nzcaclkavbrozahmrotjytbpygwjttzfbjhcpivmzireqcrooicpaho";
	string gaziszhuvf = "didiuekzzelgesgy";
	double meztqcbpecv = 11588;
	double ijmurtuujlej = 52386;
	string lszbbqiyskrjfw = "tudmfraqjtknmogpfhrtqfkef";
	if (string("tudmfraqjtknmogpfhrtqfkef") != string("tudmfraqjtknmogpfhrtqfkef"))
	{
		int uek;
		for (uek = 26; uek > 0; uek--)
		{
		}
	}
	return 94078;
}

double qbnvgod::mbvllhgfvgmuyas(string vmkwugzvgktmbv, string veckov, int yaxyhmlmeu, double vrjvqkxmclz)
{
	int mjjkshtd = 289;
	double themoabq = 13767;
	double bhukbknkf = 9159;
	double xvhzioaodz = 18433;
	bool ppmmfeerk = false;
	double ikmpa = 1380;
	bool aexaibmxoxsnag = false;
	double pjcztyqqnkus = 24991;
	double caaqsfgpvact = 32230;
	if (9159 != 9159)
	{
		int zzd;
		for (zzd = 78; zzd > 0; zzd--)
		{
			continue;
		}
	}
	if (1380 != 1380)
	{
		int tsjjnd;
		for (tsjjnd = 54; tsjjnd > 0; tsjjnd--)
		{
			continue;
		}
	}
	if (9159 == 9159)
	{
		int lerqxmkzxd;
		for (lerqxmkzxd = 53; lerqxmkzxd > 0; lerqxmkzxd--)
		{
		}
	}
	return 12459;
}

bool qbnvgod::hcgaftcnfvwwhkbdyrj(string iheiq, double dgclufpnxlyxku, bool oywmy, double gzmfmsxpj, bool yksffixodlbc,
                                  string repqg, double fsgfqfgfv, string ibzngwrzqtrgxoq, double ofxzfgjewr)
{
	double xrblmlzkds = 45083;
	bool gqlomvyuyl = false;
	string nfytnqiorrteaki = "lpmlfttnvgpeeoeifnqbdxqgrmptdehwbkphlckdpbobjvufw";
	double alwpa = 33146;
	bool bxiotfwnz = true;
	int zvsoaace = 910;
	string sfjnrcecfuz = "ygpygofrgmuglycodquzkauoykexcjzqjbelgjiwpxxkucjspqavakbcqpwkiccdgmbqfaodshjzrvnouqilctqlbdgkpk";
	double mvrgjo = 58621;
	int ewwfixsfgazwimv = 3562;
	string dyxnuxlrjy = "ngiyaxbknhifvpyzappvivtsqfvjxrrdgygmmwiauxeqwidtjkfgoakmohhwpezkzntenmesq";
	return true;
}

bool qbnvgod::ztulxrfwdxch()
{
	string bwzckbmpndib = "enzuetacqyjehtspiozrzteoaepheabncldfkjyvpydrmejwegvurrbdwiobjnuqibvathkzfbmvtumtshwnd";
	string kkhsnwwejawssw = "xyvqwjzbnvladeohvxebkfzeyeusdugamwogouynankncbacjpmxxvaekq";
	int ifhdxdokutsxpt = 2519;
	int nkgxucrgh = 720;
	double hcuhzvasoniry = 629;
	string paglb = "notdcpqbrqeeocxlseimsx";
	string frryjysbnm = "qdbguilrvjltucevnicuxxmvrmzkgszjrok";
	double spibbozvmlpfcld = 14468;
	if (string("xyvqwjzbnvladeohvxebkfzeyeusdugamwogouynankncbacjpmxxvaekq") != string(
		"xyvqwjzbnvladeohvxebkfzeyeusdugamwogouynankncbacjpmxxvaekq"))
	{
		int vtfzb;
		for (vtfzb = 61; vtfzb > 0; vtfzb--)
		{
		}
	}
	if (14468 == 14468)
	{
		int wipmvbvivi;
		for (wipmvbvivi = 6; wipmvbvivi > 0; wipmvbvivi--)
		{
		}
	}
	if (14468 == 14468)
	{
		int tdpsblrnm;
		for (tdpsblrnm = 90; tdpsblrnm > 0; tdpsblrnm--)
		{
		}
	}
	if (string("qdbguilrvjltucevnicuxxmvrmzkgszjrok") != string("qdbguilrvjltucevnicuxxmvrmzkgszjrok"))
	{
		int uguwcsll;
		for (uguwcsll = 60; uguwcsll > 0; uguwcsll--)
		{
		}
	}
	return false;
}

qbnvgod::qbnvgod()
{
	this->hcgaftcnfvwwhkbdyrj(string("qz"), 634, true, 32282, false, string("bog"), 9464,
	                          string("vrclvavhvddkntkxxfwnmnmybuwhfiwqi"), 2397);
	this->ztulxrfwdxch();
	this->qllvgjmflwslnfbtlrjgnodq(916, false, string("jpztnwzbnwhafjbwusediymykyczfpzglxkegzbrspkaugquirluoru"), 581,
	                               string("jkpuinjvddzdxaikxftperivxhbqbpokbeygqioe"),
	                               string("bbuhhqgcmlprgjwfeqneoxfzlkpzkmyujhwhsskhazckikvencyysdcdzvafwod"));
	this->mbvllhgfvgmuyas(string("ufozyvtoszkldbmqzmskfsfwzrgrjyiqkkuufjkrvymjtyglzjnjnvke"),
	                      string("wmppkkabiakkysqximrumhuqeftgrxcddjdu"), 3084, 35921);
	this->hnkdiyieoghoetgqu(true, 12936, 3316, 4590, 501, 16304, false, string("llpefbzllwwgzlact"));
	this->nkkhtyaqoe(1594, string("zzbbhlxxltyphewbejcz"), 1009,
	                 string("fqaxehkyepckcsndfctcfdlkuqzyvmwcmsnfiebfkllbflbhywnqdmsqdatdrqzelrfnvisyofcerpkyieha"));
	this->tvqeledulzqilzwx(string("doomnrigefjmtuumbclpylzhvslcrsvhoacpcxmzfmoofttedulkfdvbvwkgfmjxbhfvrnymdfs"), 3068,
	                       false, 21315, string("vlqtqqujvxtytgmtiwudwmviezuwep"), true, true);
	this->ydpaukfvwwdborc(string("wjcfbtapuiipeufqicgeivvqoehtovdckjxaumenetqvy"), false, true,
	                      string(
		                      "vzfjehactodptrovstpaaezedpuqbwzswienocrnpanhfpgggaawqdxnnzqrxasqqnabqixanvdnjemeplnlxmog"),
	                      string("ltpnohtyepwqqorlfmzhssvienyverfu"), 1163, string("iimjyurwnydimwsbsw"), false,
	                      string("cygqptfatueuabodnnslegpfxnegvbkfljcetvewzyamedrltgq"),
	                      string("mymmkduwbtbipawpdrzcqpqxzpoonaiqpxafynbrrdukazvfnpvggnpqhfqyxcmrymv"));
	this->ypumxjkhieklurlfsyf(string("sweixxiqwswbuqdtzxximgqrdiqnmlwbjkjrakuaojwbrb"),
	                          string("rspgwzvmypzpmryakpyzvonvzgzsolvoaswtl"), string("mblowgfmdstvkrwgf"), false, 180,
	                          true, true, false);
	this->vljcyeefkzapliclztkygext(6603, string("hduzftmebzqyvkjqzhzksofldpdmjbcpqctcoqtayacekbryteshtzxxqyxsut"),
	                               string("k"), false, 5663, 22451, 22605, string("qacfwmilizlmbeonuhj"), 2186, 80662);
}
