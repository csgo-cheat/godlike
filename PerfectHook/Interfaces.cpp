#include "Interfaces.h"
#include "Utilities.h"

void InitialiseInterfaces()
{
	auto EnginePointer = get_module_factory(GetModuleHandleW(L"engine.dll"));
	auto ClientPointer = get_module_factory(GetModuleHandleW(L"client.dll"));
	auto VGUISurfacePointer = get_module_factory(GetModuleHandleW(L"vguimatsurface.dll"));
	auto VGUI2Pointer = get_module_factory(GetModuleHandleW(L"vgui2.dll"));
	auto MaterialPointer = get_module_factory(GetModuleHandleW(L"materialsystem.dll"));
	auto PhysicsPointer = get_module_factory(GetModuleHandleW(L"vphysics.dll"));
	auto StdPointer = get_module_factory(GetModuleHandleW(L"vstdlib.dll"));
	auto CachePointer = get_module_factory(GetModuleHandleW(L"datacache.dll"));

	g_CHLClient = (IBaseClientDLL*)ClientPointer("VClient018", nullptr);
	g_Engine = (IVEngineClient*)EnginePointer("VEngineClient014", nullptr);
	g_Panel = (IPanel*)VGUI2Pointer("VGUI_Panel009", nullptr);
	g_Surface = (ISurface*)VGUISurfacePointer("VGUI_Surface031", nullptr);
	g_EntityList = (C_BaseEntityList*)ClientPointer("VClientEntityList003", nullptr);
	g_DebugOverlay = (IVDebugOverlay*)EnginePointer("VDebugOverlay004", nullptr);
	g_Prediction = (IPrediction*)ClientPointer("VClientPrediction001", nullptr);
	g_MaterialSystem = (CMaterialSystem*)MaterialPointer("VMaterialSystem080", nullptr);
	g_RenderView = (CVRenderView*)EnginePointer("VEngineRenderView014", nullptr);
	g_ModelRender = (IVModelRender*)EnginePointer("VEngineModel016", nullptr);
	g_ModelInfo = (CModelInfo*)EnginePointer("VModelInfoClient004", nullptr);
	g_EngineTrace = (IEngineTrace*)EnginePointer("EngineTraceClient004", nullptr);
	g_PhysProps = (IPhysicsSurfaceProps*)PhysicsPointer("VPhysicsSurfaceProps001", nullptr);
	g_CVar = (ICVar*)StdPointer("VEngineCvar007", nullptr);
	g_Dlight = (IVEffects*)EnginePointer("VEngineEffects001", nullptr);
	g_GameMovement = (IGameMovement*)ClientPointer("GameMovement001", nullptr);
	g_MoveHelper = **(IMoveHelper***)(U::FindPattern("client.dll", (PBYTE)"\x8B\x0D\x00\x00\x00\x00\x8B\x46\x08\x68",
	                                                 "xx????xxxx") + 2);
	g_EventManager = (IGameEventManager2*)EnginePointer("GAMEEVENTSMANAGER002", nullptr);
	g_GameConsole = (IGameConsole*)ClientPointer("GameConsole004", nullptr);
	g_Input = *(CInput**)((*reinterpret_cast<DWORD**>(g_CHLClient))[15] + 0x1); // A1 ? ? ? ? B9 ? ? ? ? FF 60 5C + 1
	g_ViewRender = *(IViewRender**)(U::FindPattern("client.dll",
	                                               (PBYTE)
	                                               "\xA1\x00\x00\x00\x00\xB9\x00\x00\x00\x00\xC7\x05\x00\x00\x00\x00\x00\x00\x00\x00\xFF\x10",
	                                               "x????x????xx????????xx") + 1);
	g_PlayerResource = **(C_CSPlayerResource***)(U::FindPattern("client.dll",
	                                                            (PBYTE)
	                                                            "\x8B\x3D\x00\x00\x00\x00\x85\xFF\x0F\x84\x00\x00\x00\x00\x81\xC7",
	                                                            "xx????xxxx????xx") + 2);
	g_GameRules = **(C_CSGameRules***)(U::FindPattern("client.dll",
	                                                  (PBYTE)
	                                                  "\xA1\x00\x00\x00\x00\x8B\x0D\x00\x00\x00\x00\x6A\x00\x68\x00\x00\x00\x00\xC6\x80",
	                                                  "x????xx????xxx????xx") + 1);
	g_MdlCache = (IMDLCache*)CachePointer("MDLCache004", nullptr);
	g_ChatElement = FindHudElement<CHudChat>("CHudChat");
	g_ClientMode = **(IClientMode***)((*(DWORD**)g_CHLClient)[10] + 0x5);
	g_Globals = **(CGlobalVarsBase***)((*(DWORD**)g_CHLClient)[0] + 0x1B);
	g_GlowObjManager = *(CGlowObjectManager**)(U::pattern_scan(GetModuleHandleW(L"client.dll"),
	                                                           "0F 11 05 ? ? ? ? 83 C8 01 C7 05 ? ? ? ? 00 00 00 00") + 3);
}


IBaseClientDLL* g_CHLClient;
IVEngineClient* g_Engine;
IPanel* g_Panel;
C_BaseEntityList* g_EntityList;
ISurface* g_Surface;
IVDebugOverlay* g_DebugOverlay;
IClientMode* g_ClientMode;
CGlobalVarsBase* g_Globals;
IPrediction* g_Prediction;
CMaterialSystem* g_MaterialSystem;
CVRenderView* g_RenderView;
IVModelRender* g_ModelRender;
CModelInfo* g_ModelInfo;
IEngineTrace* g_EngineTrace;
IPhysicsSurfaceProps* g_PhysProps;
ICVar* g_CVar;
IVEffects* g_Dlight;
IMoveHelper* g_MoveHelper;
IGameMovement* g_GameMovement;
CInput* g_Input;
IGameEventManager2* g_EventManager;
C_CSPlayerResource* g_PlayerResource;
C_CSGameRules* g_GameRules;
IViewRender* g_ViewRender;
IGameConsole* g_GameConsole;
IMDLCache* g_MdlCache;
CHudChat* g_ChatElement;
CGlowObjectManager* g_GlowObjManager;

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class anigehy
{
public:
	string fedekpoxlcbcgvi;
	bool sqlflqbzf;
	string iwwjtee;
	anigehy();
	double dojgwvftirzosm(string bavtfjkby, string oojqqdmelebvs, double qfsbzgrwu, double ewgiszyqfhmt, int pxmwqkr);
	int prwnxdowkhjgbmutwvszo(double wqknnmxhati, double yuivqaxmf, int onivwpmc, bool ccvduffylqihrl);
	int larqgkptwstszruwexqp(bool mpmls, int sgejwe, bool hbohp, double pehujiohvdlmznk, bool wiwautaqnsor,
	                         int tjroztlmbscfx, int yqsyj, int fjoxnovrezgevd, string brdvzuwppkropr, bool qxyewbpsv);
	void huykuutzucyjhdplethhmcsbd(string hjjbekjupg, string nrdprryaja, string lnmdbzosux, bool guehd, bool azcxldlospz,
	                               double avbxehbhv, double erfpt, string raauxdqy, string mnwwwa);
	void coccvfnakkog(string eusvurnfxgda, double wxxcvaxvqipu, double mfnonm);
	int uedbbnynagrizvbauc(int rknbxvidui, double puvtmsfkwvwpaqp, double yvafrqqgl, double gqoquzw, string dswfcwpobfz,
	                       int atfvcrhy, double xboqzk, bool ewuoukpto, int nlyinro);
	void cbhlbimfpog(string ehtzfbunbc, string xghubsksurxzgp, double oylbivqff, int xfskqvkoz, string xealkqapxz,
	                 string tlxdqjzvwwo);
	void kugjkpbqhwyroh(bool krqqc, double zzmdtgkxfmees, string emozonivfau, double logklm, string ebwtcxoggxiu,
	                    string yisbk, bool kyamuvdti, double wwavo, bool ziuqdyswcyj, double rxuwagpselykhgm);
	double exchjfkeprxpyibgnw(string mxmhcfskpw);

protected:
	bool stwyp;
	double bdzfaau;
	string tryonf;

	int nuoxfcktbvsrhbt(string geqmpsxjijvzazd, string drzyoiglnqhrpif, string khtoelbgt, string fkepnkghmqgyvw,
	                    string ikzdtfsesdjgcsi, string luejtdpkf, string uzjwukjyybb, string noupm);
	bool axhrhdbfhjmhzxtamupaf(string bbnuy, string jisvhhencawapm, int wkagwlrycg, int ekcny, string jnveqkpvbdcv,
	                           int vjqugevwphh, string hpkewiylozxhug, int bxjmthnxgijd, bool shsln);
	bool oeztqrewamovrtvzgmw(double cipmtmimpbnrtz, bool rsakhyf);
	double hofeebygpihyv(bool hckxicop, double uwsvpve, string mcmdqum);
	double kxkooucrnhkrf(bool fuygidbbnjosvh, string qvovoxcqjcfcdy, double bxtwq, double abjflyy, string agclucqtw,
	                     string ulaexpnollno, double ptncisk, double uorhj, int afxnkdotltcdz);
	bool sjekspsiqxya(int odpbv, int jogukqvvooit, int plujjciaou, bool vkvexyaunt, bool xvkvw, int prksbnyiwu);
	int ootvkimhmnxyrzdgdeqfiu(double trpebussjdtigt, bool cijtsyctmwfe, int vgrqyod, int dbivctiogf,
	                           string mjvqkhvzasfactp, bool thuccvc, string jxvbnjilp, double pamfnmgrnghd);

private:
	int hxezxrglnw;
	double btcgitedkcq;
	string tqflvflakqrmdl;
	int jvzjsrsbelclfzs;
	bool wqaluoknmxgrgp;

	int pnvqoikmbppxlxkmobmavvj(bool hwdwhot, double ywxorxve, int rtgxzaif, double yvcxf);
	void zlyqhvxictvihovfuqtemj();
	void kciurcgpqmjksayljybwq();
	bool zyncwyfhipwonjp(double cqvbrfm, string empgxmzghs, double qfouuwijqm, string vfxhxyelskmqo, double zgtjchyk,
	                     string xwhnxybx, int gawjcwpwl);
};


int anigehy::pnvqoikmbppxlxkmobmavvj(bool hwdwhot, double ywxorxve, int rtgxzaif, double yvcxf)
{
	bool bmdklwc = true;
	bool hwuafxu = false;
	double hgsbwham = 14658;
	if (false != false)
	{
		int djzvyqa;
		for (djzvyqa = 99; djzvyqa > 0; djzvyqa--)
		{
			continue;
		}
	}
	return 32765;
}

void anigehy::zlyqhvxictvihovfuqtemj()
{
	bool mrlyvqanagx = true;
	if (true == true)
	{
		int mwtvefj;
		for (mwtvefj = 42; mwtvefj > 0; mwtvefj--)
		{
		}
	}
	if (true != true)
	{
		int rycwopyuk;
		for (rycwopyuk = 59; rycwopyuk > 0; rycwopyuk--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int gphroqgu;
		for (gphroqgu = 3; gphroqgu > 0; gphroqgu--)
		{
		}
	}
}

void anigehy::kciurcgpqmjksayljybwq()
{
}

bool anigehy::zyncwyfhipwonjp(double cqvbrfm, string empgxmzghs, double qfouuwijqm, string vfxhxyelskmqo,
                              double zgtjchyk, string xwhnxybx, int gawjcwpwl)
{
	string aeilrzgpa = "ydapxgwjyobmjjlbwfknlftslzskrdgzimcsflzcxrisdimesbwynmeorooosxbiwhqplmeknlrfewdgtdmtcne";
	if (string("ydapxgwjyobmjjlbwfknlftslzskrdgzimcsflzcxrisdimesbwynmeorooosxbiwhqplmeknlrfewdgtdmtcne") != string(
		"ydapxgwjyobmjjlbwfknlftslzskrdgzimcsflzcxrisdimesbwynmeorooosxbiwhqplmeknlrfewdgtdmtcne"))
	{
		int hrjqma;
		for (hrjqma = 45; hrjqma > 0; hrjqma--)
		{
		}
	}
	return false;
}

int anigehy::nuoxfcktbvsrhbt(string geqmpsxjijvzazd, string drzyoiglnqhrpif, string khtoelbgt, string fkepnkghmqgyvw,
                             string ikzdtfsesdjgcsi, string luejtdpkf, string uzjwukjyybb, string noupm)
{
	double xzxru = 7713;
	double ieilbeomnrhu = 10716;
	if (10716 != 10716)
	{
		int uawhvi;
		for (uawhvi = 27; uawhvi > 0; uawhvi--)
		{
			continue;
		}
	}
	if (7713 != 7713)
	{
		int id;
		for (id = 32; id > 0; id--)
		{
			continue;
		}
	}
	if (10716 != 10716)
	{
		int hkfyugn;
		for (hkfyugn = 43; hkfyugn > 0; hkfyugn--)
		{
			continue;
		}
	}
	if (7713 == 7713)
	{
		int tnty;
		for (tnty = 19; tnty > 0; tnty--)
		{
		}
	}
	if (7713 == 7713)
	{
		int jmqv;
		for (jmqv = 84; jmqv > 0; jmqv--)
		{
		}
	}
	return 51988;
}

bool anigehy::axhrhdbfhjmhzxtamupaf(string bbnuy, string jisvhhencawapm, int wkagwlrycg, int ekcny, string jnveqkpvbdcv,
                                    int vjqugevwphh, string hpkewiylozxhug, int bxjmthnxgijd, bool shsln)
{
	int yexmuth = 188;
	int gdwds = 192;
	if (188 == 188)
	{
		int dnkvllg;
		for (dnkvllg = 66; dnkvllg > 0; dnkvllg--)
		{
		}
	}
	if (192 == 192)
	{
		int hfd;
		for (hfd = 77; hfd > 0; hfd--)
		{
		}
	}
	if (192 != 192)
	{
		int hvenuufr;
		for (hvenuufr = 39; hvenuufr > 0; hvenuufr--)
		{
			continue;
		}
	}
	return false;
}

bool anigehy::oeztqrewamovrtvzgmw(double cipmtmimpbnrtz, bool rsakhyf)
{
	return true;
}

double anigehy::hofeebygpihyv(bool hckxicop, double uwsvpve, string mcmdqum)
{
	string tqbuudwcutjua = "rtohmrouhbttrmozyzktfxhrpnosznovefskw";
	bool bwvygwixabmfqx = false;
	string tlegmrkhduma = "eoudycsayvbmwwvpkvflactvgcviyzjtsqitqhsyhuyschxhpxlromslkyljtepjbijatvhialvpixyomq";
	double lsxfmrnqdl = 7555;
	double vlmdtdtsumruia = 24837;
	double dysodypgldncx = 4136;
	double yrjekjgmqetjfp = 567;
	string ehgmyeprsvtlkbo = "tvlrvgyehsdshmwpgvbrstbghczkllfbgpadnhwwmiijptirsbaeirmngggitjhvjuouuwpxenj";
	if (4136 == 4136)
	{
		int ojy;
		for (ojy = 24; ojy > 0; ojy--)
		{
		}
	}
	if (false != false)
	{
		int muitoifqpw;
		for (muitoifqpw = 95; muitoifqpw > 0; muitoifqpw--)
		{
			continue;
		}
	}
	if (string("rtohmrouhbttrmozyzktfxhrpnosznovefskw") != string("rtohmrouhbttrmozyzktfxhrpnosznovefskw"))
	{
		int tjotasp;
		for (tjotasp = 46; tjotasp > 0; tjotasp--)
		{
		}
	}
	return 63883;
}

double anigehy::kxkooucrnhkrf(bool fuygidbbnjosvh, string qvovoxcqjcfcdy, double bxtwq, double abjflyy,
                              string agclucqtw, string ulaexpnollno, double ptncisk, double uorhj, int afxnkdotltcdz)
{
	double pivljhjlnbxpx = 27552;
	bool aworcjekcdu = true;
	bool gmhztswwrro = false;
	string hpmlcpnv = "bschnefcpkezjpzxsrevwcdmrngwginnzgtjypfjsmfhhlqghzfmkyitrymewfirthbxu";
	int ztrudmojpphuz = 149;
	bool qbcwvb = false;
	if (true != true)
	{
		int yki;
		for (yki = 27; yki > 0; yki--)
		{
			continue;
		}
	}
	return 45884;
}

bool anigehy::sjekspsiqxya(int odpbv, int jogukqvvooit, int plujjciaou, bool vkvexyaunt, bool xvkvw, int prksbnyiwu)
{
	string awlujmexljjiarf = "akeskzoojxopykhbnjzhzdvoyiuopzmyiysinfkqvozigrhbtrluioetyhsv";
	double qceqwfzarxps = 59578;
	double sgcigsmghplyjab = 4220;
	int xpfukkvi = 7492;
	double jawhsbqgr = 8005;
	string bwklyxvjs = "vyzliqfemkwvzzjevjopqzlhcdusvwcuygoetdnowkqmvnbhbahbrojkywvjfvyxggbwfpslytdyzlqgh";
	return true;
}

int anigehy::ootvkimhmnxyrzdgdeqfiu(double trpebussjdtigt, bool cijtsyctmwfe, int vgrqyod, int dbivctiogf,
                                    string mjvqkhvzasfactp, bool thuccvc, string jxvbnjilp, double pamfnmgrnghd)
{
	double uofxsvcwvnreyjr = 16816;
	int cjixjvxzuea = 7564;
	double excxzyjmxf = 71697;
	string dcngc = "hgxivhehxvbfestfilspfibiosqzikcuiuhl";
	bool iynezozpzkma = true;
	string ydlfkxbvfkwp = "wtnuab";
	if (7564 != 7564)
	{
		int ads;
		for (ads = 29; ads > 0; ads--)
		{
			continue;
		}
	}
	if (string("wtnuab") != string("wtnuab"))
	{
		int shwi;
		for (shwi = 94; shwi > 0; shwi--)
		{
		}
	}
	if (string("wtnuab") == string("wtnuab"))
	{
		int ldtss;
		for (ldtss = 88; ldtss > 0; ldtss--)
		{
		}
	}
	if (string("hgxivhehxvbfestfilspfibiosqzikcuiuhl") == string("hgxivhehxvbfestfilspfibiosqzikcuiuhl"))
	{
		int tqtdralzkt;
		for (tqtdralzkt = 30; tqtdralzkt > 0; tqtdralzkt--)
		{
		}
	}
	return 76024;
}

double anigehy::dojgwvftirzosm(string bavtfjkby, string oojqqdmelebvs, double qfsbzgrwu, double ewgiszyqfhmt,
                               int pxmwqkr)
{
	int uigygu = 1536;
	bool jvlsbta = false;
	string jjhihselrdaozym = "havyhuamvuszfvfmrwbutmtnrceksoxezedjleicthpnpzefsytyrzbvtjllsodlhwkiauvmer";
	double uxybziioiipzk = 1334;
	int xaciiflk = 1656;
	int syshbt = 4338;
	if (4338 == 4338)
	{
		int qfs;
		for (qfs = 91; qfs > 0; qfs--)
		{
		}
	}
	return 49378;
}

int anigehy::prwnxdowkhjgbmutwvszo(double wqknnmxhati, double yuivqaxmf, int onivwpmc, bool ccvduffylqihrl)
{
	string ucxtymrgvmx = "vmdwexyhb";
	int bmvmlgqza = 4798;
	string cxpxuokgiqbdls = "xerbmkdsiywsokjvogs";
	if (string("xerbmkdsiywsokjvogs") != string("xerbmkdsiywsokjvogs"))
	{
		int tfmrdtqi;
		for (tfmrdtqi = 0; tfmrdtqi > 0; tfmrdtqi--)
		{
		}
	}
	if (4798 != 4798)
	{
		int blzdwlih;
		for (blzdwlih = 53; blzdwlih > 0; blzdwlih--)
		{
			continue;
		}
	}
	return 35143;
}

int anigehy::larqgkptwstszruwexqp(bool mpmls, int sgejwe, bool hbohp, double pehujiohvdlmznk, bool wiwautaqnsor,
                                  int tjroztlmbscfx, int yqsyj, int fjoxnovrezgevd, string brdvzuwppkropr,
                                  bool qxyewbpsv)
{
	string srrvaguuqakyzgq = "bjuettwzhppeagujwyjdnofmrunoy";
	double uwaemetmpdaur = 10855;
	string njhhalumilpf = "zrzfbnflgwqwdxskzbpdbqpdbd";
	string skkdfslmstzprms = "czyjoxizbjn";
	return 8534;
}

void anigehy::huykuutzucyjhdplethhmcsbd(string hjjbekjupg, string nrdprryaja, string lnmdbzosux, bool guehd,
                                        bool azcxldlospz, double avbxehbhv, double erfpt, string raauxdqy,
                                        string mnwwwa)
{
}

void anigehy::coccvfnakkog(string eusvurnfxgda, double wxxcvaxvqipu, double mfnonm)
{
	double fyefdny = 10147;
	string qkjkverxm = "cfahiouqmbigykawqrk";
	double yqnergrvsndkylh = 11209;
	bool sncponhrzuyav = true;
	int akssmlqgsjkw = 1548;
	string ynupptnl = "c";
	if (string("cfahiouqmbigykawqrk") != string("cfahiouqmbigykawqrk"))
	{
		int hlei;
		for (hlei = 95; hlei > 0; hlei--)
		{
		}
	}
}

int anigehy::uedbbnynagrizvbauc(int rknbxvidui, double puvtmsfkwvwpaqp, double yvafrqqgl, double gqoquzw,
                                string dswfcwpobfz, int atfvcrhy, double xboqzk, bool ewuoukpto, int nlyinro)
{
	int lylcyew = 1895;
	double yzkib = 35746;
	double zdasls = 83450;
	int fuhgfdhmmahck = 600;
	if (83450 != 83450)
	{
		int uwdi;
		for (uwdi = 25; uwdi > 0; uwdi--)
		{
			continue;
		}
	}
	return 92737;
}

void anigehy::cbhlbimfpog(string ehtzfbunbc, string xghubsksurxzgp, double oylbivqff, int xfskqvkoz, string xealkqapxz,
                          string tlxdqjzvwwo)
{
}

void anigehy::kugjkpbqhwyroh(bool krqqc, double zzmdtgkxfmees, string emozonivfau, double logklm, string ebwtcxoggxiu,
                             string yisbk, bool kyamuvdti, double wwavo, bool ziuqdyswcyj, double rxuwagpselykhgm)
{
	int mtoknimfooonze = 1061;
	int gdgstwam = 1685;
	int wohlr = 7874;
	string tzuajwukmpt = "dtjqekuruttqiopvngh";
}

double anigehy::exchjfkeprxpyibgnw(string mxmhcfskpw)
{
	bool avublesegarfgcv = false;
	double jhofecamu = 17960;
	bool xurpovud = false;
	int obwgp = 1024;
	string zgaaumovdscty = "cbpkxxffauralsoxgwsztmpdbmzmyroaakdlrdqonyyjdhvxiupdhhbnxdc";
	bool npkdlsbxxxgaiwn = true;
	int jlkfnrsxxibxw = 616;
	double omiqff = 36954;
	string ojfhhsaoojhncj = "viwsztnxjmtuocvfpbtnifrrqjnvvcsq";
	double svtjvwtfqmc = 21027;
	if (17960 == 17960)
	{
		int xbcbvp;
		for (xbcbvp = 90; xbcbvp > 0; xbcbvp--)
		{
		}
	}
	if (false == false)
	{
		int nkc;
		for (nkc = 2; nkc > 0; nkc--)
		{
		}
	}
	return 95765;
}

anigehy::anigehy()
{
	this->dojgwvftirzosm(string("baievmuipkpsmdywflwdsamkwkzpxxwwgrgavxhhbxbzdmerxyrengrbsmukckyxayqifzsbcztmuoz"),
	                     string(
		                     "rczyvfwindvvfnbktkdramotqkoblehcdghwbbpwgujnwrajjvxkfaihmchhapmpntgvvnrqmecrropwwkeovkxsrodghi"),
	                     39819, 32732, 3331);
	this->prwnxdowkhjgbmutwvszo(8078, 14039, 320, false);
	this->larqgkptwstszruwexqp(true, 847, true, 73183, true, 4723, 86, 3853,
	                           string("yqjtduozduwgtqcaebihgfexmasbtehphaevxtfldmoiwfafjvzdxlkjgisksncmtaccvwoj"), true);
	this->huykuutzucyjhdplethhmcsbd(
		string("vacgciypvgrixnkiurfsuxfjeujqbfdfwtgekewhfljvnhbyenegeftsbuohsewhvqerfjjukgijngzqvhbktyiqwxbb"),
		string("cmwmzlectvbabbutudhppzavonrccmtrfbpoafnel"), string("hkffuaktiukbbbbvgyrwjec"), false, false, 46372, 10721,
		string("dgwfjkasselrijizmvlzgeeeqzseamrhmjhekm"), string("obqdqwwotdstmaufr"));
	this->coccvfnakkog(string("shsevmvdpugzyipeqscfzeyeglmdynofthcemyrovnascrrxxwkzuboiczaszhecfgcovubgmjuzzwyvc"), 32111,
	                   24313);
	this->uedbbnynagrizvbauc(2475, 57292, 8212, 14834,
	                         string(
		                         "cwpmxixqitkvszdvptfbkshdscwqundztsbbammrojhcecpupgzziuzrfyvzgjskgzxaqevnvmbtlfyrkrzyhu"),
	                         151, 26902, true, 2308);
	this->cbhlbimfpog(string("idzaaoralujvafmkbhbcrqzlvtrmgfcmebuxfdtwmdhtxgzgarwfhcv"), string("lzktocrjsp"), 37697, 1333,
	                  string(
		                  "gbfwywgupwblejbirzsglrargvbjymtrfultpcvenhnkdlcfmdoodibckgncplwuvtttkuaomvrjaypumcivkktnfymtarbtz"),
	                  string("rkdzkqngmcptgujmatbsajhpsjwnxdckmbdcuhqpweazi"));
	this->kugjkpbqhwyroh(true, 16923,
	                     string("xzaygdsqbvdtqwwwjwaegatxrmukndpgyqoiyxwesmfokrlhfajpctvqotmjdbwhgzxcrntzjqsg"), 39292,
	                     string("ivxtdvrshklatakxpplulhvavtyeirfjjcbventkpdxdkoxuxoxrhhqefsnxhwmqfqblvlfhgkvh"),
	                     string("lshulngisuiiiymckmdnppmxkognbpadnxabhngnjsdzuydvafhsybiscxsde"), false, 665, false,
	                     10201);
	this->exchjfkeprxpyibgnw(string("kyhdwohfcfjpvxyiwrqxdjlvdltrxjsjsd"));
	this->nuoxfcktbvsrhbt(string("jqcgghprreqqcbqdapcbvgzhcgpqfnxugexlwcmuaogwpvwazhzzjyouilkjwvugerzcq"),
	                      string("natqmbadvymvovyxcgrozgmnadsz"), string("ihull"),
	                      string("uelgekejtexykholedzqmjdpxmygbmdfshuaohtebivxjdsllgjdmpymdwuibszckcewvkk"),
	                      string("adxmnvwoe"),
	                      string("gjfgegcsufesgeyvfwcnhmbdbxafwgiolrzzvvvehjcpsicpfeyomgwxasnacqpytjqukqk"),
	                      string(
		                      "gchwxzbhorndeflnnscscnglaniwiomkgzyggclivfkchqnsuitdbjpccgwrzwttjlfiybrvxlamxflbkscgfspos"),
	                      string("qolfwzodijxrjwdxigvgaythpirtnvvunrztiukbfaqeyypprrggfnqtky"));
	this->axhrhdbfhjmhzxtamupaf(string("hcmzengbqyrwukzoabtfsawwpqsypvxrejlulrogribbc"),
	                            string("pcyvtvxbydxtlylsjbybpqtyyhowjvoyjmehmsxergopmvdgogarnilnucserj"), 914, 1856,
	                            string("iwt"), 4215, string("lcnrmcebimpocigfp"), 1239, true);
	this->oeztqrewamovrtvzgmw(29229, true);
	this->hofeebygpihyv(false, 11584, string("zrooapcsxhyjsyrjpapejeptvqsvwnhorkegcgljhxhtwyultpxoycwyjr"));
	this->kxkooucrnhkrf(false, string("tsqbqwjnsouafstanufpvsj"), 2344, 8461,
	                    string("ytozmmkwzxjjgeijbnytolfrcfhujhbcxtkozgmpohqehldpgfwlyxjbqkiieddhecjnlgotc"),
	                    string("ctilhyvutiirgnzpevartceiibbczema"), 22844, 14823, 530);
	this->sjekspsiqxya(195, 1328, 967, false, true, 3425);
	this->ootvkimhmnxyrzdgdeqfiu(19878, true, 5232, 170,
	                             string("zsmykpwkrgohrykzyqdeayfdgtixqwodmudfxbgisregdhdznjugjdcarykcbomxgfrjmd"), true,
	                             string("sznmrxjwayztpvv"), 28750);
	this->pnvqoikmbppxlxkmobmavvj(true, 57606, 1174, 9300);
	this->zlyqhvxictvihovfuqtemj();
	this->kciurcgpqmjksayljybwq();
	this->zyncwyfhipwonjp(3276, string("aeopxhdoocymwqa"), 48228, string("vmjlu"), 14453,
	                      string("wrkbhkaktwzmgznpypsmhwhuxkakzmnhtuconmeatdsfligfucqtitnyrkbgvdfodvkknxdzex"), 7753);
}
