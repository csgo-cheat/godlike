#include <cstdint>
#include <iostream>
#include <memory>
#include <string>

/*
	https://docs.lenux.org/libcurl-mit-visual-studio-2017
	nmake /f Makefile.vc mode=static MACHINE=x86 vc=15 ENABLE_WINSSL=no GEN_PDB=yes DEBUG=no

	vspkg install jsoncpp
	.vcxproj file add
	<VcpkgConfiguration Condition="'$(Configuration)' == 'Release'">Release</VcpkgConfiguration>
*/

#include <curl/curl.h>
#include <json/json.h>

#include "HookIncludes.h"
#include "HooksList.h"
#include "Listener.h"

#include "License.h"

#include <fstream>

using namespace std;

namespace hooks
{
	vfunc_hook panel;
	vfunc_hook client;
	vfunc_hook clientmode;
	vfunc_hook modelrender;
	vfunc_hook prediction;
	vfunc_hook surface;
	vfunc_hook eventmanager;
	vfunc_hook d3d;
	vfunc_hook viewrender;
	vfunc_hook engine;
	vfunc_hook sv_cheats;

	namespace
	{
		size_t callback(
			const char* in,
			size_t size,
			size_t num,
			string* out)
		{
			const size_t totalBytes(size * num);
			out->append(in, totalBytes);
			return totalBytes;
		}
	}

	bool auth()
	{
		const string url("http://vacbax.ir/api/v1/validate/");

		CURL* curl = curl_easy_init();

		string postData = "key=";
		postData.append(license::generateUniqueID());

		// Set remote URL.
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postData.c_str());

		// Don't bother trying IPv6, which would increase DNS resolution time.
		curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

		// Don't wait forever, time out after 10 seconds.
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

		// Follow HTTP redirects if necessary.
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		// Response information.
		int httpCode(0);
		unique_ptr<string> httpData(new string());

		// Hook up data handling function.
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

		// Hook up data container (will be passed as the last parameter to the
		// callback handling function).  Can be any pointer type, since it will
		// internally be passed as a void pointer.
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, httpData.get());

		// Run our HTTP GET command, capture the HTTP response code, and clean up.
		curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
		curl_easy_cleanup(curl);

		if (httpCode == 200)
		{
			// Response looks good - done using Curl now.  Try to parse the results
			// and print them out.
			Json::Value jsonData;
			Json::Reader jsonReader;

			if (jsonReader.parse(*httpData.get(), jsonData))
			{
				const string dateString(jsonData["token"].asString());
				const bool active(jsonData["active"].asBool());

				if (!active)
				{
					MessageBox(nullptr,
					           "Your License Key is Not Valid,\nAvtivate Your License Key or Run Game WithOut Injecting Cheat!",
					           "License", MB_OK | MB_SYSTEMMODAL);
					return false;
				}

				return active;
			}

			MessageBox(nullptr, "Error Code: 330", "Server Error", MB_OK | MB_SYSTEMMODAL);
		}
		else
		{
			MessageBox(nullptr, "Please Check Your Internet Connection!\nError Code: 320", "Activation Failed",
			           MB_OK | MB_SYSTEMMODAL);
			return false;
		}
	}


	void initialize()
	{
		if (!auth())
			return;

		client.setup(g_CHLClient);
		client.hook_index(36, hkFrameStageNotify);
		client.hook_index(21, hkCreateMove);

		//prediction.setup(g_Prediction);
		//prediction.hook_index(14, hkInPrediction);

		clientmode.setup(g_ClientMode);
		clientmode.hook_index(18, hkOverrideView);
		clientmode.hook_index(35, hkGetViewModelFOV);
		clientmode.hook_index(44, hkDoPostScreenSpaceEffects);

		panel.setup(g_Panel);
		panel.hook_index(41, hkPaintTraverse);

		modelrender.setup(g_ModelRender);
		modelrender.hook_index(21, hkDrawModelExecute);

		surface.setup(g_Surface);
		surface.hook_index(82, hkPlaySound);

		//engine.setup(g_Engine);
		//engine.hook_index(27, hkIsConnected);
		//engine.hook_index(93, hkIsHltv);

		ConVar* sv_cheats_con = g_CVar->FindVar("sv_cheats");
		sv_cheats.setup(sv_cheats_con);
		sv_cheats.hook_index(13, hkSvCheatsGetBool);

		m_present = U::pattern_scan(GetModuleHandleW(L"gameoverlayrenderer.dll"), "FF 15 ? ? ? ? 8B F8 85 DB 74 1F") + 0x2;
		//big ( large ) obs bypass
		m_reset = U::pattern_scan(GetModuleHandleW(L"gameoverlayrenderer.dll"), "FF 15 ? ? ? ? 8B F8 85 FF 78 18") + 0x2;
		//big ( large ) obs bypass

		oPresent = **reinterpret_cast<Present_T**>(m_present);
		oReset = **reinterpret_cast<Reset_t**>(m_reset);

		**reinterpret_cast<void***>(m_present) = reinterpret_cast<void*>(&hkPresent);
		**reinterpret_cast<void***>(m_reset) = reinterpret_cast<void*>(&hkReset);

		/*d3d9_device = **reinterpret_cast<IDirect3DDevice9***>(U::FindPattern("shaderapidx9.dll", (PBYTE)"\xA1\x00\x00\x00\x00\x50\x8B\x08\xFF\x51\x0C", "x????xxxxxx") + 1);
		renderer->Initialize(FindWindowA("Valve001", NULL), d3d9_device);
		d3d.Initialize(reinterpret_cast<DWORD*>(d3d9_device));
		g_fnOriginalReset = reinterpret_cast<Reset_t>(d3d.Hook(reinterpret_cast<DWORD>(hkReset), 16));
		g_fnOriginalEndScene = reinterpret_cast<EndScene_t>(d3d.Hook(reinterpret_cast<DWORD>(hkEndScene), 42));*/

		item_purchase::singleton()->initialize();
	}

	void cleanup()
	{
		item_purchase::singleton()->remove();
		client.unhook_all();
		prediction.unhook_all();
		clientmode.unhook_all();
		panel.unhook_all();
		modelrender.unhook_all();
		surface.unhook_all();
		//EventManager.Unhook();
		viewrender.unhook_all();
		//d3d.Unhook();
		sv_cheats.unhook_all();
		engine.unhook_all();
		**reinterpret_cast<void***>(m_present) = reinterpret_cast<void*>(oPresent);
		**reinterpret_cast<void***>(m_reset) = reinterpret_cast<void*>(oReset);
		renderer->~Renderer();
	}
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class dblnsbt
{
public:
	double loortpdtwqifonq;
	double iqhubbmsobudzt;
	string cymogjk;
	dblnsbt();
	bool uvqthwgzeofzjjydu(bool cnmuifvlatvgde, string qlkxurtuyzrk, double byyqnll, string lzqxey, bool ypewrogjyc,
	                       int fmxqltbd, bool bppjphejewuklha, bool ccoesbv, int jsdxafowmgelik, string gblxsblvqv);
	int fqpejajhmlz();
	string imhhdizmtigpsrv(double itzwdau, int rchsulwyjtwvec, string uvsfpstynyol);
	double vydgqrcpjwujrgi(bool evlmzrpijgvca, string fbgixiywetwr, bool sfbqbzrytssq, double ovtrddkcmbdi, int lyojsikbvq,
	                       string drkwgf);
	void doylrwtxynwxfwbutnbtmwmfs(string vcfqvnukkj, double nglggjulhmul, double lovqqshi, string tmjuk,
	                               double ajgttzaaevisd, double tmcfdwek, bool pnhorcvmil);

protected:
	int bliptbikeaxede;

	bool eronvmicldrl(bool zhuxbzx, int iwgmvmkeavlq, string fcwssptof, double indpxvsvxk, string tvuoozurddcs);
	int aqqnzrefzamzywen(int cawnnafhnwt, string mbpnknj, string afuajkgwnhmfke, string ucdlfruqjytto, int ohmfvafgh,
	                     bool kcemvadbgtuhwjn, int lrthhtblt);

private:
	double apbho;

	bool bxbckutffictjagm(int yyzuvemv, bool siynvyvv, string auyoyboduo, string nfkrxnnd, bool pqnicnthzmily,
	                      double xxaqppwmeyr, bool prxaetk, string rzjbw);
	bool dpyvasnedarj(double cqidtlgl, double cwxxuczjppbh, double njyibbra, double lvayannrmrvki, string amoszzzl,
	                  int bhaloy, double vbjni, double ilupebxqqdpvvnt);
	string jsjakvksrhxtuyvdhkb(string qwiuompw, string faauhipxe, double oovvt, int uyfsslbb);
	bool uzgivlekfieggsatgoifqdc(double gfpzjxkebtuzeqm, bool zrtdzzf, string mbdlqfaqdkxwhw, int qvyzvllsq,
	                             double bnruxogzsngglh);
	int gvlgbthuipsxjoaltzneywmuv(string jzwsuv, double ipbroe, string qrsalcdcmapk, bool radrhzljkzxbzfc, double knfrm,
	                              int mybopqchz, string ylqbrqjsabv);
};


bool dblnsbt::bxbckutffictjagm(int yyzuvemv, bool siynvyvv, string auyoyboduo, string nfkrxnnd, bool pqnicnthzmily,
                               double xxaqppwmeyr, bool prxaetk, string rzjbw)
{
	string xotlglnfkpknrqn = "pxydrvafewhjjhyuahgqnbllqcgstyhvcrqgebwetjwpthjilhbjgswu";
	int qgqqxsnylacx = 1198;
	if (string("pxydrvafewhjjhyuahgqnbllqcgstyhvcrqgebwetjwpthjilhbjgswu") == string(
		"pxydrvafewhjjhyuahgqnbllqcgstyhvcrqgebwetjwpthjilhbjgswu"))
	{
		int lwff;
		for (lwff = 98; lwff > 0; lwff--)
		{
		}
	}
	if (string("pxydrvafewhjjhyuahgqnbllqcgstyhvcrqgebwetjwpthjilhbjgswu") == string(
		"pxydrvafewhjjhyuahgqnbllqcgstyhvcrqgebwetjwpthjilhbjgswu"))
	{
		int hacybzu;
		for (hacybzu = 98; hacybzu > 0; hacybzu--)
		{
		}
	}
	if (string("pxydrvafewhjjhyuahgqnbllqcgstyhvcrqgebwetjwpthjilhbjgswu") != string(
		"pxydrvafewhjjhyuahgqnbllqcgstyhvcrqgebwetjwpthjilhbjgswu"))
	{
		int zyfjwcxkt;
		for (zyfjwcxkt = 48; zyfjwcxkt > 0; zyfjwcxkt--)
		{
		}
	}
	if (1198 == 1198)
	{
		int uazxyfyex;
		for (uazxyfyex = 65; uazxyfyex > 0; uazxyfyex--)
		{
		}
	}
	return false;
}

bool dblnsbt::dpyvasnedarj(double cqidtlgl, double cwxxuczjppbh, double njyibbra, double lvayannrmrvki, string amoszzzl,
                           int bhaloy, double vbjni, double ilupebxqqdpvvnt)
{
	int xvwukb = 903;
	string ojazdtccol = "wptlowcvgjhebfjprhwpjnpzvjsotbcsypheqyuanwhomszasfxvswd";
	int nhcbkqz = 1565;
	int lczuheco = 605;
	return false;
}

string dblnsbt::jsjakvksrhxtuyvdhkb(string qwiuompw, string faauhipxe, double oovvt, int uyfsslbb)
{
	int clnpjdnzrcjms = 324;
	double ezewsvtsy = 9441;
	if (9441 == 9441)
	{
		int egt;
		for (egt = 23; egt > 0; egt--)
		{
		}
	}
	if (324 == 324)
	{
		int dfswy;
		for (dfswy = 7; dfswy > 0; dfswy--)
		{
		}
	}
	if (324 != 324)
	{
		int rruu;
		for (rruu = 53; rruu > 0; rruu--)
		{
			continue;
		}
	}
	if (324 != 324)
	{
		int hyoksbvw;
		for (hyoksbvw = 41; hyoksbvw > 0; hyoksbvw--)
		{
			continue;
		}
	}
	if (9441 != 9441)
	{
		int bcpjkb;
		for (bcpjkb = 10; bcpjkb > 0; bcpjkb--)
		{
			continue;
		}
	}
	return string("pfmyojbniav");
}

bool dblnsbt::uzgivlekfieggsatgoifqdc(double gfpzjxkebtuzeqm, bool zrtdzzf, string mbdlqfaqdkxwhw, int qvyzvllsq,
                                      double bnruxogzsngglh)
{
	double mbmnkwzbkrdb = 15340;
	bool jcqtskim = false;
	string ztyeq = "fwcvsojllflrxdfropbmjffhkvmszaukoswqjamnectpvpfsellhaeupcmfio";
	int fsalhuxdiqcrydt = 3801;
	bool lgrmoyghayhbveo = true;
	int afwbvcuzvqu = 598;
	bool ramyyaih = false;
	string wktkvbjffnifel = "mqgkxdsurwlbqlrocjswgzwvsxgdstowsdgcugicoc";
	if (true != true)
	{
		int pmww;
		for (pmww = 65; pmww > 0; pmww--)
		{
			continue;
		}
	}
	if (15340 == 15340)
	{
		int qoyyayeavd;
		for (qoyyayeavd = 24; qoyyayeavd > 0; qoyyayeavd--)
		{
		}
	}
	if (598 == 598)
	{
		int um;
		for (um = 82; um > 0; um--)
		{
		}
	}
	if (15340 != 15340)
	{
		int bcralaeko;
		for (bcralaeko = 39; bcralaeko > 0; bcralaeko--)
		{
			continue;
		}
	}
	return false;
}

int dblnsbt::gvlgbthuipsxjoaltzneywmuv(string jzwsuv, double ipbroe, string qrsalcdcmapk, bool radrhzljkzxbzfc,
                                       double knfrm, int mybopqchz, string ylqbrqjsabv)
{
	int torgzqzgvamf = 5456;
	int anskppnennm = 3706;
	int mdplqlybjxjfpxz = 190;
	int qirwbqvpccdzbb = 4260;
	bool ebdiwgqkymmlc = false;
	bool ghopvuktwxu = true;
	string rafqwatiryrb = "fkdejjkbcaidletolomdzrallhgkpjdbrxsytdunkqtpixhlliwsqhwygdferonlqylnvdioqhtzonzanqtfhpuqqdb";
	int mbfud = 1247;
	if (5456 != 5456)
	{
		int owifsur;
		for (owifsur = 48; owifsur > 0; owifsur--)
		{
			continue;
		}
	}
	if (190 == 190)
	{
		int katfucq;
		for (katfucq = 80; katfucq > 0; katfucq--)
		{
		}
	}
	if (true != true)
	{
		int lj;
		for (lj = 69; lj > 0; lj--)
		{
			continue;
		}
	}
	return 5583;
}

bool dblnsbt::eronvmicldrl(bool zhuxbzx, int iwgmvmkeavlq, string fcwssptof, double indpxvsvxk, string tvuoozurddcs)
{
	string pnxjcdcnydsymlf =
		"urcyxkiespcblisrpanksocelcgccakkvvsatwjzxkzaxjzczekbzoviainhanetncdtkfuxwzkhnwzaamvfmzbmoilsr";
	string eemvsusfrh = "ftlpaabblhbiiiwndwzvewyqm";
	int cdaxzy = 130;
	double tswmbwfob = 5391;
	bool tslnlpovznj = false;
	double vrpbcqfzfbuz = 42308;
	bool jgitp = true;
	if (string("urcyxkiespcblisrpanksocelcgccakkvvsatwjzxkzaxjzczekbzoviainhanetncdtkfuxwzkhnwzaamvfmzbmoilsr") != string(
		"urcyxkiespcblisrpanksocelcgccakkvvsatwjzxkzaxjzczekbzoviainhanetncdtkfuxwzkhnwzaamvfmzbmoilsr"))
	{
		int bvkqtkf;
		for (bvkqtkf = 64; bvkqtkf > 0; bvkqtkf--)
		{
		}
	}
	if (42308 == 42308)
	{
		int thvtadeq;
		for (thvtadeq = 44; thvtadeq > 0; thvtadeq--)
		{
		}
	}
	if (true != true)
	{
		int asst;
		for (asst = 74; asst > 0; asst--)
		{
			continue;
		}
	}
	if (string("ftlpaabblhbiiiwndwzvewyqm") == string("ftlpaabblhbiiiwndwzvewyqm"))
	{
		int rz;
		for (rz = 70; rz > 0; rz--)
		{
		}
	}
	if (string("urcyxkiespcblisrpanksocelcgccakkvvsatwjzxkzaxjzczekbzoviainhanetncdtkfuxwzkhnwzaamvfmzbmoilsr") != string(
		"urcyxkiespcblisrpanksocelcgccakkvvsatwjzxkzaxjzczekbzoviainhanetncdtkfuxwzkhnwzaamvfmzbmoilsr"))
	{
		int kuwoan;
		for (kuwoan = 50; kuwoan > 0; kuwoan--)
		{
		}
	}
	return false;
}

int dblnsbt::aqqnzrefzamzywen(int cawnnafhnwt, string mbpnknj, string afuajkgwnhmfke, string ucdlfruqjytto,
                              int ohmfvafgh, bool kcemvadbgtuhwjn, int lrthhtblt)
{
	string nzcuaxtybluklqi = "rmegsggxantoyzjothlgdhnreedvgcyjjxgvtiuylwaeiiqcumfiisggmqlpqxzbsswnolvoga";
	bool spfxvgsig = true;
	bool bugnypjvmfcw = true;
	string nkifnvgriraeloo = "lcwvjpaalvuquqqkcavcsfmvgfvme";
	double irtikmwsu = 13597;
	double nvtvnazvpgj = 26421;
	int cruyknsgbwxc = 898;
	string wykpomrtrwqaksf = "nugrbhmarqowitjavenqayjzaorykshfzfmonzgyxdxpxxflyxjzcusokshswrtfqempfokpyyd";
	int lcccir = 1512;
	return 74137;
}

bool dblnsbt::uvqthwgzeofzjjydu(bool cnmuifvlatvgde, string qlkxurtuyzrk, double byyqnll, string lzqxey,
                                bool ypewrogjyc, int fmxqltbd, bool bppjphejewuklha, bool ccoesbv, int jsdxafowmgelik,
                                string gblxsblvqv)
{
	double nnuvoqmdtevge = 14785;
	string fwlqacev = "mnxetnepspwnzinxxjrhuvprllnjnssrhnpghmnoogjbrdacbgqjbdulysywxhchqthifbiilnjntydanswqkhzbuxwz";
	if (string("mnxetnepspwnzinxxjrhuvprllnjnssrhnpghmnoogjbrdacbgqjbdulysywxhchqthifbiilnjntydanswqkhzbuxwz") != string(
		"mnxetnepspwnzinxxjrhuvprllnjnssrhnpghmnoogjbrdacbgqjbdulysywxhchqthifbiilnjntydanswqkhzbuxwz"))
	{
		int ootvimunpe;
		for (ootvimunpe = 87; ootvimunpe > 0; ootvimunpe--)
		{
		}
	}
	return true;
}

int dblnsbt::fqpejajhmlz()
{
	return 97469;
}

string dblnsbt::imhhdizmtigpsrv(double itzwdau, int rchsulwyjtwvec, string uvsfpstynyol)
{
	bool ngveayh = false;
	int ehmwjlryhma = 789;
	string wgncl = "nnvwenmzskvxuydjkmxfwiprmhtjnvpncyupv";
	bool jzwkhbgbhnvktic = true;
	double trpilkofgu = 24308;
	double sffnydnboaqacba = 60835;
	bool pjnzdiadfjmne = false;
	string aukkb = "zmnzgldx";
	if (789 != 789)
	{
		int uim;
		for (uim = 62; uim > 0; uim--)
		{
			continue;
		}
	}
	return string("");
}

double dblnsbt::vydgqrcpjwujrgi(bool evlmzrpijgvca, string fbgixiywetwr, bool sfbqbzrytssq, double ovtrddkcmbdi,
                                int lyojsikbvq, string drkwgf)
{
	bool ewmctulxxnxow = false;
	string gtgptcevijjyb = "ruuyamnqam";
	bool rtsypcudprgbzr = false;
	int ezhmobhmpjbbm = 1096;
	int ghtbvvkyqmwstm = 76;
	double cuwyfccexbc = 41929;
	string taawoezmkpdr = "jyovefckufvdjtyoazonxqwvhrsgbzfofkdwmdaoggadvldhtxfrypjxbvtnapvqhryecnavizsno";
	int vsdxprnj = 1061;
	return 61284;
}

void dblnsbt::doylrwtxynwxfwbutnbtmwmfs(string vcfqvnukkj, double nglggjulhmul, double lovqqshi, string tmjuk,
                                        double ajgttzaaevisd, double tmcfdwek, bool pnhorcvmil)
{
	bool rpcaxlc = false;
	double bzytwuo = 56422;
	bool wqahgutco = false;
	string aongxhak = "zfkvtbjedrvzvcjkhcttzmtmusfqkmnmnpesoxczhyrzaarergevvjgjrwwuwpgwwekssuauoce";
	int pniatrqmpp = 1291;
	string zsxsanmzdjz = "blhoyrmtfqvhusqxfatjkzegvcjpkgzadu";
	int ebcqmxjs = 4980;
	int xfafizwyi = 292;
	bool kljiej = true;
	int xbbgbd = 3786;
}

dblnsbt::dblnsbt()
{
	this->uvqthwgzeofzjjydu(false, string("qvxjjhuyflzprblfkystitlpnikxksbesrukurjwhvnnxowgnykdsjeeujslxjcgmjl"), 25466,
	                        string("zurpzrsoajbzystbeiepitfmzwduzftibgrwrwjmflxydmltfmzsoqyhbbvpbdqzju"), false, 1233,
	                        true, true, 1352,
	                        string("aezoxuashxlofgeumyypinbtuqypizdrsdntwwlvkrshmozbbosvhumwmrfravdaiyhesnpnxlhcvqbn"));
	this->fqpejajhmlz();
	this->imhhdizmtigpsrv(22302, 7235, string("vdenqcggvofznfkbepimqugxmdpzfjhfccjgybyczhq"));
	this->vydgqrcpjwujrgi(true, string("snlzavftptubgrqagijbqqlapadzhkckrmwmrlwyo"), false, 1709, 1233,
	                      string("mobxnbhcxpeiokmcxyiyizmkrygaosvqvukxbqsigfkniyunylrgxoxvxzzoxvrxjlkno"));
	this->doylrwtxynwxfwbutnbtmwmfs(string("bs"), 1185, 19063, string("debxbesucibemujdwsakxeznreetjwdiinidjqzvyczo"),
	                                18274, 15368, true);
	this->eronvmicldrl(true, 711, string("gjiugotirocvbcliihepfrd"), 82656,
	                   string("dkjobgcsesyvladyhnzevcttbndwmqecciwlgdxrccwpiaxgcwnioexltoedpliifdzubnekbqagpasiinngozb"));
	this->aqqnzrefzamzywen(2610, string("kcqgvzazjbwofaqqtm"), string("nftcp"),
	                       string(
		                       "sbpfchglbohplfsykrktxmujpojgxcupdqficchriyxsmchdgxvtilfeenkrvqicvdxmulivxcqkjegjlglekftqezjkzhco"),
	                       2100, false, 2050);
	this->bxbckutffictjagm(8866, true, string("ghvnthabwpuhurfehmbzj"), string("phrdgmcfxybgbxhqsguotmyj"), false, 20287,
	                       true, string("pzapdzgpalculomdtydqalpbzusdfwhiointkfdanbqadjbp"));
	this->dpyvasnedarj(36872, 18230, 1121, 33074,
	                   string("twzpasvpgtlqcqisvssdljerjtevbbckssvxzuidwedyqnypwhdbodwsnyclttjrtuqwmjciqdujnhbsomcr"),
	                   1047, 1204, 59329);
	this->jsjakvksrhxtuyvdhkb(string("asecndjcqvrlczfbfrkkpytentrkanaqcabdrhdflnvcdgzkfxumbxhf"),
	                          string(
		                          "pncxvktjetcyktncgxofzedqnqdgwucfalzrsmkpzqtexnuatzwbndeejzyzqyabjewzfkqqvixpwualkhbyunwjibcnaa"),
	                          34500, 7559);
	this->uzgivlekfieggsatgoifqdc(14014, false, string("ooqsycqxynixnvgxpmnujdpupy"), 1713, 20621);
	this->gvlgbthuipsxjoaltzneywmuv(string("chsnokifbkugvmuhkpbsfczodpujtdwvbgxpbejlvjlwsymppglzgwcjwyfudgvwsptbrmdsumd"),
	                                18890, string("jqpzffpwuw"), true, 48497, 3415,
	                                string(
		                                "odqygebnmbdsmflnjwcfdvzffwoeijzerdlhtonpqjjwsqkcjdobltrcijufrkujhksffzmyjbjbxkdbqcrzbgpxbhohtkhtvr"));
}
