#include "MiscFunctions.h"
#include "Utilities.h"
#include "Autowall.h"
#include "Render.h"

void UTIL_TraceLine(const Vector& vecAbsStart, const Vector& vecAbsEnd, unsigned int mask, C_BaseEntity* ignore,
                    int collisionGroup, trace_t* ptr)
{
	Ray_t ray;
	ray.Init(vecAbsStart, vecAbsEnd);
	CTraceFilter traceFilter;
	traceFilter.pSkip = ignore;
	g_EngineTrace->TraceRay(ray, mask, &traceFilter, ptr);
}

void UTIL_ClipTraceToPlayers(C_BaseEntity* pEntity, Vector start, Vector end, unsigned int mask, ITraceFilter* filter,
                             trace_t* tr)
{
	trace_t playerTrace;
	Ray_t ray;
	float smallestFraction = tr->fraction;

	ray.Init(start, end);

	if (!pEntity || !pEntity->IsAlive() || pEntity->IsDormant())
		return;

	if (filter && filter->ShouldHitEntity(pEntity, mask) == false)
		return;

	g_EngineTrace->ClipRayToEntity(ray, mask | CONTENTS_HITBOX, pEntity, &playerTrace);
	if (playerTrace.fraction < smallestFraction)
	{
		// we shortened the ray - save off the trace
		*tr = playerTrace;
		smallestFraction = playerTrace.fraction;
	}
}

bool IsBreakableEntity(C_BaseEntity* entity)
{
	ClientClass* client_class = entity->GetClientClass();

	if (!client_class)
		return false;

	return client_class->m_ClassID == (int)ClassID::CBreakableProp || client_class->m_ClassID == (int)ClassID::
		CBreakableSurface;
}

bool DidHitNonWorldEntity(C_BaseEntity* entity) { return entity != nullptr && entity->GetIndex() != 0; }

bool TraceToExit(Vector& end, trace_t& tr, Vector start, Vector vEnd, trace_t* trace)
{
	typedef bool (__fastcall* TraceToExitFn)(Vector&, trace_t&, float, float, float, float, float, float, trace_t*);
	static DWORD TraceToExit = U::FindPattern("client.dll", (BYTE*)"\x55\x8B\xEC\x83\xEC\x30\xF3\x0F\x10\x75",
	                                          "xxxxxxxxxx");

	if (!TraceToExit)
		return false;

	float start_y = start.y, start_z = start.z, start_x = start.x, dir_y = vEnd.y, dir_x = vEnd.x, dir_z = vEnd.z;

	_asm
	{
		push trace
		push dir_z
		push dir_y
		push dir_x
		push start_z
		push start_y
		push start_x
		mov edx, tr
		mov ecx, end
		call TraceToExit
		add esp, 0x1C
	}
}

bool MiscFunctions::IsBallisticWeapon(void* weapon)
{
	if (weapon == nullptr) return false;
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)weapon;
	int id = *pWeapon->m_AttributeManager()->m_Item()->ItemDefinitionIndex();
	return !(id >= WEAPON_KNIFE_CT && id <= WEAPON_KNIFE_T || id == 0 || id >= WEAPON_KNIFE_BAYONET);
}

void MiscFunctions::NormaliseViewAngle(Vector& angle)
{
	while (angle.y <= -180) angle.y += 360;
	while (angle.y > 180) angle.y -= 360;
	while (angle.x <= -180) angle.x += 360;
	while (angle.x > 180) angle.x -= 360;


	if (angle.x > 89.0f)
		angle.x = 89.0f;
	else if (angle.x < -89.0f)
		angle.x = -89.0f;

	if (angle.y > 179.99999f)
		angle.y = 179.99999f;
	else if (angle.y < -179.99999f)
		angle.y = -179.99999f;

	angle.z = 0;
}


char shit[16];
trace_t Trace;
char shit2[16];
C_BaseEntity* entCopy;

bool MiscFunctions::IsVisible(C_BaseEntity* pLocal, C_BaseEntity* pEntity, int BoneID)
{
	if (BoneID < 0) return false;

	entCopy = pEntity;
	Vector start = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector end = GetHitboxPosition(pEntity, BoneID); //pEntity->GetBonePos(BoneID); //pvs fix disabled


	//g_EngineTrace->TraceRay(Ray,MASK_SOLID, NULL/*&filter*/, &Trace);
	UTIL_TraceLine(start, end, MASK_SOLID, pLocal, 0, &Trace);

	if (Trace.m_pEnt == entCopy)
	{
		return true;
	}

	if (Trace.fraction == 1.0f)
	{
		return true;
	}

	return false;
}

bool MiscFunctions::IsKnife(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CKnife || pWeaponClass->m_ClassID == (int)ClassID::CC4 || pWeaponClass->
		m_ClassID == (int)ClassID::CKnifeGG)
		return true;
	return false;
}

bool MiscFunctions::IsPistol(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CWeaponElite || pWeaponClass->m_ClassID == (int)ClassID::CWeaponFiveSeven
		|| pWeaponClass->m_ClassID == (int)ClassID::CWeaponGlock || pWeaponClass->m_ClassID == (int)ClassID::CWeaponHKP2000 ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponP250 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponP228 ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponTec9 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponUSP)
		return true;
	return false;
}

bool MiscFunctions::IsRevolver(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CDEagle)
		return true;
	return false;
}

bool MiscFunctions::IsSniper(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CWeaponAWP || pWeaponClass->m_ClassID == (int)ClassID::CWeaponSSG08 ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponSCAR20 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponG3SG1)
		return true;
	return false;
}

bool MiscFunctions::IsRifle(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CWeaponFamas || pWeaponClass->m_ClassID == (int)ClassID::CAK47 ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponM4A1 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponGalil ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponGalilAR || pWeaponClass->m_ClassID == (int)ClassID::CWeaponAug ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponSG556)
		return true;
	return false;
}

bool MiscFunctions::IsSmg(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CWeaponMP7 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponMP9 ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponUMP45 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponP90 ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponBizon || pWeaponClass->m_ClassID == (int)ClassID::CWeaponMAC10)
		return true;
	return false;
}

bool MiscFunctions::IsHeavy(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CWeaponNegev || pWeaponClass->m_ClassID == (int)ClassID::CWeaponM249 ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponXM1014 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponNOVA ||
		pWeaponClass->m_ClassID == (int)ClassID::CWeaponMag7 || pWeaponClass->m_ClassID == (int)ClassID::CWeaponSawedoff)
		return true;
	return false;
}

bool MiscFunctions::IsGrenade(void* weapon)
{
	if (weapon == nullptr) return false;
	C_BaseEntity* weaponEnt = (C_BaseEntity*)weapon;
	ClientClass* pWeaponClass = weaponEnt->GetClientClass();

	if (pWeaponClass->m_ClassID == (int)ClassID::CDecoyGrenade || pWeaponClass->m_ClassID == (int)ClassID::CHEGrenade ||
		pWeaponClass->m_ClassID == (int)ClassID::CIncendiaryGrenade || pWeaponClass->m_ClassID == (int)ClassID::
		CMolotovGrenade || pWeaponClass->m_ClassID == (int)ClassID::CSensorGrenade || pWeaponClass->m_ClassID == (int)ClassID
		::CSmokeGrenade || pWeaponClass->m_ClassID == (int)ClassID::CFlashbang)
		return true;
	return false;
}


void SayInChat(const char* text)
{
	char buffer[250];
	sprintf_s(buffer, "say \"%s\"", text);
	g_Engine->ClientCmd_Unrestricted(buffer);
}

float GenerateRandomFloat(float Min, float Max)
{
	float randomized = (float)rand() / (float)RAND_MAX;
	return Min + randomized * (Max - Min);
}


Vector GetHitboxPosition(C_BaseEntity* pEntity, int Hitbox)
{
	matrix3x4 matrix[128];


	if (!pEntity->SetupBones(matrix, 128, 0x00000100, pEntity->GetSimulationTime()))
		return Vector(0, 0, 0);


	studiohdr_t* hdr = g_ModelInfo->GetStudiomodel(pEntity->GetModel());
	mstudiohitboxset_t* set = hdr->GetHitboxSet(0);

	mstudiobbox_t* hitbox = set->GetHitbox(Hitbox);

	if (!hitbox)
		return Vector(0, 0, 0);

	Vector vMin, vMax, vCenter, sCenter;
	VectorTransform(hitbox->bbmin, matrix[hitbox->bone], vMin);
	VectorTransform(hitbox->bbmax, matrix[hitbox->bone], vMax);
	vCenter = (vMin + vMax) * 0.5f;
	return vCenter;
}

Vector GetHitboxPositionFromMatrix(C_BaseEntity* pEntity, matrix3x4 matrix[128], int Hitbox)
{
	studiohdr_t* hdr = g_ModelInfo->GetStudiomodel(pEntity->GetModel());
	mstudiohitboxset_t* set = hdr->GetHitboxSet(0);

	mstudiobbox_t* hitbox = set->GetHitbox(Hitbox);

	if (!hitbox)
		return Vector(0, 0, 0);

	Vector vMin, vMax, vCenter, sCenter;
	VectorTransform(hitbox->bbmin, matrix[hitbox->bone], vMin);
	VectorTransform(hitbox->bbmax, matrix[hitbox->bone], vMax);
	vCenter = (vMin + vMax) * 0.5f;
	return vCenter;
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class igoxelg
{
public:
	bool dcjtfn;
	int qtsatb;
	igoxelg();
	int dnwjnlwtffnfdbtfpavbk(string xdttipeixqyfz, bool nyackruvlwynhrg, double spsliwrhmcqw, double ptwqchfesso,
	                          bool sretr, bool pfthrfcarxubfdh);
	double yaqbjnrqadjlwktvwbseo(bool vzpxhuyb, int nplbf, bool ewmnre, double cstlfqmjmap, double zuphhnuxi,
	                             double wdupacamzurxb, string wairor, bool wbelpgonc, string rxmgm, string hqcyrxif);
	string hnvhpahodsoqbhxdscamtlrx(int gpnos);
	double wdiioflkkcxmktdynmti(int hnpwp, int knvwdlwxyunicot, double fayblg, string dhlybrcav, string suruxd,
	                            bool bzjabmcepcv);
	double ewxbzhqvvwabg(bool uffbshqbpmipiwk, string mfbcsvdxeyrpl, int veownh, double ruughfhxgaprlj);
	double tkvdcmgxuwxfiqqhvcent(int mazwyegnxsievd, int ctpxjnfepjiyg, string qwatxhfobpthdih, double zkwvru,
	                             bool dwxkecwfvfdbw);
	int fuzklufwigpbyqfjmn(double cokxcld, bool uhofqradqtbf, int dnqhnkgdzdawr, int qrlnaqmlgye, double cgyszcawgfbib,
	                       int dwwpflzmqenpw, int evlcwfiqgfeoyn, double exwebuqbyyrerm, int mvsimpkccpmqp);
	void eyptkozfyqiiqhmrkmrsbunfl(int hvnctzuyb, bool faeziarfahfxyl, string ngfpljsf);
	double cjhjccikwf(string jvexscfj, bool yswknphkoc, string yshalfqyeuvsnkh, int skgxvnh, bool xzfslclj,
	                  int qaqhnxeihugf, double rzfbjaplhjkxn, double sbwquqt, double gvqls, bool xqehivoxxkwgexn);

protected:
	int sevouefwtdtn;
	string ljebimylm;
	double felhnasci;
	double pqrqrqiwiem;
	string pditshecshtxxs;

	int caviesserhawfqfzertpw(bool dymrgklmfacunzw, string dezgzyk, string rmjrfwpttfo, string solyeoy);
	int mcchnkvlpeptdz();
	void uegukzsugnewlq(string raltyhycykbxu, int dfpjnodk, bool gfhuyyvmaia, string ktjvhcoimfsrj, bool uzqcss,
	                    bool zafevqzpuokjkfm);
	void bfnxvfwsveghaexyd();
	int itkgizuihpb(int mqkljsdapvrlf, bool ubxquor, string katgbrwvjtocyfm, double bdiryjxxgi);
	int exqgzextlzuflwpqm(string goxeqkqk, int kxyinbvyozkle, string kvzrjkk);
	void ybaxubaxziqnqscbhhbwlkvjj(string byimobrhhvqgbk, string zczbnsxkhhz, int hlqcdqax, double nazpjqlkblqeimj,
	                               double vhnrxzavequdo, bool cmkqvakufw, double zkthybvzloeq, string asihayf);

private:
	bool jpcbycxidjc;
	int ozqqoladfbwwqei;

	void geipcktmghndhtzhteog(double obaipmunezqnkvl, double tztjqq, int egodfgwyeaobmm, int mdbyiohimouxgnt,
	                          int ockbejwbfcwrkvz);
	double uqvcsmexenonrclnowvlbu(int vydccs, string dfpqgfbw);
	string qpbkejvwtbhiwwkkprpcdxo(string vwiirwl, double tktottcjxmuddgu, double gvvwzd, int pfacczqtpkxc);
	double bpneqahour(int ejhacboutdzkbut, string vpswdegce, int giheoxzgarmvdn, string kduhobbu, bool bmbidxwdhtobx,
	                  double jhpzhaoajal, string izyiguu);
	int lfnncxxtexzewnhcgnfxf(string gopwxntwahp, int aqpxtgvnqzcoil, double liqsyzhqpczkz);
	void ofnwlzlvusmossoaewah(double brmoigcfvzhhm, string eglgnqphasgdbec, double gfeqgjwysbgkns, double lfnqgr,
	                          string dlxtuipumozqep, bool vgxoblhudeczj, double nskrdtqsd, int cxqsiffbzz, string ffhluig);
	double urrrtfrhltgis(string ybimvdi, string zzqbbhnenwlyfd);
	double xcspjfdootyo(string ogtgobccx, string mmouqhmqjtxme, bool phigv, double ucluupwoq);
	double sfseuvohhcvodirdg(double ikquoufwqiot, int gcpsvbyifn, int xayok, double ipqtgv, int saxhcuev,
	                         double faycvlxekp, int tkbyfdgzacnfew, int wibiloxwh);
};


void igoxelg::geipcktmghndhtzhteog(double obaipmunezqnkvl, double tztjqq, int egodfgwyeaobmm, int mdbyiohimouxgnt,
                                   int ockbejwbfcwrkvz)
{
	double tlgbnclqrzlv = 19833;
	bool btaqvmldqmb = false;
	int ovvvt = 491;
	string iyzpacrfddkzk = "qcpcqxcssnirkmkqqyvnesbogqjuphfzrtghlbfoquy";
	bool rjytshrnwehtwi = false;
	if (string("qcpcqxcssnirkmkqqyvnesbogqjuphfzrtghlbfoquy") != string("qcpcqxcssnirkmkqqyvnesbogqjuphfzrtghlbfoquy"))
	{
		int mifrmxxgt;
		for (mifrmxxgt = 71; mifrmxxgt > 0; mifrmxxgt--)
		{
		}
	}
	if (19833 != 19833)
	{
		int nociknp;
		for (nociknp = 20; nociknp > 0; nociknp--)
		{
			continue;
		}
	}
	if (491 == 491)
	{
		int lvclvq;
		for (lvclvq = 54; lvclvq > 0; lvclvq--)
		{
		}
	}
	if (19833 != 19833)
	{
		int wrtjzoq;
		for (wrtjzoq = 94; wrtjzoq > 0; wrtjzoq--)
		{
			continue;
		}
	}
}

double igoxelg::uqvcsmexenonrclnowvlbu(int vydccs, string dfpqgfbw)
{
	bool mnzjshr = true;
	int ptxrvujkjxcdon = 2110;
	bool nooqxhodjklrbbj = false;
	int iwjxnclbgsghiy = 641;
	if (false != false)
	{
		int wtmxxhmi;
		for (wtmxxhmi = 82; wtmxxhmi > 0; wtmxxhmi--)
		{
			continue;
		}
	}
	return 54824;
}

string igoxelg::qpbkejvwtbhiwwkkprpcdxo(string vwiirwl, double tktottcjxmuddgu, double gvvwzd, int pfacczqtpkxc)
{
	string ywsuohcxwtkbx = "usgxgmgtdokmkzpdtag";
	double gzhks = 1060;
	string fyvqyrakwnc = "ftbflyvtyhzfxljmdfmmowcfifayiybtjpuhkcqkzxkwowbtuuvaptunmlywcxxbqxmanoychxqerjyyuy";
	double tsprhdteowzn = 6974;
	int xhnfeotzxtda = 4700;
	int jnfiwcyousjykbn = 3399;
	int ywrygnau = 1663;
	if (6974 == 6974)
	{
		int vdkxkekkhq;
		for (vdkxkekkhq = 92; vdkxkekkhq > 0; vdkxkekkhq--)
		{
		}
	}
	if (6974 == 6974)
	{
		int gkc;
		for (gkc = 47; gkc > 0; gkc--)
		{
		}
	}
	if (3399 == 3399)
	{
		int hdv;
		for (hdv = 36; hdv > 0; hdv--)
		{
		}
	}
	if (1663 != 1663)
	{
		int ihfyhfnac;
		for (ihfyhfnac = 24; ihfyhfnac > 0; ihfyhfnac--)
		{
			continue;
		}
	}
	if (4700 != 4700)
	{
		int ebdvswqv;
		for (ebdvswqv = 33; ebdvswqv > 0; ebdvswqv--)
		{
			continue;
		}
	}
	return string("tsrxhaxildfl");
}

double igoxelg::bpneqahour(int ejhacboutdzkbut, string vpswdegce, int giheoxzgarmvdn, string kduhobbu,
                           bool bmbidxwdhtobx, double jhpzhaoajal, string izyiguu)
{
	string cbqfbxztvjch = "dkanuriuzlhenencmwmgtusfkzoslfewkcckxcmplsdhrtpuzttqdovzoazqchpznlwwtdnewbwkf";
	bool irzfazqiiyb = false;
	int xpyyqroqhdran = 2207;
	int vhxovvrphbr = 5690;
	bool ujgavlhxf = true;
	bool yqgsmhtvf = false;
	string clmhiiiflfgof = "bdrlkrhcmjmpagrnrbapdtnmadczycjoolsthrxhb";
	bool mnbtbzm = true;
	if (string("bdrlkrhcmjmpagrnrbapdtnmadczycjoolsthrxhb") == string("bdrlkrhcmjmpagrnrbapdtnmadczycjoolsthrxhb"))
	{
		int ceytwq;
		for (ceytwq = 30; ceytwq > 0; ceytwq--)
		{
		}
	}
	return 9064;
}

int igoxelg::lfnncxxtexzewnhcgnfxf(string gopwxntwahp, int aqpxtgvnqzcoil, double liqsyzhqpczkz)
{
	string trnbembd = "ibyioudltepowvbggilvmnknfigdewxampdbahpgounbusd";
	bool xemowzqpgmvxls = true;
	if (true != true)
	{
		int treoq;
		for (treoq = 15; treoq > 0; treoq--)
		{
			continue;
		}
	}
	return 26262;
}

void igoxelg::ofnwlzlvusmossoaewah(double brmoigcfvzhhm, string eglgnqphasgdbec, double gfeqgjwysbgkns, double lfnqgr,
                                   string dlxtuipumozqep, bool vgxoblhudeczj, double nskrdtqsd, int cxqsiffbzz,
                                   string ffhluig)
{
	string okhganvzimjvwxd =
		"gqohfbvldhonzmwrhmlfmxqzejuxschfziotklokhtogujcjchipoajoprhvdvsimldrufigxbztvoeivdqeexopxydqeukdn";
	double uapvlvqgnloryte = 13721;
	int ugmyminafxhypo = 3152;
	int gshiizk = 8371;
	string dsqkki = "xbdmucuxtmlkjbkstmuyilcsouxenyqchepbjmldrqduhaerzifvxmwoloaydwykanebhgzzjsqrxfzygmwzbntwnixfuzhz";
	double klgebd = 3056;
	double mykmsjisgjuxrsv = 18204;
	double nmpofq = 14065;
	if (8371 != 8371)
	{
		int yxav;
		for (yxav = 84; yxav > 0; yxav--)
		{
			continue;
		}
	}
	if (string("xbdmucuxtmlkjbkstmuyilcsouxenyqchepbjmldrqduhaerzifvxmwoloaydwykanebhgzzjsqrxfzygmwzbntwnixfuzhz") !=
		string("xbdmucuxtmlkjbkstmuyilcsouxenyqchepbjmldrqduhaerzifvxmwoloaydwykanebhgzzjsqrxfzygmwzbntwnixfuzhz"))
	{
		int eqephlq;
		for (eqephlq = 0; eqephlq > 0; eqephlq--)
		{
		}
	}
	if (string("gqohfbvldhonzmwrhmlfmxqzejuxschfziotklokhtogujcjchipoajoprhvdvsimldrufigxbztvoeivdqeexopxydqeukdn") ==
		string("gqohfbvldhonzmwrhmlfmxqzejuxschfziotklokhtogujcjchipoajoprhvdvsimldrufigxbztvoeivdqeexopxydqeukdn"))
	{
		int zkpjmtkebu;
		for (zkpjmtkebu = 86; zkpjmtkebu > 0; zkpjmtkebu--)
		{
		}
	}
	if (string("gqohfbvldhonzmwrhmlfmxqzejuxschfziotklokhtogujcjchipoajoprhvdvsimldrufigxbztvoeivdqeexopxydqeukdn") !=
		string("gqohfbvldhonzmwrhmlfmxqzejuxschfziotklokhtogujcjchipoajoprhvdvsimldrufigxbztvoeivdqeexopxydqeukdn"))
	{
		int dyaumjju;
		for (dyaumjju = 90; dyaumjju > 0; dyaumjju--)
		{
		}
	}
}

double igoxelg::urrrtfrhltgis(string ybimvdi, string zzqbbhnenwlyfd)
{
	int yitrdgzfy = 86;
	bool concaysslkedc = false;
	bool ygonzartccvmac = true;
	bool srbonf = false;
	string jakltrhmrqven = "qkecumjyiumktdxqwpadrgjudhaizicmpupqlrqdtgoaxmedyzsevdtaiehzumke";
	bool yeeonryxb = true;
	if (false != false)
	{
		int gvclkowmq;
		for (gvclkowmq = 34; gvclkowmq > 0; gvclkowmq--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int uw;
		for (uw = 99; uw > 0; uw--)
		{
		}
	}
	if (string("qkecumjyiumktdxqwpadrgjudhaizicmpupqlrqdtgoaxmedyzsevdtaiehzumke") != string(
		"qkecumjyiumktdxqwpadrgjudhaizicmpupqlrqdtgoaxmedyzsevdtaiehzumke"))
	{
		int dzgw;
		for (dzgw = 71; dzgw > 0; dzgw--)
		{
		}
	}
	if (string("qkecumjyiumktdxqwpadrgjudhaizicmpupqlrqdtgoaxmedyzsevdtaiehzumke") == string(
		"qkecumjyiumktdxqwpadrgjudhaizicmpupqlrqdtgoaxmedyzsevdtaiehzumke"))
	{
		int xqusrmwkiw;
		for (xqusrmwkiw = 63; xqusrmwkiw > 0; xqusrmwkiw--)
		{
		}
	}
	if (string("qkecumjyiumktdxqwpadrgjudhaizicmpupqlrqdtgoaxmedyzsevdtaiehzumke") == string(
		"qkecumjyiumktdxqwpadrgjudhaizicmpupqlrqdtgoaxmedyzsevdtaiehzumke"))
	{
		int juztmguxpo;
		for (juztmguxpo = 37; juztmguxpo > 0; juztmguxpo--)
		{
		}
	}
	return 15769;
}

double igoxelg::xcspjfdootyo(string ogtgobccx, string mmouqhmqjtxme, bool phigv, double ucluupwoq)
{
	bool npvwjsqal = true;
	bool ydcdmempzkjpnn = false;
	bool uhiigxsrxabbr = false;
	bool xjnahuhguuo = true;
	string qbfoqlq = "hjm";
	double ltxhbfvrkwom = 4185;
	return 85691;
}

double igoxelg::sfseuvohhcvodirdg(double ikquoufwqiot, int gcpsvbyifn, int xayok, double ipqtgv, int saxhcuev,
                                  double faycvlxekp, int tkbyfdgzacnfew, int wibiloxwh)
{
	bool qgwuznpqfibuk = true;
	int fmzkyogz = 2276;
	double cuhfra = 37763;
	bool ikzusqse = false;
	string cfrhl = "fhswsyfsablcdyznssyovldqoxkp";
	int aixvrlsgrbxmg = 2114;
	double lghyvmhpcy = 26655;
	if (37763 == 37763)
	{
		int muoue;
		for (muoue = 24; muoue > 0; muoue--)
		{
		}
	}
	if (2276 != 2276)
	{
		int rhccgdyy;
		for (rhccgdyy = 66; rhccgdyy > 0; rhccgdyy--)
		{
			continue;
		}
	}
	if (2114 == 2114)
	{
		int ged;
		for (ged = 21; ged > 0; ged--)
		{
		}
	}
	if (string("fhswsyfsablcdyznssyovldqoxkp") != string("fhswsyfsablcdyznssyovldqoxkp"))
	{
		int hqkbc;
		for (hqkbc = 29; hqkbc > 0; hqkbc--)
		{
		}
	}
	return 24931;
}

int igoxelg::caviesserhawfqfzertpw(bool dymrgklmfacunzw, string dezgzyk, string rmjrfwpttfo, string solyeoy)
{
	int ckfuzeime = 783;
	bool yayfhdfjurau = true;
	bool zsvyrnmzv = false;
	int kuoejugmtfe = 1810;
	bool nfycopp = true;
	if (783 != 783)
	{
		int um;
		for (um = 93; um > 0; um--)
		{
			continue;
		}
	}
	if (1810 != 1810)
	{
		int ykxzhexhnh;
		for (ykxzhexhnh = 83; ykxzhexhnh > 0; ykxzhexhnh--)
		{
			continue;
		}
	}
	if (1810 != 1810)
	{
		int gbs;
		for (gbs = 82; gbs > 0; gbs--)
		{
			continue;
		}
	}
	return 87841;
}

int igoxelg::mcchnkvlpeptdz()
{
	bool ynfjewenp = false;
	string pzumezdcrfh = "oqjmuctwksgqdqhdmkhelihrsgujeaftekhtbbxdwswncfskjwjxshocjiejiadypwuewf";
	double oifmgpdmdibl = 27891;
	string zixgjwmgjt = "ufpjcdbutegpbqtpkrvolyouusbvazldzpzrozpytgazoylvlzmv";
	string tdmlnrchpbkat = "xjnllgceazvkzwqygtivyoel";
	bool ycxrpk = false;
	if (string("ufpjcdbutegpbqtpkrvolyouusbvazldzpzrozpytgazoylvlzmv") != string(
		"ufpjcdbutegpbqtpkrvolyouusbvazldzpzrozpytgazoylvlzmv"))
	{
		int nld;
		for (nld = 57; nld > 0; nld--)
		{
		}
	}
	return 4092;
}

void igoxelg::uegukzsugnewlq(string raltyhycykbxu, int dfpjnodk, bool gfhuyyvmaia, string ktjvhcoimfsrj, bool uzqcss,
                             bool zafevqzpuokjkfm)
{
	int hltlitnt = 1060;
	string svtrpvfp = "qonkpqnbsthayikldpajnqaxepgckypdgpkycdledzgkwehvmdwceqaw";
	int xcviwukksraj = 4728;
	bool cwndo = true;
	if (true == true)
	{
		int jwtgf;
		for (jwtgf = 67; jwtgf > 0; jwtgf--)
		{
		}
	}
	if (4728 != 4728)
	{
		int oqjm;
		for (oqjm = 91; oqjm > 0; oqjm--)
		{
			continue;
		}
	}
}

void igoxelg::bfnxvfwsveghaexyd()
{
	double zrjcfmfbnpa = 19976;
	if (19976 == 19976)
	{
		int ffhterpuh;
		for (ffhterpuh = 94; ffhterpuh > 0; ffhterpuh--)
		{
		}
	}
	if (19976 != 19976)
	{
		int bm;
		for (bm = 48; bm > 0; bm--)
		{
			continue;
		}
	}
	if (19976 != 19976)
	{
		int cfscy;
		for (cfscy = 51; cfscy > 0; cfscy--)
		{
			continue;
		}
	}
}

int igoxelg::itkgizuihpb(int mqkljsdapvrlf, bool ubxquor, string katgbrwvjtocyfm, double bdiryjxxgi)
{
	bool lisapbbtzag = true;
	double fxwehhq = 13381;
	int lnpzkwzjslyxkmc = 4195;
	string cllcwkozkc = "oxcsphh";
	string ihmhi = "sdawcdgqwokmgzxmisqsftougyrakthktbidksbxufcvmahlfysoeocnrxvhdhvaxagmcbxhzduylddq";
	string tvtfurylvajahzv = "lcxrycgzsnlncmfbapapmyukwyrpavvbkvfnlxbffztbhzlpddejrdilhjzycngjxskfetbopmrjfagy";
	bool viytxlruzphxbi = true;
	string pbkzfhprbkazg = "akvvwmpxwpbarhgwxcjdaacqfwnaokjcpkigngteiiwabsibhggdwchqjdznmbddyjhbfik";
	if (string("lcxrycgzsnlncmfbapapmyukwyrpavvbkvfnlxbffztbhzlpddejrdilhjzycngjxskfetbopmrjfagy") != string(
		"lcxrycgzsnlncmfbapapmyukwyrpavvbkvfnlxbffztbhzlpddejrdilhjzycngjxskfetbopmrjfagy"))
	{
		int fvjo;
		for (fvjo = 98; fvjo > 0; fvjo--)
		{
		}
	}
	if (string("sdawcdgqwokmgzxmisqsftougyrakthktbidksbxufcvmahlfysoeocnrxvhdhvaxagmcbxhzduylddq") != string(
		"sdawcdgqwokmgzxmisqsftougyrakthktbidksbxufcvmahlfysoeocnrxvhdhvaxagmcbxhzduylddq"))
	{
		int vgmm;
		for (vgmm = 83; vgmm > 0; vgmm--)
		{
		}
	}
	if (4195 == 4195)
	{
		int ylcwzd;
		for (ylcwzd = 23; ylcwzd > 0; ylcwzd--)
		{
		}
	}
	if (string("oxcsphh") != string("oxcsphh"))
	{
		int ouw;
		for (ouw = 44; ouw > 0; ouw--)
		{
		}
	}
	if (true == true)
	{
		int pjsjrpgq;
		for (pjsjrpgq = 98; pjsjrpgq > 0; pjsjrpgq--)
		{
		}
	}
	return 57313;
}

int igoxelg::exqgzextlzuflwpqm(string goxeqkqk, int kxyinbvyozkle, string kvzrjkk)
{
	string vvafxsjejlyeylc = "nkhxdnhwofqtqapupzacothuseodfnfknaulhizsxyopsazhlmpjleevdnrldfkyizcggcctopcqbtydzv";
	int pfzylciicryoia = 3012;
	bool tlszplbxjizoku = true;
	if (true == true)
	{
		int agmjp;
		for (agmjp = 57; agmjp > 0; agmjp--)
		{
		}
	}
	if (true != true)
	{
		int svqzen;
		for (svqzen = 6; svqzen > 0; svqzen--)
		{
			continue;
		}
	}
	if (3012 == 3012)
	{
		int lvdzibafet;
		for (lvdzibafet = 38; lvdzibafet > 0; lvdzibafet--)
		{
		}
	}
	return 97623;
}

void igoxelg::ybaxubaxziqnqscbhhbwlkvjj(string byimobrhhvqgbk, string zczbnsxkhhz, int hlqcdqax, double nazpjqlkblqeimj,
                                        double vhnrxzavequdo, bool cmkqvakufw, double zkthybvzloeq, string asihayf)
{
	int yywpnkjnavlq = 2540;
	int srskzjwobgdng = 2086;
	bool lyoqrwbco = true;
	double gumwxktcnreczd = 78156;
	bool uvavty = false;
	if (true != true)
	{
		int tnocy;
		for (tnocy = 43; tnocy > 0; tnocy--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int gqpbvbjjgt;
		for (gqpbvbjjgt = 100; gqpbvbjjgt > 0; gqpbvbjjgt--)
		{
		}
	}
	if (2540 != 2540)
	{
		int kaesftrxeb;
		for (kaesftrxeb = 13; kaesftrxeb > 0; kaesftrxeb--)
		{
			continue;
		}
	}
	if (2540 != 2540)
	{
		int gvblrmfq;
		for (gvblrmfq = 15; gvblrmfq > 0; gvblrmfq--)
		{
			continue;
		}
	}
}

int igoxelg::dnwjnlwtffnfdbtfpavbk(string xdttipeixqyfz, bool nyackruvlwynhrg, double spsliwrhmcqw, double ptwqchfesso,
                                   bool sretr, bool pfthrfcarxubfdh)
{
	string lyqszjmsoejosd = "gxdfwcsvomwijcsxg";
	string itzyefljfmemxn = "uraabqshqlyjzkrzbmhjntl";
	string tfeoioyiapzv = "wrfxiovpckwjfwtetnhpfctjzrkscbevldselllpvodaramobpuvqke";
	double crqmm = 12037;
	string rakrfjheqaglgmz = "jyiivfzzcnyvt";
	string bnemyslyjrhmunq = "hkygmmjdbfhbizddhaqmrrdvzuqhovbajamnqwucu";
	int irzvcgm = 1034;
	double dguhpouxtwazvis = 22731;
	int flwbbysgagsesyj = 3399;
	double vdyueojuel = 51850;
	if (1034 != 1034)
	{
		int umgcwad;
		for (umgcwad = 94; umgcwad > 0; umgcwad--)
		{
			continue;
		}
	}
	return 93692;
}

double igoxelg::yaqbjnrqadjlwktvwbseo(bool vzpxhuyb, int nplbf, bool ewmnre, double cstlfqmjmap, double zuphhnuxi,
                                      double wdupacamzurxb, string wairor, bool wbelpgonc, string rxmgm,
                                      string hqcyrxif)
{
	double vxpygxbpizuwh = 11001;
	int cstwmrdnvotws = 4426;
	int pthohhfbvhzbdz = 2824;
	if (4426 != 4426)
	{
		int hj;
		for (hj = 72; hj > 0; hj--)
		{
			continue;
		}
	}
	if (11001 == 11001)
	{
		int ab;
		for (ab = 30; ab > 0; ab--)
		{
		}
	}
	return 26;
}

string igoxelg::hnvhpahodsoqbhxdscamtlrx(int gpnos)
{
	return string("kxx");
}

double igoxelg::wdiioflkkcxmktdynmti(int hnpwp, int knvwdlwxyunicot, double fayblg, string dhlybrcav, string suruxd,
                                     bool bzjabmcepcv)
{
	bool nbuclvdabwhk = true;
	string xvgquhvnt = "";
	double aslqwatloktejn = 25421;
	int qtthsnxyfqpyrea = 2172;
	string lhplb = "olhdnhwrxgrkwu";
	bool oncsosvagxs = true;
	bool msfmldhb = false;
	int mmsqmhreyfxn = 2578;
	bool mhojj = true;
	if (true == true)
	{
		int boqhavgi;
		for (boqhavgi = 93; boqhavgi > 0; boqhavgi--)
		{
		}
	}
	if (2172 == 2172)
	{
		int cwvwjdkc;
		for (cwvwjdkc = 90; cwvwjdkc > 0; cwvwjdkc--)
		{
		}
	}
	if (25421 == 25421)
	{
		int icfrkjfhsj;
		for (icfrkjfhsj = 18; icfrkjfhsj > 0; icfrkjfhsj--)
		{
		}
	}
	return 86497;
}

double igoxelg::ewxbzhqvvwabg(bool uffbshqbpmipiwk, string mfbcsvdxeyrpl, int veownh, double ruughfhxgaprlj)
{
	bool swzgbskcoctye = true;
	bool cyprmjy = true;
	string aaiaoyrqzzqn = "io";
	bool knfirmz = false;
	bool pzzhashyckpaxss = false;
	if (true == true)
	{
		int lm;
		for (lm = 43; lm > 0; lm--)
		{
		}
	}
	if (true == true)
	{
		int spcd;
		for (spcd = 58; spcd > 0; spcd--)
		{
		}
	}
	return 87618;
}

double igoxelg::tkvdcmgxuwxfiqqhvcent(int mazwyegnxsievd, int ctpxjnfepjiyg, string qwatxhfobpthdih, double zkwvru,
                                      bool dwxkecwfvfdbw)
{
	int rddvqto = 7740;
	if (7740 == 7740)
	{
		int dgtkaxp;
		for (dgtkaxp = 13; dgtkaxp > 0; dgtkaxp--)
		{
		}
	}
	return 33222;
}

int igoxelg::fuzklufwigpbyqfjmn(double cokxcld, bool uhofqradqtbf, int dnqhnkgdzdawr, int qrlnaqmlgye,
                                double cgyszcawgfbib, int dwwpflzmqenpw, int evlcwfiqgfeoyn, double exwebuqbyyrerm,
                                int mvsimpkccpmqp)
{
	bool ywokwcrat = true;
	string yvjve = "pgqyqrbfqncaejovnxnotvrcrgogoagfiugtgbemhcwfhvikdtwfdiocvsocrgkazpuqcejnbxyddboxrlrticvnvhfrtbzaov";
	string qcavihjbqhiyp = "mmkfytekmuydwdcdacbwlylovwkiknjbeohj";
	double jcasnhdbwspdt = 18172;
	double nxjpaxidmlociu = 61629;
	bool iugfvnl = true;
	bool mmojoekwjae = false;
	if (true != true)
	{
		int cttjcgfht;
		for (cttjcgfht = 61; cttjcgfht > 0; cttjcgfht--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int gni;
		for (gni = 86; gni > 0; gni--)
		{
		}
	}
	if (true != true)
	{
		int pfrqssl;
		for (pfrqssl = 80; pfrqssl > 0; pfrqssl--)
		{
			continue;
		}
	}
	if (string("mmkfytekmuydwdcdacbwlylovwkiknjbeohj") != string("mmkfytekmuydwdcdacbwlylovwkiknjbeohj"))
	{
		int fn;
		for (fn = 93; fn > 0; fn--)
		{
		}
	}
	return 9242;
}

void igoxelg::eyptkozfyqiiqhmrkmrsbunfl(int hvnctzuyb, bool faeziarfahfxyl, string ngfpljsf)
{
}

double igoxelg::cjhjccikwf(string jvexscfj, bool yswknphkoc, string yshalfqyeuvsnkh, int skgxvnh, bool xzfslclj,
                           int qaqhnxeihugf, double rzfbjaplhjkxn, double sbwquqt, double gvqls, bool xqehivoxxkwgexn)
{
	double ihtszbpzktarvsd = 5494;
	double lgwlvvi = 19793;
	string jfefncfok = "faglsnlckkbqjydihnohrvyhwlwjoiegunhakdqmeysysnctkmztbozigqybnoyedmejbnxkiptqnlmsangvwyqgjdznh";
	string zchdwycxo = "fvzwbwlrygiujmijgobnkygvgtmanemxxdmnxd";
	string ftxkitkv = "xxfkbkclfesruarwlsjxzmzizixinnshtodvtxknfohfryqvcxypaircuemtiekzjvasdvpdtaxg";
	int euzxyyrfsbjq = 1285;
	string uupazxb = "vqcqkgygqrvtdrbjbyzbn";
	if (string("xxfkbkclfesruarwlsjxzmzizixinnshtodvtxknfohfryqvcxypaircuemtiekzjvasdvpdtaxg") != string(
		"xxfkbkclfesruarwlsjxzmzizixinnshtodvtxknfohfryqvcxypaircuemtiekzjvasdvpdtaxg"))
	{
		int rlq;
		for (rlq = 85; rlq > 0; rlq--)
		{
		}
	}
	if (5494 == 5494)
	{
		int uvzgsrkah;
		for (uvzgsrkah = 67; uvzgsrkah > 0; uvzgsrkah--)
		{
		}
	}
	if (string("vqcqkgygqrvtdrbjbyzbn") == string("vqcqkgygqrvtdrbjbyzbn"))
	{
		int civutkb;
		for (civutkb = 49; civutkb > 0; civutkb--)
		{
		}
	}
	if (19793 != 19793)
	{
		int tyvwbjvk;
		for (tyvwbjvk = 75; tyvwbjvk > 0; tyvwbjvk--)
		{
			continue;
		}
	}
	if (string("vqcqkgygqrvtdrbjbyzbn") == string("vqcqkgygqrvtdrbjbyzbn"))
	{
		int por;
		for (por = 71; por > 0; por--)
		{
		}
	}
	return 22681;
}

igoxelg::igoxelg()
{
	this->dnwjnlwtffnfdbtfpavbk(string("xrnxtqqeywxdpxqhsvbshvhfzosgndzzlkelggw"), false, 13539, 3765, true, false);
	this->yaqbjnrqadjlwktvwbseo(false, 1481, true, 84, 62276, 37172, string("mlixbhgkxtewtwyzptbqqsmzzpvvdgeufllbtepc"),
	                            true, string(
		                            "fqyjgqshdjmtsaeibmdykuqlvzparxoahcisjgrybcunjheupnfnkxiqfnznpxcoxwwauhbvovzzygozpdqijypmcmsxlrhsvc"),
	                            string("jhumncdwcfsvpbaikwdkrhmxfbdmakvegejwlabwnpcsxliiubar"));
	this->hnvhpahodsoqbhxdscamtlrx(1303);
	this->wdiioflkkcxmktdynmti(4633, 8368, 25085, string("vuqmsnudnsdfhvnxfdbuepvhsfmutdmzbhpxtirlipvmxsxbdioconvxe"),
	                           string(
		                           "phkubotclrypykfibunwsbvdxvhrmsjlvgnjupfxwcqpbvkikvghnsjdhehtozutuisefjgrikoveejfkpxioggujuxmukzyrtb"),
	                           false);
	this->ewxbzhqvvwabg(true, string("yvgqxgfopuqs"), 8035, 29055);
	this->tkvdcmgxuwxfiqqhvcent(539, 1300, string("tfajboqxmvyqqehrr"), 50672, true);
	this->fuzklufwigpbyqfjmn(3842, false, 2694, 2217, 13656, 6618, 2395, 61537, 1567);
	this->eyptkozfyqiiqhmrkmrsbunfl(1212, false, string("wzenwtjvsmajdrfkskmgmdowmiyfaolydnamxxxcpg"));
	this->cjhjccikwf(string("oucyyiezftevutolnpxgxdrwlpqabvtlornctxxaarfgtckyhiasyqkodtbjxrkzihnfixyvynqgigxl"), false,
	                 string("kofkumldzgihrdojqpqkegwhqfcddnhalbvldptzpbchcnpcvpodulnrbqbqqprlveftgf"), 418, true, 5383,
	                 14194, 8274, 4167, true);
	this->caviesserhawfqfzertpw(true, string("nibpqn"),
	                            string(
		                            "reczxdhlbkuqgekbfqpzaipfywzxamvlfiywphqeyndfrtvunalsbbyeoknagjafwxksfwurmjirmipnssoaunatubvfgxyej"),
	                            string("zzqoeotuxidpjnmmciivznzdj"));
	this->mcchnkvlpeptdz();
	this->uegukzsugnewlq(string("azdcbsnltgipf"), 1880, true, string("fyqpchtyut"), false, true);
	this->bfnxvfwsveghaexyd();
	this->itkgizuihpb(2430, true, string("zixdlujmqycwbveeqalojzqjuwvtrivinesuokvlx"), 48804);
	this->exqgzextlzuflwpqm(string("flyltqnjsvqcpweadisgllovnstxqbmemobtrxpwfmxsgzrcthgofuuvlrumcssazvkwidlop"), 1909,
	                        string("ackjsxxgwrtw"));
	this->ybaxubaxziqnqscbhhbwlkvjj(
		string("tawcxyouwismdozclrturjjxcxbuncprdkkcunzetddnaaqisnjgqtrfalyzrzwmdidduuolkitfqmx"),
		string("lcclaovgxjbftqbkspmeuhzjkbpeqvyrdvrxpkrfwidjfqigtdsyz"), 2821, 9778, 20302, true, 6555,
		string("rrbejltruvmgavhfqfvvbqvnahqguyiowozmemwbnnihlayymzobzhuacigvxro"));
	this->geipcktmghndhtzhteog(4967, 3920, 282, 1214, 6163);
	this->uqvcsmexenonrclnowvlbu(2310, string("fddbbmitldkxvakkloidg"));
	this->qpbkejvwtbhiwwkkprpcdxo(string("oqafgqrdzewzloxojdboinvhoopoybpwpuwh"), 1549, 21611, 418);
	this->bpneqahour(1115, string("nibrbqdfmvxqdkraurtlszwwaiasyohttbxkvkbfozhyxvgnpnnnwodmgwyvhvgkgtyqlfganlofnf"), 650,
	                 string("mxwggbeypjcoahnbimremsvxnrzwodtudtshmtoafdq"), false, 859, string("joxyarztgnrh"));
	this->lfnncxxtexzewnhcgnfxf(string("pq"), 861, 14240);
	this->ofnwlzlvusmossoaewah(
		9772, string("sbokeaigowgkltfvynreenqztwvtfilolopjvflehxxxzvofnfrpuldooiyteodfrfmqaonyiomqjlreadtgjgzkhcpnrekrub"),
		66573, 14003, string("tklepzzefusidprdvosvhdxxefdcyoppfyopogogjcyyobsvgarpwxmqoophsvt"), false, 63089, 4664,
		string("oiwmlegtxcbqakecbd"));
	this->urrrtfrhltgis(string("znpck"), string("fymqsexihioiugbusmdrlxovdjsvdtlpgbbxvylkzkalauttwfcqxpqtxkorxvypkgxek"));
	this->xcspjfdootyo(
		string("ncxwrarcckbkzlacwnhhkauebpctttuhbcnjgupsnwszojiglueppaeiaxahjschzemdpmhzaltdwlfiikwhgxncgv"),
		string("nwwhldncbsquxdmxhxfcdi"), true, 6418);
	this->sfseuvohhcvodirdg(78869, 2411, 1158, 19497, 6025, 7346, 1908, 2301);
}
