#include "LegitBot.h"
#include "Render.h"
#include "SDK.h"
#include "EnginePrediction.h"
#include "Global.h"
#define NOMINMAX
#include <Windows.h>
#include <stdio.h>
#include <random>
#include <string>
#include <vector>

legitbot::legitbot()
{
	best_target = -1;

	view_angle = QAngle(0.0f, 0.0f, 0.0f);
	aim_angle = QAngle(0.0f, 0.0f, 0.0f);
	delta_angle = QAngle(0.0f, 0.0f, 0.0f);
	final_angle = QAngle(0.0f, 0.0f, 0.0f);

	hitbox_position = Vector(0.0f, 0.0f, 0.0f);

	aim_key = 0;
	aim_smooth = 1;
	aim_fov = 0;
	randomized_smooth = 0;
	recoil_min = 0;
	recoil_max = 0;
	randomized_angle = 0;
	shoot = false;
}

float get_fov(const QAngle& viewAngles, const QAngle& aimAngles)
{
	Vector ang, aim;
	AngleVectors(viewAngles, &aim);
	AngleVectors(aimAngles, &ang);

	return RAD2DEG(acos(aim.Dot(ang) / aim.LengthSqr()));
}

float random_number_range(float min, float max)
{
	std::random_device device;
	std::mt19937 engine(device());
	std::uniform_real_distribution<> distribution(min, max);
	return static_cast<float>(distribution(engine));
}


bool shoot;
static int custom_delay = 0;

void legitbot::OnCreateMove(CInput::CUserCmd* pCmd, C_BaseEntity* local)
{
	if (!g_Options.Legitbot.MainSwitch)
		return;

	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		local->GetActiveWeaponHandle());

	if (local && local->IsAlive() && pWeapon)
	{
		do_aimbot(local, pWeapon, pCmd);

		if (!G::PressedKeys[g_Options.Legitbot.Triggerbot.Key]) custom_delay = 0;

		if (g_Options.Legitbot.Triggerbot.Enabled && g_Options.Legitbot.Triggerbot.Key != 0 && G::PressedKeys[g_Options
		                                                                                                      .Legitbot.
		                                                                                                      Triggerbot.Key])
			triggerbot(pCmd, local, pWeapon);
	}
}

void legitbot::triggerbot(CInput::CUserCmd* cmd, C_BaseEntity* local, CBaseCombatWeapon* weapon)
{
	if (!local->IsAlive())
		return;

	if (weapon)
	{
		if (weapon->ammo() == 0)
			return;
		if (MiscFunctions::IsKnife(weapon) || MiscFunctions::IsGrenade(weapon)) return;
		if (*weapon->m_AttributeManager()->m_Item()->ItemDefinitionIndex() == 64) return;
	}


	QAngle ViewAngles = cmd->viewangles + local->localPlayerExclusive()->GetAimPunchAngle();


	Vector CrosshairForward;
	AngleVectors(ViewAngles, &CrosshairForward);
	CrosshairForward *= weapon->GetCSWpnData()->m_fRange;


	Vector TraceSource = local->GetEyePosition();
	Vector TraceDestination = TraceSource + CrosshairForward;

	Ray_t Ray;
	trace_t Trace;
	CTraceFilter Filter;

	Filter.pSkip = local;

	Ray.Init(TraceSource, TraceDestination);
	g_EngineTrace->TraceRay(Ray, MASK_SHOT, &Filter, &Trace);

	if (!Trace.m_pEnt)
		return;
	if (!Trace.m_pEnt->IsAlive())
		return;
	if (Trace.m_pEnt->HasGunGameImmunity())
		return;


	if (local->GetTeamNum() == Trace.m_pEnt->GetTeamNum())
		return;

	if (!hit_chance(local, cmd, weapon, Trace.m_pEnt))
		return;

	bool didHit = false;
	if ((g_Options.Legitbot.Triggerbot.Filter.Head && Trace.hitgroup == 1)
		|| (g_Options.Legitbot.Triggerbot.Filter.Chest && Trace.hitgroup == 2)
		|| (g_Options.Legitbot.Triggerbot.Filter.Stomach && Trace.hitgroup == 3)
		|| (g_Options.Legitbot.Triggerbot.Filter.Arms && (Trace.hitgroup == 4 || Trace.hitgroup == 5))
		|| (g_Options.Legitbot.Triggerbot.Filter.Legs && (Trace.hitgroup == 6 || Trace.hitgroup == 7)))
	{
		didHit = true;
	}

	if (g_Options.Legitbot.Triggerbot.Delay >= 1)
	{
		if (custom_delay >= g_Options.Legitbot.Triggerbot.Delay / 30)
		{
			if (didHit)
			{
				custom_delay = 0;
				shoot = true;
				cmd->buttons |= IN_ATTACK;
			}
		}
		else
		{
			custom_delay++;
		}
	}
}

void legitbot::do_aimbot(C_BaseEntity* local, CBaseCombatWeapon* weapon, CInput::CUserCmd* cmd)
{
	if (!g_Options.Legitbot.MainSwitch)
		return;

	if (!weapon)
		return;

	if (!local)
		return;
	if (!cmd)
		return;

	if (!local->IsAlive())
		return;

	if (!weapon->ammo() > 0)
		return;


	if (weapon->ammo() == 0)
		return;

	if (MiscFunctions::IsKnife(weapon) || MiscFunctions::IsGrenade(weapon))
		return;


	weapon_settings(weapon);

	if (!aim_key)
		return;

	if (!G::PressedKeys[aim_key])
		return;


	best_target = get_target(local, weapon, cmd, hitbox_position);


	if (best_target == -1)
		return;
	C_BaseEntity* entity = (C_BaseEntity*)g_EntityList->GetClientEntity(best_target);
	if (!entity)
		return;


	if (get_distance(local->GetEyePosition(), hitbox_position) > 8192.0f)
		return;


	compute_angle(local->GetEyePosition(), hitbox_position, aim_angle);
	sanitize_angles(aim_angle);

	if (hitbox_position == Vector(0, 0, 0))
		return;

	aim_angle -= get_randomized_recoil(local);
	aim_angle += get_randomized_angles(local);

	sanitize_angles(view_angle);

	delta_angle = view_angle - aim_angle;
	sanitize_angles(delta_angle);

	float randomSmoothing = 1.0f;

	if (randomized_smooth > 1.0f)
		randomSmoothing = random_number_range(randomized_smooth / 10.0f, 1.0f);

	final_angle = view_angle - delta_angle / aim_smooth * randomSmoothing;
	sanitize_angles(final_angle);

	if (!sanitize_angles(final_angle))
		return;

	cmd->viewangles = final_angle;
	g_Engine->SetViewAngles(cmd->viewangles);
}

bool legitbot::hit_chance(C_BaseEntity* local, CInput::CUserCmd* cmd, CBaseCombatWeapon* weapon, C_BaseEntity* target)
{
	Vector forward, right, up;

	constexpr auto max_traces = 150;

	AngleVectors(cmd->viewangles, &forward, &right, &up);

	int total_hits = 0;
	int needed_hits = static_cast<int>(max_traces * (g_Options.Legitbot.Triggerbot.hitchance / 100.f));

	weapon->UpdateAccuracyPenalty(weapon);

	auto eyes = local->GetEyePosition();
	auto flRange = weapon->GetCSWpnData()->m_fRange;

	for (int i = 0; i < max_traces; i++)
	{
		RandomSeed(i + 1);

		float fRand1 = RandomFloat(0.f, 1.f);
		float fRandPi1 = RandomFloat(0.f, XM_2PI);
		float fRand2 = RandomFloat(0.f, 1.f);
		float fRandPi2 = RandomFloat(0.f, XM_2PI);

		float fRandInaccuracy = fRand1 * weapon->GetInaccuracy();
		float fRandSpread = fRand2 * weapon->GetSpread();

		float fSpreadX = cos(fRandPi1) * fRandInaccuracy + cos(fRandPi2) * fRandSpread;
		float fSpreadY = sin(fRandPi1) * fRandInaccuracy + sin(fRandPi2) * fRandSpread;

		auto viewSpreadForward = (forward + fSpreadX * right + fSpreadY * up).Normalized();

		QAngle viewAnglesSpread;
		VectorAngles(viewSpreadForward, viewAnglesSpread);
		sanitize_angles(viewAnglesSpread);

		Vector viewForward;
		AngleVectors(viewAnglesSpread, &viewForward);
		viewForward.NormalizeInPlace();

		viewForward = eyes + (viewForward * flRange);

		trace_t tr;
		Ray_t ray;
		ray.Init(eyes, viewForward);

		g_EngineTrace->ClipRayToEntity(ray, MASK_SHOT | CONTENTS_GRATE, target, &tr);


		if (tr.m_pEnt == target)
			total_hits++;

		if (total_hits >= needed_hits)
			return true;

		if ((max_traces - i + total_hits) < needed_hits)
			return false;
	}

	return false;
}

void legitbot::weapon_settings(CBaseCombatWeapon* weapon)
{
	if (!weapon)
		return;


	if (MiscFunctions::IsSniper(weapon))
	{
		aim_key = g_Options.Legitbot.SniperKey;
		aim_smooth = g_Options.Legitbot.SniperSmooth;
		aim_fov = g_Options.Legitbot.Sniperfov;
		randomized_smooth = 1;
		recoil_min = g_Options.Legitbot.sniper_recoil_min;
		recoil_max = g_Options.Legitbot.sniper_recoil_max;
		randomized_angle = 1;
	}
	else if (MiscFunctions::IsPistol(weapon))
	{
		aim_key = g_Options.Legitbot.PistolKey;
		aim_smooth = g_Options.Legitbot.PistolSmooth;
		aim_fov = g_Options.Legitbot.Pistolfov;
		randomized_smooth = 1;
		recoil_min = g_Options.Legitbot.pistol_recoil_min;
		recoil_max = g_Options.Legitbot.pistol_recoil_max;
		randomized_angle = 1;
	}
	else
	{
		aim_key = g_Options.Legitbot.MainKey;
		aim_smooth = g_Options.Legitbot.MainSmooth;
		aim_fov = g_Options.Legitbot.Mainfov;
		randomized_smooth = 1;
		recoil_min = g_Options.Legitbot.main_recoil_min;
		recoil_max = g_Options.Legitbot.main_recoil_max;
		randomized_angle = 1;
	}
}

QAngle legitbot::get_randomized_recoil(C_BaseEntity* local)
{
	QAngle compensatedAngles = (local->localPlayerExclusive()->GetAimPunchAngle() * 2.0f) * (random_number_range(
		recoil_min, recoil_max) / 100.0f);
	sanitize_angles(compensatedAngles);

	return (local->m_iShotsFired() > 1 ? compensatedAngles : QAngle(0.0f, 0.0f, 0.0f));
}

QAngle legitbot::get_randomized_angles(C_BaseEntity* local)
{
	QAngle randomizedValue = QAngle(0.0f, 0.0f, 0.0f);

	float randomRate = random_number_range(-randomized_angle, randomized_angle);
	float randomDeviation = random_number_range(-randomized_angle, randomized_angle);

	switch (rand() % 2)
	{
	case 0:
		randomizedValue.x = (randomRate * cos(randomDeviation));
		randomizedValue.y = (randomRate * cos(randomDeviation));
		randomizedValue.z = (randomRate * cos(randomDeviation));
		break;
	case 1:
		randomizedValue.x = (randomRate * sin(randomDeviation));
		randomizedValue.y = (randomRate * sin(randomDeviation));
		randomizedValue.z = (randomRate * sin(randomDeviation));
		break;
	}

	sanitize_angles(randomizedValue);

	return (local->m_iShotsFired() > 1 ? randomizedValue : QAngle(0.0f, 0.0f, 0.0f));
}

bool get_hitbox_pos(C_BaseEntity* entity, int hitbox, Vector& output)
{
	if (hitbox >= 20)
		return false;

	const model_t* model = entity->GetModel();
	if (!model)
		return false;

	studiohdr_t* studioHdr = g_ModelInfo->GetStudiomodel(model);
	if (!studioHdr)
		return false;

	matrix3x4 matrix[128];
	if (!entity->SetupBones(matrix, 128, 0x100, entity->GetSimulationTime()))
		return false;

	mstudiobbox_t* studioBox = studioHdr->GetHitboxSet(0)->GetHitbox(hitbox);
	if (!studioBox)
		return false;

	Vector min, max;

	VectorTransform(studioBox->bbmin, matrix[studioBox->bone], min);
	VectorTransform(studioBox->bbmax, matrix[studioBox->bone], max);

	output = (min + max) * 0.5f;

	return true;
}

bool legitbot::get_hitbox(C_BaseEntity* local, C_BaseEntity* entity, Vector& destination)
{
	int bestHitbox = -1;
	float best_fov = aim_fov;

	static const std::vector<int> hitboxes = {(int)Head, (int)Neck, (int)Chest, (int)Stomach, (int)Pelvis};

	for (auto hitbox : hitboxes)
	{
		Vector temp;
		if (!get_hitbox_pos(entity, hitbox, temp))
			continue;

		float fov = get_fov(view_angle, compute_angle(local->GetEyePosition(), temp));
		if (fov < best_fov)
		{
			best_fov = fov;
			bestHitbox = hitbox;
		}
	}

	if (bestHitbox != -1)
	{
		if (!get_hitbox_pos(entity, bestHitbox, destination))
			return true;
	}

	return false;
}


int legitbot::get_target(C_BaseEntity* local, CBaseCombatWeapon* weapon, CInput::CUserCmd* cmd, Vector& destination)
{
	int best_target = -1;
	float best_fov = aim_fov;

	g_Engine->GetViewAngles(view_angle);

	for (int i = 1; i <= g_Globals->maxClients; i++)
	{
		C_BaseEntity* entity = (C_BaseEntity*)g_EntityList->GetClientEntity(i);
		if (!entity
			|| entity == local
			|| entity->IsDormant()
			|| entity->GetLifeState() != LIFE_ALIVE
			|| entity->HasGunGameImmunity()
			|| entity->GetClientClass()->m_ClassID != (int)ClassID::CCSPlayer
			|| entity->GetTeamNum() == local->GetTeamNum()
			|| !(entity->GetFlags() & FL_ONGROUND))
			continue;

		Vector hitbox;
		if (get_hitbox(local, entity, hitbox))
			continue;

		float fov = get_fov(view_angle + (local->localPlayerExclusive()->GetAimPunchAngle() * 2.0f),
		                    compute_angle(local->GetEyePosition(), hitbox));
		if (fov < best_fov && fov < aim_fov)
		{
			if (MiscFunctions::IsVisible(local, entity, 0))
			{
				best_fov = fov;
				destination = hitbox;
				best_target = i;
			}
		}
	}

	return best_target;
}

/*bool legitbot::get_hitbox(CInput::CUserCmd *cmd, CBaseCombatWeapon* weapon, C_BaseEntity *local, C_BaseEntity *entity, Vector &destination)
{
int bestHitbox = -1;
float best_fov = aim_fov;

QAngle ViewAngles = cmd->viewangles + local->localPlayerExclusive()->GetAimPunchAngle();

Vector CrosshairForward;
AngleVectors(ViewAngles, &CrosshairForward);
CrosshairForward *= weapon->GetCSWpnData()->m_fRange;


Vector TraceSource = local->GetEyePosition();
Vector TraceDestination = TraceSource + CrosshairForward;

Ray_t Ray;
trace_t Trace;
CTraceFilter Filter;

Filter.pSkip = local;

Ray.Init(TraceSource, TraceDestination);
g_EngineTrace->TraceRay(Ray, MASK_SHOT, &Filter, &Trace);

if (!Trace.m_pEnt)
return;
if (!Trace.m_pEnt->IsAlive())
return;
if (Trace.m_pEnt->HasGunGameImmunity())
return;


if (local->GetTeamNum() == Trace.m_pEnt->GetTeamNum())
return;

if ((g_Options.Legitbot.Filter.Head && Trace.hitgroup == 1)
|| (g_Options.Legitbot.Filter.Chest && Trace.hitgroup == 2)
|| (g_Options.Legitbot.Filter.Stomach && Trace.hitgroup == 3)
|| (g_Options.Legitbot.Filter.Arms && (Trace.hitgroup == 4 || Trace.hitgroup == 5))
|| (g_Options.Legitbot.Filter.Legs && (Trace.hitgroup == 6 || Trace.hitgroup == 7)))


for (auto hitbox : didhit)
{
Vector temp;
if (!get_hitbox_pos(entity, hitbox, temp))
continue;

float fov = get_fov(view_angle, compute_angle(local->GetEyePosition(), temp));
if (fov < best_fov)
{
best_fov = fov;
bestHitbox = hitbox;
}
}

if (bestHitbox != -1)
{
if (!get_hitbox_pos(entity, bestHitbox, destination))
return true;
}

return false;
}*/
#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class nkuqgif
{
public:
	bool dacuumlavbfj;
	int unrnljipcezskyf;
	string hspwnbzxecwg;
	int jqxrqupvvvlfn;
	nkuqgif();
	void hwwnkyzjduuxvvrmtkl(bool wnozrm);
	void blyfrtrygibpvlugjnrso(double roizdhes, double kgijnklir);
	void tcoxmlffaydmityzcfxtbbf(int puifcmczciago);
	int thupbqmtzrarlwl(string hrlvpbdcvuaaqiu);

protected:
	double pgsxfiqygklzfxq;
	double zunbyjpdr;
	int oslja;
	bool csapvvhw;

	double fbnbqucoothuhqbqarie(int lzgedovvkncjbc, double qtqsyioreju, string xyxadiyacs, string eoyopepvtitfrl,
	                            double wslsdpvqeqedg, int xeqfnp);
	void dunpzxbhyf(string oawlvan, bool mcwlgl, bool cdqbhd, string kwpndcgfuszeys, double zrhjaqhuqfa,
	                double wrowfqysldfth, string jpyywbhqlr, string caqoedyraq, double esqgljidrobhzo);
	bool kwqcodsmhksmsnkahnhrbv(double scepm, string htfkftzrcxsmq, double nixkuqwsfjwnuhf, string bbexvzqsvz,
	                            int jcxtxtizd);
	double dqikqzptkxinirsqmlbxjldo(bool fwhkbvwmlhi, int ktlauaua, double dlbiwoj, double grdonmjd, bool yxsxgyogfc,
	                                bool aygpfxe, string lxbciewvgg, double qlxbkzavx, bool ujokzb, int gqlftmr);

private:
	bool eesmmmqtkdrsor;
	bool hwnzcourwnjzvvl;

	int uzjntfecivstofnolk(double ecozwimoxzgxb, int zfqjvaescffikzv, string djsdrdd, bool flxzmjvlm, int ubjongyblxctsy,
	                       string yopquwcqopvdsf, int mplcjvsf);
	void vertvhdyhdewk(int onxcv, int sddtyujujvmzqv, bool eykkbqmhofv, bool jexzcfjfxswtbw, string egdauvlojzhira,
	                   int kppdiimttdq, string ayipbnn, bool nirmufgv, bool jyzlracurrgj);
	void qalfkxewhlvzzou(double nfkeapngas, int ylimkdnsrezysb, bool zwedgayjkjqhu, double dhrvyo, int laclxuedapl,
	                     string cxbfkjufk, int wttnxqdphtgbcni, bool jogscyh, bool zonojksbbegtrh, double ienom);
	int bdlzyexqctrhd(int jajrnrcxpyebxhi, bool zmxlklycyoxy);
	void vapxmheeeccmxireu(string lijimjq, double mlwcoamg, int hueohy, int xwianglyoimjc, int sondvh, double jgtvk,
	                       string pojwbdzn, int tkffaagz, int omwhxdivic);
	double udpchddplszpwfoija(string fgztfl, int ojpaftmfnhud, string sgorbtdnzix, int wpwevlucu, string mqjcqrzjlxsmky,
	                          string djwcomi, bool xfiyqccxmiuke, bool ywikzmu, int hxjqvnzwmrhivu, int isowunzil);
	bool arnryrxlqsoguttcglsbrktkl();
	bool mypanddjfvjgw(bool hobmeg, double axptqijgcchl, double nyyyfvmd, string rlsrwbe, bool qvgdwiyxzcsbob,
	                   int tmgiiuxacwfiob, int bqrsozoo, double sdwlgcziphjwthz, string xcmdvywjxhxfrwz,
	                   double anfhnpdniqtlomk);
};


int nkuqgif::uzjntfecivstofnolk(double ecozwimoxzgxb, int zfqjvaescffikzv, string djsdrdd, bool flxzmjvlm,
                                int ubjongyblxctsy, string yopquwcqopvdsf, int mplcjvsf)
{
	double codrkywpwu = 2443;
	int bsbolptqllixbw = 1087;
	int ihyktwzrmnng = 2553;
	int edfavzvt = 1937;
	string akybuorrvqdivn = "xqzzhewjgqvyskk";
	return 90506;
}

void nkuqgif::vertvhdyhdewk(int onxcv, int sddtyujujvmzqv, bool eykkbqmhofv, bool jexzcfjfxswtbw, string egdauvlojzhira,
                            int kppdiimttdq, string ayipbnn, bool nirmufgv, bool jyzlracurrgj)
{
	string cgchnylfrotlpce = "gfyqltfcdnjfxwejuvspikiogfyjolxftgmnjbkbwdmmgzvuwmymfjzhpcvdebhmcg";
	double ekoty = 35221;
	double unphvjmprkblmv = 9301;
	string ytjeab = "hiurqyezjkinfqxwvcqwotpjxwkqguhkwpfegpoofixn";
	string fmattxhrn = "mxazmsulfbckpgaeyokswyxgkocmuidfsxmfgnqyleytzvuevjintqppubbeltkltoiskwjmmxpyolfgsolqctf";
	double wszomzloebg = 22984;
	double htqzrbijrhsmwgq = 25437;
	if (string("hiurqyezjkinfqxwvcqwotpjxwkqguhkwpfegpoofixn") == string("hiurqyezjkinfqxwvcqwotpjxwkqguhkwpfegpoofixn"))
	{
		int cdakmpctk;
		for (cdakmpctk = 13; cdakmpctk > 0; cdakmpctk--)
		{
		}
	}
	if (string("hiurqyezjkinfqxwvcqwotpjxwkqguhkwpfegpoofixn") != string("hiurqyezjkinfqxwvcqwotpjxwkqguhkwpfegpoofixn"))
	{
		int arl;
		for (arl = 90; arl > 0; arl--)
		{
		}
	}
	if (35221 == 35221)
	{
		int vilwuorhgx;
		for (vilwuorhgx = 14; vilwuorhgx > 0; vilwuorhgx--)
		{
		}
	}
	if (string("hiurqyezjkinfqxwvcqwotpjxwkqguhkwpfegpoofixn") != string("hiurqyezjkinfqxwvcqwotpjxwkqguhkwpfegpoofixn"))
	{
		int ajcgpkebm;
		for (ajcgpkebm = 81; ajcgpkebm > 0; ajcgpkebm--)
		{
		}
	}
}

void nkuqgif::qalfkxewhlvzzou(double nfkeapngas, int ylimkdnsrezysb, bool zwedgayjkjqhu, double dhrvyo, int laclxuedapl,
                              string cxbfkjufk, int wttnxqdphtgbcni, bool jogscyh, bool zonojksbbegtrh, double ienom)
{
	double itpvukivfxgf = 19807;
	double hrtcqqdvyjibmeb = 5252;
	if (19807 == 19807)
	{
		int ulyab;
		for (ulyab = 65; ulyab > 0; ulyab--)
		{
		}
	}
	if (5252 != 5252)
	{
		int porjayy;
		for (porjayy = 96; porjayy > 0; porjayy--)
		{
			continue;
		}
	}
	if (5252 != 5252)
	{
		int vnmddgafd;
		for (vnmddgafd = 50; vnmddgafd > 0; vnmddgafd--)
		{
			continue;
		}
	}
	if (5252 == 5252)
	{
		int fjzl;
		for (fjzl = 75; fjzl > 0; fjzl--)
		{
		}
	}
	if (19807 != 19807)
	{
		int uuv;
		for (uuv = 9; uuv > 0; uuv--)
		{
			continue;
		}
	}
}

int nkuqgif::bdlzyexqctrhd(int jajrnrcxpyebxhi, bool zmxlklycyoxy)
{
	string lokefxwj = "fvmqxirikqikkomvisoheanimyk";
	if (string("fvmqxirikqikkomvisoheanimyk") == string("fvmqxirikqikkomvisoheanimyk"))
	{
		int phlejmr;
		for (phlejmr = 77; phlejmr > 0; phlejmr--)
		{
		}
	}
	if (string("fvmqxirikqikkomvisoheanimyk") != string("fvmqxirikqikkomvisoheanimyk"))
	{
		int qxv;
		for (qxv = 82; qxv > 0; qxv--)
		{
		}
	}
	return 67680;
}

void nkuqgif::vapxmheeeccmxireu(string lijimjq, double mlwcoamg, int hueohy, int xwianglyoimjc, int sondvh,
                                double jgtvk, string pojwbdzn, int tkffaagz, int omwhxdivic)
{
	string tfiwiuwqtzaqptf = "xhfpytfhtdrjapjkbsvjizgsmeshky";
	int uwjnyvcfvdn = 937;
	double kunljscnejyg = 6281;
	double nuycgzzeie = 25808;
	if (937 != 937)
	{
		int rqunuqxqsn;
		for (rqunuqxqsn = 70; rqunuqxqsn > 0; rqunuqxqsn--)
		{
			continue;
		}
	}
}

double nkuqgif::udpchddplszpwfoija(string fgztfl, int ojpaftmfnhud, string sgorbtdnzix, int wpwevlucu,
                                   string mqjcqrzjlxsmky, string djwcomi, bool xfiyqccxmiuke, bool ywikzmu,
                                   int hxjqvnzwmrhivu, int isowunzil)
{
	bool mgplli = true;
	string ppwsxhguolayiwy = "xsmhxjrprezzyadmynsjkwjqgatetcbyckoiskfonrcmyqhlaaslrcvssgrxrptelhykanesmvnzgxsjqqpiszrwpbx";
	bool utnqpuvpdskxm = false;
	bool jpdpgapyfqt = true;
	bool gifza = true;
	string nckpmhfna = "vnhivhnfybybpvuqkplayzfefdx";
	if (true != true)
	{
		int sq;
		for (sq = 1; sq > 0; sq--)
		{
			continue;
		}
	}
	return 51178;
}

bool nkuqgif::arnryrxlqsoguttcglsbrktkl()
{
	string mrctp = "p";
	bool ubtvpdilyzrla = true;
	bool josyxxxyzdosyg = false;
	int hqtac = 7377;
	bool vdxxzohhpukq = false;
	int ddlicfqorylnnh = 502;
	int dxwuuvecjvf = 1230;
	if (7377 == 7377)
	{
		int ybwage;
		for (ybwage = 7; ybwage > 0; ybwage--)
		{
		}
	}
	if (false == false)
	{
		int akimghuid;
		for (akimghuid = 69; akimghuid > 0; akimghuid--)
		{
		}
	}
	return false;
}

bool nkuqgif::mypanddjfvjgw(bool hobmeg, double axptqijgcchl, double nyyyfvmd, string rlsrwbe, bool qvgdwiyxzcsbob,
                            int tmgiiuxacwfiob, int bqrsozoo, double sdwlgcziphjwthz, string xcmdvywjxhxfrwz,
                            double anfhnpdniqtlomk)
{
	return true;
}

double nkuqgif::fbnbqucoothuhqbqarie(int lzgedovvkncjbc, double qtqsyioreju, string xyxadiyacs, string eoyopepvtitfrl,
                                     double wslsdpvqeqedg, int xeqfnp)
{
	string wxixrglatb = "dwkvvnonxbidqldbrgciejqjvjsuqnirycvbyeobuiazeusnrawcbemwvttfnchoykdgcohgulezhtxoucidxlkcyomdkd";
	bool leujwzkbfkhl = false;
	string zavvngovvcyklg = "jqvpuhqwlyzqyssfzzrxetyqziwekvxgvxtnyndquwncj";
	if (false != false)
	{
		int tgchwulbt;
		for (tgchwulbt = 21; tgchwulbt > 0; tgchwulbt--)
		{
			continue;
		}
	}
	if (string("jqvpuhqwlyzqyssfzzrxetyqziwekvxgvxtnyndquwncj") != string("jqvpuhqwlyzqyssfzzrxetyqziwekvxgvxtnyndquwncj"))
	{
		int nbclb;
		for (nbclb = 51; nbclb > 0; nbclb--)
		{
		}
	}
	if (string("dwkvvnonxbidqldbrgciejqjvjsuqnirycvbyeobuiazeusnrawcbemwvttfnchoykdgcohgulezhtxoucidxlkcyomdkd") != string(
		"dwkvvnonxbidqldbrgciejqjvjsuqnirycvbyeobuiazeusnrawcbemwvttfnchoykdgcohgulezhtxoucidxlkcyomdkd"))
	{
		int aybmz;
		for (aybmz = 97; aybmz > 0; aybmz--)
		{
		}
	}
	return 8789;
}

void nkuqgif::dunpzxbhyf(string oawlvan, bool mcwlgl, bool cdqbhd, string kwpndcgfuszeys, double zrhjaqhuqfa,
                         double wrowfqysldfth, string jpyywbhqlr, string caqoedyraq, double esqgljidrobhzo)
{
	int fmvkpyzo = 5539;
	int jkrdfu = 1471;
	bool fprbgwkf = true;
	bool gpzbflsxgyqlewo = false;
	string uajtescrcr = "getwajrqiuyqohbwyqzlky";
	double edkmzojyg = 12948;
}

bool nkuqgif::kwqcodsmhksmsnkahnhrbv(double scepm, string htfkftzrcxsmq, double nixkuqwsfjwnuhf, string bbexvzqsvz,
                                     int jcxtxtizd)
{
	int qzfdkzshbt = 2731;
	double udskszyvxkkwu = 156;
	string iojnnqmrl = "czdimdmlmpvvccyhblezvdfmhoarjtgyydnxtoltngmpladdojeztybrioadkrbrlznfxvkmmtpijuxnfgnen";
	double kkikguvwbhh = 47742;
	int kzhmicg = 3478;
	double otigibs = 43479;
	bool jojkn = false;
	if (3478 != 3478)
	{
		int ob;
		for (ob = 3; ob > 0; ob--)
		{
			continue;
		}
	}
	return true;
}

double nkuqgif::dqikqzptkxinirsqmlbxjldo(bool fwhkbvwmlhi, int ktlauaua, double dlbiwoj, double grdonmjd,
                                         bool yxsxgyogfc, bool aygpfxe, string lxbciewvgg, double qlxbkzavx,
                                         bool ujokzb, int gqlftmr)
{
	double xvdmqnppsuloqq = 6003;
	int ucxsjjl = 1206;
	int defdliolww = 6501;
	double simwqlhjkxvypn = 27386;
	double jkcsobncmwuxy = 10296;
	return 92920;
}

void nkuqgif::hwwnkyzjduuxvvrmtkl(bool wnozrm)
{
	bool cvcezahesw = false;
	int rovtqj = 6972;
}

void nkuqgif::blyfrtrygibpvlugjnrso(double roizdhes, double kgijnklir)
{
	double sqxrsbh = 72690;
	int wgyacgumngwvwne = 3269;
	if (72690 == 72690)
	{
		int firlhz;
		for (firlhz = 38; firlhz > 0; firlhz--)
		{
		}
	}
	if (3269 != 3269)
	{
		int mtgpsb;
		for (mtgpsb = 23; mtgpsb > 0; mtgpsb--)
		{
			continue;
		}
	}
}

void nkuqgif::tcoxmlffaydmityzcfxtbbf(int puifcmczciago)
{
	bool ndwowf = false;
	int favxvoqkcvb = 693;
	int vaivb = 1971;
	string dbmbvugxuvkf = "vunxmohohhdwuztwyqaevffnspjoqzasuwrop";
	double wrhmvo = 72311;
	if (1971 != 1971)
	{
		int wwcaohm;
		for (wwcaohm = 79; wwcaohm > 0; wwcaohm--)
		{
			continue;
		}
	}
	if (72311 != 72311)
	{
		int gdlon;
		for (gdlon = 86; gdlon > 0; gdlon--)
		{
			continue;
		}
	}
	if (693 != 693)
	{
		int xupknf;
		for (xupknf = 12; xupknf > 0; xupknf--)
		{
			continue;
		}
	}
}

int nkuqgif::thupbqmtzrarlwl(string hrlvpbdcvuaaqiu)
{
	string mqdicucfhquu =
		"fxkrmoahnuokznvtdifsgrydfsrqvqxawvnjicvqvmvkirnzrthlqdbjrwdmutgfmztxvjqcvsypxqmuulznaypyywgvfxtkvo";
	double dauuwit = 3119;
	string tpgmyqpq = "bnmhvuvmxvjjqwchobomgfllnrgqkonxclnxzeyzjgaptiudropychfofscgivtnj";
	int glnxokmnvs = 3820;
	double phfbbr = 6475;
	bool csjvcuaipzfbxlx = true;
	string iaogaeofcpfdk = "qvtecpftlcynmzzribzhfxkozovlkgoclwkiknaoifwngbeqnvolfkzpmkqcafxyml";
	bool sbnpzejwt = true;
	if (3119 != 3119)
	{
		int us;
		for (us = 64; us > 0; us--)
		{
			continue;
		}
	}
	if (string("fxkrmoahnuokznvtdifsgrydfsrqvqxawvnjicvqvmvkirnzrthlqdbjrwdmutgfmztxvjqcvsypxqmuulznaypyywgvfxtkvo") ==
		string("fxkrmoahnuokznvtdifsgrydfsrqvqxawvnjicvqvmvkirnzrthlqdbjrwdmutgfmztxvjqcvsypxqmuulznaypyywgvfxtkvo"))
	{
		int wpjokffv;
		for (wpjokffv = 34; wpjokffv > 0; wpjokffv--)
		{
		}
	}
	if (string("qvtecpftlcynmzzribzhfxkozovlkgoclwkiknaoifwngbeqnvolfkzpmkqcafxyml") != string(
		"qvtecpftlcynmzzribzhfxkozovlkgoclwkiknaoifwngbeqnvolfkzpmkqcafxyml"))
	{
		int ois;
		for (ois = 3; ois > 0; ois--)
		{
		}
	}
	return 62758;
}

nkuqgif::nkuqgif()
{
	this->hwwnkyzjduuxvvrmtkl(false);
	this->blyfrtrygibpvlugjnrso(73395, 87748);
	this->tcoxmlffaydmityzcfxtbbf(3204);
	this->thupbqmtzrarlwl(string("owguvbncicfqwkohtknkndzylyqmgidkxicxoqqesnphecxrefzfhizwsewqwgjojgyu"));
	this->fbnbqucoothuhqbqarie(814, 33573, string("orah"), string("jybdtwy"), 50147, 2419);
	this->dunpzxbhyf(string("hatjrkthsjktwfkiqswdltzrc"), false, true,
	                 string("hzoxfpoudeesvqfoyzmrheywprkueyuaxnksfhrbnndsitmqdujhftdbryejwdunnoqeo"), 30092, 912,
	                 string("cefdqcvabxuuzfkbbgx"),
	                 string(
		                 "eogptmzbswwfedcgsszjpfpfxdqolzxuoxgvzibnugoyipaqimbcqyerigpgnqjejkftuziuquijikomvwueripcsytzui"),
	                 3469);
	this->kwqcodsmhksmsnkahnhrbv(23170, string("lupmspvgejnfflfvyyvzuzyhxmxwzd"), 8163,
	                             string("skqjtjqzymgyximcjfzlhvmgxeopfminkymhlei"), 577);
	this->dqikqzptkxinirsqmlbxjldo(false, 2743, 56538, 10203, false, true, string("zocdtxbko"), 44041, false, 1523);
	this->uzjntfecivstofnolk(54060, 4527, string("mzkkpeqp"), false, 4961,
	                         string("knvrfrzoyrfvyueexetokcofcgzmqzwcslualbpgxbmkwbaouzybh"), 792);
	this->vertvhdyhdewk(7432, 116, true, true,
	                    string("hpvudipnzkppebygxxpyrengfzaaqahvyqijualeysbwmhunltyowwzsdhzuywzodfaj"), 5616,
	                    string("mfbjptwtuwenkrqgfxncccgejsmm"), false, false);
	this->qalfkxewhlvzzou(13369, 692, false, 67237, 3217, string("nnvbyhwziorkbgtaaxj"), 1713, true, true, 38545);
	this->bdlzyexqctrhd(1813, false);
	this->vapxmheeeccmxireu(string("tdyexltpqcdafhbswazvobkixqmrosgqjcdcrjdkzmdrcigrbfxlxelqbvldrpgnplwjjabyteufbkkadjr"),
	                        22394, 4093, 2130, 1476, 18663,
	                        string(
		                        "sjytrrnlutebhvxnvbswtycigzqnjuisweikluvlreqdlrekqkktgxlkrqyjjcjqlbaykwiwegkeuuawinzfjgf"),
	                        1884, 7708);
	this->udpchddplszpwfoija(string("rqnrlzvr"), 3147, string("irqtjcxfcxygszo"), 1627,
	                         string(
		                         "nlglcbppalfwwlocrqryrlhfejisqwqlnqvbydtckkefhfrkiwvheoireiazngjzzksosazovxxfgksjwpeoivespv"),
	                         string("gjibxxlbxuxocisohvhyxsmfcdsrfmlyiizbbzp"), false, true, 1830, 2850);
	this->arnryrxlqsoguttcglsbrktkl();
	this->mypanddjfvjgw(false, 1203, 5334, string("jvstlhoujkgacdnitdvtuybbnqeuhtsockevuntxsyyyklzqedgjyj"), false, 3521,
	                    2259, 17229, string("cutdadxaqcbaqhibupmypwfiatldbbiubdvzwegbpplosgvcpuugfhnmumnegpxngaf"), 3569);
}
