#include "ESP.h"
#include "Interfaces.h"
#include "Render.h"
#include <ctime>
#include <iostream>
#include <algorithm>
#include "GrenadePrediction.h"
#include "LagComp.h"
#include "Autowall.h"


visuals::visuals()
{
	BombCarrier = nullptr;
}

int width = 0;
int height = 0;
bool done = false;

void visuals::OnPaintTraverse(C_BaseEntity* local)
{
	for (int i = 0; i < g_EntityList->GetHighestEntityIndex(); i++)
	{
		C_BaseEntity* entity = g_EntityList->GetClientEntity(i);
		player_info_t pinfo;
		if (entity == local && local->IsAlive() && g_Engine->GetPlayerInfo(g_Engine->GetLocalPlayer(), &pinfo))
		{
			if (g_Input->m_fCameraInThirdPerson && g_Options.Visuals.Enabled)
			{
				Vector max = entity->GetCollideable()->OBBMaxs();
				Vector pos, pos3D;
				Vector top, top3D;
				pos3D = entity->GetOrigin();
				top3D = pos3D + Vector(0, 0, max.z);

				if (!g_Render->WorldToScreen(pos3D, pos) || !g_Render->WorldToScreen(top3D, top))
					return;

				float height = (pos.y - top.y);
				float width = height / 4.f;
				if (g_Options.Visuals.Box)
				{
					Color color;
					color = GetPlayerColor(entity, local);
					PlayerBox(top.x, top.y, width, height, color);
				}
				if (g_Options.Visuals.HP)
					DrawHealth(pos, top, local->GetHealth());

				if (g_Options.Visuals.Name)
					g_Render->DrawString2(g_Render->font.ESP, (int)top.x, (int)top.y - 6, Color::White(), FONT_CENTER, pinfo.name);
			}
		}
		if (entity && entity != local && !entity->IsDormant())
		{
			if (g_Engine->GetPlayerInfo(i, &pinfo) && entity->IsAlive())
			{
				if (g_Options.Visuals.backtrackline)
				{
					if (local->IsAlive())
					{
						for (int t = 0; t < g_Options.Legitbot.backtrackTicks; ++t)
						{
							Vector screenbacktrack[64][12];

							if (headPositions[i][t].simtime && headPositions[i][t].simtime + 1 > local->GetSimulationTime())
							{
								if (g_Render->WorldToScreen(headPositions[i][t].hitboxPos, screenbacktrack[i][t]))
								{
									g_Surface->DrawSetColor(Color(int(g_Options.Colors.backtrackdots_color[0] * 255.f),
									                              int(g_Options.Colors.backtrackdots_color[1] * 255.f),
									                              int(g_Options.Colors.backtrackdots_color[2] * 255.f)));
									g_Surface->DrawOutlinedRect(screenbacktrack[i][t].x, screenbacktrack[i][t].y, screenbacktrack[i][t].x + 2,
									                            screenbacktrack[i][t].y + 2);
								}
							}
						}
					}
					else
					{
						memset(&headPositions[0][0], 0, sizeof(headPositions));
					}
				}
				if (g_Options.Ragebot.FakeLagFix)
				{
					if (local->IsAlive())
					{
						Vector screenbacktrack[64];

						if (backtracking->records[i].tick_count + 12 > g_Globals->tickcount)
						{
							if (g_Render->WorldToScreen(backtracking->records[i].headPosition, screenbacktrack[i]))
							{
								g_Surface->DrawSetColor(Color::Red());
								g_Surface->DrawOutlinedRect(screenbacktrack[i].x, screenbacktrack[i].y, screenbacktrack[i].x + 2,
								                            screenbacktrack[i].y + 2);
							}
						}
					}
					else
					{
						memset(&backtracking->records[0], 0, sizeof(backtracking->records));
					}
				}
				if (g_Options.Visuals.Enabled && g_Options.Visuals.Filter.Players)
				{
					if (g_Options.Visuals.DLight)
						DLight(local, entity);

					DrawPlayer(entity, pinfo, local);
				}
				if (g_Options.Visuals.DrawAwall && local->IsAlive())
					DrawAwall();
			}
			if (g_Options.Visuals.Enabled)
			{
				ClientClass* cClass = (ClientClass*)entity->GetClientClass();
				if (g_Options.Visuals.WeaponsWorld && cClass->m_ClassID != (int)ClassID::CBaseWeaponWorldModel && ((
					strstr(cClass->m_pNetworkName, "Weapon") || cClass->m_ClassID == (int)ClassID::CDEagle || cClass->m_ClassID == (int
					)ClassID::CAK47)))
				{
					DrawDrop(entity);
				}
				if (g_Options.Visuals.C4World)
				{
					if (cClass->m_ClassID == (int)ClassID::CPlantedC4)
						DrawBombPlanted(entity, local);
				}

				if (cClass->m_ClassID == (int)ClassID::CC4)
					DrawBomb(entity, cClass);
				if (g_Options.Visuals.GrenadeESP && strstr(cClass->m_pNetworkName, "Projectile"))
				{
					DrawThrowable(entity);
				}
			}
		}
	}
	if (g_Options.Misc.SpecList) SpecList(local);

	if (g_Options.Misc.Hitmarker)
		Hitmarker();

	NightMode();
	grenade_prediction::instance().Paint();

	if (g_Options.Visuals.SpreadCrosshair)
	{
		g_Engine->GetScreenSize(width, height);
		if (local && local->IsAlive())
		{
			static Vector ViewAngles;
			g_Engine->GetViewAngles(ViewAngles);
			ViewAngles += local->localPlayerExclusive()->GetAimPunchAngle() * 2.f;

			static Vector fowardVec;
			AngleVectors(ViewAngles, &fowardVec);
			fowardVec *= 10000;

			// Get ray start / end
			Vector start = local->GetOrigin() + local->GetViewOffset();
			Vector end = start + fowardVec, endScreen;

			CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
				local->GetActiveWeaponHandle());
			float cone = pWeapon->GetSpread() + pWeapon->GetInaccuracy();
			if (cone > 0.0f)
			{
				if (cone < 0.01f) cone = 0.01f;
				float size = (cone * height) * 0.7f;
				Color color(int(g_Options.Colors.color_spread[0] * 255.f), int(g_Options.Colors.color_spread[1] * 255.f),
				            int(g_Options.Colors.color_spread[2] * 255.f));

				if (g_Render->WorldToScreen(end, endScreen))
				{
					g_Render->OutlineCircle(endScreen.x, endScreen.y, (int)size, 48, color);
				}
			}
		}
	}
	if (g_Options.Visuals.RecoilCrosshair)
	{
		g_Engine->GetScreenSize(width, height);
		if (local && local->IsAlive())
		{
			static Vector ViewAngles;
			g_Engine->GetViewAngles(ViewAngles);
			ViewAngles += local->localPlayerExclusive()->GetAimPunchAngle() * 2.f;

			Vector fowardVec;
			AngleVectors(ViewAngles, &fowardVec);
			fowardVec *= 10000;

			Vector start = local->GetOrigin() + local->GetViewOffset();
			Vector end = start + fowardVec, endScreen;

			if (g_Render->WorldToScreen(end, endScreen) && local->IsAlive())
			{
				g_Render->Line(endScreen.x - 4, endScreen.y, endScreen.x + 4, endScreen.y, Color(
					               int(g_Options.Colors.color_recoil[0] * 255.f), int(g_Options.Colors.color_recoil[1] * 255.f),
					               int(g_Options.Colors.color_recoil[2] * 255.f)));
				g_Render->Line(endScreen.x, endScreen.y - 4, endScreen.x, endScreen.y + 4, Color(
					               int(g_Options.Colors.color_recoil[0] * 255.f), int(g_Options.Colors.color_recoil[1] * 255.f),
					               int(g_Options.Colors.color_recoil[2] * 255.f)));
			}
		}
	}
}

std::string CleanItemName(std::string name)
{
	std::string Name = name;
	// Tidy up the weapon Name
	if (Name[0] == 'C')
		Name.erase(Name.begin());

	// Remove the word Weapon
	auto startOfWeap = Name.find("Weapon");
	if (startOfWeap != std::string::npos)
		Name.erase(Name.begin() + startOfWeap, Name.begin() + startOfWeap + 6);

	return Name;
}

wchar_t* CharToWideChar(const char* text)
{
	size_t size = strlen(text) + 1;
	wchar_t* wa = new wchar_t[size];
	mbstowcs_s(nullptr, wa, size / 4, text, size);
	return wa;
}


C_CSPlayerResource* playerresources;

void visuals::DrawPlayer(C_BaseEntity* entity, player_info_t pinfo, C_BaseEntity* local)
{
	Vector max = entity->GetCollideable()->OBBMaxs();
	Vector pos, pos3D;
	Vector top, top3D;
	pos3D = entity->GetOrigin();
	top3D = pos3D + Vector(0, 0, max.z);

	if (!g_Render->WorldToScreen(pos3D, pos) || !g_Render->WorldToScreen(top3D, top))
		return;

	float height = (pos.y - top.y);
	float width = height / 4.f;

	Color color;

	if (g_Options.Visuals.Filter.EnemyOnly && (entity->GetTeamNum() == local->GetTeamNum()))
		return;
	color = GetPlayerColor(entity, local);

	if (g_Options.Visuals.Box)
		PlayerBox(top.x, top.y, width, height, color);

	if (g_Options.Visuals.HP)
		DrawHealth(pos, top, entity->GetHealth());

	if (g_Options.Visuals.Name)
		g_Render->DrawString2(g_Render->font.ESP, (int)top.x, (int)top.y - 6, Color::White(), FONT_CENTER, pinfo.name);

	if (g_Options.Visuals.Skeleton)
		DrawSkeleton(entity);

	int bottom = 0;

	/*auto m_AnimOverlay = entity->GetAnimationOverlay();

	for (int i = 0; i < 14; i++)
	{
	    char sequence_string[64];
	    sprintf_s(sequence_string, sizeof(sequence_string) - 1, "%d | %d", i, m_AnimOverlay.m_pElements[i].Sequence);
	    g_Render->DrawString2(g_Render->font.ESP, top.x + 20 + height, top.y + 10 * i, Color::White(), FONT_LEFT, sequence_string);
	}*/

	std::vector<std::string> weapon;
	std::vector<std::string> bomb;
	std::vector<std::string> rank;
	std::vector<std::string> wins;

	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)g_EntityList->GetClientEntityFromHandle(
		entity->GetActiveWeaponHandle());
	if (g_Options.Visuals.Weapon && pWeapon)
	{
		int weapon_id = pWeapon->m_AttributeManager()->m_Item()->GetItemDefinitionIndex();

		auto weapon_name = ItemDefinitionIndexToString(weapon_id);
		weapon.push_back(weapon_name);
	}


	if (g_Options.Visuals.C4 && entity == BombCarrier)
	{
		bomb.push_back("Bomb");
	}

	int i = 0;
	if (g_Options.Visuals.Weapon)
	{
		for (auto Text : weapon)
		{
			g_Render->DrawString2(g_Render->font.Guns, (int)top.x, int(top.y + height + 8 + (10 * bottom++)), Color::White(),
			                      FONT_CENTER, "%s", Text.c_str());
			i++;
		}
	}
	if (g_Options.Visuals.C4)
	{
		for (auto Text : bomb)
		{
			g_Render->DrawString2(g_Render->font.Guns, (int)top.x, int(top.y + height + 8 + (10 * bottom++)), Color::Red(),
			                      FONT_CENTER, Text.c_str());
			i++;
		}
	}

	if (g_Options.Visuals.money)
	{
		g_Render->Textf(int(top.x + width + 3), int(top.y + 12), Color(50, 226, 120, 255), g_Render->font.ESP, "%i",
		                entity->GetMoney());
	}

	if (g_Options.Visuals.IsScoped && entity->IsScoped())
	{
		g_Render->DrawString2(g_Render->font.ESP, (int)top.x + width + 3, int(top.y + 7), Color::Red(), FONT_CENTER,
		                      "*Scoped*");
		i++;
	}

	if (g_Options.Visuals.HasDefuser && entity->hasDefuser())
	{
		g_Render->DrawString2(g_Render->font.ESP, (int)top.x, int(top.y + height + 8 + (14 * bottom++)), Color::LightBlue(),
		                      FONT_CENTER, "Defuser");
		i++;
	}
	if (g_Options.Visuals.Flashed && entity->IsFlashed())
	{
		g_Render->DrawString2(g_Render->font.ESP, (int)top.x, int(top.y + height + 15 + (18 * bottom++)), Color::White(),
		                      FONT_CENTER, "*Flashed*");
		i++;
	}
}

void visuals::Hitmarker()
{
	if (G::hitmarkeralpha < 0.f)
		G::hitmarkeralpha = 0.f;
	else if (G::hitmarkeralpha > 0.f)
		G::hitmarkeralpha -= 0.01f;

	int W, H;
	g_Engine->GetScreenSize(W, H);

	if (G::hitmarkeralpha > 0.f)
	{
		g_Render->Line(W / 2 - 10, H / 2 - 10, W / 2 - 5, H / 2 - 5, Color(int(g_Options.Colors.hitmark_color[0] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[1] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[2] * 255.f),
		                                                                   (G::hitmarkeralpha * 255.f)));
		g_Render->Line(W / 2 - 10, H / 2 + 10, W / 2 - 5, H / 2 + 5, Color(int(g_Options.Colors.hitmark_color[0] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[1] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[2] * 255.f),
		                                                                   (G::hitmarkeralpha * 255.f)));
		g_Render->Line(W / 2 + 10, H / 2 - 10, W / 2 + 5, H / 2 - 5, Color(int(g_Options.Colors.hitmark_color[0] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[1] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[2] * 255.f),
		                                                                   (G::hitmarkeralpha * 255.f)));
		g_Render->Line(W / 2 + 10, H / 2 + 10, W / 2 + 5, H / 2 + 5, Color(int(g_Options.Colors.hitmark_color[0] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[1] * 255.f),
		                                                                   int(g_Options.Colors.hitmark_color[2] * 255.f),
		                                                                   (G::hitmarkeralpha * 255.f)));
	}
}

void visuals::PlayerBox(float x, float y, float w, float h, Color clr)
{
	g_Surface->DrawSetColor(clr);
	g_Surface->DrawOutlinedRect(int(x - w), int(y), int(x + w), int(y + h));
	g_Surface->DrawSetColor(Color::Black());
	g_Surface->DrawOutlinedRect(int(x - w - 1), int(y - 1), int(x + w + 1), int(y + h + 1));
	g_Surface->DrawOutlinedRect(int(x - w + 1), int(y + 1), int(x + w - 1), int(y + h - 1));
}

Color visuals::GetPlayerColor(C_BaseEntity* entity, C_BaseEntity* local)
{
	int TeamNum = entity->GetTeamNum();
	bool IsVis = MiscFunctions::IsVisible(local, entity, Head);

	Color color;
	static float rainbow;
	rainbow += 0.005f;
	if (rainbow > 1.f)
		rainbow = 0.f;
	if (TeamNum == TEAM_CS_T)
	{
		if (IsVis)
			color = Color(32, 130, 43);
		else
			color = Color(int(g_Options.Colors.box_color_t[0] * 255.f), int(g_Options.Colors.box_color_t[1] * 255.f),
			              int(g_Options.Colors.box_color_t[2] * 255.f));
	}
	else
	{
		if (IsVis)
			color = Color(32, 130, 43);
		else
			color = Color(int(g_Options.Colors.box_color_ct[0] * 255.f), int(g_Options.Colors.box_color_ct[1] * 255.f),
			              int(g_Options.Colors.box_color_ct[2] * 255.f));
	}


	return color;
}

void visuals::DrawSkeleton(C_BaseEntity* entity)
{
	studiohdr_t* pStudioHdr = g_ModelInfo->GetStudiomodel(entity->GetModel());

	if (!pStudioHdr)
		return;

	Vector vParent, vChild, sParent, sChild;

	for (int j = 0; j < pStudioHdr->numbones; j++)
	{
		mstudiobone_t* pBone = pStudioHdr->GetBone(j);

		if (pBone && (pBone->flags & 0x100) && (pBone->parent != -1))
		{
			vChild = entity->GetBonePos(j);
			vParent = entity->GetBonePos(pBone->parent);

			if (g_Render->WorldToScreen(vParent, sParent) && g_Render->WorldToScreen(vChild, sChild))
			{
				g_Render->Line(sParent.x, sParent.y, sChild.x, sChild.y, Color(int(g_Options.Colors.color_skeleton[0] * 255.f),
				                                                               int(g_Options.Colors.color_skeleton[1] * 255.f),
				                                                               int(g_Options.Colors.color_skeleton[2] * 255.f)));
			}
		}
	}
}

void visuals::DrawHealth(C_BaseEntity* entity, ESPBox size)
{
	int health = entity->GetHealth();
	int HP = health;
	if (HP > 100)
		HP = 100;
	int hp = health;
	float r = float(255 - health * 2.55);
	float g = float(health * 2.55);
	hp = (size.h - ((size.h * hp) / 100));

	g_Render->Outline(int(size.x - 4), int(size.y + hp), (int)2, int(size.h - hp + 1), Color((int)r, (int)g, 0));
	g_Render->Outline(int(size.x - 5), int(size.y - 1), (int)3, int(size.h + 2), Color(0, 0, 0, 150));
}


void visuals::DrawHealth(Vector bot, Vector top, float health)
{
	float h = (bot.y - top.y);
	float offset = (h / 4.f) + 5;
	float w = h / 64.f;

	UINT hp = UINT(h - (UINT)((h * health) / 100)); // Percentage

	int Red = int(255 - (health * 2.55));
	int Green = int(health * 2.55);

	g_Render->DrawOutlinedRect(int((top.x - offset) - 1), int(top.y - 1), 3, int(h + 2), Color::Black());

	g_Render->Line(int((top.x - offset)), int(top.y + hp), int((top.x - offset)), int(top.y + h),
	               Color(Red, Green, 0, 255));
}

void visuals::DrawDrop(C_BaseEntity* entity)
{
	if (entity)
	{
		CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)entity;

		auto owner = pWeapon->GetOwnerHandle();

		if (owner > -1)
			return;

		Vector pos3D = entity->GetAbsOrigin2();

		if (pos3D.x == 0.0f && pos3D.y == 0.0f && pos3D.z == 0.0f)
			return;

		Vector pos;

		if (!g_Render->WorldToScreen(pos3D, pos))
			return;

		int weaponID = pWeapon->m_AttributeManager()->m_Item()->GetItemDefinitionIndex();
		auto weaponName = ItemDefinitionIndexToString(weaponID);


		g_Render->Text(pos.x, pos.y, Color(255, 255, 255, 255), g_Render->font.DroppedGuns, weaponName);
	}
}

float damage;
char bombdamagestringdead[24];
char bombdamagestringalive[24];

void visuals::DrawBombPlanted(C_BaseEntity* entity, C_BaseEntity* local)
{
	BombCarrier = nullptr;

	Vector vOrig;
	Vector vScreen;
	vOrig = entity->GetOrigin();
	CCSBomb* Bomb = (CCSBomb*)entity;
	float flBlow = Bomb->GetC4BlowTime();
	float lifetime = flBlow - (g_Globals->interval_per_tick * local->GetTickBase());
	if (g_Render->WorldToScreen(vOrig, vScreen))
	{
		if (local->IsAlive())
		{
			float flDistance = local->GetEyePosition().DistTo(entity->GetEyePosition());
			float a = 450.7f;
			float b = 75.68f;
			float c = 789.2f;
			float d = ((flDistance - b) / c);
			float flDamage = a * exp(-d * d);

			damage = float((std::max)((int)ceilf(CSGO_Armor(flDamage, local->ArmorValue())), 0));

			sprintf_s(bombdamagestringdead, sizeof(bombdamagestringdead) - 1, "DEAD");
			sprintf_s(bombdamagestringalive, sizeof(bombdamagestringalive) - 1, "Health left: %.0f",
			          local->GetHealth() - damage);
			if (lifetime > -2.f)
			{
				if (damage >= local->GetHealth())
				{
					g_Render->Text((int)vScreen.x, int(vScreen.y + 10), Color(250, 42, 42, 255), g_Render->font.Defuse,
					               bombdamagestringdead);
				}
				else if (local->GetHealth() > damage)
				{
					g_Render->Text((int)vScreen.x, int(vScreen.y + 10), Color(0, 255, 0, 255), g_Render->font.Defuse,
					               bombdamagestringalive);
				}
			}
		}
		char buffer[64];
		if (lifetime > 0.01f && !Bomb->IsBombDefused())
		{
			sprintf_s(buffer, "Bomb: %.1f", lifetime);
			g_Render->Text((int)vScreen.x, (int)vScreen.y, Color(250, 42, 42, 255), g_Render->font.ESP, buffer);
		}
	}

	g_Engine->GetScreenSize(width, height);
	int halfX = width / 2;
	int halfY = height / 2;


	if (Bomb->GetBombDefuser() > 0)
	{
		float countdown = Bomb->GetC4DefuseCountDown() - (local->GetTickBase() * g_Globals->interval_per_tick);
		if (countdown > 0.01f)
		{
			if (lifetime > countdown)
			{
				char defuseTimeString[24];
				sprintf_s(defuseTimeString, sizeof(defuseTimeString) - 1, "Defusing: %.1f", countdown);
				g_Render->Text(halfX - 50, halfY + 200, Color(0, 255, 0, 255), g_Render->font.Defuse, defuseTimeString);
			}
			else
			{
				g_Render->Text(halfX - 50, halfY + 200, Color(255, 0, 0, 255), g_Render->font.Defuse, "No More Time, Get Out!");
			}
		}
	}
}

void visuals::DrawAwall()
{
	int MidX;
	int MidY;
	g_Engine->GetScreenSize(MidX, MidY);

	int damage;
	if (CanWallbang(damage))
	{
		g_Render->OutlineCircle(MidX / 2, MidY / 2, 10, 10, Color(0, 255, 0));
		g_Render->Textf(MidX / 2, MidY / 2 + 6, Color(255, 255, 255, 255), g_Render->font.ESP, "DMG: %1i", damage);
	}
	else
	{
		g_Render->OutlineCircle(MidX / 2, MidY / 2, 10, 10, Color(255, 0, 0));
		g_Render->Textf(MidX / 2, MidY / 2 + 6, Color(255, 255, 255, 255), g_Render->font.ESP, "DMG: 0");
	}
}

void visuals::DrawBomb(C_BaseEntity* entity, ClientClass* cClass)
{
	// Null it out incase bomb has been dropped or planted
	BombCarrier = nullptr;
	CBaseCombatWeapon* BombWeapon = (CBaseCombatWeapon *)entity;
	Vector vOrig;
	Vector vScreen;
	vOrig = entity->GetOrigin();
	bool adopted = true;
	auto parent = BombWeapon->GetOwnerHandle();
	if (parent || (vOrig.x == 0 && vOrig.y == 0 && vOrig.z == 0))
	{
		C_BaseEntity* pParentEnt = (g_EntityList->GetClientEntityFromHandle(parent));
		if (pParentEnt && pParentEnt->IsAlive())
		{
			BombCarrier = pParentEnt;
			adopted = false;
		}
	}
	if (g_Options.Visuals.C4World)
	{
		if (adopted)
		{
			if (g_Render->WorldToScreen(vOrig, vScreen))
			{
				g_Render->Text((int)vScreen.x, (int)vScreen.y, Color(112, 20, 20, 255), g_Render->font.ESP, "Bomb");
			}
		}
	}
}

void visuals::DrawBox(ESPBox size, Color color)
{
	g_Render->Outline(size.x, size.y, size.w, size.h, color);
	g_Render->Outline(size.x - 1, size.y - 1, size.w + 2, size.h + 2, Color(10, 10, 10, 150));
	g_Render->Outline(size.x + 1, size.y + 1, size.w - 2, size.h - 2, Color(10, 10, 10, 150));
}

bool visuals::GetBox(C_BaseEntity* entity, ESPBox& result)
{
	// Variables
	Vector vOrigin, min, max, sMin, sMax, sOrigin,
	       flb, brt, blb, frt, frb, brb, blt, flt;
	float left, top, right, bottom;

	// Get the locations
	vOrigin = entity->GetOrigin();
	min = entity->collisionProperty()->GetMins() + vOrigin;
	max = entity->collisionProperty()->GetMaxs() + vOrigin;

	// Points of a 3d bounding box
	Vector points[] = {
		Vector(min.x, min.y, min.z),
		Vector(min.x, max.y, min.z),
		Vector(max.x, max.y, min.z),
		Vector(max.x, min.y, min.z),
		Vector(max.x, max.y, max.z),
		Vector(min.x, max.y, max.z),
		Vector(min.x, min.y, max.z),
		Vector(max.x, min.y, max.z)
	};

	// Get screen positions
	if (!g_Render->WorldToScreen(points[3], flb) || !g_Render->WorldToScreen(points[5], brt)
		|| !g_Render->WorldToScreen(points[0], blb) || !g_Render->WorldToScreen(points[4], frt)
		|| !g_Render->WorldToScreen(points[2], frb) || !g_Render->WorldToScreen(points[1], brb)
		|| !g_Render->WorldToScreen(points[6], blt) || !g_Render->WorldToScreen(points[7], flt))
		return false;

	// Put them in an array (maybe start them off in one later for speed?)
	Vector arr[] = {flb, brt, blb, frt, frb, brb, blt, flt};

	// Init this shit
	left = flb.x;
	top = flb.y;
	right = flb.x;
	bottom = flb.y;

	// Find the bounding corners for our box
	for (int i = 1; i < 8; i++)
	{
		if (left > arr[i].x)
			left = arr[i].x;
		if (bottom < arr[i].y)
			bottom = arr[i].y;
		if (right < arr[i].x)
			right = arr[i].x;
		if (top > arr[i].y)
			top = arr[i].y;
	}

	// Width / height
	result.x = (int)left;
	result.y = (int)top;
	result.w = int(right - left);
	result.h = int(bottom - top);
	result.gay = (int)top;

	return true;
}

void visuals::BoxAndText(C_BaseEntity* entity, std::string text)
{
	ESPBox Box;
	std::vector<std::string> Info;
	if (GetBox(entity, Box))
	{
		Info.push_back(text);
		if (g_Options.Visuals.GrenadeESP)
		{
			DrawBox(Box, Color(255, 255, 255, 255));
			int i = 0;
			for (auto kek : Info)
			{
				g_Render->Text(Box.x + 1, Box.y + 1, Color(255, 255, 255, 255), g_Render->font.ESP, kek.c_str());
				i++;
			}
		}
	}
}

void visuals::DrawThrowable(C_BaseEntity* throwable)
{
	model_t* nadeModel = (model_t*)throwable->GetModel();

	if (!nadeModel)
		return;

	studiohdr_t* hdr = g_ModelInfo->GetStudiomodel(nadeModel);

	if (!hdr)
		return;

	if (!strstr(hdr->name, "thrown") && !strstr(hdr->name, "dropped"))
		return;

	std::string nadeName = "Unknown Grenade";

	IMaterial* mats[32];
	g_ModelInfo->GetModelMaterials(nadeModel, hdr->numtextures, mats);

	for (int i = 0; i < hdr->numtextures; i++)
	{
		IMaterial* mat = mats[i];
		if (!mat)
			continue;

		if (strstr(mat->GetName(), "flashbang"))
		{
			nadeName = "Flashbang";
			break;
		}
		if (strstr(mat->GetName(), "m67_grenade") || strstr(mat->GetName(), "hegrenade"))
		{
			nadeName = "HE";
			break;
		}
		if (strstr(mat->GetName(), "smoke"))
		{
			nadeName = "Smoke";
			break;
		}
		if (strstr(mat->GetName(), "decoy"))
		{
			nadeName = "Decoy";
			break;
		}
		if (strstr(mat->GetName(), "incendiary") || strstr(mat->GetName(), "molotov"))
		{
			nadeName = "Molotov";
			break;
		}
	}

	BoxAndText(throwable, nadeName);
}

void visuals::DLight(C_BaseEntity* local, C_BaseEntity* entity)
{
	player_info_t pinfo;
	if (local && entity && entity != local)
	{
		if (g_Engine->GetPlayerInfo(entity->GetIndex(), &pinfo) && entity->IsAlive() && !entity->IsDormant())
		{
			if (local->GetTeamNum() != entity->GetTeamNum())
			{
				dlight_t* pElight = g_Dlight->CL_AllocElight(entity->GetIndex());
				pElight->origin = entity->GetOrigin() + Vector(0.0f, 0.0f, 35.0f);
				pElight->radius = 400.0f;
				pElight->color.b = int(g_Options.Colors.dlight_color[0] * 255);
				pElight->color.g = int(g_Options.Colors.dlight_color[1] * 255);
				pElight->color.r = int(g_Options.Colors.dlight_color[2] * 255);
				pElight->die = g_Globals->curtime + 0.05f;
				pElight->decay = pElight->radius; // / 5.0f;
				pElight->key = entity->GetIndex();

				dlight_t* pDlight = g_Dlight->CL_AllocDlight(entity->GetIndex());
				pDlight->origin = entity->GetOrigin();
				pDlight->radius = 400.0f;
				pDlight->color.b = int(g_Options.Colors.dlight_color[0] * 255);
				pDlight->color.g = int(g_Options.Colors.dlight_color[1] * 255);
				pDlight->color.r = int(g_Options.Colors.dlight_color[2] * 255);
				pDlight->die = g_Globals->curtime + 0.05f;
				pDlight->decay = pDlight->radius; // / 5.0f;
				pDlight->key = entity->GetIndex();
			}
		}
	}
}

void visuals::NightMode()
{
	if (g_Options.Misc.nightMode)
	{
		if (!done)
		{
			static auto sv_skyname = g_CVar->FindVar("sv_skyname");
			static auto r_DrawSpecificStaticProp = g_CVar->FindVar("r_DrawSpecificStaticProp");
			r_DrawSpecificStaticProp->SetValue(1);
			sv_skyname->SetValue("sky_csgo_night02");

			for (MaterialHandle_t i = g_MaterialSystem->FirstMaterial(); i != g_MaterialSystem->InvalidMaterial(); i =
			     g_MaterialSystem->NextMaterial(i))
			{
				IMaterial* pMaterial = g_MaterialSystem->GetMaterial(i);

				if (!pMaterial)
					continue;

				const char* group = pMaterial->GetTextureGroupName();
				const char* name = pMaterial->GetName();

				if (strstr(group, "World textures"))
				{
					pMaterial->ColorModulate(0.10, 0.10, 0.10);
				}
				if (strstr(group, "StaticProp"))
				{
					pMaterial->ColorModulate(0.30, 0.30, 0.30);
				}
				if (strstr(name, "models/props/de_dust/palace_bigdome"))
				{
					pMaterial->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
				}
				if (strstr(name, "models/props/de_dust/palace_pillars"))
				{
					pMaterial->ColorModulate(0.30, 0.30, 0.30);
				}

				if (strstr(group, "Particle textures"))
				{
					pMaterial->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, true);
				}
				done = true;
			}
		}
	}
	else
	{
		if (done)
		{
			for (MaterialHandle_t i = g_MaterialSystem->FirstMaterial(); i != g_MaterialSystem->InvalidMaterial(); i =
			     g_MaterialSystem->NextMaterial(i))
			{
				IMaterial* pMaterial = g_MaterialSystem->GetMaterial(i);

				if (!pMaterial)
					continue;

				const char* group = pMaterial->GetTextureGroupName();
				const char* name = pMaterial->GetName();

				if (strstr(group, "World textures"))
				{
					pMaterial->ColorModulate(1, 1, 1);
				}
				if (strstr(group, "StaticProp"))
				{
					pMaterial->ColorModulate(1, 1, 1);
				}
				if (strstr(name, "models/props/de_dust/palace_bigdome"))
				{
					pMaterial->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, false);
				}
				if (strstr(name, "models/props/de_dust/palace_pillars"))
				{
					pMaterial->ColorModulate(1, 1, 1);
				}
				if (strstr(group, "Particle textures"))
				{
					pMaterial->SetMaterialVarFlag(MATERIAL_VAR_NO_DRAW, false);
				}
			}
			done = false;
		}
	}
}

void visuals::SpecList(C_BaseEntity* local)
{
	RECT scrn = g_Render->GetViewport();
	int kapi = 0;

	if (local)
	{
		for (int i = 0; i < g_EntityList->GetHighestEntityIndex(); i++)
		{
			// Get the entity
			C_BaseEntity* pEntity = g_EntityList->GetClientEntity(i);
			player_info_t pinfo;
			if (pEntity && pEntity != local)
			{
				if (g_Engine->GetPlayerInfo(i, &pinfo) && !pEntity->IsAlive() && !pEntity->IsDormant())
				{
					HANDLE obs = pEntity->GetObserverTargetHandle();
					if (obs)
					{
						C_BaseEntity* pTarget = g_EntityList->GetClientEntityFromHandle(obs);
						player_info_t pinfo2;
						if (pTarget && pTarget->GetIndex() == local->GetIndex())
						{
							if (g_Engine->GetPlayerInfo(pTarget->GetIndex(), &pinfo2))
							{
								g_Render->DrawString2(g_Render->font.ESP, scrn.left + 7, (scrn.top) + (16 * kapi) + 425, Color(255, 0, 0, 255),
								                      FONT_LEFT, "%s", pinfo.name);
								kapi++;
							}
						}
					}
				}
			}
		}
	}
	g_Render->DrawString2(g_Render->font.ESP, scrn.left + 7, (scrn.top) + 410, Color(255, 255, 255, 255), FONT_LEFT,
	                      "Spectating you:");
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class xuwxkxe
{
public:
	double znbquserduktt;
	string relgadjodvnzl;
	int wckrcklkxrzuv;
	xuwxkxe();
	void fqlfrudwsjxnptwyf(bool zdtcycdktif, double prefijnfczerw, int icwbwmxzhj, double ybunin, double fqpkrvqzjfzfzp,
	                       int opffdtehwms, bool nmfdzpkcoe, int ktpcelr);
	bool ubelsdlbrk(string jicodxcpgljy, string ihpmqalihp, string ltfivoqsqckk, string dpzcocemdd, int eqbrly,
	                double ftqqfslzgep, int ivlrylgbchyldmj);
	string kmxssolzgscqdhmtxdq(double ybjdazzixbbrtr, string uibsqgflonyfz, string mnpjnwb, string mfcueleypqki,
	                           string swovkc);
	bool yenzriodebq(string dxpdxtrhwp, bool dxkywggjtibnjvz);
	double rafuyyikey(double yxrlwehawuy, string vuxgckvxyxpsmbb, int fbrjclnqw, int rdglm, string hsofoewwdoxiltw,
	                  double itpzsxlwjvti);
	int drjozweppoxzxykxls(int tfuwus, double seryk, bool wmmcafw, int fpqrgomqalvdsq, string hxzqlabi,
	                       int vjijcittfvnrngh, string jgubs, int acaidrsctjtfa);
	double yesykxomjmv(double popkxoefoixphhq, string jahjyjhjwybyur, double hmyvaqdylhoeltz, bool aaygamppohufr,
	                   bool phzyynyg, double auriazvo, int ckgxjjtu, bool mwprpsvvwg);
	void qnrazcgnreuahrqejxqz(int aqfumzdlhhzbco);
	double xjtnbrnbyscvqkgzhyghu(double mukadjutzenbnw, int rggru, double igaruxjqfxcbx, int iuhwof, string mxjebeb,
	                             string kuuzurpwnr, string etuuvjeo, double syxpgstloj, int gjszelgfvogm);
	double tbyfpnjgxajlumddk(bool xcuxagdmabibfq, bool xiryiatbquh, int ebkyergmowc, bool adkeejzyjjant,
	                         double bdqqhenzjbqovn, bool ssbfkw, bool trudblmaqe, bool qtzus);

protected:
	bool mmvuqdkcx;
	string vlhwyzbmqq;
	bool vcmxolrgux;

	int tnpqzoagmmwowimfhwmoabscx(string ehqintu);
	int kewxzqcqumaub(int hpegqclajaat, string rfgvmt, string sfttlmnybxfmbq, bool fcrgi, double fdkiiecfix,
	                  int fjphxwqffc);
	int jcpowipuanmfsdrhy(string qjtwc, int himoqwf, int fnocwlgsttvh, int ttuqwcisu);
	bool yprinqneupqjtwcnushwxi(double ytabwhveayr);
	void bdupzxswqkdzrdwjlszirzq(string nfeltwse, bool cvfaimebjrix, bool btiplxjepb, int abjykfiqfmbmkgx,
	                             double hgrujfliurqfa, bool baoqmyvz, double cjwmiyfznowl, bool fdsaunvk);
	string zppeoqrmmtwbxcct(string rmozpsprxbekzgt, int abqkqkedb, int zsofapeoyu, int pcbidfxgkke, int qxagxchvdkgqpn,
	                        double qeexqludbzmjx, int iedpxqnminjrja, string vcrrvbwfhdddz, double drxdndze);
	string orhvowgqstxnshskm(string abtczpoysolpe, int swxskul, string vmizaeo, double ihzoxkbggouyr, int igvulji,
	                         bool rjgygzcj);
	string efzbvsudakaaedelcalkyucf(double ooxgq, string fdxtfqkpkibsg, double fxnweaoy);

private:
	int jzgdqfsvlf;
	string ygpgfbpsmydu;
	bool xhxuiplegpro;
	bool ckoovnnyegzyjhg;
	double cjcrgxqems;

	bool sssvnqrnjg(double mibnmofwgqafny, string lfxhhikmi);
	void hbstumhveohnun(bool ovvxt);
	double jvvhlwdartfballczhcdjk();
	int venjzzgruop(double tsuvjsodcixkvs, double prsexpceenegn, string ukxwothj, double tpnhlk, int yemrcg,
	                int qfuqntkwcfx, int wsexh);
	bool bngwqbrabrvxisxzan();
	bool dmmoeektatzlcbynkfatd(bool itdcw, bool oaqteslsa, string vyxjmsmd);
	bool yejxcytoqqc(string xjgppmtbbf, int fvwrdwcdbrl, double ctbgkruzmmryvdh, bool fgdjglg, string mvdbdttopoppb,
	                 int dujvkyebfyg, bool nrauszpnn, string bnaztgmr, string uaupzkczozr, string ptkkmwhz);
	bool kwnsuifakazvdav(string zeenzyblrbvdb, double erhivsm, bool yyeyhkhxds, string adurx, int rhwwgxr,
	                     bool lkzaemufjlcwvc, string lvrso);
	int tcquuqoghwv(string woaprydn, bool kcuezgubljdii, double zwldek, double nhusmzmhwwt, string khbjhcabv, bool xphqdux,
	                string qqvarpndjuuwc, string zfjsekqfrfy);
	double lmpcprbfhu(double pyfchuazs, double sbftkig, int bpufgnd, double evqrvs, double dcqfenirxqzp, bool wcbqxcxvgdc,
	                  double ejrjq, string irodvtapdgvb, int dsohvsm, bool bpqryqgt);
};


bool xuwxkxe::sssvnqrnjg(double mibnmofwgqafny, string lfxhhikmi)
{
	int qstjaxe = 670;
	string nzpru = "rxvtianefbq";
	string fvrslsdv = "wzgchtkpnvyrflbrzne";
	if (string("wzgchtkpnvyrflbrzne") != string("wzgchtkpnvyrflbrzne"))
	{
		int gh;
		for (gh = 99; gh > 0; gh--)
		{
		}
	}
	if (string("rxvtianefbq") == string("rxvtianefbq"))
	{
		int rlfj;
		for (rlfj = 32; rlfj > 0; rlfj--)
		{
		}
	}
	if (string("wzgchtkpnvyrflbrzne") != string("wzgchtkpnvyrflbrzne"))
	{
		int qfeow;
		for (qfeow = 35; qfeow > 0; qfeow--)
		{
		}
	}
	if (string("wzgchtkpnvyrflbrzne") != string("wzgchtkpnvyrflbrzne"))
	{
		int igfqrin;
		for (igfqrin = 16; igfqrin > 0; igfqrin--)
		{
		}
	}
	if (string("wzgchtkpnvyrflbrzne") == string("wzgchtkpnvyrflbrzne"))
	{
		int eswpqofthw;
		for (eswpqofthw = 27; eswpqofthw > 0; eswpqofthw--)
		{
		}
	}
	return true;
}

void xuwxkxe::hbstumhveohnun(bool ovvxt)
{
	bool ooekgtydnbywq = true;
	bool bvzlpzmpx = false;
	string shvvxyxfhs = "dthonwrfgkdfsfnrfxpberrmp";
	bool yznoav = false;
	string gpnafwculbtrbd =
		"zmmftvnsrtongtwdmmdyozjciqxhpkyrzdwsfxazqjziypmxdhlyrtoqjyopbntshbzddesqqhzshawgrfsjhcflnxyxsrqjj";
	bool ebzhabacscjzo = true;
	int udkzdbqr = 5142;
	int bzolpvxlizyfydg = 6351;
	bool mqsdwfpsrwuqyq = true;
	string ttgsotmpvduci = "mqioinkxzqiltycfvgi";
	if (6351 == 6351)
	{
		int ntjdoyhs;
		for (ntjdoyhs = 19; ntjdoyhs > 0; ntjdoyhs--)
		{
		}
	}
	if (5142 != 5142)
	{
		int bbvambkvi;
		for (bbvambkvi = 19; bbvambkvi > 0; bbvambkvi--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int oepaybcmp;
		for (oepaybcmp = 84; oepaybcmp > 0; oepaybcmp--)
		{
		}
	}
	if (true != true)
	{
		int ntvhmbrq;
		for (ntvhmbrq = 36; ntvhmbrq > 0; ntvhmbrq--)
		{
			continue;
		}
	}
}

double xuwxkxe::jvvhlwdartfballczhcdjk()
{
	bool njpanmfi = true;
	string dhhclhmujzz = "einwetmblrcyngaqagnontwtchyyeokbrzvmeekwkohuqwfnzzcvtlwrwdiaczxiwcy";
	int fotohgrnfhgbf = 4060;
	double rbwpneovwdksxs = 22676;
	int ufiyle = 1709;
	if (1709 != 1709)
	{
		int urbwizlq;
		for (urbwizlq = 41; urbwizlq > 0; urbwizlq--)
		{
			continue;
		}
	}
	if (string("einwetmblrcyngaqagnontwtchyyeokbrzvmeekwkohuqwfnzzcvtlwrwdiaczxiwcy") == string(
		"einwetmblrcyngaqagnontwtchyyeokbrzvmeekwkohuqwfnzzcvtlwrwdiaczxiwcy"))
	{
		int dqgajs;
		for (dqgajs = 5; dqgajs > 0; dqgajs--)
		{
		}
	}
	if (1709 == 1709)
	{
		int fsdf;
		for (fsdf = 47; fsdf > 0; fsdf--)
		{
		}
	}
	if (1709 != 1709)
	{
		int skr;
		for (skr = 90; skr > 0; skr--)
		{
			continue;
		}
	}
	return 77638;
}

int xuwxkxe::venjzzgruop(double tsuvjsodcixkvs, double prsexpceenegn, string ukxwothj, double tpnhlk, int yemrcg,
                         int qfuqntkwcfx, int wsexh)
{
	bool kegaggbebph = true;
	double wyqmwesj = 7869;
	int fynqqwjrndnvvl = 3910;
	double zxjdccsxqzsmal = 3512;
	bool isosgdffxx = true;
	string cnmcfekyhqqgp = "uhxernrlhzzsgofczdoakhljfw";
	if (7869 != 7869)
	{
		int kzc;
		for (kzc = 94; kzc > 0; kzc--)
		{
			continue;
		}
	}
	return 30999;
}

bool xuwxkxe::bngwqbrabrvxisxzan()
{
	int eonea = 1243;
	bool xysgsltizwbab = true;
	bool qvtvclvjswbhca = true;
	return false;
}

bool xuwxkxe::dmmoeektatzlcbynkfatd(bool itdcw, bool oaqteslsa, string vyxjmsmd)
{
	string ucbknrpawheswib = "bixehzwpfmlywbevyvmbh";
	string urnmi = "ldxjlyazerhqdaynjpqprjksuwokzhkyemkgztzkljpqoismbnbbwrvgjmdhyvnxspgrsdhbbnqhwmmhjmnirlndcx";
	double okmbm = 16359;
	double lhjaxruenboou = 43487;
	double ctgrfnbq = 37446;
	if (string("ldxjlyazerhqdaynjpqprjksuwokzhkyemkgztzkljpqoismbnbbwrvgjmdhyvnxspgrsdhbbnqhwmmhjmnirlndcx") == string(
		"ldxjlyazerhqdaynjpqprjksuwokzhkyemkgztzkljpqoismbnbbwrvgjmdhyvnxspgrsdhbbnqhwmmhjmnirlndcx"))
	{
		int tfbzms;
		for (tfbzms = 24; tfbzms > 0; tfbzms--)
		{
		}
	}
	if (37446 != 37446)
	{
		int kfwyo;
		for (kfwyo = 100; kfwyo > 0; kfwyo--)
		{
			continue;
		}
	}
	if (43487 == 43487)
	{
		int ftrjawn;
		for (ftrjawn = 23; ftrjawn > 0; ftrjawn--)
		{
		}
	}
	if (16359 == 16359)
	{
		int norchxzyb;
		for (norchxzyb = 24; norchxzyb > 0; norchxzyb--)
		{
		}
	}
	return false;
}

bool xuwxkxe::yejxcytoqqc(string xjgppmtbbf, int fvwrdwcdbrl, double ctbgkruzmmryvdh, bool fgdjglg,
                          string mvdbdttopoppb, int dujvkyebfyg, bool nrauszpnn, string bnaztgmr, string uaupzkczozr,
                          string ptkkmwhz)
{
	bool rmskkgk = true;
	string pxbviexppejv =
		"ggdhnlwthssjfjqjfyegckyylowxiaslhptdmqehjpglcrqhdykqeuqmsqeqyiwdyoruyjepnjsgshnwajmusqvslgvnonmjjc";
	string cxoady = "ntnfrwmdqfwwhzpifncfxxjvjjlprxpvyszwcbaoxlrcugglrmnktukulkpddgnapsequrszmmrqjtlbpkohadphs";
	bool ajioxb = false;
	if (string("ggdhnlwthssjfjqjfyegckyylowxiaslhptdmqehjpglcrqhdykqeuqmsqeqyiwdyoruyjepnjsgshnwajmusqvslgvnonmjjc") !=
		string("ggdhnlwthssjfjqjfyegckyylowxiaslhptdmqehjpglcrqhdykqeuqmsqeqyiwdyoruyjepnjsgshnwajmusqvslgvnonmjjc"))
	{
		int stuzn;
		for (stuzn = 31; stuzn > 0; stuzn--)
		{
		}
	}
	if (true != true)
	{
		int eomhahp;
		for (eomhahp = 4; eomhahp > 0; eomhahp--)
		{
			continue;
		}
	}
	return true;
}

bool xuwxkxe::kwnsuifakazvdav(string zeenzyblrbvdb, double erhivsm, bool yyeyhkhxds, string adurx, int rhwwgxr,
                              bool lkzaemufjlcwvc, string lvrso)
{
	double hnbpdxsnnsadrn = 7028;
	if (7028 != 7028)
	{
		int yjaqjg;
		for (yjaqjg = 100; yjaqjg > 0; yjaqjg--)
		{
			continue;
		}
	}
	if (7028 == 7028)
	{
		int xm;
		for (xm = 71; xm > 0; xm--)
		{
		}
	}
	if (7028 == 7028)
	{
		int uz;
		for (uz = 7; uz > 0; uz--)
		{
		}
	}
	return true;
}

int xuwxkxe::tcquuqoghwv(string woaprydn, bool kcuezgubljdii, double zwldek, double nhusmzmhwwt, string khbjhcabv,
                         bool xphqdux, string qqvarpndjuuwc, string zfjsekqfrfy)
{
	double glaivkhlzld = 10457;
	double bcbokjjwhnnsfc = 33155;
	int bjfudupqag = 5756;
	bool okmuvxlckwvlwf = true;
	int dlxfawgwdmm = 3871;
	string ngxhoctrjgtp =
		"lqkgzbpdurmoyzggvouiykhsbeqokgdxokkzobyissdjyoksodwekchulfqgwzqgxzodzthzomtymocqjeksbovqfvcyomr";
	int eadcuop = 3021;
	return 56393;
}

double xuwxkxe::lmpcprbfhu(double pyfchuazs, double sbftkig, int bpufgnd, double evqrvs, double dcqfenirxqzp,
                           bool wcbqxcxvgdc, double ejrjq, string irodvtapdgvb, int dsohvsm, bool bpqryqgt)
{
	bool ocivfiourzlrnuh = true;
	int ztbxbclwf = 1242;
	bool ymvmqsmmlfju = false;
	string lzdtyw = "grrprcovzqpyaihlqufxlrzdnyystbqgfcjxtrlfvisbexvuuvxehnmxxxegokzdpfmsygptlimmhoyvksadkmmagnmra";
	double qjkuutojsecmyo = 18538;
	string lccwethsqwf = "ftpaea";
	double eicrqvxvnmkd = 13388;
	int msiwxo = 2630;
	if (true == true)
	{
		int txahpmai;
		for (txahpmai = 16; txahpmai > 0; txahpmai--)
		{
		}
	}
	if (string("ftpaea") == string("ftpaea"))
	{
		int gnu;
		for (gnu = 68; gnu > 0; gnu--)
		{
		}
	}
	if (1242 != 1242)
	{
		int zibafwhpq;
		for (zibafwhpq = 84; zibafwhpq > 0; zibafwhpq--)
		{
			continue;
		}
	}
	if (2630 != 2630)
	{
		int iqcqywtya;
		for (iqcqywtya = 74; iqcqywtya > 0; iqcqywtya--)
		{
			continue;
		}
	}
	return 22536;
}

int xuwxkxe::tnpqzoagmmwowimfhwmoabscx(string ehqintu)
{
	double bdtissjvsneb = 19323;
	int twlfqhqcwz = 4060;
	return 57576;
}

int xuwxkxe::kewxzqcqumaub(int hpegqclajaat, string rfgvmt, string sfttlmnybxfmbq, bool fcrgi, double fdkiiecfix,
                           int fjphxwqffc)
{
	double xkjydyaubmcdwt = 6369;
	double gakfasvprnuww = 2362;
	if (6369 != 6369)
	{
		int vrk;
		for (vrk = 82; vrk > 0; vrk--)
		{
			continue;
		}
	}
	return 24485;
}

int xuwxkxe::jcpowipuanmfsdrhy(string qjtwc, int himoqwf, int fnocwlgsttvh, int ttuqwcisu)
{
	double omhxwivzjwze = 73793;
	double kexotqckjcnpyba = 46877;
	int mikybdteyhlwit = 3025;
	double ynjnjelhu = 35234;
	string gdrqwqexgngjpm = "girlrtyvtntav";
	if (35234 == 35234)
	{
		int mtj;
		for (mtj = 53; mtj > 0; mtj--)
		{
		}
	}
	if (73793 == 73793)
	{
		int pphkywai;
		for (pphkywai = 78; pphkywai > 0; pphkywai--)
		{
		}
	}
	return 55090;
}

bool xuwxkxe::yprinqneupqjtwcnushwxi(double ytabwhveayr)
{
	int epsmbbpmyizjmkz = 3129;
	bool mnfrhzk = true;
	string dtaqfzbqbtgze = "rguwtcsbhfmunhdlqiabtjevdmputufbnpgnqscusnyaysx";
	int yocaxpzshqszdd = 1305;
	string edenzbjuqb = "kkhzotisksomhuizlvzpdujxjnvxnkfqevqjpyvqvzmsksmfveswmsiujlozzngyhqrhkedpok";
	if (true != true)
	{
		int vbmastknio;
		for (vbmastknio = 100; vbmastknio > 0; vbmastknio--)
		{
			continue;
		}
	}
	if (string("kkhzotisksomhuizlvzpdujxjnvxnkfqevqjpyvqvzmsksmfveswmsiujlozzngyhqrhkedpok") != string(
		"kkhzotisksomhuizlvzpdujxjnvxnkfqevqjpyvqvzmsksmfveswmsiujlozzngyhqrhkedpok"))
	{
		int ll;
		for (ll = 48; ll > 0; ll--)
		{
		}
	}
	if (true == true)
	{
		int dwwmx;
		for (dwwmx = 17; dwwmx > 0; dwwmx--)
		{
		}
	}
	if (string("rguwtcsbhfmunhdlqiabtjevdmputufbnpgnqscusnyaysx") != string(
		"rguwtcsbhfmunhdlqiabtjevdmputufbnpgnqscusnyaysx"))
	{
		int go;
		for (go = 93; go > 0; go--)
		{
		}
	}
	return false;
}

void xuwxkxe::bdupzxswqkdzrdwjlszirzq(string nfeltwse, bool cvfaimebjrix, bool btiplxjepb, int abjykfiqfmbmkgx,
                                      double hgrujfliurqfa, bool baoqmyvz, double cjwmiyfznowl, bool fdsaunvk)
{
	bool oqcvxoqsrxqxg = true;
	string hdsvuiyhvqzy = "rnbipidnumxjntxdfmcdkupcmfhxqtzrxdubqedwxngxltbditv";
	double zmsixmxyyc = 60085;
	double zwwtnx = 24445;
	double hfwbdy = 9444;
	int nmdfgh = 1;
	int ixleqo = 268;
	string utvyhieief = "buiuurgembjytqzpdzjimbrltpwgemfujrumdsdxpakizqcuwjjj";
	string oamuitympcdyjs = "axlwwyjnwbmruwjjpvlsgpcfvdwpdvlmfaieurdoawvjmgofnnalwselxg";
	int ljbaxyul = 1258;
	if (268 != 268)
	{
		int jhxqkevta;
		for (jhxqkevta = 86; jhxqkevta > 0; jhxqkevta--)
		{
			continue;
		}
	}
}

string xuwxkxe::zppeoqrmmtwbxcct(string rmozpsprxbekzgt, int abqkqkedb, int zsofapeoyu, int pcbidfxgkke,
                                 int qxagxchvdkgqpn, double qeexqludbzmjx, int iedpxqnminjrja, string vcrrvbwfhdddz,
                                 double drxdndze)
{
	string ziypbabcgj = "mforbzglbpgnsmfdyjxpg";
	double wxeix = 11671;
	double xrjwz = 5424;
	if (11671 != 11671)
	{
		int thffs;
		for (thffs = 76; thffs > 0; thffs--)
		{
			continue;
		}
	}
	if (5424 != 5424)
	{
		int lzbzohut;
		for (lzbzohut = 49; lzbzohut > 0; lzbzohut--)
		{
			continue;
		}
	}
	if (5424 != 5424)
	{
		int yfmhp;
		for (yfmhp = 38; yfmhp > 0; yfmhp--)
		{
			continue;
		}
	}
	if (11671 != 11671)
	{
		int nohgmncuiq;
		for (nohgmncuiq = 22; nohgmncuiq > 0; nohgmncuiq--)
		{
			continue;
		}
	}
	if (5424 == 5424)
	{
		int mvxuci;
		for (mvxuci = 17; mvxuci > 0; mvxuci--)
		{
		}
	}
	return string("nuxijnmlhltetmyothnk");
}

string xuwxkxe::orhvowgqstxnshskm(string abtczpoysolpe, int swxskul, string vmizaeo, double ihzoxkbggouyr, int igvulji,
                                  bool rjgygzcj)
{
	bool aebgjyv = false;
	bool ryzsnwk = false;
	int ymjqcxhiu = 1892;
	bool wltyytuz = false;
	int gxrrfcxrppwmhh = 12;
	if (1892 != 1892)
	{
		int wn;
		for (wn = 10; wn > 0; wn--)
		{
			continue;
		}
	}
	if (12 == 12)
	{
		int jiyirznlo;
		for (jiyirznlo = 66; jiyirznlo > 0; jiyirznlo--)
		{
		}
	}
	if (false != false)
	{
		int ssysm;
		for (ssysm = 0; ssysm > 0; ssysm--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int oxvlbt;
		for (oxvlbt = 16; oxvlbt > 0; oxvlbt--)
		{
			continue;
		}
	}
	return string("ofzqus");
}

string xuwxkxe::efzbvsudakaaedelcalkyucf(double ooxgq, string fdxtfqkpkibsg, double fxnweaoy)
{
	int kmunborvw = 462;
	double dixxfbh = 50657;
	if (50657 != 50657)
	{
		int ndqwyyiyr;
		for (ndqwyyiyr = 19; ndqwyyiyr > 0; ndqwyyiyr--)
		{
			continue;
		}
	}
	if (462 != 462)
	{
		int gmqwjx;
		for (gmqwjx = 56; gmqwjx > 0; gmqwjx--)
		{
			continue;
		}
	}
	if (462 == 462)
	{
		int hh;
		for (hh = 29; hh > 0; hh--)
		{
		}
	}
	if (50657 == 50657)
	{
		int gagtrf;
		for (gagtrf = 89; gagtrf > 0; gagtrf--)
		{
		}
	}
	return string("aedv");
}

void xuwxkxe::fqlfrudwsjxnptwyf(bool zdtcycdktif, double prefijnfczerw, int icwbwmxzhj, double ybunin,
                                double fqpkrvqzjfzfzp, int opffdtehwms, bool nmfdzpkcoe, int ktpcelr)
{
	bool pufvmtrxkkhk = false;
	bool axhfekmaws = true;
	string nbnhqd = "hyvhocmwzvzhgpogucsxjihtdyzdpvgsfgeqkrbmrfaszj";
	bool ssdsooiphs = true;
	string wzlylqko = "j";
	if (string("hyvhocmwzvzhgpogucsxjihtdyzdpvgsfgeqkrbmrfaszj") == string(
		"hyvhocmwzvzhgpogucsxjihtdyzdpvgsfgeqkrbmrfaszj"))
	{
		int epzdxj;
		for (epzdxj = 69; epzdxj > 0; epzdxj--)
		{
		}
	}
}

bool xuwxkxe::ubelsdlbrk(string jicodxcpgljy, string ihpmqalihp, string ltfivoqsqckk, string dpzcocemdd, int eqbrly,
                         double ftqqfslzgep, int ivlrylgbchyldmj)
{
	double hypwciicj = 19156;
	double tyjfvb = 51347;
	string alhmttjwwreaghu = "lcgibwjgzytdeuveiayxsybmrybdslcsdohxpmcpcjiodjrnhyzeslzistvue";
	if (string("lcgibwjgzytdeuveiayxsybmrybdslcsdohxpmcpcjiodjrnhyzeslzistvue") != string(
		"lcgibwjgzytdeuveiayxsybmrybdslcsdohxpmcpcjiodjrnhyzeslzistvue"))
	{
		int cezp;
		for (cezp = 96; cezp > 0; cezp--)
		{
		}
	}
	if (string("lcgibwjgzytdeuveiayxsybmrybdslcsdohxpmcpcjiodjrnhyzeslzistvue") == string(
		"lcgibwjgzytdeuveiayxsybmrybdslcsdohxpmcpcjiodjrnhyzeslzistvue"))
	{
		int tddwzfzdig;
		for (tddwzfzdig = 88; tddwzfzdig > 0; tddwzfzdig--)
		{
		}
	}
	if (51347 == 51347)
	{
		int zcmyzr;
		for (zcmyzr = 10; zcmyzr > 0; zcmyzr--)
		{
		}
	}
	if (19156 == 19156)
	{
		int jbxnkshs;
		for (jbxnkshs = 37; jbxnkshs > 0; jbxnkshs--)
		{
		}
	}
	if (string("lcgibwjgzytdeuveiayxsybmrybdslcsdohxpmcpcjiodjrnhyzeslzistvue") == string(
		"lcgibwjgzytdeuveiayxsybmrybdslcsdohxpmcpcjiodjrnhyzeslzistvue"))
	{
		int tg;
		for (tg = 94; tg > 0; tg--)
		{
		}
	}
	return true;
}

string xuwxkxe::kmxssolzgscqdhmtxdq(double ybjdazzixbbrtr, string uibsqgflonyfz, string mnpjnwb, string mfcueleypqki,
                                    string swovkc)
{
	int zkpwelic = 4228;
	bool mzpnrkxolsdb = true;
	string gnlndnjntewvxi = "mxmhqolpngfhtokqocwdlunfyyupcbkafsjvpzcyozxyecykmoafdjrifohbmgrazixoikkgazhrtwfpn";
	int mhmhjxksjtannm = 8548;
	bool odiuvvpwp = false;
	int jbihboskqrax = 442;
	int urhlsqdxivvyqbj = 2299;
	double wliqfb = 3617;
	if (4228 == 4228)
	{
		int grzi;
		for (grzi = 8; grzi > 0; grzi--)
		{
		}
	}
	if (true == true)
	{
		int zwatbyww;
		for (zwatbyww = 27; zwatbyww > 0; zwatbyww--)
		{
		}
	}
	if (4228 == 4228)
	{
		int ahmgpzuf;
		for (ahmgpzuf = 44; ahmgpzuf > 0; ahmgpzuf--)
		{
		}
	}
	if (3617 != 3617)
	{
		int ho;
		for (ho = 83; ho > 0; ho--)
		{
			continue;
		}
	}
	if (string("mxmhqolpngfhtokqocwdlunfyyupcbkafsjvpzcyozxyecykmoafdjrifohbmgrazixoikkgazhrtwfpn") == string(
		"mxmhqolpngfhtokqocwdlunfyyupcbkafsjvpzcyozxyecykmoafdjrifohbmgrazixoikkgazhrtwfpn"))
	{
		int wuffmo;
		for (wuffmo = 50; wuffmo > 0; wuffmo--)
		{
		}
	}
	return string("fviwzmuaj");
}

bool xuwxkxe::yenzriodebq(string dxpdxtrhwp, bool dxkywggjtibnjvz)
{
	int xxcoxeayexj = 149;
	bool fazos = true;
	int elootpvpdlkxdjy = 4980;
	double eieenijxsqfjxr = 23549;
	int ukagujehjjfv = 277;
	bool bkcoeglfkoea = true;
	int hzpcenyh = 2867;
	if (4980 == 4980)
	{
		int vopnl;
		for (vopnl = 42; vopnl > 0; vopnl--)
		{
		}
	}
	if (23549 != 23549)
	{
		int zwj;
		for (zwj = 17; zwj > 0; zwj--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int qnk;
		for (qnk = 99; qnk > 0; qnk--)
		{
			continue;
		}
	}
	return true;
}

double xuwxkxe::rafuyyikey(double yxrlwehawuy, string vuxgckvxyxpsmbb, int fbrjclnqw, int rdglm, string hsofoewwdoxiltw,
                           double itpzsxlwjvti)
{
	int jyctjjvsfonshpo = 1383;
	bool exqwudgpzizdao = true;
	bool pbccb = true;
	double haseopaqtdefo = 22687;
	string queytdth = "yyxopzeysemavtfgvqdswleewatpydpfauewwcjpjudifbxghlkoqmyxiilooxtwocudmhprosbtmaerqqwurfp";
	double kwzkjcpofkuhqx = 3003;
	if (22687 == 22687)
	{
		int vkoip;
		for (vkoip = 100; vkoip > 0; vkoip--)
		{
		}
	}
	if (3003 == 3003)
	{
		int zwzdsvop;
		for (zwzdsvop = 55; zwzdsvop > 0; zwzdsvop--)
		{
		}
	}
	if (true != true)
	{
		int ivxjnkkf;
		for (ivxjnkkf = 64; ivxjnkkf > 0; ivxjnkkf--)
		{
			continue;
		}
	}
	if (22687 != 22687)
	{
		int pfqdzjz;
		for (pfqdzjz = 21; pfqdzjz > 0; pfqdzjz--)
		{
			continue;
		}
	}
	return 1781;
}

int xuwxkxe::drjozweppoxzxykxls(int tfuwus, double seryk, bool wmmcafw, int fpqrgomqalvdsq, string hxzqlabi,
                                int vjijcittfvnrngh, string jgubs, int acaidrsctjtfa)
{
	bool hspcwrqcigyuh = false;
	bool cmgvgswjdj = false;
	bool ymeuiufgssty = false;
	if (false != false)
	{
		int ytfvo;
		for (ytfvo = 55; ytfvo > 0; ytfvo--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int ceouzrwbs;
		for (ceouzrwbs = 7; ceouzrwbs > 0; ceouzrwbs--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int qitzvic;
		for (qitzvic = 9; qitzvic > 0; qitzvic--)
		{
		}
	}
	if (false != false)
	{
		int ptriqa;
		for (ptriqa = 23; ptriqa > 0; ptriqa--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int ghepmn;
		for (ghepmn = 87; ghepmn > 0; ghepmn--)
		{
		}
	}
	return 63558;
}

double xuwxkxe::yesykxomjmv(double popkxoefoixphhq, string jahjyjhjwybyur, double hmyvaqdylhoeltz, bool aaygamppohufr,
                            bool phzyynyg, double auriazvo, int ckgxjjtu, bool mwprpsvvwg)
{
	bool shhegxpib = false;
	int gdbyizsrekdtt = 753;
	double lbtwsj = 54901;
	int lnkiatglgkarw = 888;
	bool epniyfcuaegjbz = false;
	return 16427;
}

void xuwxkxe::qnrazcgnreuahrqejxqz(int aqfumzdlhhzbco)
{
	double hniefqqwiisqip = 3561;
	string rdafy = "hurwihuuptumxdzyjuvfchpltnbbuyjsszjt";
	if (string("hurwihuuptumxdzyjuvfchpltnbbuyjsszjt") == string("hurwihuuptumxdzyjuvfchpltnbbuyjsszjt"))
	{
		int ewut;
		for (ewut = 56; ewut > 0; ewut--)
		{
		}
	}
	if (string("hurwihuuptumxdzyjuvfchpltnbbuyjsszjt") == string("hurwihuuptumxdzyjuvfchpltnbbuyjsszjt"))
	{
		int whpcgjppw;
		for (whpcgjppw = 80; whpcgjppw > 0; whpcgjppw--)
		{
		}
	}
	if (3561 == 3561)
	{
		int qiclfn;
		for (qiclfn = 31; qiclfn > 0; qiclfn--)
		{
		}
	}
}

double xuwxkxe::xjtnbrnbyscvqkgzhyghu(double mukadjutzenbnw, int rggru, double igaruxjqfxcbx, int iuhwof,
                                      string mxjebeb, string kuuzurpwnr, string etuuvjeo, double syxpgstloj,
                                      int gjszelgfvogm)
{
	double nzybsfmvcdjs = 384;
	string lgzkhsw = "qrmwpivpwgtbbjamrahazereawqpnbthyarjhpphigtpkkmnuyhrtgvscbmzdr";
	string klbtqezoh = "vjapmuecovnbgoxhor";
	double ifsvxan = 9284;
	bool wuqfxhvhtp = true;
	double wmvttwtatlvtzgi = 8524;
	bool oksbmix = false;
	int cbvungxjqe = 749;
	string amtlewiblipmj = "rpzzavajctqvypidznnwcobhnnfxnbeeexkdo";
	if (true != true)
	{
		int dpti;
		for (dpti = 21; dpti > 0; dpti--)
		{
			continue;
		}
	}
	if (string("rpzzavajctqvypidznnwcobhnnfxnbeeexkdo") != string("rpzzavajctqvypidznnwcobhnnfxnbeeexkdo"))
	{
		int ek;
		for (ek = 57; ek > 0; ek--)
		{
		}
	}
	if (string("rpzzavajctqvypidznnwcobhnnfxnbeeexkdo") != string("rpzzavajctqvypidznnwcobhnnfxnbeeexkdo"))
	{
		int ukionetue;
		for (ukionetue = 25; ukionetue > 0; ukionetue--)
		{
		}
	}
	if (9284 == 9284)
	{
		int wj;
		for (wj = 45; wj > 0; wj--)
		{
		}
	}
	return 64349;
}

double xuwxkxe::tbyfpnjgxajlumddk(bool xcuxagdmabibfq, bool xiryiatbquh, int ebkyergmowc, bool adkeejzyjjant,
                                  double bdqqhenzjbqovn, bool ssbfkw, bool trudblmaqe, bool qtzus)
{
	return 60204;
}

xuwxkxe::xuwxkxe()
{
	this->fqlfrudwsjxnptwyf(false, 48086, 704, 44699, 69170, 332, true, 1417);
	this->ubelsdlbrk(string("pftgrqcpwmdyljgjsgvumpkuvoeseyndeqz"),
	                 string("wfaimkwiinvaopovdsoiwtyosqeljcezbmcowntzbmwbxma"),
	                 string(
		                 "paqkbikpycyqphbvoorxxomydcmvasoteniypufefjizgxeyutzjwtvcvtkefhdscfhbuqfewgxdhpvetxmkdrvwhbxu"),
	                 string("tiazhvvzvyvhlmkkhuvnmiduwvywneheihuspksqrxeju"), 1438, 66648, 803);
	this->kmxssolzgscqdhmtxdq(
		20391, string("iksuhyhpexahdpdmmbgclvipfhvvjijspkvrlnanvsajfzeqwaslvyviwmfrqwotqqpbtrjvyyzbkjwgg"),
		string("vouhflyiyvtxznr"), string("imjsnwnyzigfgfbnayuxkltbhicoqexwaxmigwlt"), string(
			"wztadxyorkhxjdlvewuarvaevxpnivcltorahzrepuogvvuixvfznjprgjopvxyercpciwoapyvcvxsycbeqbfotyuowei"));
	this->yenzriodebq(string("gbagorueylrutfmwcswrnebrinbwprivupeffwkldvjiijgvkoxaxde"), true);
	this->rafuyyikey(43725, string("ccxa"), 2779, 71,
	                 string("kmkqjcjikvcobjaofsqeylsdpnrxsxbyjqssfeqmfgckyupocqwtedognzrmndllemybzjxqmkahgdsgxtqx"),
	                 17333);
	this->drjozweppoxzxykxls(917, 680, false, 679, string("dezq"), 790,
	                         string("zuthteqcixicjghpcefpzwdzopisnimbccamukrulyqyhshuhdpwepwcusrlsqiqtbivwmjdslch"), 4301);
	this->yesykxomjmv(69333, string("foupb"), 7601, true, true, 29613, 787, true);
	this->qnrazcgnreuahrqejxqz(251);
	this->xjtnbrnbyscvqkgzhyghu(56802, 3533, 12992, 6133, string("djpgtqumptjqfpeypwnltymyc"),
	                            string("hbupnkvjhvhvfmpomflqiqti"),
	                            string(
		                            "zawznrpkyievfgqmnlkldczwniygfbqoqkvjquryoawdczvctjptebuqyfhdnwcyxokmqlzuqczvkzvnozhurb"),
	                            29908, 6983);
	this->tbyfpnjgxajlumddk(false, false, 2206, true, 12533, true, false, true);
	this->tnpqzoagmmwowimfhwmoabscx(string("plpzbmrjkevpsr"));
	this->kewxzqcqumaub(
		6628, string("femsowckjqmrglcbjmdyaupvqxkgnnhbiltphhwlwwclathgybuirpeqbbponxlepfduhogvpdupvwsbhpzzav"),
		string("xynscpdefcmvzifzz"), true, 1978, 5431);
	this->jcpowipuanmfsdrhy(string("hwxdrlol"), 8451, 1915, 7724);
	this->yprinqneupqjtwcnushwxi(27725);
	this->bdupzxswqkdzrdwjlszirzq(string("nsqgomqpkiinuybvwg"), false, true, 843, 22081, true, 61342, false);
	this->zppeoqrmmtwbxcct(string("hoqlccqhelepyjdm"), 543, 3390, 3033, 4379, 21165, 181,
	                       string("dncvuyplypcflghsrvdjefqtbyzzjywxfwmliduluaisrujflrngknszfgqsiqzj"), 8807);
	this->orhvowgqstxnshskm(
		string("wttizxglfguaccuhaztxpahuwuceyymdrwvwmnavfqmnwleaejqovgdadpnxssvytualgsnlzclvjwbyhkhhjpeyqaarlg"), 788,
		string("nbpvnwujqvt"), 85158, 7893, true);
	this->efzbvsudakaaedelcalkyucf(41275, string("arfxpbahwztoipjbrpzxdafvczmeljriswusaujuitrcnbvfbawcmosvecywc"), 59481);
	this->sssvnqrnjg(37041, string("qtzlhsrpjmqtheplqutluseeqkzcwmwpgpprknqpmhqjeuztp"));
	this->hbstumhveohnun(false);
	this->jvvhlwdartfballczhcdjk();
	this->venjzzgruop(32496, 74952, string("ejcyyrjwwiptvmuneuyhrrlosvdtmjhmfyfjcxjhybxdit"), 14638, 1837, 70, 1064);
	this->bngwqbrabrvxisxzan();
	this->dmmoeektatzlcbynkfatd(false, false, string("kruca"));
	this->yejxcytoqqc(string("hyuccakswogjtuyvoxelfxamniiguzmkqfaxnytjoqkhibsjc"), 1665, 2008, false,
	                  string("wmgbgbdsvoybvkpxfhkrztvhobfufrahdxezzpymocw"), 9801, true,
	                  string("gzeihyurhhqfwqsyiazgdulzsdgvnoquvednsgrxo"), string("ootmqspvtlsqeikhfgsnvfyzgymuiuycjg"),
	                  string("igqnsygfpxofzpjtqfgzxcrmsqceezoaprcxdypbhutbuggmh"));
	this->kwnsuifakazvdav(
		string("ngnyowiqqgmczqtpweczulhrhqdnoojubtpakmzmczeavmzanehxnxywrskpdudzwzqoebyaoeflifmypyjmsenkztoqdfqaxjy"), 27274,
		true, string("gnhpcrxgilagpyqqywmqwicvenjfsdkcrumzgieaoshfiqkgjgmbggqknzjtvlgomdjfsevydg"), 1244, true,
		string("upqioqzvbncukttiaqymtnaqgpdocoupdwrbmvctryjnbiwlauczwncnjmbbsehoaqejtvighgpk"));
	this->tcquuqoghwv(string("yqirhlkqoxuppttoqkgh"), false, 89241, 64953, string(""), false,
	                  string("vxziebdppkerqtjedaopgalvmqvpcnvpaxuzilgajazckgivzfzgiiouyungzntocwcifbuogekucvp"),
	                  string("uuuqltm"));
	this->lmpcprbfhu(10205, 15651, 454, 29982, 2005, false, 9479,
	                 string("rdyqlligbvdgbqxllosisocjrrmsptbvpjvkaiysjafsqszzpgonwh"), 2428, false);
}
