//
// Created by A on 01/03/2018.
//

#include "license.h"
using namespace std;
using namespace base64;

namespace license
{
	int getCpuHash()
	{
		int cpuinfo[4] = {0, 0, 0, 0};
		__cpuid(cpuinfo, 0);
		int hash = 0;
		auto* ptr = (int *)(&cpuinfo[0]);
		for (int i = 0; i < 8; i++)
			hash += ptr[i];

		return hash;
	}

	LPSTR getMachineName()
	{
#define INFO_BUFFER_SIZE 32767
		LPSTR infoBuf;
		DWORD bufCharCount = INFO_BUFFER_SIZE;

		GetComputerName(infoBuf, &bufCharCount);

		return infoBuf;
	}

	int getVolumeHash()
	{
		DWORD serialNum = 0;

		// Determine if this volume uses an NTFS file system.
		GetVolumeInformation("c:\\", nullptr, 0, &serialNum, nullptr, nullptr, nullptr, 0);
		auto hash = (int)((serialNum + (serialNum >> 16)) & 0xFFFF);

		return hash;
	}

	string CreateGuid()
	{
		GUID pPuid{};
		CoCreateGuid(&pPuid);

		OLECHAR* guidString;
		StringFromCLSID(pPuid, &guidString);

		wstring ws(guidString);
		string str(ws.begin(), ws.end());

		return str;
	}

	string generateUniqueID()
	{
		string name = getMachineName();
		string cpuHash = to_string(getCpuHash());
		string volumeHash = to_string(getVolumeHash());
		string guid = CreateGuid();

		//string data =  name + "." + cpuHash + "." + volumeHash + "." + guid;

		string data = name + "|" + volumeHash;

		return base64_encode((reinterpret_cast<const unsigned char*>(data.c_str())), data.length());
	}
}
