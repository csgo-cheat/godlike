#pragma once

#include <windows.h>
#include <intrin.h>
#include <string>
#include <atlstr.h>

#include "base64.h"

using namespace std;

namespace license
{
	string generateUniqueID();
	int getVolumeHash();
	int getCpuHash();
	LPSTR getMachineName();
};
