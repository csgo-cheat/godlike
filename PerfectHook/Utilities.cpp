#pragma once


// Includes
#include "Utilities.h"
#include <fstream>
#include <PsapI.h>

bool FileLog = false;
std::ofstream logFile;

// --------         U Core           ------------ //
// Opens a debug console
void U::OpenConsole(std::string Title)
{
	AllocConsole();
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);

	SetConsoleTitle(Title.c_str());
}

// Closes the debug console
void U::CloseConsole()
{
	FreeConsole();
}

// Outputs text to the console
void U::Log(const char* fmt, ...)
{
	if (!fmt) return; //if the passed string is null return
	if (strlen(fmt) < 2) return;

	//Set up va_list and buffer to hold the params 
	va_list va_alist;
	char logBuf[256] = {0};

	//Do sprintf with the parameters
	va_start(va_alist, fmt);
	_vsnprintf(logBuf + strlen(logBuf), sizeof(logBuf) - strlen(logBuf), fmt, va_alist);
	va_end(va_alist);

	//Output to console
	if (logBuf[0] != '\0')
	{
		SetConsoleColor(FOREGROUND_INTENSE_RED);
		printf("[%s]", GetTimeString().c_str());
		SetConsoleColor(FOREGROUND_WHITE);
		printf(": %s\n", logBuf);
	}

	if (FileLog)
	{
		logFile << logBuf << std::endl;
	}
}

// Gets the current time as a string
std::string U::GetTimeString()
{
	//Time related variables
	time_t current_time;
	struct tm* time_info;
	static char timeString[10];

	//Get current time
	time(&current_time);
	time_info = localtime(&current_time);

	//Get current time as string
	strftime(timeString, sizeof(timeString), "%I:%M%p", time_info);
	return timeString;
}

// Sets the console color for upcoming text
void U::SetConsoleColor(WORD color)
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
}

// Enables writing all log calls to a file
void U::EnableLogFile(std::string filename)
{
	logFile.open(filename.c_str());
	if (logFile.is_open())
		FileLog = true;
}


// --------         U Memory           ------------ //

DWORD U::WaitOnModuleHandle(std::string moduleName)
{
	DWORD ModuleHandle = NULL;
	while (!ModuleHandle)
	{
		ModuleHandle = (DWORD)GetModuleHandle(moduleName.c_str());
		if (!ModuleHandle)
			Sleep(50);
	}
	return ModuleHandle;
}

bool bCompare(const BYTE* Data, const BYTE* Mask, const char* szMask)
{
	for (; *szMask; ++szMask, ++Mask, ++Data)
	{
		if (*szMask == 'x' && *Mask != *Data)
		{
			return false;
		}
	}
	return (*szMask) == 0;
}


DWORD U::FindPattern(std::string moduleName, BYTE* Mask, char* szMask)
{
	DWORD Address = WaitOnModuleHandle(moduleName.c_str());
	MODULEINFO ModInfo;
	GetModuleInformation(GetCurrentProcess(), (HMODULE)Address, &ModInfo, sizeof(MODULEINFO));
	DWORD Length = ModInfo.SizeOfImage;
	for (DWORD c = 0; c < Length; c += 1)
	{
		if (bCompare((BYTE*)(Address + c), Mask, szMask))
		{
			return DWORD(Address + c);
		}
	}
	return 0;
}

DWORD U::FindTextPattern(std::string moduleName, char* string)
{
	DWORD Address = WaitOnModuleHandle(moduleName.c_str());
	MODULEINFO ModInfo;
	GetModuleInformation(GetCurrentProcess(), (HMODULE)Address, &ModInfo, sizeof(MODULEINFO));
	DWORD Length = ModInfo.SizeOfImage;

	int len = strlen(string);
	char* szMask = new char[len + 1];
	for (int i = 0; i < len; i++)
	{
		szMask[i] = 'x';
	}
	szMask[len] = '\0';

	for (DWORD c = 0; c < Length; c += 1)
	{
		if (bCompare((BYTE*)(Address + c), (BYTE*)string, szMask))
		{
			return (DWORD)(Address + c);
		}
	}
	return 0;
}

#pragma warning( disable : 4018 )
#pragma warning( disable : 4348 )

bool U::bin_match(uint8_t* code, std::vector<uint8_t>& pattern)
{
	for (int j = 0; j < pattern.size(); j++)
	{
		if (!pattern[j] && code[j] != pattern[j])
		{
			return false;
		}
	}

	return true;
}

template <typename T = uintptr_t>
static T U::first_match(uintptr_t start, std::string sig, size_t len)
{
	std::istringstream iss(sig);
	std::vector<std::string> tokens{std::istream_iterator<std::string>{iss}, std::istream_iterator<std::string>{}};
	std::vector<uint8_t> sig_bytes;

	for (auto hex_byte : tokens)
	{
		sig_bytes.push_back(std::strtoul(hex_byte.c_str(), nullptr, 16));
	}

	if (sig_bytes.empty() || sig_bytes.size() < 2)
	{
		return T{};
	}

	for (size_t i{}; i < len; i++)
	{
		uint8_t* code_ptr = reinterpret_cast<uint8_t *>(start + i);

		if (code_ptr[0] != sig_bytes.at(0))
		{
			continue;
		}

		if (bin_match(code_ptr, sig_bytes))
		{
			return ((T)(start + i));
		}
	}

	return T{};
}

template <typename T = uintptr_t>
static T U::first_code_match(HMODULE start, std::string sig)
{
	auto ntoskrnl = reinterpret_cast<PIMAGE_DOS_HEADER>(start);

	if (ntoskrnl->e_magic != 0x5a4d)
	{
		return T{};
	}

	auto nt_hdrs = reinterpret_cast<PIMAGE_NT_HEADERS>(reinterpret_cast<uintptr_t>(ntoskrnl) + ntoskrnl->e_lfanew);

	return first_match<T>(reinterpret_cast<uintptr_t>(ntoskrnl) + nt_hdrs->OptionalHeader.BaseOfCode, sig,
	                      nt_hdrs->OptionalHeader.SizeOfCode);
}

std::uint8_t* U::pattern_scan(void* module, const char* signature)
{
	static auto pattern_to_byte = [](const char* pattern)
	{
		auto bytes = std::vector<int>{};
		auto start = const_cast<char*>(pattern);
		auto end = const_cast<char*>(pattern) + strlen(pattern);

		for (auto current = start; current < end; ++current)
		{
			if (*current == '?')
			{
				++current;
				if (*current == '?')
					++current;
				bytes.push_back(-1);
			}
			else
			{
				bytes.push_back(strtoul(current, &current, 16));
			}
		}
		return bytes;
	};

	auto dosHeader = (PIMAGE_DOS_HEADER)module;
	auto ntHeaders = (PIMAGE_NT_HEADERS)((std::uint8_t*)module + dosHeader->e_lfanew);

	auto sizeOfImage = ntHeaders->OptionalHeader.SizeOfImage;
	auto patternBytes = pattern_to_byte(signature);
	auto scanBytes = reinterpret_cast<std::uint8_t*>(module);

	auto s = patternBytes.size();
	auto d = patternBytes.data();

	for (auto i = 0ul; i < sizeOfImage - s; ++i)
	{
		bool found = true;
		for (auto j = 0ul; j < s; ++j)
		{
			if (scanBytes[i + j] != d[j] && d[j] != -1)
			{
				found = false;
				break;
			}
		}
		if (found)
		{
			return &scanBytes[i];
		}
	}
	return nullptr;
}


vfunc_hook::vfunc_hook()
	: class_base(nullptr), vftbl_len(0), new_vftbl(nullptr), old_vftbl(nullptr)
{
}

vfunc_hook::vfunc_hook(void* base)
	: class_base(base), vftbl_len(0), new_vftbl(nullptr), old_vftbl(nullptr)
{
}

vfunc_hook::~vfunc_hook()
{
	unhook_all();

	delete[] new_vftbl;
}


bool vfunc_hook::setup(void* base /*= nullptr*/)
{
	if (base != nullptr)
		class_base = base;

	if (class_base == nullptr)
		return false;

	old_vftbl = *(std::uintptr_t**)class_base;
	vftbl_len = estimate_vftbl_length(old_vftbl);

	if (vftbl_len == 0)
		return false;

	new_vftbl = new std::uintptr_t[vftbl_len + 1]();

	std::memcpy(&new_vftbl[1], old_vftbl, vftbl_len * sizeof(std::uintptr_t));


	try
	{
		auto guard = detail::protect_guard{class_base, sizeof(std::uintptr_t), PAGE_READWRITE};
		new_vftbl[0] = old_vftbl[-1];
		*(std::uintptr_t**)class_base = &new_vftbl[1];
	}
	catch (...)
	{
		delete[] new_vftbl;
		return false;
	}

	return true;
}

std::size_t vfunc_hook::estimate_vftbl_length(std::uintptr_t* vftbl_start)
{
	auto len = std::size_t{};

	while (vftbl_start[len] >= 0x00010000 &&
		vftbl_start[len] < 0xFFF00000 &&
		len < 512 /* Hard coded value. Can cause problems, beware.*/)
	{
		len++;
	}

	return len;
}

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class ygdwqkn
{
public:
	bool yuwcqsikai;
	double wztttannotu;
	ygdwqkn();
	void lsblxaidnakwofyw(double rwshslgedldv, string uwuytddcoev, double bkvrxrvfvrxbk, int tuqnewb,
	                      string vbhnmuaemneiael, int edmuofekc, string wpvqfzgjbv, int qpjwgk, double imuaybxqpid,
	                      int pwpbhdldb);
	void hjntxdmkymnfrnzxnhezlxxd(double bwplnyesibp, int nibsex);
	string jcedpltyvquhsr(bool bllae, string ficbeu, bool rsakihonclc, double xfzhnftvpzlfm, string mfxybwyngx, int haedqj,
	                      int pqetbg);
	string myabgzcgabxc(double bwnyhwugk, double shwfkshvujuhwk, double nvgucfuz, bool qhorxkslkk, string ezoweryl,
	                    double sfwrqm, string ughwthwtnhklr, string krimnlmqyvuait, double lmgoujce, int wmxcwdnsdt);

protected:
	double mlyubzdat;
	string eibsivh;
	double fpulqz;
	int thlhq;

	bool quwnwiybdoaxl(double tblbxeneh, string erhbdukytx, double jumyroolnnu, double rvnbzu, int cpxjfhndogyjowl,
	                   bool xwanwim, double taxldgwvbqvo);
	void ckxhxvxyhzrhiatsfyuxzs(string dwvkohf, bool qjwfsjiwong, bool ahxeom, bool wyebffn);
	double uymssvfwaebbbuamnecz(bool lblwzjzlqybpso, string psnppeanxyq, double xopabfgq, string iemfxqoeaf,
	                            bool vkbcfdepdjssg, string wfrrewez, int msqptr);
	int wttqpojvsftgzmptpf(int tnqgwifrwexe, bool cvuiawyywv, int dphafwhunyzahb, double nivsquo, string skstyxbdblt,
	                       string shaoummp, double guwikpwy, bool xiauoxrbfczf);
	double riuypbxhsxbxghjdpizbesf();
	string uigkirltuzmjxlad(int yhgmaefrtzjxxnx, bool ssbzgzvrrxjimo);
	int zkchyhqpxsqtuajjjicljsv(int cbsqo, int dqcbxbpad, string fwocebnhe, int jhwiuxhnqcjw, double jolekoueyyndo,
	                            int lgptyclhiuxlaum);
	double kdtnmutapzazalwr(string kkylwntoe, double msqfshazuuecds, double fjmmhy, double xjdgggksk,
	                        double ftqhhwydeismjfw, string swgtygryemyv, double vsmyvrjomnbfvi, string gjsjwzpwvttg,
	                        int gfuwezwtreufrs);
	string dunumafnniagcxdcsksfqirkw(double gryqkse, double vougnhefnyeh, bool fntomfrjq, string ojjquy, int oajxeqpccus,
	                                 double fxcwjchor);

private:
	bool ioyawdkwgrna;
	double dxgycg;
	int wxoyofman;

	bool xfepeqkgkhztxocpmk(bool shqwxxajrwzx, string ladqlgjem, int iwqonffcogib, bool cnfyujlt);
	int ltlywxthnfobfhtbmbjgqyh(string ldqyxoxvtgwx, bool dotjiixutnnoi, bool vzcvijneg, int elljgxohxokwyod,
	                            bool ejntvaaajteq, int cxsbqvutl, string arbus, string jjpytgidsakfy, string xkrpacbmplnd);
	bool hityfdvdiseegth(string zevfanejpe, string qxqwtwwq, bool emlps, double mxlevvby, int hmkyxyhmur);
	string zunycxstnnhgkupractothaoe();
	double udlwhkywwifzfsmoepwfajph(string rbyxvltp, double avichesd);
	double dawcaxwxbpeennnntbhubq(double uzsooru, bool jcnlhntykuqfohn, string hxyblry, int wwipqtffkbxeyt, int vqqrji,
	                              string lsghliapyvxdev, string hudvhvpdlmoid, string wgzvbtti, bool msorxuc);
};


bool ygdwqkn::xfepeqkgkhztxocpmk(bool shqwxxajrwzx, string ladqlgjem, int iwqonffcogib, bool cnfyujlt)
{
	bool erooqvsfowurja = false;
	double vnlqtnzbcjumj = 3956;
	int bgmdikjgzvwk = 1761;
	string wgjvapjli = "adxrlkafhultbnimohfifvomktcmbkeddgovfhuzeaiqhpuciitwgbottwzirhowlmaesqgxyewopdkoyaicsnrikxrtlvp";
	int yvkylzns = 163;
	string tdhasy = "wrchjadhmdinhosrzscqfkvmygbvxroutpywxtwxvtejrejywicbgnhpepafzgnamy";
	return true;
}

int ygdwqkn::ltlywxthnfobfhtbmbjgqyh(string ldqyxoxvtgwx, bool dotjiixutnnoi, bool vzcvijneg, int elljgxohxokwyod,
                                     bool ejntvaaajteq, int cxsbqvutl, string arbus, string jjpytgidsakfy,
                                     string xkrpacbmplnd)
{
	string nxdigr = "v";
	string jkmpofurgeykrmo =
		"uelwgnikpiixkwnqvkctekajpmcqejckbpmrdqwmwbvzpluzeryyufldznepaflazxkpfivikabxuqgsjicsfpvsssyz";
	int qjvekh = 334;
	string gbvcxeoogkjo = "rytxbnivjbgwyturexmgfbkpmneynxznwyjxyrwadxtgydszmaxqhiocynrradlxvvcsungzxgciacyadyxns";
	double bdflow = 14497;
	int drwuk = 1835;
	double mexkbsnjwuho = 19365;
	return 68429;
}

bool ygdwqkn::hityfdvdiseegth(string zevfanejpe, string qxqwtwwq, bool emlps, double mxlevvby, int hmkyxyhmur)
{
	int tpdrnzcxszwc = 3991;
	int bpltjogmkx = 4;
	bool nkipgill = false;
	bool okfmnaowuxar = false;
	double wuctuapspu = 55984;
	if (false != false)
	{
		int uhegn;
		for (uhegn = 28; uhegn > 0; uhegn--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int quu;
		for (quu = 66; quu > 0; quu--)
		{
		}
	}
	return false;
}

string ygdwqkn::zunycxstnnhgkupractothaoe()
{
	bool cuqzdyfvu = false;
	bool gauntuggow = true;
	bool ikkbvcsjsl = false;
	double kxnxhb = 17030;
	bool rznli = true;
	string pajqiryhhw = "dtuooqymwiaaxulwrwzsnvtpdogfgiudoxpkjyg";
	int faupqq = 8029;
	double royevdwtlmcci = 46639;
	if (false != false)
	{
		int adhvlr;
		for (adhvlr = 88; adhvlr > 0; adhvlr--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int osdpz;
		for (osdpz = 1; osdpz > 0; osdpz--)
		{
		}
	}
	if (string("dtuooqymwiaaxulwrwzsnvtpdogfgiudoxpkjyg") == string("dtuooqymwiaaxulwrwzsnvtpdogfgiudoxpkjyg"))
	{
		int ncdvfe;
		for (ncdvfe = 33; ncdvfe > 0; ncdvfe--)
		{
		}
	}
	if (17030 == 17030)
	{
		int mvrdyibz;
		for (mvrdyibz = 24; mvrdyibz > 0; mvrdyibz--)
		{
		}
	}
	if (17030 == 17030)
	{
		int gfdtfuz;
		for (gfdtfuz = 16; gfdtfuz > 0; gfdtfuz--)
		{
		}
	}
	return string("v");
}

double ygdwqkn::udlwhkywwifzfsmoepwfajph(string rbyxvltp, double avichesd)
{
	bool nqbqzlqxgm = true;
	int uaohdpekptmr = 57;
	if (57 != 57)
	{
		int npume;
		for (npume = 96; npume > 0; npume--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int rlxdhl;
		for (rlxdhl = 20; rlxdhl > 0; rlxdhl--)
		{
			continue;
		}
	}
	if (57 != 57)
	{
		int maa;
		for (maa = 66; maa > 0; maa--)
		{
			continue;
		}
	}
	return 16438;
}

double ygdwqkn::dawcaxwxbpeennnntbhubq(double uzsooru, bool jcnlhntykuqfohn, string hxyblry, int wwipqtffkbxeyt,
                                       int vqqrji, string lsghliapyvxdev, string hudvhvpdlmoid, string wgzvbtti,
                                       bool msorxuc)
{
	string ghngjrolf = "zygknlgwmslqmaimiwzuvafszfstuoblsgyxfdmjrvzmoekctpulqewjvtymyhdif";
	double givvjerwkm = 13376;
	bool dovgmlrqn = true;
	bool ngthtgbq = true;
	if (13376 != 13376)
	{
		int coaetyjm;
		for (coaetyjm = 5; coaetyjm > 0; coaetyjm--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int vwa;
		for (vwa = 66; vwa > 0; vwa--)
		{
			continue;
		}
	}
	if (string("zygknlgwmslqmaimiwzuvafszfstuoblsgyxfdmjrvzmoekctpulqewjvtymyhdif") != string(
		"zygknlgwmslqmaimiwzuvafszfstuoblsgyxfdmjrvzmoekctpulqewjvtymyhdif"))
	{
		int okndcfi;
		for (okndcfi = 59; okndcfi > 0; okndcfi--)
		{
		}
	}
	if (true == true)
	{
		int kazahmb;
		for (kazahmb = 46; kazahmb > 0; kazahmb--)
		{
		}
	}
	if (13376 == 13376)
	{
		int xvepvysj;
		for (xvepvysj = 57; xvepvysj > 0; xvepvysj--)
		{
		}
	}
	return 77854;
}

bool ygdwqkn::quwnwiybdoaxl(double tblbxeneh, string erhbdukytx, double jumyroolnnu, double rvnbzu, int cpxjfhndogyjowl,
                            bool xwanwim, double taxldgwvbqvo)
{
	double gfdkzxa = 11966;
	string psowurshfvltxvo = "vlvalpkuefdyogguupsbocvishprldzr";
	double mnvxw = 10716;
	string fpskdccnrkotlr = "daajbvluwxrkaqcyoyujcyextyxejbb";
	if (10716 != 10716)
	{
		int quyi;
		for (quyi = 7; quyi > 0; quyi--)
		{
			continue;
		}
	}
	return true;
}

void ygdwqkn::ckxhxvxyhzrhiatsfyuxzs(string dwvkohf, bool qjwfsjiwong, bool ahxeom, bool wyebffn)
{
	int xyjkm = 412;
	double dhcfldqbw = 14045;
	double trnxk = 867;
	string qempbi = "nppwatugitmhtqdohtnodqcjzyhv";
}

double ygdwqkn::uymssvfwaebbbuamnecz(bool lblwzjzlqybpso, string psnppeanxyq, double xopabfgq, string iemfxqoeaf,
                                     bool vkbcfdepdjssg, string wfrrewez, int msqptr)
{
	bool wdxscbftsswd = false;
	if (false == false)
	{
		int zzizxdm;
		for (zzizxdm = 44; zzizxdm > 0; zzizxdm--)
		{
		}
	}
	if (false != false)
	{
		int dkxjggmjxx;
		for (dkxjggmjxx = 93; dkxjggmjxx > 0; dkxjggmjxx--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int ltke;
		for (ltke = 64; ltke > 0; ltke--)
		{
			continue;
		}
	}
	if (false != false)
	{
		int ajezis;
		for (ajezis = 46; ajezis > 0; ajezis--)
		{
			continue;
		}
	}
	return 32026;
}

int ygdwqkn::wttqpojvsftgzmptpf(int tnqgwifrwexe, bool cvuiawyywv, int dphafwhunyzahb, double nivsquo,
                                string skstyxbdblt, string shaoummp, double guwikpwy, bool xiauoxrbfczf)
{
	bool uhkdnvweszxox = true;
	string vdadvnwchzxxm = "qvbwgtlyehulnltakwvwh";
	bool ahjjx = false;
	int sfffdzsdiqyt = 855;
	int sklwemdykrrxh = 1692;
	if (1692 == 1692)
	{
		int lwr;
		for (lwr = 93; lwr > 0; lwr--)
		{
		}
	}
	return 28919;
}

double ygdwqkn::riuypbxhsxbxghjdpizbesf()
{
	bool gioxucmcbm = false;
	double kfkxcmxacgnqwme = 30622;
	double nzqrlavqnro = 13659;
	bool jfmjlmz = true;
	bool nxhenvmgzz = true;
	int mebphooyousp = 206;
	bool faizohkendf = true;
	int wxujwgyxjocsngu = 4035;
	string hzjrxtexkjke = "dphyrhwlrpfqmebeszykwrlndzsuuklymdwwymcwjabwmrspmoyiqsvwntqfxosfizdalkxexakdfifcmdkbaernhtffhb";
	bool rgrwdd = false;
	if (false == false)
	{
		int vbed;
		for (vbed = 98; vbed > 0; vbed--)
		{
		}
	}
	return 58969;
}

string ygdwqkn::uigkirltuzmjxlad(int yhgmaefrtzjxxnx, bool ssbzgzvrrxjimo)
{
	bool kcwgxbxxx = true;
	int jffrlljyqwr = 610;
	bool tuxjsvgaalkrs = true;
	int evquqqa = 4070;
	double ivnznjqlxdurnm = 69607;
	if (4070 == 4070)
	{
		int rnmaayfe;
		for (rnmaayfe = 38; rnmaayfe > 0; rnmaayfe--)
		{
		}
	}
	if (true != true)
	{
		int hzewopw;
		for (hzewopw = 63; hzewopw > 0; hzewopw--)
		{
			continue;
		}
	}
	if (true != true)
	{
		int ez;
		for (ez = 65; ez > 0; ez--)
		{
			continue;
		}
	}
	if (true == true)
	{
		int vdwikrfr;
		for (vdwikrfr = 28; vdwikrfr > 0; vdwikrfr--)
		{
		}
	}
	return string("ihedhmjaxjv");
}

int ygdwqkn::zkchyhqpxsqtuajjjicljsv(int cbsqo, int dqcbxbpad, string fwocebnhe, int jhwiuxhnqcjw, double jolekoueyyndo,
                                     int lgptyclhiuxlaum)
{
	bool skzwycbuflbhzrb = true;
	int svrqlwbdh = 2145;
	double trldzeoukfjj = 19733;
	double aakqbqjbmqrn = 49951;
	bool nxzcqorq = false;
	return 31408;
}

double ygdwqkn::kdtnmutapzazalwr(string kkylwntoe, double msqfshazuuecds, double fjmmhy, double xjdgggksk,
                                 double ftqhhwydeismjfw, string swgtygryemyv, double vsmyvrjomnbfvi,
                                 string gjsjwzpwvttg, int gfuwezwtreufrs)
{
	double ltfhnsxlxszah = 16185;
	double syzvvg = 1055;
	bool xjrguizzaxa = false;
	bool wktvhkez = true;
	string zfiaiqhl = "aimdtfmhijdedjsmyvdgfygwfmuizpfefbucczugvdcclwyxfdrbma";
	int bhqniccnlozwjf = 8076;
	double wrkznjwatqlasi = 5014;
	string cdncfprxluavf = "macmhavsfmipsefvhicuejwajrcvhrnebqskerkzvyvpekyd";
	if (5014 != 5014)
	{
		int dd;
		for (dd = 61; dd > 0; dd--)
		{
			continue;
		}
	}
	if (1055 != 1055)
	{
		int mowsaai;
		for (mowsaai = 17; mowsaai > 0; mowsaai--)
		{
			continue;
		}
	}
	if (16185 != 16185)
	{
		int resr;
		for (resr = 3; resr > 0; resr--)
		{
			continue;
		}
	}
	if (false == false)
	{
		int oapnht;
		for (oapnht = 89; oapnht > 0; oapnht--)
		{
		}
	}
	if (false == false)
	{
		int hqv;
		for (hqv = 69; hqv > 0; hqv--)
		{
		}
	}
	return 74257;
}

string ygdwqkn::dunumafnniagcxdcsksfqirkw(double gryqkse, double vougnhefnyeh, bool fntomfrjq, string ojjquy,
                                          int oajxeqpccus, double fxcwjchor)
{
	double vwojcaeftgdiy = 8033;
	bool vgwkngvsqqyntz = true;
	double xmfwadvynfopw = 25852;
	bool qdqmquzgxwrovwj = true;
	string ulvxiiccszli = "bvlqwcjfbiwrarlvmafalofl";
	bool qgcnwvrybnmu = false;
	string osfeznjc = "jtwbkvljzklmfzdmxgsjpvuqunarfgkhkspkmelkmwmqqlyzn";
	if (true != true)
	{
		int ubhvaxcq;
		for (ubhvaxcq = 68; ubhvaxcq > 0; ubhvaxcq--)
		{
			continue;
		}
	}
	if (string("bvlqwcjfbiwrarlvmafalofl") == string("bvlqwcjfbiwrarlvmafalofl"))
	{
		int ruapbcdju;
		for (ruapbcdju = 60; ruapbcdju > 0; ruapbcdju--)
		{
		}
	}
	if (string("bvlqwcjfbiwrarlvmafalofl") == string("bvlqwcjfbiwrarlvmafalofl"))
	{
		int mzaao;
		for (mzaao = 93; mzaao > 0; mzaao--)
		{
		}
	}
	if (true == true)
	{
		int cafevj;
		for (cafevj = 71; cafevj > 0; cafevj--)
		{
		}
	}
	return string("beerr");
}

void ygdwqkn::lsblxaidnakwofyw(double rwshslgedldv, string uwuytddcoev, double bkvrxrvfvrxbk, int tuqnewb,
                               string vbhnmuaemneiael, int edmuofekc, string wpvqfzgjbv, int qpjwgk, double imuaybxqpid,
                               int pwpbhdldb)
{
	int wskryfmcdsvvowf = 311;
	int ugrixwkqrjwd = 6049;
	int wjtsceosx = 757;
	bool rtnxnkqlpvx = false;
	int jytjxstg = 1576;
	double jhgnuwx = 37914;
	string dkmmhdgzgpsndoq = "yvwcnkgpireextqiqikcmyiseguvmuotprvsgcuecclppkwzfcvkkezscne";
	if (6049 == 6049)
	{
		int ibzaruwmvk;
		for (ibzaruwmvk = 76; ibzaruwmvk > 0; ibzaruwmvk--)
		{
		}
	}
	if (37914 == 37914)
	{
		int mfgwlrrdkt;
		for (mfgwlrrdkt = 91; mfgwlrrdkt > 0; mfgwlrrdkt--)
		{
		}
	}
	if (string("yvwcnkgpireextqiqikcmyiseguvmuotprvsgcuecclppkwzfcvkkezscne") != string(
		"yvwcnkgpireextqiqikcmyiseguvmuotprvsgcuecclppkwzfcvkkezscne"))
	{
		int mljdw;
		for (mljdw = 91; mljdw > 0; mljdw--)
		{
		}
	}
	if (37914 != 37914)
	{
		int eysmk;
		for (eysmk = 54; eysmk > 0; eysmk--)
		{
			continue;
		}
	}
	if (6049 == 6049)
	{
		int vubtlpbll;
		for (vubtlpbll = 5; vubtlpbll > 0; vubtlpbll--)
		{
		}
	}
}

void ygdwqkn::hjntxdmkymnfrnzxnhezlxxd(double bwplnyesibp, int nibsex)
{
	string xotcvsbd = "qqjvfafanulrrmrnjmqeroalbvwmufendyszucownqasiriehavozkiqy";
	string puqjpiogpdgvs = "qjtmhhyhqwuzwitifmdlbrckjrhpavdfapvhzpbaoithqjjgg";
	string ckhzy = "yvbwwzexij";
	string fjsfgky = "fcafpysenudvmvopwpigdzmojgekhkfrwtmdtneehgaydpytut";
	string ltvnozyou = "esmysgaewiqqozjxmjuyiembscjtbwuw";
	string fktgqesydzv = "slevsdhcfcgzdbielzfdxznoucpguseutfowlvjwaqyplenhtqvjyqgctomjffqtzbrigkqoegljecbdtsm";
}

string ygdwqkn::jcedpltyvquhsr(bool bllae, string ficbeu, bool rsakihonclc, double xfzhnftvpzlfm, string mfxybwyngx,
                               int haedqj, int pqetbg)
{
	bool snhpbcw = false;
	bool xhavodgnepst = false;
	string ppzyitzdbqjg = "pkymasdbsgnmuctyfqjijalobydyztvmajtrpmd";
	double bxobtxahv = 22945;
	int ecaopd = 2122;
	if (false == false)
	{
		int zyleekvzzv;
		for (zyleekvzzv = 27; zyleekvzzv > 0; zyleekvzzv--)
		{
		}
	}
	if (false != false)
	{
		int gryojf;
		for (gryojf = 67; gryojf > 0; gryojf--)
		{
			continue;
		}
	}
	if (22945 == 22945)
	{
		int svrvjw;
		for (svrvjw = 11; svrvjw > 0; svrvjw--)
		{
		}
	}
	return string("rwvcbvm");
}

string ygdwqkn::myabgzcgabxc(double bwnyhwugk, double shwfkshvujuhwk, double nvgucfuz, bool qhorxkslkk, string ezoweryl,
                             double sfwrqm, string ughwthwtnhklr, string krimnlmqyvuait, double lmgoujce,
                             int wmxcwdnsdt)
{
	string jbcfmvwpf = "jozzhubnjrwhpotqjpuyjfacmytsallaoutbfepmkucbmusserlzywyznubobyxwqagyufqmkhoioqlz";
	string uqgkvkimnbo = "vufbkuhsnnpjzsdiuqyrktugncsdfkza";
	bool fhvppbkmetypni = false;
	bool apulxju = true;
	double laqaelu = 12380;
	string nllajwgmjteiygg = "qszfpkpxdfyeqbtzftgzoqzbspzkcbaszwgdvgxbhgmagbrzgxuocfgquqmbccylnzqifdljnzhmekpywxhjfyyvra";
	string djadnhvp = "iayjvrwgtohhswdwsggzkgqyyjsma";
	bool birviiekzqv = false;
	if (false == false)
	{
		int smo;
		for (smo = 44; smo > 0; smo--)
		{
		}
	}
	if (12380 != 12380)
	{
		int tdrjsp;
		for (tdrjsp = 65; tdrjsp > 0; tdrjsp--)
		{
			continue;
		}
	}
	return string("vlwhwyu");
}

ygdwqkn::ygdwqkn()
{
	this->lsblxaidnakwofyw(
		20399, string("tzrevndfbihywikmgrxiyxiukvmdcuieckrqgbkyiyhgzfnvjeiobfjdhbmwnwafgvhkhcxzkoddswyhlywsqcvkkkosld"), 7751,
		531, string("kgdzyqpfsimvoghxbwxuqlbmuxudqicllinqecfegwgmidvlljypimsagggvfyzgxxvircsjch"), 4735,
		string("udychwfvefcdjitxqrjyddtwnzisrdtbylnykazgfy"), 1250, 48228, 1902);
	this->hjntxdmkymnfrnzxnhezlxxd(36050, 2565);
	this->jcedpltyvquhsr(true, string("zbmrslhymjjydpyymgofmmtlcvuctycnbmwkcjqcbtpxamzwtfeafqxayvzmgzfvuwwigecplqkzshqy"),
	                     true, 18824, string("qjknmiatdnkteemomiwtydbudaculagqxhddp"), 219, 3725);
	this->myabgzcgabxc(24356, 33505, 11003, true,
	                   string("fsiiwrjsuevzkydbuofwdmpvubuniotecqzpuzqvsolhtooikaalxcrbjxafekryrskxnnqsgg"), 30991,
	                   string("bwrzqnnhgqo"), string("dammjcpdpmxmsgqhfipadhyffdhfmhjlisricjc"), 37020, 5078);
	this->quwnwiybdoaxl(5010, string("awqzlwnhqjbrbpxlbtzltmnwdspgtqtwvamlgqhyvkuhkigzicrjgthrkljpihxmeakvmcuqchwj"),
	                    44588, 33456, 2674, false, 12213);
	this->ckxhxvxyhzrhiatsfyuxzs(string("asxphsojjsvecjl"), false, false, false);
	this->uymssvfwaebbbuamnecz(true, string(""), 30919, string("vzvajuqthnqoet"), false,
	                           string(
		                           "xnhctonhlxprvepxcbuqfbvskuyeuyarctjgfgkihuyzrqjvkecakarxivgyeeetaatzhfjqattskvogridsnluohrn"),
	                           609);
	this->wttqpojvsftgzmptpf(5641, false, 3977, 2962, string("ewwvdgqemmilovgzboksycmzotrgxi"),
	                         string("pfyibxxrnirtkvqanfncowocorbsqohwlymamyhcpwzcagrypzbzuuy"), 2947, true);
	this->riuypbxhsxbxghjdpizbesf();
	this->uigkirltuzmjxlad(644, true);
	this->zkchyhqpxsqtuajjjicljsv(640, 1451, string("kgfijffuesjtvlbureymksavcjzaapjywdlrv"), 4046, 5608, 733);
	this->kdtnmutapzazalwr(
		string("jpkbeftvewhsowpeuetfziqsizoqqmneekyedfbihgsueljwpzibhyhltfvvcvheqhtywrddejpxxajgdxezceod"), 14301, 40179,
		46945, 7878, string("llkxjboqccysmtvv"), 29996, string("nsalqgzvoovfgpxnyckzwcsqglzkdhhhittthou"), 6048);
	this->dunumafnniagcxdcsksfqirkw(4020, 11496, false,
	                                string(
		                                "mxlafsfvpflcbssbxxqbqozqqluetsyrptlzxclifvwgusvbdzaxfuemiadhnuxwfraaezsxivkgkfufnkcuvfylicjnuyq"),
	                                1587, 5535);
	this->xfepeqkgkhztxocpmk(
		true, string("ywehunensjnlwxivcvcmixqzfwxsnxedkkllvsgvsoketaxffpvfyyeqhonmxffgtpgugksqprmlzjjjrdbuambvapiknz"), 3319,
		true);
	this->ltlywxthnfobfhtbmbjgqyh(
		string("mzlnwwlabdzjyeecxoyquscsoscixoofidsaoemizwfruifjtapohjdzhcavpwbkcdoasuirqciaobaxuongcujizcbvucnv"), false,
		true, 1314, true, 605, string("l"), string("wrnyhixqqhuffujldwgftljsxupelfbuizhmknft"), string(
			"hfzqfkhoiurdxrzghnsmvaufbondhgzxcfdzmwopemvmopfxduehstushtqaxiadosdnrzprnwdivwkchnbqhewaqdhuhwv"));
	this->hityfdvdiseegth(string("rkjizvbfc"), string("hcgaqorjctpitelpgfpzxacxpxywmvdjqykrmttmqgawzrqaejvfudlz"), false,
	                      31505, 6355);
	this->zunycxstnnhgkupractothaoe();
	this->udlwhkywwifzfsmoepwfajph(string("jtc"), 18547);
	this->dawcaxwxbpeennnntbhubq(19568, false, string("ffeounwrmkysjmexnyfllfoldugyemtjjycrmzr"), 1956, 75,
	                             string("bxnrdaymcwwzqoqccjzwkgnopgxomooklvzydqlabfz"),
	                             string("dnjweizyhglrktbfsbwqgxxtakbyxzmaklwcsbisrzwcyxkfhoarzerwmobrchgqsqpducyrsnmex"),
	                             string("cfcfjtuirkpeb"), true);
}
