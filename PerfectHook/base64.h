#pragma once

#include <iostream>

using namespace std;

namespace base64
{
	string base64_decode(string const& encoded_string);
	string base64_encode(unsigned char const* bytes_to_encode, unsigned int in_len);
};
